<nav class="navbar navbar-default">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
	  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="/v1/base.php">
        <span><img alt="SIS-AXS" src="/media/logo25.png"></span> SIS-AXS
      </a>
	  
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
		<ul class="nav navbar-nav">
			<li id="tab0" class="active"><a href="#" onclick="event.preventDefault(); javascript:switchtab(0)">REAL TIME DATA <span class="sr-only">(current)</span></a></li>
			<li id="tab1"><a href="#" onclick="event.preventDefault(); switchtab(1)">ALL DATA</a></li>
			<li id="tab2"><a href="#" onclick="event.preventDefault(); switchtab(2)">REPORTS</a></li>
			<li id="tab3"><a href="#" onclick="event.preventDefault(); switchtab(3)">MAINTENANCE</a></li>
		</ul>

	  <?php if (isset($_SESSION['login_user'])){ ?>
      <ul class="nav navbar-nav navbar-right">
        <li><p class="navbar-text">Signed in as <?php echo $_SESSION['login_user']; ?></p></li>
        <li><p class="navbar-btn"><a href="logout.php" class="btn btn-danger">Sign out</a></p></li>
      </ul>
	  <?php } ?>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>  

