<?php
session_start();
if (!isset($_SESSION['login_user']))
{
	header("Location: login.php");
}

//var_dump($_GET['id']);
	$id = $_SESSION['login_cust_id'];
	$login_type = $_SESSION['login_type'];
	$clusters = $_SESSION['login_typeref_id'];
//var_dump($login_type);
	$array = array();
	
	include 'dbinc.php';
	include 'common.php';


	$mysqli = new mysqli("$mysql_hostname", "$mysql_user", "$mysql_password", "$mysql_database");
	if (mysqli_connect_errno())
	{
		printf("Connect failed: %s\n", mysqli_connect_error());
		return null;
	}
	
	
	$sql = "SELECT Name, Address, logo 
				FROM customers 
				WHERE (id = $id) ";
	$result = $mysqli->query($sql);
	if($result )
	{
		$data = $result->fetch_row();
		$name = $data[0];
		$address = nl2br($data[1]);
		$logo = "/media/tree/".$data[2];
		$result->close();
	}
	
		
	echo "<img src='$logo'>";
	echo "<h3>$name</h3>";
	echo "<h4>$address</h4>";
	
	
	$sql = "SELECT count(Id) 
				FROM clusters  
				WHERE (CustomerId = $id) 
			
			";			
	$result = $mysqli->query($sql);
	if($result )
	{
		$data = $result->fetch_row();
		$count = $data[0];
		$result->close();
	}
	
	echo "<br><h5>Total Clusters $count</h5>";

	$sql = "SELECT count(Id) 
				FROM siteinfo  
				WHERE ((CustomerId = $id) AND (a.site_enabled = 1)) 
			
			";			
	$result = $mysqli->query($sql);
	if($result )
	{
		$data = $result->fetch_row();
		$count = $data[0];
		$result->close();
	}
	
	echo "<h5>Total Sites $count</h5>";
	
	///////////////////////
	
	if ($login_type == 1)	//Head, show all clusters and sites for the customer
	{
		$sql = "SELECT 
				a.Id, a.site_id, a.ClusterId, b.name, a.SiteName, a.SiteIndoorType 
			FROM siteinfo a 
			
				INNER JOIN clusters b 
				ON a.ClusterId = b.id			

			WHERE ((a.CustomerId = $id) AND (a.site_enabled = 1))
			ORDER BY ClusterId 

			
			";
	}
	else if ($login_type == 2)	//Cluster Head, show only the cluster of the head
	{
		$clustersid = $clusters[0];	//Only a single cluster
		$sql = "SELECT 
				a.Id, a.site_id, a.ClusterId, b.name, a.SiteName, a.SiteIndoorType
			FROM siteinfo a 
			
				INNER JOIN clusters b 
				ON a.ClusterId = b.id			

			WHERE ((a.CustomerId = $id) AND (a.ClusterId = $clustersid) AND (a.site_enabled = 1))
			ORDER BY ClusterId 

			
			";					
	}
	else if ($login_type == 4)	//Zone Head, show all the clusters under the zone
	{
		$clustersid = join(',', $clusters);
		//echo "$clustersid <br>";
		$sql = "SELECT 
				a.Id, a.site_id, a.ClusterId, b.name, a.SiteName, a.SiteIndoorType 
			FROM siteinfo a 
			
				INNER JOIN clusters b 
				ON a.ClusterId = b.id			

			WHERE ((a.CustomerId = $id) AND (a.ClusterId IN ($clustersid)) AND (a.site_enabled = 1))
			ORDER BY ClusterId 

			
			";					
	}
	else
	{
		//Show no cluster/site
		$sql = null;
	}
		//print("<script> var siteListArray = [];</script>");
	if ($sql != null)
	{
		//print("<script>");					// var siteListArray = [];");
		print("<script> var siteListArray = [];");

		$result = $mysqli->query($sql);	
		$last_cluster = 0;
		while($data = $result->fetch_row()){
			//var_dump($data);
			
			if ($last_cluster != $data[2])
			{
				//New Cluster				
				$last_cluster = $data[2];				
			}
			
			print("siteListArray.push(['','".trim($data[1])."','".trim($data[4])."','".trim($data[3])."',".$data[5]."]);");
		}
		print("</script>");
		$result->close();
	}
	
	mysqli_close($mysqli);

?>
<style>
    #led_white {
        width: 28px;
        height: 28px;
        background: url(/media/leds.png) 0 0;
    }

    #led_red {
        width: 28px;
        height: 28px;
        background: url(/media/leds.png) -30px 0;
    }

    #led_orange {
        width: 28px;
        height: 28px;
        background: url(/media/leds.png) -62px 0;
    }

    #led_green {
        width: 28px;
        height: 28px;
        background: url(/media/leds.png) -124px 0;
    }

</style>
<script src="base64.js"></script>
<script src="notification.js"></script>

<script>


   $(document).ready(function () {
        $('#example').DataTable({
            data: siteListArray,
            "columnDefs": [{
                "targets": -1,
                "data": "",
                "defaultContent": ""
                //"defaultContent": QuantaAjax(siteListArray[1][0],siteListArray[1][3])
                //},
            },
                {

                    "targets": [0, 1],
                    //"data": "download_link",
                    "render": function (data, type, full, meta) {
                        return '<b>' + data + '</b>';
                    }
                }]
        });
    });

</script>
<script  type="text/javascript">

    var alarm = false;
    var alarm_led = "";
    var status_array = [];
    var status_resp = [];

    var timestamp = "";
    var status = "";
    var colomn_index = 0;

            var table = $('#example').DataTable();


            function QuantaAjax() {

            table.on('draw.dt', function () {

                // var index = table.row( this ).index();
                //console.log(index[0]);
                //table.column(-2,{draw:'applied'}).nodes().each( function (cell, index) {
                table.rows({draw: 'applied'}).eq( 0 ).filter(function (index,cell) {
                    //console.log("times");
                    jQuery.ajax({
                        type: "POST",
                        url: 'alarmstatus.php',
                        cache: false,
                        async: false,

                        dataType: 'json',
                        data: {
                            functionname: 'CallQuantaForDatatableById',
                            arguments: [siteListArray[index][1], siteListArray[index][4]]
                        },

                        success: function (obj, textstatus) {
                            if (!('error' in obj)) {

                                //console.log(obj);
                                //var object = JSON.parse(obj);  									//Object is already parsed..!
                                var byteArray = Base64Binary.decodeArrayBuffer(obj.status);
                                var int8View = new Uint8Array(byteArray);

                                if(obj.id != table.cell( index, 0 ).data())     // First Run OR New Data Received..!
                                {
                                    if(table.cell( index, 0 ).data() != "")
                                    {
                                        new Notification( "Updated: "+siteListArray[index][2], {
                                            body: "The new data has been received successfully!"
                                            //icon : "<img id='led_green' src='/media/img_trans.gif'>"
                                        });
                                    }
                                    status_array = [];
                                    table.cell(index, 0).data(obj.id);
                                    table.cell(index, 3).data(obj.timestamp);

                                    if (obj.device_id == 1)				//Diesel Generator Source
                                    {
                                        if (obj.data_validated != 2) 	//Single DG
                                        {

                                            //Essential Alarms Selection will be done according to the requirements and after cycle testing...!

                                            //0x0000000000 Smoke fire    0 means No alarm,  1 means Alarm
                                            if ((int8View[4] & 0x01) != false) {
                                                status_array.push([0,0, "Smoke fire"]);		// 0 Smoke Fire
                                            }
                                            //$FLAG_05_MAINS 	= 0x0000000020;	//Mains fail	1 Means main Fail,  0 Means  Main healthy
                                            if ((int8View[4] & 0x20) != false) {
                                                status_array.push([1,0, "Mains Fail"]);  	// 1 Mains Fail/Healthy
                                            }
                                            //$FLAG_06_BATT 	= 0x0000000040;	//Site battery Low  1 Means Site Battery Low ,0 Means  Battery Ok
                                            if ((int8View[4] & 0x40) != false) {
                                                status_array.push([2,0, "Site Batter Low"]);	//
                                            }
                                            //$FLAG_07_TEMP 	= 0x0000000080;	//Room Temperature  1 Means high Temperature, 0 Means Normal temp
                                            if ((int8View[4] & 0x80) != false) {
                                                status_array.push([3,0, "High Temperature"]);
                                            }
                                            //$FLAG_08_LLOP 	= 0x0000000100;	//LLOP
							                if ((int8View[3] & 0x01) != false) {
								                status_array.push([4,1, "LLOP"]);
							                }
                                            if (obj.indoor_type == 0)	//Door Alarm Only Applicable if Indoor
                                            {
                                                //$FLAG_01_DOOR 	= 0x0000000002;	//Door Open     0  means  Door Close , 1 Means  Door open
                                                if ((int8View[4] & 0x02) != false) {
                                                    status_array.push([5,1, "Door Open"]);
                                                }
                                            }

                                            //$FLAG_02_AUTO 	= 0x0000000004;	//Auto/Man Mode 0 Means  Auto Mode, 1 Means  Man Mode
                                            if ((int8View[4] & 0x04) != false) {
                                                status_array.push([6,0, "Mannual"]);
                                            }
							//$FLAG_034_LOAD 	= 0x0000000018;	//00    :Load on EB,01: Load on DG, 10: Load on site Battery   11: Not used
//							if ((int8View[4] & 0x08) == false)
//							{
//								if ((int8View[4] & 0x10) == false)
//									wdocument.rite( "[- Load on EB -]<br>");				//00
//								else
//									document.write( "[- Load on site Battery -]<br>");	//10
//							}
//							else
//							{
//								if ((int8View[4] & 0x10) == false)
//									document.write( "[- Load on DG -]<br>");				//01
//								else
//									document.write( "[- Not Used -]<br>");	//11
//							}


                                            //$FLAG_09_HCT 	= 0x0000000200;	//HCT/HWT
//							if ((int8View[3] & 0x02) != false)
//								status_array.push([8, "HCT/HWT"]);

                                            //$FLAG_10_ALT 	= 0x0000000400;	//Alternator Fault
//							if ((int8View[3] & 0x04) != false)
//								status_array.push([9, "Alternator Fault"]);
//
//							//$FLAG_11_DG 	= 0x0000000800;	//DG Over Speed
//							if ((int8View[3] & 0x08) != false)
//								status_array.push([10, "DG Over Speed"]);
//
//							//$FLAG_12_DG 	= 0x0000001000;	//DG Over Load
//							if ((int8View[3] & 0x10) != false)
//								status_array.push([11, "DG Over Load"]);

                                            //$FLAG_13_DG 	= 0x0000002000;	//DG Low Fuel
                                            if ((int8View[3] & 0x20) != false)
                                                status_array.push([12,0, "DG Low Fuel"]);

                                            //$FLAG_14_DG 	= 0x0000004000;	//DG Start Fail
                                            if ((int8View[3] & 0x40) != false)
                                                status_array.push([13,0, "DG Start Fail"]);

                                            //$FLAG_15_DG 	= 0x0000008000;	//DG Stop Fail
                                            if ((int8View[3] & 0x80) != false)
                                                status_array.push([14,0, "DG Stop Fail"]);

                                            //$FLAG_16_DG 	= 0x0000010000;	//DG battery Low
                                            if ((int8View[2] & 0x01) != false)
                                                status_array.push([15,0, "DG battery Low"]);

                                            //$FLAG_17_LCU 	= 0x0000020000;	//LCU fail
                                            if ((int8View[2] & 0x02) != false)
                                                status_array.push([16,0, "LCU fail"]);

                                            //$FLAG_18_RECT 	= 0x0000040000;	//Rectifier Fail
                                            if ((int8View[2] & 0x04) != false)
                                                status_array.push([17,0, "Rectifier Fail"]);

                                            //$FLAG_19_RECT 	= 0x0000080000;	//Multi Rectifier Fail
                                            if ((int8View[2] & 0x08) != false)
                                                status_array.push([18,0, "Multi Rectifier Fail"]);

                                            //$FLAG_20_LVD 	= 0x0000100000;	//LVD TRIP
                                            if ((int8View[2] & 0x10) != false)
                                                status_array.push([19,1, "LVD TRIP"]);

                                            //$FLAG_21_LVD 	= 0x0000200000;	//LVD BY pass
                                            if ((int8View[2] & 0x20) != false)
                                                status_array.push([20,1, "LVD BY pass"]);


//							console.log(status_array[0]);
//							console.log(status_array[1]);


                                            //$FLAG_22_RES 	= 0x0000400000;	//Reserved
                                            //$FLAG_23_RES 	= 0x0000800000;	//Reserved
                                            //$FLAG_24_RES 	= 0x0001000000;	//Reserved
                                            //$FLAG_25_RES 	= 0x0002000000;	//Reserved
                                            //$FLAG_26_RES 	= 0x0004000000;	//Reserved
                                            //$FLAG_27_RES 	= 0x0008000000;	//Reserved
                                            //$FLAG_28_RES 	= 0x0010000000;	//Reserved
                                            //$FLAG_29_RES 	= 0x0020000000;	//Reserved
                                            //$FLAG_30_RES 	= 0x0040000000;	//Reserved
                                            //$FLAG_31_RES 	= 0x0080000000;	//Reserved


                                            //$FLAG_32_DG 	= 0x0100000000;	//DG OFF
                                            if ((int8View[0] & 0x01) != false)
                                                status_array.push([21,2, "DG OFF"]);

                                            //$FLAG_33_DG 	= 0x0200000000;	//DG ON
                                            if ((int8View[0] & 0x02) != false)
                                                status_array.push([22,2, "DG ON"]);

                                            //$FLAG_34_DG 	= 0x0400000000;	//DG Cranking
                                            if ((int8View[0] & 0x04) != false)
                                                status_array.push([23,1, "DG Cranking"]);

                                            //$FLAG_35_DG 	= 0x0800000000;	//DG Start in Progress
                                            if ((int8View[0] & 0x08) != false)
                                                status_array.push([24,2, "DG Start in Progress"]);

                                            //$FLAG_36_DG 	= 0x1000000000;	//DG Cool Down (Idle Running)
                                            if ((int8View[0] & 0x10) != false)
                                                status_array.push([25,2, "DG Cool Down (Idle Running)"]);

                                            //$FLAG_37_DG 	= 0x2000000000;	//DG STOP  Normally
                                            if ((int8View[0] & 0x20) != false)
                                                status_array.push([26,2, "DG STOP  Normally"]);

                                            //$FLAG_38_DG 	= 0x8000000000;	//DG STOP due to Fault
                                            if ((int8View[0] & 0x40) != false)
                                                status_array.push([27,0, "DG STOP due to Fault"]);

                                            //$FLAG_39_DG 	= 0x8000000000;	//DG Stop Due to Maximum Run Exipary
                                            if ((int8View[0] & 0x80) != false)
                                                status_array.push([28,0, "DG Stop Due to Maximum Run Exipary"]);


                                            //status_resp = [];
//                                            if (status_array.length == 1 && status_array[0][0] == 5) {
//                                                alarm_led = "<img id='led_orange' src='/media/img_trans.gif'>";
//                                                alarm = true;
//                                            }
//                                            else if (status_array.length >= 1) {
//                                                alarm_led = "<img id='led_red' src='/media/img_trans.gif'>";
//                                                alarm = true;
//                                            }
//                                            else {
//                                                alarm_led = "<img id='led_green' src='/media/img_trans.gif'>";
//                                                alarm = false;
//                                            }

                                        }
                                        else        //Double DG
                                        {



                                            //0x0000000000 Smoke fire    0 means No alarm,  1 means Alarm
                                            if ((int8View[5] & 0x01) != false)
                                                status_array.push([0,0, "Smoke fire"]);

                                            //$FLAG_01_DOOR 	= 0x0000000002;	//Door Open     0  means  Door Close , 1 Means  Door open
                                            if ((int8View[5] & 0x02) != false)
                                                status_array.push([1,1, "Door Open"]);

                                            //$FLAG_02_AUTO 	= 0x0000000004;	//Auto/Man Mode 0 Means  Auto Mode, 1 Means  Man Mode
                                            if ((int8View[5] & 0x04) != false)
                                                status_array.push([2,0, "Mannual"]);

                                            //$FLAG_034_LOAD 	= 0x0000000018;
                                            //000    :Load on EB,001: Load on DG1, 011: Load on site Battery   010: Load on DG2, 111 ( site in battery with emg case) other’s Not used

//							var  sub_data = (int8View[5] & 0x38) / 8;
//
//							if (sub_data == 0)
//								document.write( "[- Load on EB -]<br>");			//000
//							else if (sub_data == 1)
//								document.write( "[- Load on DG 1 -]<br>");			//001
//							else if (sub_data == 2)
//								document.write( "[- Load on DG 2 -]<br>");			//010
//							else if (sub_data == 3)
//								document.write( "[- Load on site Battery -]<br>");	//011
//							else if (sub_data == 7)
//								document.write( "[- Load on site Battery (emg case) -]<br>");	//111
//							else
//								document.write( "[- Not Used -]<br>");				//remaining


                                            //$FLAG_05_MAINS 	= 0x0000000020;	//Mains fail	1 Means main Fail,  0 Means  Main healthy
                                            if ((int8View[5] & 0x40) != false)
                                                status_array.push([4,0, "Mains fail"]);

                                            //$FLAG_06_BATT 	= 0x0000000040;	//Site battery Low  1 Means Site Battery Low ,0 Means  Battery Ok
                                            if ((int8View[5] & 0x80) != false)
                                                status_array.push([5,0, "Site battery Low"]);

                                            //$FLAG_07_TEMP 	= 0x0000000080;	//Room Temperature  1 Means high Temperature, 0 Means Normal temp
                                            if ((int8View[4] & 0x01) != false)
                                                status_array.push([6,0, "Room Temperature"]);

                                            //$FLAG_08_LLOP 	= 0x0000000100;	//LLOP
                                            if ((int8View[4] & 0x02) != false)
                                                status_array.push([7,1, "DG 1 LLOP"]);

                                            //$FLAG_09_HCT 	= 0x0000000200;	//HCT/HWT
                                            if ((int8View[4] & 0x04) != false)
                                                status_array.push([8,1, "DG 1 HCT/HWT"]);

                                            //$FLAG_10_ALT 	= 0x0000000400;	//Alternate Fault
                                            if ((int8View[4] & 0x08) != false)
                                                status_array.push([9,0, "DG 1 Alternate Fault"]);

                                            //$FLAG_11_DG 	= 0x0000000800;	//DG Over Speed
                                            if ((int8View[4] & 0x10) != false)
                                                status_array.push([10,0, "DG 1 Over Speed"]);

                                            //$FLAG_12_DG 	= 0x0000001000;	//DG Over Load
                                            if ((int8View[4] & 0x20) != false)
                                                status_array.push([11,0, "DG 1 Over Load"]);

                                            //$FLAG_13_DG 	= 0x0000002000;	//DG Low Fuel
                                            if ((int8View[4] & 0x40) != false)
                                                status_array.push([12,0, "DG 1 Low Fuel"]);

                                            //$FLAG_14_DG 	= 0x0000004000;	//DG Start Fail
                                            if ((int8View[4] & 0x80) != false)
                                                status_array.push([13,0, "DG 1 Start Fail"]);

                                            //$FLAG_15_DG 	= 0x0000008000;	//DG Stop Fail
                                            if ((int8View[3] & 0x01) != false)
                                                status_array.push([14,0, "DG 1 Stop Fail"]);

                                            //$FLAG_16_DG 	= 0x0000010000;	//DG battery Low
                                            if ((int8View[3] & 0x02) != false)
                                                status_array.push([15,0, "DG 1 battery Low"]);

                                            //$FLAG_17_LCU 	= 0x0000020000;	//LCU fail
                                            if ((int8View[3] & 0x04) != false)
                                                status_array.push([16,0, "LCU fail"]);

                                            //$FLAG_18_RECT 	= 0x0000040000;	//Rectifier Fail
                                            if ((int8View[3] & 0x08) != false)
                                                status_array.push([17,0, "Rectifier Fail"]);

                                            //$FLAG_19_RECT 	= 0x0000080000;	//Multi Rectifier Fail
                                            if ((int8View[3] & 0x10) != false)
                                                status_array.push([18,0, "Multi Rectifier Fail"]);

                                            //$FLAG_20_LVD 	= 0x0000100000;	//LVD TRIP
                                            if ((int8View[3] & 0x20) != false)
                                                status_array.push([19,1, "LVD TRIP"]);


                                            //$FLAG_21_LVD 	= 0x0000200000;	//LVD BY pass
                                            if ((int8View[3] & 0x40) != false)
                                                status_array.push([20,1, "LVD BY pass"]);

                                            ////////////////////////////
                                            ////////////////////////////
                                            ////////////////////////////


                                            //$FLAG_08_LLOP 	= 0x0000000100;	//LLOP
                                            if ((int8View[3] & 0x80) != false)
                                                status_array.push([21,1, "DG 2 LLOP"]);

                                            //$FLAG_09_HCT 	= 0x0000000200;	//HCT/HWT
                                            if ((int8View[2] & 0x01) != false)
                                                status_array.push([22,1, "DG 2 HCT/HWT"]);

                                            //$FLAG_10_ALT 	= 0x0000000400;	//Alternate Fault
                                            if ((int8View[2] & 0x02) != false)
                                                status_array.push([23,0, "DG 2 Alternate Fault"]);

                                            //$FLAG_11_DG 	= 0x0000000800;	//DG Over Speed
                                            if ((int8View[2] & 0x04) != false)
                                                status_array.push([24,0, "DG 2 Over Speed"]);

                                            //$FLAG_12_DG 	= 0x0000001000;	//DG Over Load
                                            if ((int8View[2] & 0x08) != false)
                                                status_array.push([25,0, "DG 2 Over Load"]);

                                            //$FLAG_13_DG 	= 0x0000002000;	//DG Low Fuel
                                            if ((int8View[2] & 0x10) != false)
                                                status_array.push([26,0, "DG Low Fuel"]);

                                            //$FLAG_14_DG 	= 0x0000004000;	//DG Start Fail
                                            if ((int8View[2] & 0x20) != false)
                                                status_array.push([27,0, "DG 2 Start Fail"]);

                                            //$FLAG_15_DG 	= 0x0000008000;	//DG Stop Fail
                                            if ((int8View[2] & 0x40) != false)
                                                status_array.push([28,0, "DG 2 Stop Fail"]);

                                            //$FLAG_16_DG 	= 0x0000010000;	//DG battery Low
                                            if ((int8View[2] & 0x80) != false)
                                                status_array.push([29,0, "DG 2 battery Low"]);

                                            //$FLAG_32_DG 	= 0x0100000000;	//DG OFF
                                            if ((int8View[1] & 0x01) != false)
                                                status_array.push([30,2, "DG 1 OFF"]);

                                            //$FLAG_33_DG 	= 0x0200000000;	//DG ON
                                            if ((int8View[1] & 0x02) != false)
                                                status_array.push([31,2, "DG 1 ON"]);

                                            //$FLAG_34_DG 	= 0x0400000000;	//DG Cranking
                                            if ((int8View[1] & 0x04) != false)
                                                status_array.push([32,1, "DG 1 Cranking"]);

                                            //$FLAG_35_DG 	= 0x0800000000;	//DG Start in Progress
                                            if ((int8View[1] & 0x08) != false)
                                                status_array.push([33,2, "DG 1 Start in Progress"]);

                                            //$FLAG_36_DG 	= 0x1000000000;	//DG Cool Down (Idle Running)
                                            if ((int8View[1] & 0x10) != false)
                                                status_array.push([34,2, "DG 1 Cool Down (Idle Running)"]);

                                            //$FLAG_37_DG 	= 0x2000000000;	//DG STOP  Normally
                                            if ((int8View[1] & 0x20) != false)
                                                status_array.push([35,2, "DG 1 STOP  Normally"]);

                                            //$FLAG_38_DG 	= 0x8000000000;	//DG STOP due to Fault
                                            if ((int8View[1] & 0x40) != false)
                                                status_array.push([36,0, "DG 1 STOP due to Fault"]);

                                            //$FLAG_39_DG 	= 0x8000000000;	//DG Stop Due to Maximum Run Expiry
                                            if ((int8View[1] & 0x80) != false)
                                                status_array.push([37,0, "DG 1 Stop Due to Maximum Run Expiry"]);

                                            //$FLAG_32_DG 	= 0x0100000000;	//DG OFF
                                            if ((int8View[0] & 0x01) != false)
                                                status_array.push([38,2, "DG 2 OFF"]);

                                            //$FLAG_33_DG 	= 0x0200000000;	//DG ON
                                            if ((int8View[0] & 0x02) != false)
                                                status_array.push([39,2, "DG 2 ON"]);

                                            //$FLAG_34_DG 	= 0x0400000000;	//DG Cranking
                                            if ((int8View[0] & 0x04) != false)
                                                status_array.push([40,1, "DG 2 Cranking"]);

                                            //$FLAG_35_DG 	= 0x0800000000;	//DG Start in Progress
                                            if ((int8View[0] & 0x08) != false)
                                                status_array.push([41,2, "DG 2 Start in Progress"]);

                                            //$FLAG_36_DG 	= 0x1000000000;	//DG Cool Down (Idle Running)
                                            if ((int8View[0] & 0x10) != false)
                                                status_array.push([42,2, "DG 2 Cool Down (Idle Running)"]);

                                            //$FLAG_37_DG 	= 0x2000000000;	//DG STOP  Normally
                                            if ((int8View[0] & 0x20) != false)
                                                status_array.push([43,2, "DG 2 STOP  Normally"]);

                                            //$FLAG_38_DG 	= 0x8000000000;	//DG STOP due to Fault
                                            if ((int8View[0] & 0x40) != false)
                                                status_array.push([44,0, "DG 2 STOP due to Fault"]);

                                            //$FLAG_39_DG 	= 0x8000000000;	//DG Stop Due to Maximum Run Expiry
                                            if ((int8View[0] & 0x80) != false)
                                                status_array.push([45,0, "DG 2 Stop Due to Maximum Run Expiry"]);

                                            //console.log(status_array.length);
//                                            if (status_array.length == 1 && status_array[0][0] == 1) {
//                                                alarm_led = "<img id='led_orange' src='/media/img_trans.gif'>";
//                                                alarm = true;
//                                            }
//                                            else if (status_array.length >= 1) {
//                                                alarm_led = "<img id='led_red' src='/media/img_trans.gif'>";
//                                                alarm = true;
//                                            }
//                                            else if (status_array.length == 0) {
//                                                alarm_led = "<img id='led_green' src='/media/img_trans.gif'>";
//                                                alarm = false;
//                                            }
                                        }

                                        if(status_array.length > 0)
                                        {
                                            var array_index = 0;
                                            var alarm_status = 3;       //3 For No Color, 2 for Green, 1 for Orange, 0 For red
                                            while ( array_index < status_array.length && status_array [array_index][1] == 2)
                                            {
                                                if( status_array [array_index][1] == 2 && alarm_status != 2)
                                                {
                                                    alarm_led = "<img id='led_green' src='/media/img_trans.gif'>";
                                                    alarm_status = 2;
                                                }
                                                array_index++;
                                            }
                                            while ( array_index < status_array.length && (status_array [array_index][1] >= 1 && status_array [array_index][1] <= 2) )
                                            {
                                                if( status_array [array_index][1] == 1 && alarm_status != 1)
                                                {
                                                    alarm_led = "<img id='led_orange' src='/media/img_trans.gif'>";
                                                    alarm_status = 1;
                                                }
                                                array_index++;
                                            }
                                            while ( array_index < status_array.length && (status_array [array_index][1] >= 0 && status_array [array_index][1] <= 2) )
                                            { //console(array_index);
                                                if( status_array [array_index][1] == 0 && alarm_status != 0)
                                                {
                                                    alarm_led = "<img id='led_red' src='/media/img_trans.gif'>";
                                                    alarm_status = 0;
                                                }
                                                array_index++;
                                            }
                                        }
                                        else
                                        {
                                            alarm_led = "<img id='led_green' src='/media/img_trans.gif'>";
                                            console.log(obj.device_id);
                                        }
                                    }
                                    //console.log(obj.device_id);
                                    table.cell( index, -1 ).data("<p>" + alarm_led + "</p>");
                                    //cell ( index. -1 ).innerHTML = "<p>" + alarm_led + "</p>";
                                    //status_resp = [];
                                    //status_resp.push([obj.timestamp, alarm, alarm_led]);
                                }
                                //console.log(obj.device_id);
//                                else if(obj.id == table.cell( index,0 ).data())
//                                    console.log(table.cell( index, 0 ).data()+" NO change");


                            }
                            else {
                                console.log(obj.error);
                            }
                        }
                    });
                });
//                for (colomn_index = 2; colomn_index < 4; colomn_index++) {
//
//                    table.column(colomn_index).nodes().each(function (node, index, dt) {
//                        table.cell(node).data(status_resp[index][colomn_index - 2]);
//
//                    });
//                }


                //event.stopPropagation();
            }).draw();


//                table.column(-1, {draw: 'applied'}).nodes().each(function (cell, i) {
//                    cell.innerHTML = "<p>" + status_resp[i][2] + "</p>";
//                });


//            $('#example tr').each(function () {
//                var abc = $(this).children('td').eq(-1).data();
//                //console.log(abc);
//
//                if (abc) {
//                    $(this).children('td').eq(-2).css('background-color', '#00bd00');
//                }
//
//            });
//        $('#example tbody').on( 'click', 'td', function () {
//            //alert( 'Clicked on cell in visible column: '+table.cell( this ).index().columnVisible );
//            var cell = table.cell( this );
//            console.log(cell.data());
//        } );
            $('#example').dataTable({
                "destroy": true
            });

                //console.log(vari);
            }
                QuantaAjax();
    //QuantaAjax();
            setInterval(function () {
                QuantaAjax();
            },1000*60*6);

</script>

<table id="example" class="display" cellspacing="0" width="100%">
	<thead>
		<tr>
            <th>#</th>
			<th>Site Id</th>
			<th>Site Name</th>
			<th>Time of Get</th>
			<th>Alarms</th>
		</tr>
	</thead>
	<tfoot>
		<tr>
            <th>#</th>
			<th>Site Id</th>
			<th>Site Name</th>
			<th>Time of Get</th>
			<th>Alarms</th>
		</tr>
	</tfoot>
</table>

