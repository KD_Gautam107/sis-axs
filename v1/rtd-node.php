<?php
session_start();
if (!isset($_SESSION['login_user']))
{
	header("Location: login.php");
}


if($_SERVER["REQUEST_METHOD"] == "GET")
{
	$id = $_GET['id'];
	if ($id[0] == 'n')
	{
		$id = ltrim($id, "n");
	}
	else
	{
		echo "Validation FAILED<br>";
		die;
	}
}


include 'common.php';
$data_array = GetSiteDetailFromId($id);
//var_dump($data_array);
$data_quanta = GetDeviceRecordFromId($data_array[20]);
//var_dump($data_quanta);
$stats = unpack ( "C*" , $data_quanta[7] );

//var_dump($data_array);
//var_dump($data_quanta);
//var_dump ($stats);
//var_dump (bin2hex($data_quanta[7]));
?>

<div class="panel panel-default">
  <div class="panel-body">
    <div id="id_data_control"></div>
		<a role="button" class="btn btn-primary" href="#" onclick="event.preventDefault(); LoadContentState();">Refresh</a> <em>Time of Current Data: <?php echo $data_quanta[5]; ?></em>
  </div>
</div>


<div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title" title='<?php echo $id." / ".$data_array[20]; ?>'>
		<a data-toggle="collapse" href="#collapseSiteDetails">Site Details</a>
	</h3>
  </div>
  <div id="collapseSiteDetails" class="panel-collapse collapse in">
	<div class="panel-body">

		<h3><img src="/media/icons/ic_settings_input_antenna_black_36dp.png" title="Site Name"> <?php echo $data_array[5]; ?></h3>
		
		<div style="margin-top:20px;">
			<table class="table table-bordered table-condensed table-striped">
				<tr><td class='col-md-3'>Site Id</td><td class='col-md-9'><strong><?php echo $data_array[0]; ?></strong></td></tr>
				<tr><td><img src="/media/icons/ic_place_black_24dp.png" title="Location">Location</td>
				
				<td>
					<?php 
						//Lat/Lon
						if (ctype_space($data_quanta[46]) || $data_quanta[46] == ''
								|| ctype_space($data_quanta[47]) || $data_quanta[47] == '')
						{
							echo "-";
						}
						else
						{
					?>
						<a id="MapToggleButton" role="button" class="btn btn-primary" href="#" onclick="event.preventDefault();ToggleMapLocation(<?php echo $data_quanta[46].",".$data_quanta[47]; ?>);">Show on Map</a>
						<div id="locationMapWrapper"></div>

					<?php
						}
					
					?>
					
				</td></tr>
			
				<tr><td><img src="/media/icons/ic_gamepad_black_24dp.png" title="Cluster">Cluster</td><td><strong><?php echo $data_array[19]; ?></strong></td></tr>
			
				<tr><td><img src="/media/icons/ic_data_usage_black_24dp.png" title="Circle">Circle</td><td><strong><?php echo $data_array[4]; ?></strong></td></tr>
				<tr><td><img src="/media/icons/ic_location_city_black_24dp.png" title="District">District</td><td><strong><?php echo $data_array[6]; ?></strong></td></tr>
				
				<tr><td><img src="/media/icons/ic_home_black_24dp.png" title="Site Type Indoor or Outdoor">Type</td><td><strong><?php if ($data_array[10] == 0) echo "Indoor"; else echo "Outdoor"; ?></strong></td></tr>
				
				<tr><td><img src="/media/icons/ic_event_black_24dp.png" title="Date of Installation">Installation Date</td><td><strong>
					<?php if ($data_array[21] == null) echo "-"; 
							else 
							{
								echo date_create_from_format('Y-m-d', $data_array[21])->format('F j, Y');
								//echo $data_array[21]; 
							}
					?>
						
						</strong></td></tr>
				
			</table>
		</div>
			
		
	
		
		<div class="row">
			<div class="col-sm-6 col-md-4">
				<div class="thumbnail">
				  <img src="/media/icons/ic_person_outline_black_48dp.png" width="100px" height="100px">
				  <div class="caption">
					<h3><?php echo $data_quanta[50];?></h3>
					<h4>Senior Asset Manager</h4>
					<p>Responsible for all sites under <em><?php echo $data_array[19]; ?></em></p>
					<p><a class="btn btn-default" href="tel:<?php echo $data_quanta[51]; ?>"><img src="/media/icons/ic_call_black_36dp.png" /></a> <?php echo $data_quanta[51]; ?></p>
					
				  </div>
				</div>
			</div>
			<div class="col-sm-6 col-md-4">
				<div class="thumbnail">
				  <img src="/media/icons/ic_person_outline_black_48dp.png" width="100px" height="100px">
				  <div class="caption">
					<h3><?php echo $data_quanta[52];?></h3>
					<h4>Asset Manager</h4>
					<p>Responsible for the site <em><?php echo $data_array[5]; ?></em></p>
					<p><a class="btn btn-default" href="tel:<?php echo $data_quanta[53]; ?>"><img src="/media/icons/ic_call_black_36dp.png" /></a> <?php echo $data_quanta[53]; ?></p>
					
				  </div>
				</div>
			</div>
			<div class="col-sm-6 col-md-4">
				<div class="thumbnail">
				  <img src="/media/icons/ic_person_outline_black_48dp.png" width="100px" height="100px">
				  <div class="caption">
					<h3><?php echo $data_quanta[54];?></h3>
					<h4>Technician</h4>
					<p>Technician for the site <em><?php echo $data_array[5]; ?></em></p>
					<p><a class="btn btn-default" href="tel:<?php echo $data_quanta[55]; ?>"><img src="/media/icons/ic_call_black_36dp.png" /></a> <?php echo $data_quanta[55]; ?></p>
					
				  </div>
				</div>
			</div>

		</div>
		
		
		
	<br><br>
	
	<div style="margin-top:20px;">
		<table class="table table-bordered table-condensed">
		
			<?php
				$data_type_str = "Unknown";
				$data_type_class = "";
				if ($data[3] == 0) {$data_type_str = "Periodic"; $data_type_class = "success";}
				if ($data[3] == 1) {$data_type_str = "Fault"; $data_type_class = "danger";}
			?>		
			<tr class="<?php echo $data_type_class; ?>"><td class='col-md-3'><img src="/media/icons/ic_view_list_black_24dp.png" title="Data Type">Data Type</td><td class='col-md-9'><strong><?php echo $data_type_str; ?></strong></td></tr>
			
			
			<?php 
			//Not for Solar Site
			if ($data_quanta[6] == 1) 
			{	
				if (($data_quanta[6] == 1) && ($data_array[30] == 1))	//Double DG
				{
				
					if (($stats[6] & 0x04) == false) 
					{
						$data_type_str = "Automatic";
						$data_type_class = "success";
					}
					else
					{
						$data_type_str = "Manual";
						$data_type_class = "danger";
					}
			?>
					
					<tr class="<?php echo $data_type_class; ?>"><td><img src="/media/icons/ic_accessibility_black_24dp.png" title="Operation Mode">Operation Mode</td><td><strong><?php echo $data_type_str; ?></strong></td></tr>
					
			<?php		
					//000    :Load on EB,	001: Load on DG1, 011: Load on site Battery   
					//010	: Load on DG2, 111 ( site in battery with emg case) 
					// other’s Not used
					
					//$FLAG_034_LOAD 	= 0x0000000018;	
					
					$flg_data = ($stats[6] & 0x38) / 8;
					if ($flg_data == 0)
					{
						//echo "[- Load on EB -]<br>";			//000
						$data_type_str =  "EB";				
						$data_type_class = "success";
					}
					else if ($flg_data == 1)
					{
						//echo "[- Load on DG 1 -]<br>";			//001
						$data_type_str =  "DG 1";				
						$data_type_class = "warning";
					}
					else if ($flg_data == 2)
					{
						//echo "[- Load on DG 2 -]<br>";			//010
						$data_type_str =  "DG 2";				
						$data_type_class = "warning";
					}
					else if ($flg_data == 3)
					{
						//echo "[- Load on site Battery -]<br>";	//011
						$data_type_str =  "Site Battery";
						$data_type_class = "warning";
					}
					else if ($flg_data == 7)
					{
						//echo "[- Load on site Battery (emg case) -]<br>";	//111
						$data_type_str =  "Site Battery (Emg)";
						$data_type_class = "warning";
					}	
					else
					{
						//echo "[- Not Used -]<br>";				//remaining
						$data_type_str =  "Not Used";		
						$data_type_class = "";
					}
					
					
					//Generator 1 Status
					if (($stats[2] & 0x01) == true) 		//$FLAG_32_DG 	= 0x0100000000;	//DG OFF
					{
						$data_type_str_a = "OFF";
						$data_type_class_a = "";
					}
					else if (($stats[2] & 0x02) == true) 	//$FLAG_33_DG 	= 0x0200000000;	//DG ON
					{
						$data_type_str_a = "ON";
						$data_type_class_a = "warning";
					}
					else if (($stats[2] & 0x04) == true) 	//$FLAG_34_DG 	= 0x0400000000;	//DG Cranking
					{
						$data_type_str_a = "Cranking";
						$data_type_class_a = "warning";
					}
					else if (($stats[2] & 0x08) == true) 	//$FLAG_35_DG 	= 0x0800000000;	//DG Start in Progress
					{
						$data_type_str_a = "Start in Progress";
						$data_type_class_a = "warning";
					}
					else if (($stats[2] & 0x10) == true) 		//$FLAG_36_DG 	= 0x1000000000;	//DG Cool Down (Idle Running)
					{
						$data_type_str_a = "Cool Down (Idle Running)";
						$data_type_class_a = "";
					}
					else 
					{
						$data_type_str_a = "Alert";
						$data_type_class_a = "danger";
					}

					//Generator 2 Status
					if (($stats[1] & 0x01) == true) 		//$FLAG_32_DG 	= 0x0100000000;	//DG OFF
					{
						$data_type_str_b = "OFF";
						$data_type_class_b = "";
					}
					else if (($stats[1] & 0x02) == true) 	//$FLAG_33_DG 	= 0x0200000000;	//DG ON
					{
						$data_type_str_b = "ON";
						$data_type_class_b = "warning";
					}
					else if (($stats[1] & 0x04) == true) 	//$FLAG_34_DG 	= 0x0400000000;	//DG Cranking
					{
						$data_type_str_b = "Cranking";
						$data_type_class_b = "warning";
					}
					else if (($stats[1] & 0x08) == true) 	//$FLAG_35_DG 	= 0x0800000000;	//DG Start in Progress
					{
						$data_type_str_b = "Start in Progress";
						$data_type_class_b = "warning";
					}
					else if (($stats[1] & 0x10) == true) 		//$FLAG_36_DG 	= 0x1000000000;	//DG Cool Down (Idle Running)
					{
						$data_type_str_b = "Cool Down (Idle Running)";
						$data_type_class_b = "";
					}
					else 
					{
						$data_type_str_b = "Alert";
						$data_type_class_b = "danger";
					}				
				}
				else
				{
					if (($stats[5] & 0x04) == false) 
					{
						$data_type_str = "Automatic";
						$data_type_class = "success";
					}
					else
					{
						$data_type_str = "Manual";
						$data_type_class = "danger";
					}
				?>
					
					<tr class="<?php echo $data_type_class; ?>"><td><img src="/media/icons/ic_accessibility_black_24dp.png" title="Operation Mode">Operation Mode</td><td><strong><?php echo $data_type_str; ?></strong></td></tr>
					
				<?php	
					//$FLAG_034_LOAD 	= 0x0000000018;	//00    :Load on EB,01: Load on DG, 10: Load on site Battery   11: Not used
					if (($stats[5] & 0x08) == false) 
					{
						if (($stats[5] & 0x10) == false) 
						{
							$data_type_str =  "EB";				//00
							$data_type_class = "success";
						}
						else
						{
							$data_type_str =  "Site Battery";	//10
							$data_type_class = "warning";
						}
					}
					else
					{
						if (($stats[5] & 0x10) == false) 
						{
							$data_type_str =  "DG";				//01
							$data_type_class = "warning";
						}
						else
						{
							$data_type_str =  "Not Used";		//11
							$data_type_class = "";
						}
					}
					
					
					if (($stats[1] & 0x01) == true) 		//$FLAG_32_DG 	= 0x0100000000;	//DG OFF
					{
						$data_type_str_a = "OFF";
						$data_type_class_a = "";
					}
					else if (($stats[1] & 0x02) == true) 	//$FLAG_33_DG 	= 0x0200000000;	//DG ON
					{
						$data_type_str_a = "ON";
						$data_type_class_a = "warning";
					}
					else if (($stats[1] & 0x04) == true) 	//$FLAG_34_DG 	= 0x0400000000;	//DG Cranking
					{
						$data_type_str_a = "Cranking";
						$data_type_class_a = "warning";
					}
					else if (($stats[1] & 0x08) == true) 	//$FLAG_35_DG 	= 0x0800000000;	//DG Start in Progress
					{
						$data_type_str_a = "Start in Progress";
						$data_type_class_a = "warning";
					}
					else if (($stats[1] & 0x10) == true) 		//$FLAG_36_DG 	= 0x1000000000;	//DG Cool Down (Idle Running)
					{
						$data_type_str_a = "Cool Down (Idle Running)";
						$data_type_class_a = "";
					}
					else 
					{
						$data_type_str_a = "Alert";
						$data_type_class_a = "danger";
					}
				}
				?>
				<tr class="<?php echo $data_type_class; ?>"><td><img src="/media/icons/ic_power.png" title="Power Load On">Power Load On</td><td><strong><?php echo $data_type_str; ?></strong></td></tr>
				
				
				<?php
				if (($data_quanta[6] == 1) && ($data_array[30] == 1))	//Double DG
				{
				?>
					<tr class="<?php echo $data_type_class_a; ?>"><td><img src="/media/icons/ic_settings_applications_black_24dp.png" title="Generator Status">Generator 1 Status</td><td><strong><?php echo $data_type_str_a; ?></strong></td></tr>
					<tr class="<?php echo $data_type_class_b; ?>"><td><img src="/media/icons/ic_settings_applications_black_24dp.png" title="Generator Status">Generator 2 Status</td><td><strong><?php echo $data_type_str_b; ?></strong></td></tr>				
				
				<?php
					$data_type_str = $data_type_str_a." / ".$data_type_str_b;
				}
				else
				{
				?>
					
					<tr class="<?php echo $data_type_class_a; ?>"><td><img src="/media/icons/ic_settings_applications_black_24dp.png" title="Generator Status">Generator Status</td><td><strong><?php echo $data_type_str_a; ?></strong></td></tr>
					
				<?php 
					$data_type_str = $data_type_str_a;
				} 
				?>
			
			
			<?php 
			} //Not for Solar Site -- ENDS
			else if ($data_quanta[6] == 16)  //Phase 2
			{
				if (($stats[5] & 0x04) == false) 
				{
					$data_type_str = "Automatic";
					$data_type_class = "success";
				}
				else
				{
					$data_type_str = "Manual";
					$data_type_class = "danger";
				}
			?>
				
				<tr class="<?php echo $data_type_class; ?>"><td><img src="/media/icons/ic_accessibility_black_24dp.png" title="Operation Mode">Operation Mode</td><td><strong><?php echo $data_type_str; ?></strong></td></tr>
				
			<?php	
				//$FLAG_034_LOAD 	= 0x0000000018;	//00    :Load on EB,01: Load on DG, 10: Load on site Battery   11: Not used
				if (($stats[5] & 0x08) == false) 
				{
					if (($stats[5] & 0x10) == false) 
					{
						$data_type_str =  "EB";				//00
						$data_type_class = "success";
					}
					else
					{
						$data_type_str =  "Site Battery";	//10
						$data_type_class = "warning";
					}
				}
				else
				{
					if (($stats[5] & 0x10) == false) 
					{
						$data_type_str =  "DG";				//01
						$data_type_class = "warning";
					}
					else
					{
						$data_type_str =  "Not Used";		//11
						$data_type_class = "";
					}
				}
				
				
				if (($stats[1] & 0x01) == true) 		//$FLAG_32_DG 	= 0x0100000000;	//DG OFF
				{
					$data_type_str_a = "OFF";
					$data_type_class_a = "";
				}
				else if (($stats[1] & 0x02) == true) 	//$FLAG_33_DG 	= 0x0200000000;	//DG ON
				{
					$data_type_str_a = "ON";
					$data_type_class_a = "warning";
				}
				else if (($stats[1] & 0x04) == true) 	//$FLAG_34_DG 	= 0x0400000000;	//DG Cranking
				{
					$data_type_str_a = "Cranking";
					$data_type_class_a = "warning";
				}
				else if (($stats[1] & 0x08) == true) 	//$FLAG_35_DG 	= 0x0800000000;	//DG Start in Progress
				{
					$data_type_str_a = "Start in Progress";
					$data_type_class_a = "warning";
				}
				else if (($stats[1] & 0x10) == true) 		//$FLAG_36_DG 	= 0x1000000000;	//DG Cool Down (Idle Running)
				{
					$data_type_str_a = "Cool Down (Idle Running)";
					$data_type_class_a = "";
				}
				else 
				{
					$data_type_str_a = "Alert";
					$data_type_class_a = "danger";
				}
			?>
			
				<tr class="<?php echo $data_type_class; ?>"><td><img src="/media/icons/ic_power.png" title="Power Load On">Power Load On</td><td><strong><?php echo $data_type_str; ?></strong></td></tr>
				
				<tr class="<?php echo $data_type_class_a; ?>"><td><img src="/media/icons/ic_settings_applications_black_24dp.png" title="Generator Status">Generator Status</td><td><strong><?php echo $data_type_str_a; ?></strong></td></tr>
			<?php 	
			}
			else if($data_quanta[6] == 32)
			{

			}

			?>
			
		</table>
	</div>
	
	
	<br>
			
		
  </div>

  </div>
</div>

<script>
			
		function GenStartStop(i){
			$("#device-control-status").html("<i>Sending command...</i>");
			$.ajax({url: "equipmentcontrol.php?operation="+i+"&id="+<?php echo $id; ?>, success: function(result){
				$("#device-control-status").html("<i>"+result+"</i>");
			}});
		}
		
	
</script>

<div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title">
		<a data-toggle="collapse" href="#collapseEquipmentDetailAndControl">Equipment Detail and Control</a>
	</h3>
  </div>
  <div id="collapseEquipmentDetailAndControl" class="panel-collapse collapse in">
  <div class="panel-body">
  
	<table class="table table-bordered table-condensed">
		
		<tr>
			<td class='col-md-3'>Device Name</td>
			<td class='col-md-9'><strong><?php echo $data_array[2]; ?></strong></td>
		</tr>
		<tr>
			<td class='col-md-3'>Device Make</td>
			<td class='col-md-9'><strong><?php echo $data_array[3]; ?></strong></td>
		</tr>
		<tr>
			<td class='col-md-3'>EB Capacity</td>
			<td class='col-md-9'><strong><?php if ($data_array[23] == 0) echo "-"; else echo $data_array[23]." Kwh"; ?></strong></td>
		</tr>
		<tr>
			<td class='col-md-3'>EB Phase</td>
			<td class='col-md-9'><strong><?php if ($data_array[24] == 0) echo "-"; else echo $data_array[24]." Phase"; ?></strong></td>
		</tr>
		<tr>
			<td class='col-md-3'>Battery Capacity</td>
			<td class='col-md-9'><strong><?php if ($data_array[25] == 0) echo "-"; else echo $data_array[25]." AH"; ?></strong></td>
		</tr>
		<tr>
			<td class='col-md-3'>Battery Quantity</td>
			<td class='col-md-9'><strong><?php if ($data_array[26] == 0) echo "-"; else echo $data_array[26]; ?></strong></td>
		</tr>
		<tr>
			<td class='col-md-3'>Battery Make</td>
			<td class='col-md-9'><strong><?php echo $data_array[27]; ?></strong></td>
		</tr>
		
		<?php
			//Not for Solar Site
		if (($data_quanta[6] == 1) || ($data_quanta[6] == 16)) { ?>

			<tr>
				<td class='col-md-3'>DG Make</td>
				<td class='col-md-9'><strong><?php echo $data_array[22]; ?> </strong></td>
			</tr>
			<tr>
				<td class='col-md-3'>DG Detail</td>
				<td class='col-md-9'><strong><?php if ($data_array[11] == 0) echo "-"; else echo $data_array[11]." KVA"; ?> </strong></td>
			</tr>
			<?php
				//For Double DG Site
				if ($data_array[30] == 1) { ?>
				
			<tr>
				<td class='col-md-3'>DG 2 Make</td>
				<td class='col-md-9'><strong><?php echo $data_array[29]; ?> </strong></td>
			</tr>
			<tr>
				<td class='col-md-3'>DG 2 Detail</td>
				<td class='col-md-9'><strong><?php if ($data_array[28] == 0) echo "-"; else echo $data_array[28]." KVA"; ?> </strong></td>
			</tr>
			
			<?php } ?>	
			
			<tr>
				<td class='col-md-3'>Generator Status</td>
				<td class='col-md-9'><strong><?php echo $data_type_str_a; ?></strong></td>
			</tr>
			<tr>
				<td class='col-md-3'>Generator Control</td>
				<td class='col-md-9'><strong><?php if (ctype_space($data_array[7]) || $data_array[7] == '') //Mobile
				{
					//Mobile not available
					echo("Generator control details not available.");
				}
				else
				{
					//Available
					echo('<div class="btn-toolbar" role="toolbar" aria-label="time-frame-buttons">
							  <a role="button" class="btn btn-success');
					echo('" href="javascript:GenStartStop(1)">START</a>
							  <a role="button" class="btn btn-danger');
					echo ('" href="javascript:GenStartStop(0)">STOP</a>
							</div>');
					echo('<div id="device-control-status"></div>');
				} ?></strong></td>
			</tr>
		
		<?php } ?>
		
	</table>
 
  </div>

  </div>
</div>
  
<?php if ($data_quanta[6] == 2) { ?>
	<script>AsyncLoad("node-power-supply-solar.php?site_id=<?php echo $data_array[0]; ?>&range_data_type=0", "#id_power_supply");</script>
<?php } else { ?>
	<script>AsyncLoad("node-power-supply.php?site_id=<?php echo $data_array[0]; ?>&range_data_type=0", "#id_power_supply");</script>
<?php } ?>

<div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title">
		<a data-toggle="collapse" href="#collapsePowerSupply">Power Supply</a>
	</h3>
  </div>
  <div id="collapsePowerSupply" class="panel-collapse collapse in">
	  <div class="panel-body">
		<div id="id_power_supply"></div>
	  </div>
  </div>
</div>



<div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title">
		<a data-toggle="collapse" href="#collapseSiteStats">Site Stats (live data)</a>
	</h3>
  </div>
  <div id="collapseSiteStats" class="panel-collapse collapse in">
	  <div class="panel-body">
		<?php include 'stats.php'; ?>
	  </div>
  </div>
</div>

<div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title">
		<a data-toggle="collapse" href="#collapseAlarmDetails">Alarm Details (live data)</a>
	</h3>
  </div>
  
  <div id="collapseAlarmDetails" class="panel-collapse collapse in">
  <div class="panel-body">
	
	<table id="alarms-table" class="table table-bordered">
	  <thead>
		<tr>
		  <th data-resizable-column-id="Severity" class='col-md-1'>Sev</th>
		  <th data-resizable-column-id="alarm_name" class='col-md-4'>Alarm Name</th>
		  <th data-resizable-column-id="alarm_description" class='col-md-7'>Generation Time</th>
		</tr>
	  </thead>
	  <tbody>

	
	<?php
		
		$alarm_history = array();
		$tdid = 0;
		$anyAlarm = false;
		
		if (($data_quanta[6] == 1) && ($data_array[30] == 1))	
		{
			//Double DG Alarms
			
			if (($stats[6] & 0x01) == true)
			{
				echo "<tr class='danger'><td><img src='/media/icons/ic_info_black_24dp.png' title='Critical Alarm'></td><td>Smoke Fire Alarm</td><td id='alertid$tdid'>";
				//0x0000000000 Smoke fire    0 means No alarm,  1 means Alarm
				echo "</td></tr>";
				
				$tmp = array();
				$tmp[] = 6;
				$tmp[] = 0x01;
				$tmp[] = false;
				$tmp[] = $data_quanta[5];
				$tmp[] = "alertid".$tdid;
				$alarm_history[] = $tmp;
				$tdid++;
				$anyAlarm = true;
			}
			
			if ($data_array[10] == 0)	//Door Alarm Only Applicable if Indoor
			{
				if (($stats[6] & 0x02) == true)
				{		
					echo "<tr class='warning'><td><img src='/media/icons/ic_report_problem_black_24dp.png' title='Major Alarm'></td><td>Door</td><td id='alertid$tdid'>";
					//$FLAG_01_DOOR 	= 0x0000000002;	//Door Open     0  means  Door Close , 1 Means  Door open  
					echo "</td></tr>";
					
					$tmp = array();
					$tmp[] = 6;
					$tmp[] = 0x02;
					$tmp[] = false;
					$tmp[] = $data_quanta[5];
					$tmp[] = "alertid".$tdid;
					$alarm_history[] = $tmp;
					$tdid++;
					$anyAlarm = true;
				}
			}
			
			if (($stats[6] & 0x40) == true)
			{
				echo "<tr class='info'><td><img src='/media/icons/ic_info_outline_black_24dp.png' title='Minor Alarm'></td><td>Mains Fail</td><td id='alertid$tdid'>";
				
				//$FLAG_05_MAINS 	= 0x0000000020;	//Inverter fail		1 Means main Fail,  0 Means  Main healthy
				echo "</td></tr>";
				
				$tmp = array();
				$tmp[] = 6;
				$tmp[] = 0x40;
				$tmp[] = false;
				$tmp[] = $data_quanta[5];
				$tmp[] = "alertid".$tdid;
				$alarm_history[] = $tmp;
				$tdid++;
				$anyAlarm = true;
			}
			
			if (($stats[6] & 0x80) == true)
			{
				echo "<tr class='warning'><td><img src='/media/icons/ic_report_problem_black_24dp.png' title='Major Alarm'></td><td>Site Battery Low</td><td id='alertid$tdid'>";
				//$FLAG_06_BATT 	= 0x0000000040;	//Site battery Low  1 Means Site Battery Low ,0 Means  Battery Ok
				echo "</td></tr>";
				
				$tmp = array();
				$tmp[] = 6;
				$tmp[] = 0x80;
				$tmp[] = false;
				$tmp[] = $data_quanta[5];
				$tmp[] = "alertid".$tdid;
				$alarm_history[] = $tmp;
				$tdid++;
				$anyAlarm = true;
			}
			
			if (($stats[5] & 0x01) == true) 
			{
				echo "<tr class='info'><td><img src='/media/icons/ic_info_outline_black_24dp.png' title='Minor Alarm'></td><td>Room Temperature</td><td id='alertid$tdid'>";
				//$FLAG_07_TEMP 	= 0x0000000080;	//Room Temperature  1 Means high Temperature, 0 Means Normal temp
				echo "</td></tr>";
				
				$tmp = array();
				$tmp[] = 5;
				$tmp[] = 0x01;
				$tmp[] = false;
				$tmp[] = $data_quanta[5];
				$tmp[] = "alertid".$tdid;
				$alarm_history[] = $tmp;
				$tdid++;
				$anyAlarm = true;
			}
			
			if (($stats[5] & 0x02) == true)
			{
				echo "<tr class='warning'><td><img src='/media/icons/ic_report_problem_black_24dp.png' title='Major Alarm'></td><td>LLOP DG1</td><td id='alertid$tdid'>";
				//$FLAG_08_LLOP 	= 0x0000000100;	//LLOP
				echo "</td></tr>";
				
				$tmp = array();
				$tmp[] = 5;
				$tmp[] = 0x02;
				$tmp[] = false;
				$tmp[] = $data_quanta[5];
				$tmp[] = "alertid".$tdid;
				$alarm_history[] = $tmp;
				$tdid++;
				$anyAlarm = true;
			}
			
			if (($stats[5] & 0x04) == true)
			{
				echo "<tr class='warning'><td><img src='/media/icons/ic_report_problem_black_24dp.png' title='Major Alarm'></td><td>HCT/HWT DG2</td><td id='alertid$tdid'>";
				//$FLAG_09_HCT 	= 0x0000000200;	//HCT/HWT
				echo "</td></tr>";
				
				$tmp = array();
				$tmp[] = 4;
				$tmp[] = 0x02;
				$tmp[] = false;
				$tmp[] = $data_quanta[5];
				$tmp[] = "alertid".$tdid;
				$alarm_history[] = $tmp;
				$tdid++;
				$anyAlarm = true;
			}
			
			if (($stats[5] & 0x08) == true)
			{
				echo "<tr class='warning'><td><img src='/media/icons/ic_report_problem_black_24dp.png' title='Major Alarm'></td><td>Alternator Fault DG1</td><td id='alertid$tdid'>";
				//$FLAG_10_ALT 	= 0x0000000400;	//Alternator Fault
				echo "</td></tr>";
				
				$tmp = array();
				$tmp[] = 5;
				$tmp[] = 0x08;
				$tmp[] = false;
				$tmp[] = $data_quanta[5];
				$tmp[] = "alertid".$tdid;
				$alarm_history[] = $tmp;
				$tdid++;
				$anyAlarm = true;
			}
			
			if (($stats[5] & 0x10) == true)
			{
				echo "<tr class='warning'><td><img src='/media/icons/ic_report_problem_black_24dp.png' title='Major Alarm'></td><td>DG1 Over Speed</td><td id='alertid$tdid'>";
				//$FLAG_11_DG 	= 0x0000000800;	//DG Over Speed
				echo "</td></tr>";
				
				$tmp = array();
				$tmp[] = 5;
				$tmp[] = 0x10;
				$tmp[] = false;
				$tmp[] = $data_quanta[5];
				$tmp[] = "alertid".$tdid;
				$alarm_history[] = $tmp;
				$tdid++;
				$anyAlarm = true;
			}
			
			if (($stats[5] & 0x20) == true)
			{
				echo "<tr class='danger'><td><img src='/media/icons/ic_info_black_24dp.png' title='Critical Alarm'></td><td>DG1 Over Load</td><td id='alertid$tdid'>";
				//$FLAG_12_DG 	= 0x0000001000;	//DG Over Load
				echo "</td></tr>";
				
				$tmp = array();
				$tmp[] = 5;
				$tmp[] = 0x20;
				$tmp[] = false;
				$tmp[] = $data_quanta[5];
				$tmp[] = "alertid".$tdid;
				$alarm_history[] = $tmp;
				$tdid++;
				$anyAlarm = true;
			}
			
			if (($stats[5] & 0x40) == true)
			{
				echo "<tr class='danger'><td><img src='/media/icons/ic_info_black_24dp.png' title='Critical Alarm'></td><td>DG1 Low Fuel</td><td id='alertid$tdid'>";
				//$FLAG_13_DG 	= 0x0000002000;	//DG Low Fuel
				echo "</td></tr>";
				
				$tmp = array();
				$tmp[] = 5;
				$tmp[] = 0x40;
				$tmp[] = false;
				$tmp[] = $data_quanta[5];
				$tmp[] = "alertid".$tdid;
				$alarm_history[] = $tmp;
				$tdid++;
				$anyAlarm = true;
			}
			
			if (($stats[5] & 0x80) == true)
			{
				echo "<tr class='warning'><td><img src='/media/icons/ic_report_problem_black_24dp.png' title='Major Alarm'></td><td>DG1 Start Fail</td><td id='alertid$tdid'>";
				//$FLAG_14_DG 	= 0x0000004000;	//DG Start Fail
				echo "</td></tr>";
				
				$tmp = array();
				$tmp[] = 5;
				$tmp[] = 0x80;
				$tmp[] = false;
				$tmp[] = $data_quanta[5];
				$tmp[] = "alertid".$tdid;
				$alarm_history[] = $tmp;
				$tdid++;
				$anyAlarm = true;
			}
			
			if (($stats[4] & 0x01) == true) 
			{
				echo "<tr class='warning'><td><img src='/media/icons/ic_report_problem_black_24dp.png' title='Major Alarm'></td><td>DG1 Stop Fail</td><td id='alertid$tdid'>";
				//$FLAG_15_DG 	= 0x0000008000;	//DG Stop Fail
				echo "</td></tr>";
				
				$tmp = array();
				$tmp[] = 4;
				$tmp[] = 0x01;
				$tmp[] = false;
				$tmp[] = $data_quanta[5];
				$tmp[] = "alertid".$tdid;
				$alarm_history[] = $tmp;
				$tdid++;
				$anyAlarm = true;
			}
			
			if (($stats[4] & 0x02) == true) 
			{
				echo "<tr class='warning'><td><img src='/media/icons/ic_report_problem_black_24dp.png' title='Major Alarm'></td><td>DG1 Battery Low</td><td id='alertid$tdid'>";
				//$FLAG_16_DG 	= 0x0000010000;	//DG battery Low
				echo "</td></tr>";
				
				$tmp = array();
				$tmp[] = 4;
				$tmp[] = 0x02;
				$tmp[] = false;
				$tmp[] = $data_quanta[5];
				$tmp[] = "alertid".$tdid;
				$alarm_history[] = $tmp;
				$tdid++;
				$anyAlarm = true;
			}
			
			if (($stats[4] & 0x04) == true) 
			{
				echo "<tr class='danger'><td><img src='/media/icons/ic_info_black_24dp.png' title='Critical Alarm'></td><td>LCU Fail</td><td id='alertid$tdid'>";
				//$FLAG_17_LCU 	= 0x0000020000;	//LCU fail
				echo "</td></tr>";
				
				$tmp = array();
				$tmp[] = 4;
				$tmp[] = 0x04;
				$tmp[] = false;
				$tmp[] = $data_quanta[5];
				$tmp[] = "alertid".$tdid;
				$alarm_history[] = $tmp;
				$tdid++;
				$anyAlarm = true;
			}
			
			if (($stats[4] & 0x08) == true)
			{
				echo "<tr class='warning'><td><img src='/media/icons/ic_report_problem_black_24dp.png' title='Major Alarm'></td><td>Rectifier Fail</td><td id='alertid$tdid'>";
				//$FLAG_18_RECT 	= 0x0000040000;	//Rectifier Fail
				echo "</td></tr>";
				
				$tmp = array();
				$tmp[] = 4;
				$tmp[] = 0x08;
				$tmp[] = false;
				$tmp[] = $data_quanta[5];
				$tmp[] = "alertid".$tdid;
				$alarm_history[] = $tmp;
				$tdid++;
				$anyAlarm = true;
			}
			
			if (($stats[4] & 0x10) == true)
			{
				echo "<tr class='warning'><td><img src='/media/icons/ic_report_problem_black_24dp.png' title='Major Alarm'></td><td>Multi Rectifier Fail</td><td id='alertid$tdid'>";
				//$FLAG_19_RECT 	= 0x0000080000;	//Multi Rectifier Fail
				echo "</td></tr>";
				
				$tmp = array();
				$tmp[] = 4;
				$tmp[] = 0x10;
				$tmp[] = false;
				$tmp[] = $data_quanta[5];
				$tmp[] = "alertid".$tdid;
				$alarm_history[] = $tmp;
				$tdid++;
				$anyAlarm = true;
			}
			
			if (($stats[4] & 0x20) == true)
			{
				echo "<tr class='danger'><td><img src='/media/icons/ic_info_black_24dp.png' title='Critical Alarm'></td><td>LVD Trip</td><td id='alertid$tdid'>";
				//$FLAG_20_LVD 	= 0x0000100000;	//LVD TRIP
				echo "</td></tr>";
				
				$tmp = array();
				$tmp[] = 4;
				$tmp[] = 0x20;
				$tmp[] = false;
				$tmp[] = $data_quanta[5];
				$tmp[] = "alertid".$tdid;
				$alarm_history[] = $tmp;
				$tdid++;
				$anyAlarm = true;
			}
			
			if (($stats[4] & 0x40) == true)
			{
				echo "<tr class='info'><td><img src='/media/icons/ic_info_outline_black_24dp.png' title='Minor Alarm'></td><td>LVD BY PASS</td><td id='alertid$tdid'>";
				//$FLAG_21_LVD 	= 0x0000200000;	//LVD BY pass
				echo "</td></tr>";
				
				$tmp = array();
				$tmp[] = 4;
				$tmp[] = 0x40;
				$tmp[] = false;
				$tmp[] = $data_quanta[5];
				$tmp[] = "alertid".$tdid;
				$alarm_history[] = $tmp;
				$tdid++;
				$anyAlarm = true;
			}
			
			
			
			if (($stats[4] & 0x80) == true)
			{
				echo "<tr class='warning'><td><img src='/media/icons/ic_report_problem_black_24dp.png' title='Major Alarm'></td><td>LLOP DG2</td><td id='alertid$tdid'>";
				//$FLAG_08_LLOP 	= 0x0000000100;	//LLOP
				echo "</td></tr>";
				
				$tmp = array();
				$tmp[] = 4;
				$tmp[] = 0x80;
				$tmp[] = false;
				$tmp[] = $data_quanta[5];
				$tmp[] = "alertid".$tdid;
				$alarm_history[] = $tmp;
				$tdid++;
				$anyAlarm = true;
			}
			
			if (($stats[3] & 0x01) == true)
			{
				echo "<tr class='warning'><td><img src='/media/icons/ic_report_problem_black_24dp.png' title='Major Alarm'></td><td>HCT/HWT DG2</td><td id='alertid$tdid'>";
				//$FLAG_09_HCT 	= 0x0000000200;	//HCT/HWT
				echo "</td></tr>";
				
				$tmp = array();
				$tmp[] = 3;
				$tmp[] = 0x01;
				$tmp[] = false;
				$tmp[] = $data_quanta[5];
				$tmp[] = "alertid".$tdid;
				$alarm_history[] = $tmp;
				$tdid++;
				$anyAlarm = true;
			}
			
			if (($stats[3] & 0x02) == true)
			{
				echo "<tr class='warning'><td><img src='/media/icons/ic_report_problem_black_24dp.png' title='Major Alarm'></td><td>Alternator Fault DG2</td><td id='alertid$tdid'>";
				//$FLAG_10_ALT 	= 0x0000000400;	//Alternator Fault
				echo "</td></tr>";
				
				$tmp = array();
				$tmp[] = 3;
				$tmp[] = 0x02;
				$tmp[] = false;
				$tmp[] = $data_quanta[5];
				$tmp[] = "alertid".$tdid;
				$alarm_history[] = $tmp;
				$tdid++;
				$anyAlarm = true;
			}
			
			if (($stats[3] & 0x04) == true)
			{
				echo "<tr class='warning'><td><img src='/media/icons/ic_report_problem_black_24dp.png' title='Major Alarm'></td><td>DG2 Over Speed</td><td id='alertid$tdid'>";
				//$FLAG_11_DG 	= 0x0000000800;	//DG Over Speed
				echo "</td></tr>";
				
				$tmp = array();
				$tmp[] = 3;
				$tmp[] = 0x04;
				$tmp[] = false;
				$tmp[] = $data_quanta[5];
				$tmp[] = "alertid".$tdid;
				$alarm_history[] = $tmp;
				$tdid++;
				$anyAlarm = true;
			}
			
			if (($stats[3] & 0x08) == true)
			{
				echo "<tr class='danger'><td><img src='/media/icons/ic_info_black_24dp.png' title='Critical Alarm'></td><td>DG2 Over Load</td><td id='alertid$tdid'>";
				//$FLAG_12_DG 	= 0x0000001000;	//DG Over Load
				echo "</td></tr>";
				
				$tmp = array();
				$tmp[] = 3;
				$tmp[] = 0x08;
				$tmp[] = false;
				$tmp[] = $data_quanta[5];
				$tmp[] = "alertid".$tdid;
				$alarm_history[] = $tmp;
				$tdid++;
				$anyAlarm = true;
			}
			
			if (($stats[3] & 0x10) == true)
			{
				echo "<tr class='danger'><td><img src='/media/icons/ic_info_black_24dp.png' title='Critical Alarm'></td><td>DG2 Low Fuel</td><td id='alertid$tdid'>";
				//$FLAG_13_DG 	= 0x0000002000;	//DG Low Fuel
				echo "</td></tr>";
				
				$tmp = array();
				$tmp[] = 3;
				$tmp[] = 0x10;
				$tmp[] = false;
				$tmp[] = $data_quanta[5];
				$tmp[] = "alertid".$tdid;
				$alarm_history[] = $tmp;
				$tdid++;
				$anyAlarm = true;
			}
			
			if (($stats[3] & 0x20) == true)
			{
				echo "<tr class='warning'><td><img src='/media/icons/ic_report_problem_black_24dp.png' title='Major Alarm'></td><td>DG2 Start Fail</td><td id='alertid$tdid'>";
				//$FLAG_14_DG 	= 0x0000004000;	//DG Start Fail
				echo "</td></tr>";
				
				$tmp = array();
				$tmp[] = 3;
				$tmp[] = 0x20;
				$tmp[] = false;
				$tmp[] = $data_quanta[5];
				$tmp[] = "alertid".$tdid;
				$alarm_history[] = $tmp;
				$tdid++;
				$anyAlarm = true;
			}
			
			if (($stats[3] & 0x40) == true) 
			{
				echo "<tr class='warning'><td><img src='/media/icons/ic_report_problem_black_24dp.png' title='Major Alarm'></td><td>DG2 Stop Fail</td><td id='alertid$tdid'>";
				//$FLAG_15_DG 	= 0x0000008000;	//DG Stop Fail
				echo "</td></tr>";
				
				$tmp = array();
				$tmp[] = 3;
				$tmp[] = 0x40;
				$tmp[] = false;
				$tmp[] = $data_quanta[5];
				$tmp[] = "alertid".$tdid;
				$alarm_history[] = $tmp;
				$tdid++;
				$anyAlarm = true;
			}
			
			if (($stats[3] & 0x80) == true) 
			{
				echo "<tr class='warning'><td><img src='/media/icons/ic_report_problem_black_24dp.png' title='Major Alarm'></td><td>DG2 Battery Low</td><td id='alertid$tdid'>";
				//$FLAG_16_DG 	= 0x0000010000;	//DG battery Low
				echo "</td></tr>";
				
				$tmp = array();
				$tmp[] = 3;
				$tmp[] = 0x80;
				$tmp[] = false;
				$tmp[] = $data_quanta[5];
				$tmp[] = "alertid".$tdid;
				$alarm_history[] = $tmp;
				$tdid++;
				$anyAlarm = true;
			}
			
			if (($stats[2] & 0x20) == true)	
			{
				echo "<tr class='info'><td><img src='/media/icons/ic_info_outline_black_24dp.png' title='Minor Alarm'></td><td>DG1 STOP  Normally</td><td id='alertid$tdid'>";
				//$FLAG_37_DG 	= 0x2000000000;	//DG STOP Normally
				echo "</td></tr>";
				
				$tmp = array();
				$tmp[] = 2;
				$tmp[] = 0x20;
				$tmp[] = false;
				$tmp[] = $data_quanta[5];
				$tmp[] = "alertid".$tdid;
				$alarm_history[] = $tmp;
				$tdid++;
				$anyAlarm = true;
			}
			
			if (($stats[2] & 0x40) == true)
			{
				echo "<tr class='warning'><td><img src='/media/icons/ic_report_problem_black_24dp.png' title='Major Alarm'></td><td>DG1 STOP due to Fault</td><td id='alertid$tdid'>";
				//$FLAG_38_DG 	= 0x8000000000;	//DG STOP due to Fault
				echo "</td></tr>";
				
				$tmp = array();
				$tmp[] = 2;
				$tmp[] = 0x40;
				$tmp[] = false;
				$tmp[] = $data_quanta[5];
				$tmp[] = "alertid".$tdid;
				$alarm_history[] = $tmp;
				$tdid++;
				$anyAlarm = true;
			}
			
			if (($stats[2] & 0x80) == true) 
			{
				echo "<tr class='info'><td><img src='/media/icons/ic_info_outline_black_24dp.png' title='Minor Alarm'></td><td>DG1 Stop Due to Maximum Run Expiry</td><td id='alertid$tdid'>";
				//$FLAG_39_DG 	= 0x8000000000;	//DG Stop Due to Maximum Run Exipary 
				echo "</td></tr>";
				
				$tmp = array();
				$tmp[] = 2;
				$tmp[] = 0x80;
				$tmp[] = false;
				$tmp[] = $data_quanta[5];
				$tmp[] = "alertid".$tdid;
				$alarm_history[] = $tmp;
				$tdid++;
				$anyAlarm = true;
			}
			
			if (($stats[1] & 0x20) == true)	
			{
				echo "<tr class='info'><td><img src='/media/icons/ic_info_outline_black_24dp.png' title='Minor Alarm'></td><td>DG2 STOP  Normally</td><td id='alertid$tdid'>";
				//$FLAG_37_DG 	= 0x2000000000;	//DG STOP Normally
				echo "</td></tr>";
				
				$tmp = array();
				$tmp[] = 1;
				$tmp[] = 0x20;
				$tmp[] = false;
				$tmp[] = $data_quanta[5];
				$tmp[] = "alertid".$tdid;
				$alarm_history[] = $tmp;
				$tdid++;
				$anyAlarm = true;
			}
			
			if (($stats[1] & 0x40) == true)
			{
				echo "<tr class='warning'><td><img src='/media/icons/ic_report_problem_black_24dp.png' title='Major Alarm'></td><td>DG2 STOP due to Fault</td><td id='alertid$tdid'>";
				//$FLAG_38_DG 	= 0x8000000000;	//DG STOP due to Fault
				echo "</td></tr>";
				
				$tmp = array();
				$tmp[] = 1;
				$tmp[] = 0x40;
				$tmp[] = false;
				$tmp[] = $data_quanta[5];
				$tmp[] = "alertid".$tdid;
				$alarm_history[] = $tmp;
				$tdid++;
				$anyAlarm = true;
			}
			
			if (($stats[1] & 0x80) == true) 
			{
				echo "<tr class='info'><td><img src='/media/icons/ic_info_outline_black_24dp.png' title='Minor Alarm'></td><td>DG2 Stop Due to Maximum Run Expiry</td><td id='alertid$tdid'>";
				//$FLAG_39_DG 	= 0x8000000000;	//DG Stop Due to Maximum Run Exipary 
				echo "</td></tr>";
				
				$tmp = array();
				$tmp[] = 1;
				$tmp[] = 0x80;
				$tmp[] = false;
				$tmp[] = $data_quanta[5];
				$tmp[] = "alertid".$tdid;
				$alarm_history[] = $tmp;
				$tdid++;
				$anyAlarm = true;
			}
					
		}
		else
		{
			//Single DG Alarms
			
			if (($stats[5] & 0x01) == true)
			{
				echo "<tr class='danger'><td><img src='/media/icons/ic_info_black_24dp.png' title='Critical Alarm'></td><td>Smoke Fire Alarm</td><td id='alertid$tdid'>";
				//0x0000000000 Smoke fire    0 means No alarm,  1 means Alarm
				echo "</td></tr>";
				
				$tmp = array();
				$tmp[] = 5;
				$tmp[] = 0x01;
				$tmp[] = false;
				$tmp[] = $data_quanta[5];
				$tmp[] = "alertid".$tdid;
				$alarm_history[] = $tmp;
				$tdid++;
				$anyAlarm = true;
			}
			
			if ($data_array[10] == 0)	//Door Alarm Only Applicable if Indoor
			{
				if (($stats[5] & 0x02) == true)
				{		
					echo "<tr class='warning'><td><img src='/media/icons/ic_report_problem_black_24dp.png' title='Major Alarm'></td><td>Door</td><td id='alertid$tdid'>";
					//$FLAG_01_DOOR 	= 0x0000000002;	//Door Open     0  means  Door Close , 1 Means  Door open  
					echo "</td></tr>";
					
					$tmp = array();
					$tmp[] = 5;
					$tmp[] = 0x02;
						$tmp[] = false;
						$tmp[] = $data_quanta[5];
						$tmp[] = "alertid".$tdid;
						$alarm_history[] = $tmp;
						$tdid++;
						$anyAlarm = true;
				}
			}
			
			if (($stats[5] & 0x20) == true)
			{
				if ($data_quanta[6] == 2) { 
					echo "<tr class='info'><td><img src='/media/icons/ic_info_outline_black_24dp.png' title='Minor Alarm'></td><td>Inverter Fail</td><td id='alertid$tdid'>";
				} else {
					echo "<tr class='info'><td><img src='/media/icons/ic_info_outline_black_24dp.png' title='Minor Alarm'></td><td>Mains Fail</td><td id='alertid$tdid'>";
				}
				//$FLAG_05_MAINS 	= 0x0000000020;	//Inverter fail		1 Means main Fail,  0 Means  Main healthy
				echo "</td></tr>";
				
				$tmp = array();
				$tmp[] = 5;
				$tmp[] = 0x20;
				$tmp[] = false;
				$tmp[] = $data_quanta[5];
				$tmp[] = "alertid".$tdid;
				$alarm_history[] = $tmp;
				$tdid++;
				$anyAlarm = true;
			}
			
			if (($stats[5] & 0x40) == true)
			{
				echo "<tr class='warning'><td><img src='/media/icons/ic_report_problem_black_24dp.png' title='Major Alarm'></td><td>Site Battery Low</td><td id='alertid$tdid'>";
				//$FLAG_06_BATT 	= 0x0000000040;	//Site battery Low  1 Means Site Battery Low ,0 Means  Battery Ok
				echo "</td></tr>";
				
				$tmp = array();
				$tmp[] = 5;
				$tmp[] = 0x40;
				$tmp[] = false;
				$tmp[] = $data_quanta[5];
				$tmp[] = "alertid".$tdid;
				$alarm_history[] = $tmp;
				$tdid++;
				$anyAlarm = true;
			}
			
			if (($stats[5] & 0x80) == true) 
			{
				echo "<tr class='info'><td><img src='/media/icons/ic_info_outline_black_24dp.png' title='Minor Alarm'></td><td>Room Temperature</td><td id='alertid$tdid'>";
				//$FLAG_07_TEMP 	= 0x0000000080;	//Room Temperature  1 Means high Temperature, 0 Means Normal temp
				echo "</td></tr>";
				
				$tmp = array();
				$tmp[] = 5;
				$tmp[] = 0x80;
				$tmp[] = false;
				$tmp[] = $data_quanta[5];
				$tmp[] = "alertid".$tdid;
				$alarm_history[] = $tmp;
				$tdid++;
				$anyAlarm = true;
			}
			
			if (($stats[4] & 0x01) == true)
			{
				echo "<tr class='warning'><td><img src='/media/icons/ic_report_problem_black_24dp.png' title='Major Alarm'></td><td>LLOP</td><td id='alertid$tdid'>";
				//$FLAG_08_LLOP 	= 0x0000000100;	//LLOP
				echo "</td></tr>";
				
				$tmp = array();
				$tmp[] = 4;
				$tmp[] = 0x01;
				$tmp[] = false;
				$tmp[] = $data_quanta[5];
				$tmp[] = "alertid".$tdid;
				$alarm_history[] = $tmp;
				$tdid++;
				$anyAlarm = true;
			}
			
			if (($stats[4] & 0x02) == true)
			{
				echo "<tr class='warning'><td><img src='/media/icons/ic_report_problem_black_24dp.png' title='Major Alarm'></td><td>HCT/HWT</td><td id='alertid$tdid'>";
				//$FLAG_09_HCT 	= 0x0000000200;	//HCT/HWT
				echo "</td></tr>";
				
				$tmp = array();
				$tmp[] = 4;
				$tmp[] = 0x02;
				$tmp[] = false;
				$tmp[] = $data_quanta[5];
				$tmp[] = "alertid".$tdid;
				$alarm_history[] = $tmp;
				$tdid++;
				$anyAlarm = true;
			}
			
			if (($stats[4] & 0x04) == true)
			{
				echo "<tr class='warning'><td><img src='/media/icons/ic_report_problem_black_24dp.png' title='Major Alarm'></td><td>Alternator Fault</td><td id='alertid$tdid'>";
				//$FLAG_10_ALT 	= 0x0000000400;	//Alternator Fault
				echo "</td></tr>";
				
				$tmp = array();
				$tmp[] = 4;
				$tmp[] = 0x04;
				$tmp[] = false;
				$tmp[] = $data_quanta[5];
				$tmp[] = "alertid".$tdid;
				$alarm_history[] = $tmp;
				$tdid++;
				$anyAlarm = true;
			}
			
			if (($stats[4] & 0x08) == true)
			{
				echo "<tr class='warning'><td><img src='/media/icons/ic_report_problem_black_24dp.png' title='Major Alarm'></td><td>DG Over Speed</td><td id='alertid$tdid'>";
				//$FLAG_11_DG 	= 0x0000000800;	//DG Over Speed
				echo "</td></tr>";
				
				$tmp = array();
				$tmp[] = 4;
				$tmp[] = 0x08;
				$tmp[] = false;
				$tmp[] = $data_quanta[5];
				$tmp[] = "alertid".$tdid;
				$alarm_history[] = $tmp;
				$tdid++;
				$anyAlarm = true;
			}
			
			if (($stats[4] & 0x10) == true)
			{
				echo "<tr class='danger'><td><img src='/media/icons/ic_info_black_24dp.png' title='Critical Alarm'></td><td>DG Over Load</td><td id='alertid$tdid'>";
				//$FLAG_12_DG 	= 0x0000001000;	//DG Over Load
				echo "</td></tr>";
				
				$tmp = array();
				$tmp[] = 4;
				$tmp[] = 0x10;
				$tmp[] = false;
				$tmp[] = $data_quanta[5];
				$tmp[] = "alertid".$tdid;
				$alarm_history[] = $tmp;
				$tdid++;
				$anyAlarm = true;
			}
			
			if (($stats[4] & 0x20) == true)
			{
				echo "<tr class='danger'><td><img src='/media/icons/ic_info_black_24dp.png' title='Critical Alarm'></td><td>DG Low Fuel</td><td id='alertid$tdid'>";
				//$FLAG_13_DG 	= 0x0000002000;	//DG Low Fuel
				echo "</td></tr>";
				
				$tmp = array();
				$tmp[] = 4;
				$tmp[] = 0x20;
				$tmp[] = false;
				$tmp[] = $data_quanta[5];
				$tmp[] = "alertid".$tdid;
				$alarm_history[] = $tmp;
				$tdid++;
				$anyAlarm = true;
			}
			
			if (($stats[4] & 0x40) == true)
			{
				echo "<tr class='warning'><td><img src='/media/icons/ic_report_problem_black_24dp.png' title='Major Alarm'></td><td>DG Start Fail</td><td id='alertid$tdid'>";
				//$FLAG_14_DG 	= 0x0000004000;	//DG Start Fail
				echo "</td></tr>";
				
				$tmp = array();
				$tmp[] = 4;
				$tmp[] = 0x40;
				$tmp[] = false;
				$tmp[] = $data_quanta[5];
				$tmp[] = "alertid".$tdid;
				$alarm_history[] = $tmp;
				$tdid++;
				$anyAlarm = true;
			}
			
			if (($stats[4] & 0x80) == true) 
			{
				echo "<tr class='warning'><td><img src='/media/icons/ic_report_problem_black_24dp.png' title='Major Alarm'></td><td>DG Stop Fail</td><td id='alertid$tdid'>";
				//$FLAG_15_DG 	= 0x0000008000;	//DG Stop Fail
				echo "</td></tr>";
				
				$tmp = array();
				$tmp[] = 4;
				$tmp[] = 0x80;
				$tmp[] = false;
				$tmp[] = $data_quanta[5];
				$tmp[] = "alertid".$tdid;
				$alarm_history[] = $tmp;
				$tdid++;
				$anyAlarm = true;
			}
			
			if (($stats[3] & 0x01) == true) 
			{
				echo "<tr class='warning'><td><img src='/media/icons/ic_report_problem_black_24dp.png' title='Major Alarm'></td><td>DG Battery Low</td><td id='alertid$tdid'>";
				//$FLAG_16_DG 	= 0x0000010000;	//DG battery Low
				echo "</td></tr>";
				
				$tmp = array();
				$tmp[] = 3;
				$tmp[] = 0x01;
				$tmp[] = false;
				$tmp[] = $data_quanta[5];
				$tmp[] = "alertid".$tdid;
				$alarm_history[] = $tmp;
				$tdid++;
				$anyAlarm = true;
			}
			
			if (($stats[3] & 0x02) == true) 
			{
				echo "<tr class='danger'><td><img src='/media/icons/ic_info_black_24dp.png' title='Critical Alarm'></td><td>LCU Fail</td><td id='alertid$tdid'>";
				//$FLAG_17_LCU 	= 0x0000020000;	//LCU fail
				echo "</td></tr>";
				
				$tmp = array();
				$tmp[] = 3;
				$tmp[] = 0x02;
				$tmp[] = false;
				$tmp[] = $data_quanta[5];
				$tmp[] = "alertid".$tdid;
				$alarm_history[] = $tmp;
				$tdid++;
				$anyAlarm = true;
			}
			
			if (($stats[3] & 0x04) == true)
			{
				echo "<tr class='warning'><td><img src='/media/icons/ic_report_problem_black_24dp.png' title='Major Alarm'></td><td>Rectifier Fail</td><td id='alertid$tdid'>";
				//$FLAG_18_RECT 	= 0x0000040000;	//Rectifier Fail
				echo "</td></tr>";
				
				$tmp = array();
				$tmp[] = 3;
				$tmp[] = 0x04;
				$tmp[] = false;
				$tmp[] = $data_quanta[5];
				$tmp[] = "alertid".$tdid;
				$alarm_history[] = $tmp;
				$tdid++;
				$anyAlarm = true;
			}
			
			if (($stats[3] & 0x08) == true)
			{
				echo "<tr class='warning'><td><img src='/media/icons/ic_report_problem_black_24dp.png' title='Major Alarm'></td><td>Multi Rectifier Fail</td><td id='alertid$tdid'>";
				//$FLAG_19_RECT 	= 0x0000080000;	//Multi Rectifier Fail
				echo "</td></tr>";
				
				$tmp = array();
				$tmp[] = 3;
				$tmp[] = 0x08;
				$tmp[] = false;
				$tmp[] = $data_quanta[5];
				$tmp[] = "alertid".$tdid;
				$alarm_history[] = $tmp;
				$tdid++;
				$anyAlarm = true;
			}
			
			if (($stats[3] & 0x10) == true)
			{
				echo "<tr class='danger'><td><img src='/media/icons/ic_info_black_24dp.png' title='Critical Alarm'></td><td>LVD Trip</td><td id='alertid$tdid'>";
				//$FLAG_20_LVD 	= 0x0000100000;	//LVD TRIP
				echo "</td></tr>";
				
				$tmp = array();
				$tmp[] = 3;
				$tmp[] = 0x10;
				$tmp[] = false;
				$tmp[] = $data_quanta[5];
				$tmp[] = "alertid".$tdid;
				$alarm_history[] = $tmp;
				$tdid++;
				$anyAlarm = true;
			}
			
			if (($stats[3] & 0x20) == true)
			{
				echo "<tr class='info'><td><img src='/media/icons/ic_info_outline_black_24dp.png' title='Minor Alarm'></td><td>LVD BY PASS</td><td id='alertid$tdid'>";
				//$FLAG_21_LVD 	= 0x0000200000;	//LVD BY pass
				echo "</td></tr>";
				
				$tmp = array();
				$tmp[] = 3;
				$tmp[] = 0x20;
				$tmp[] = false;
				$tmp[] = $data_quanta[5];
				$tmp[] = "alertid".$tdid;
				$alarm_history[] = $tmp;
				$tdid++;
				$anyAlarm = true;
			}
			
			//$FLAG_22_RES 	= 0x0000400000;	//Reserved
			//$FLAG_23_RES 	= 0x0000800000;	//Reserved
			//$FLAG_24_RES 	= 0x0001000000;	//Reserved
			//$FLAG_25_RES 	= 0x0002000000;	//Reserved
			//$FLAG_26_RES 	= 0x0004000000;	//Reserved
			//$FLAG_27_RES 	= 0x0008000000;	//Reserved
			//$FLAG_28_RES 	= 0x0010000000;	//Reserved
			//$FLAG_29_RES 	= 0x0020000000;	//Reserved
			//$FLAG_30_RES 	= 0x0040000000;	//Reserved
			//$FLAG_31_RES 	= 0x0080000000;	//Reserved
			
			
			
			if (($stats[1] & 0x20) == true)	//?????TODO
			{
				echo "<tr class='info'><td><img src='/media/icons/ic_info_outline_black_24dp.png' title='Minor Alarm'></td><td>DG STOP  Normally</td><td id='alertid$tdid'>";
				//$FLAG_37_DG 	= 0x2000000000;	//DG STOP Normally
				echo "</td></tr>";
				
				$tmp = array();
				$tmp[] = 1;
				$tmp[] = 0x20;
				$tmp[] = false;
				$tmp[] = $data_quanta[5];
				$tmp[] = "alertid".$tdid;
				$alarm_history[] = $tmp;
				$tdid++;
				$anyAlarm = true;
			}
			
			if (($stats[1] & 0x40) == true)
			{
				echo "<tr class='warning'><td><img src='/media/icons/ic_report_problem_black_24dp.png' title='Major Alarm'></td><td>DG STOP due to Fault</td><td id='alertid$tdid'>";
				//$FLAG_38_DG 	= 0x8000000000;	//DG STOP due to Fault
				echo "</td></tr>";
				
				$tmp = array();
				$tmp[] = 1;
				$tmp[] = 0x40;
				$tmp[] = false;
				$tmp[] = $data_quanta[5];
				$tmp[] = "alertid".$tdid;
				$alarm_history[] = $tmp;
				$tdid++;
				$anyAlarm = true;
			}
			
			if (($stats[1] & 0x80) == true) 
			{
				echo "<tr class='info'><td><img src='/media/icons/ic_info_outline_black_24dp.png' title='Minor Alarm'></td><td>DG Stop Due to Maximum Run Expiry</td><td id='alertid$tdid'>";
				//$FLAG_39_DG 	= 0x8000000000;	//DG Stop Due to Maximum Run Exipary 
				echo "</td></tr>";
				
				$tmp = array();
				$tmp[] = 1;
				$tmp[] = 0x80;
				$tmp[] = false;
				$tmp[] = $data_quanta[5];
				$tmp[] = "alertid".$tdid;
				$alarm_history[] = $tmp;
				$tdid++;
				$anyAlarm = true;
			}
			
		}
		
		
		
	?>
		</tbody>
	</table>
	

	<?php 
		if (!$anyAlarm) 
		{
			echo "<p class='bg-success'>&nbsp;&nbsp;No Alarm</p>"; 
		}
		else
		{
			echo "<p class='bg-danger'>&nbsp;&nbsp;$tdid Alarm(s)</p>";
		}
	
	?> 
	
	
  </div>
</div>
</div>



<?php
	
	$result = GetAlarmHistory($data_array[0], $alarm_history);
	//var_dump($result);
	
	echo '<script>';
	for ($i=0; $i<count($result); $i++)
	{
		if ($result[$i][2] == false)	//last date not found
		{
			$result[$i][3] = $result[$i][3]." *";
		}
		
		echo '$("#'.$result[$i][4].'").html("'.$result[$i][3].'");';
		
	
	}
	echo '</script>';


?>
