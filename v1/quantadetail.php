<?php
session_start();
if (!isset($_SESSION['login_user'])) {
	header("Location: login.php");
}

$id = $_GET['id'];
$indoor_type = $_GET['indoor_type'];
?>

<h5>Record #<?php echo $id; ?></h5>

<?php

include 'dbinc.php';

mysql_connect($mysql_hostname, $mysql_user, $mysql_password); //database connection
mysql_select_db($mysql_database);

$order = "SELECT * FROM quanta where id=" . $id;
//order to search data
//declare in the order variable

$result = mysql_query($order);
//order executes the result is saved
//in the variable of $result



$data = mysql_fetch_row($result);


$data_id 		= $data[0];		//Unique Id
$data_timestamp = $data[1];		//Time Stamp by Server
$data_validated	= $data[2];		//Validated by Server

$data_type 		= $data[3];		//Type 0 - Periodic / 1 - Fault
$data_type_str = "Unknown";
if ($data[3] == 0) $data_type_str = "Periodic";
if ($data[3] == 1) $data_type_str = "Fault";

$data_site_id	= $data[4];		//String Site Id
$data_datetime	= $data[5];		//Date Time by Device
$data_device_id	= $data[6];		//Device Id/ Only 1, 2 & 32 known for now
$data_status	= $data[7];		//Status Bits - To Expand
$data_status_hex = bin2hex($data_status);

$data_raw		= $data[8];		//Raw Data, for device specific information


if ($data_device_id == 1) {
	echo ("<table class='table table-striped'>");

	echo ("<tr><td>Type</td><td>$data_type_str</td></tr>
				<tr><td>Site Id</td><td>$data_site_id</td></tr>
				<tr><td>Date Time</td><td>$data_datetime</td></tr>
				<tr><td>Device Id</td><td>$data_device_id</td></tr>");

	//echo ("Status: <b>$data_status</b><br>");
	//echo ("Status: <b>$data_status_hex</b></tr>");

	//$tmp = gettype($data_status);
	//echo "Var: $tmp<br>";
	//var_dump($data_status);
	$stats = unpack("C*", $data_status);
	//		var_dump($stats);
	//		var_dump($stats[5] & 0x01);
	//		var_dump($stats[5] & 0x02);
	//		var_dump($stats[5] & 0x04);
	//		var_dump($stats[5] & 0x08);
	//		var_dump($stats[5] & 0x10);
	//		var_dump($stats[5] & 0x20);
	//		var_dump($stats[5] & 0x40);
	//		var_dump($stats[5] & 0x80);

	if ($data_validated != 2) {

		echo "<tr><td>Smoke Fire Alarm</td><td";
		//0x0000000000 Smoke fire    0 means No alarm,  1 means Alarm
		if (($stats[5] & 0x01) == false)
			echo " ><img id='led_white' src='/media/img_trans.gif'>";

		else
			echo " ><img id='led_red' src='/media/img_trans.gif'>";

		echo "</td></tr>";


		if ($indoor_type == 0)	//Door Alarm Only Applicable if Indoor
		{
			echo "<tr><td>Door</td><td";
			//$FLAG_01_DOOR 	= 0x0000000002;	//Door Open     0  means  Door Close , 1 Means  Door open  
			if (($stats[5] & 0x02) == false)
				echo "  ><img id='led_white' src='/media/img_trans.gif'><br>";
			else
				echo "  ><img id='led_red' src='/media/img_trans.gif'>";

			echo "</td></tr>";
		}


		echo "<tr><td>Mode</td><td";
		//$FLAG_02_AUTO 	= 0x0000000004;	//Auto/Man Mode 0 Means  Auto Mode, 1 Means  Man Mode
		if (($stats[5] & 0x04) == false)
			echo "  >Auto";
		else
			echo "  >Manual";
		echo "</td></tr>";

		echo "<tr><td>Load</td><td>";
		//$FLAG_034_LOAD 	= 0x0000000018;	//00    :Load on EB,01: Load on DG, 10: Load on site Battery   11: Not used
		if (($stats[5] & 0x08) == false) {
			if (($stats[5] & 0x10) == false)
				echo "[- Load on EB -]<br>";				//00
			else
				echo "[- Load on site Battery -]<br>";	//10
		} else {
			if (($stats[5] & 0x10) == false)
				echo "[- Load on DG -]<br>";				//01
			else
				echo "[- Not Used -]<br>";	//11
		}
		echo "</td></tr>";

		echo "<tr><td>Mains Fail</td><td";
		//$FLAG_05_MAINS 	= 0x0000000020;	//Mains fail	1 Means main Fail,  0 Means  Main healthy
		if (($stats[5] & 0x20) == false)
			echo "  ><img id='led_white' src='/media/img_trans.gif'>";
		else
			echo "  ><img id='led_red' src='/media/img_trans.gif'>";
		echo "</td></tr>";

		echo "<tr><td>Site Battery Low</td><td";
		//$FLAG_06_BATT 	= 0x0000000040;	//Site battery Low  1 Means Site Battery Low ,0 Means  Battery Ok
		if (($stats[5] & 0x40) == false)
			echo "  ><img id='led_white' src='/media/img_trans.gif'>";
		else
			echo "  ><img id='led_red' src='/media/img_trans.gif'>";
		echo "</td></tr>";


		echo "<tr><td>Room Temperature</td><td";
		//$FLAG_07_TEMP 	= 0x0000000080;	//Room Temperature  1 Means high Temperature, 0 Means Normal temp
		if (($stats[5] & 0x80) == false)
			echo "  ><img id='led_white' src='/media/img_trans.gif'>";
		else
			echo "  ><img id='led_red' src='/media/img_trans.gif'>";
		echo "</td></tr>";


		echo "<tr><td>LLOP</td><td";
		//$FLAG_08_LLOP 	= 0x0000000100;	//LLOP
		if (($stats[4] & 0x01) == false)
			echo "  ><img id='led_white' src='/media/img_trans.gif'>";
		else
			echo "  ><img id='led_red' src='/media/img_trans.gif'>";
		echo "</td></tr>";


		echo "<tr><td>HCT/HWT</td><td";
		//$FLAG_09_HCT 	= 0x0000000200;	//HCT/HWT
		if (($stats[4] & 0x02) == false)
			echo "  ><img id='led_white' src='/media/img_trans.gif'>";
		else
			echo "  ><img id='led_red' src='/media/img_trans.gif'>";
		echo "</td></tr>";


		echo "<tr><td>Alternator Fault</td><td";
		//$FLAG_10_ALT 	= 0x0000000400;	//Alternator Fault
		if (($stats[4] & 0x04) == false)
			echo "  ><img id='led_white' src='/media/img_trans.gif'>";
		else
			echo "  ><img id='led_red' src='/media/img_trans.gif'>";
		echo "</td></tr>";


		echo "<tr><td>DG Over Speed</td><td";
		//$FLAG_11_DG 	= 0x0000000800;	//DG Over Speed
		if (($stats[4] & 0x08) == false)
			echo "  ><img id='led_white' src='/media/img_trans.gif'>";
		else
			echo "  ><img id='led_red' src='/media/img_trans.gif'>";
		echo "</td></tr>";


		echo "<tr><td>DG Over Load</td><td";
		//$FLAG_12_DG 	= 0x0000001000;	//DG Over Load
		if (($stats[4] & 0x10) == false)
			echo "  ><img id='led_white' src='/media/img_trans.gif'>";
		else
			echo "  ><img id='led_red' src='/media/img_trans.gif'>";
		echo "</td></tr>";


		echo "<tr><td>DG Low Fuel</td><td";
		//$FLAG_13_DG 	= 0x0000002000;	//DG Low Fuel
		if (($stats[4] & 0x20) == false)
			echo "  ><img id='led_white' src='/media/img_trans.gif'>";
		else
			echo "  ><img id='led_red' src='/media/img_trans.gif'>";
		echo "</td></tr>";


		echo "<tr><td>DG Start Fail</td><td";
		//$FLAG_14_DG 	= 0x0000004000;	//DG Start Fail
		if (($stats[4] & 0x40) == false)
			echo "  ><img id='led_white' src='/media/img_trans.gif'>";
		else
			echo "  ><img id='led_red' src='/media/img_trans.gif'>";
		echo "</td></tr>";


		echo "<tr><td>DG Stop Fail</td><td";
		//$FLAG_15_DG 	= 0x0000008000;	//DG Stop Fail
		if (($stats[4] & 0x80) == false)
			echo "  ><img id='led_white' src='/media/img_trans.gif'>";
		else
			echo "  ><img id='led_red' src='/media/img_trans.gif'>";
		echo "</td></tr>";


		echo "<tr><td>DG Battery</td><td";
		//$FLAG_16_DG 	= 0x0000010000;	//DG battery Low
		if (($stats[3] & 0x01) == false)
			echo "  ><img id='led_white' src='/media/img_trans.gif'>";
		else
			echo "  ><img id='led_red' src='/media/img_trans.gif'>";
		echo "</td></tr>";


		echo "<tr><td>LCU Fail</td><td";
		//$FLAG_17_LCU 	= 0x0000020000;	//LCU fail
		if (($stats[3] & 0x02) == false)
			echo "  ><img id='led_green' src='/media/img_trans.gif'>";
		else
			echo "  ><img id='led_red' src='/media/img_trans.gif'>";
		echo "</td></tr>";


		echo "<tr><td>Rectifier Fail</td><td";
		//$FLAG_18_RECT 	= 0x0000040000;	//Rectifier Fail
		if (($stats[3] & 0x04) == false)
			echo "  ><img id='led_green' src='/media/img_trans.gif'>";
		else
			echo "  ><img id='led_red' src='/media/img_trans.gif'>";
		echo "</td></tr>";


		echo "<tr><td>Multi Rectifier Fail</td><td";
		//$FLAG_19_RECT 	= 0x0000080000;	//Multi Rectifier Fail
		if (($stats[3] & 0x08) == false)
			echo "  ><img id='led_green' src='/media/img_trans.gif'>";
		else
			echo "  ><img id='led_red' src='/media/img_trans.gif'>";
		echo "</td></tr>";


		echo "<tr><td>LVD Trip</td><td";
		//$FLAG_20_LVD 	= 0x0000100000;	//LVD TRIP
		if (($stats[3] & 0x10) == false)
			echo "  ><img id='led_green' src='/media/img_trans.gif'>";
		else
			echo "  ><img id='led_red' src='/media/img_trans.gif'>";
		echo "</td></tr>";


		echo "<tr><td>LVD BY PASS</td><td";
		//$FLAG_21_LVD 	= 0x0000200000;	//LVD BY pass
		if (($stats[3] & 0x20) == false)
			echo "  ><img id='led_green' src='/media/img_trans.gif'>";
		else
			echo "  ><img id='led_red' src='/media/img_trans.gif'>";
		echo "</td></tr>";


		//$FLAG_22_RES 	= 0x0000400000;	//Reserved
		//$FLAG_23_RES 	= 0x0000800000;	//Reserved
		//$FLAG_24_RES 	= 0x0001000000;	//Reserved
		//$FLAG_25_RES 	= 0x0002000000;	//Reserved
		//$FLAG_26_RES 	= 0x0004000000;	//Reserved
		//$FLAG_27_RES 	= 0x0008000000;	//Reserved
		//$FLAG_28_RES 	= 0x0010000000;	//Reserved
		//$FLAG_29_RES 	= 0x0020000000;	//Reserved
		//$FLAG_30_RES 	= 0x0040000000;	//Reserved
		//$FLAG_31_RES 	= 0x0080000000;	//Reserved

		echo "<tr><td>DG OFF</td><td";
		//$FLAG_32_DG 	= 0x0100000000;	//DG OFF
		if (($stats[1] & 0x01) == false)
			echo "  ><img id='led_white' src='/media/img_trans.gif'>";
		else
			echo "  ><img id='led_green' src='/media/img_trans.gif'>";
		echo "</td></tr>";


		echo "<tr><td>DG ON</td><td";
		//$FLAG_33_DG 	= 0x0200000000;	//DG ON
		if (($stats[1] & 0x02) == false)
			echo "  ><img id='led_white' src='/media/img_trans.gif'>";
		else
			echo "  ><img id='led_green' src='/media/img_trans.gif'>";
		echo "</td></tr>";


		echo "<tr><td>DG Cranking</td><td";
		//$FLAG_34_DG 	= 0x0400000000;	//DG Cranking
		if (($stats[1] & 0x04) == false)
			echo "  ><img id='led_white' src='/media/img_trans.gif'>";
		else
			echo "  ><img id='led_green' src='/media/img_trans.gif'>";
		echo "</td></tr>";


		echo "<tr><td>DG Start in Progress</td><td";
		//$FLAG_35_DG 	= 0x0800000000;	//DG Start in Progress
		if (($stats[1] & 0x08) == false)
			echo "  ><img id='led_white' src='/media/img_trans.gif'>";
		else
			echo "  ><img id='led_green' src='/media/img_trans.gif'>";
		echo "</td></tr>";


		echo "<tr><td>DG Cool Down (Idle Running)</td><td";
		//$FLAG_36_DG 	= 0x1000000000;	//DG Cool Down (Idle Running)
		if (($stats[1] & 0x10) == false)
			echo "  ><img id='led_white' src='/media/img_trans.gif'>";
		else
			echo "  ><img id='led_green' src='/media/img_trans.gif'>";
		echo "</td></tr>";


		echo "<tr><td>DG STOP  Normally</td><td";
		//$FLAG_37_DG 	= 0x2000000000;	//DG STOP  Normally
		if (($stats[1] & 0x20) == false)
			echo "  ><img id='led_white' src='/media/img_trans.gif'>";
		else
			echo "  ><img id='led_green' src='/media/img_trans.gif'>";
		echo "</td></tr>";


		echo "<tr><td>DG STOP due to Fault</td><td";
		//$FLAG_38_DG 	= 0x8000000000;	//DG STOP due to Fault
		if (($stats[1] & 0x40) == false)
			echo "  ><img id='led_white' src='/media/img_trans.gif'>";
		else
			echo "  ><img id='led_green' src='/media/img_trans.gif'>";
		echo "</td></tr>";


		echo "<tr><td>DG Stop Due to Maximum Run Expiry</td><td";
		//$FLAG_39_DG 	= 0x8000000000;	//DG Stop Due to Maximum Run Exipary 
		if (($stats[1] & 0x80) == false)
			echo "  ><img id='led_white' src='/media/img_trans.gif'>";
		else
			echo "  ><img id='led_green' src='/media/img_trans.gif'>";
		echo "</td></tr>";
	} else {
		//Double DG

		echo "<tr><td>Smoke Fire Alarm</td><td";
		//0x0000000000 Smoke fire    0 means No alarm,  1 means Alarm
		if (($stats[6] & 0x01) == false)
			echo " ><img id='led_white' src='/media/img_trans.gif'>";
		else
			echo " ><img id='led_red' src='/media/img_trans.gif'>";

		echo "</td></tr>";

		echo "<tr><td>Door</td><td";
		//$FLAG_01_DOOR 	= 0x0000000002;	//Door Open     0  means  Door Close , 1 Means  Door open  
		if (($stats[6] & 0x02) == false)
			echo "  ><img id='led_white' src='/media/img_trans.gif'><br>";
		else
			echo "  ><img id='led_red' src='/media/img_trans.gif'>";

		echo "</td></tr>";


		echo "<tr><td>Mode</td><td";
		//$FLAG_02_AUTO 	= 0x0000000004;	//Auto/Man Mode 0 Means  Auto Mode, 1 Means  Man Mode
		if (($stats[6] & 0x04) == false)
			echo "  >Auto";
		else
			echo "  >Manual";
		echo "</td></tr>";

		echo "<tr><td>Load</td><td>";
		//$FLAG_034_LOAD 	= 0x0000000018;	
		//000    :Load on EB,001: Load on DG1, 011: Load on site Battery   010: Load on DG2, 111 ( site in battery with emg case) other’s Not used

		$sub_data = ($stats[6] & 0x38) / 8;

		if ($sub_data == 0)
			echo "[- Load on EB -]<br>";			//000
		else if ($sub_data == 1)
			echo "[- Load on DG 1 -]<br>";			//001
		else if ($sub_data == 2)
			echo "[- Load on DG 2 -]<br>";			//010
		else if ($sub_data == 3)
			echo "[- Load on site Battery -]<br>";	//011
		else if ($sub_data == 7)
			echo "[- Load on site Battery (emg case) -]<br>";	//111
		else
			echo "[- Not Used -]<br>";				//remaining

		echo "</td></tr>";

		echo "<tr><td>Mains Fail</td><td";
		//$FLAG_05_MAINS 	= 0x0000000020;	//Mains fail	1 Means main Fail,  0 Means  Main healthy
		if (($stats[6] & 0x40) == false)
			echo "  ><img id='led_white' src='/media/img_trans.gif'>";
		else
			echo "  ><img id='led_red' src='/media/img_trans.gif'>";
		echo "</td></tr>";

		echo "<tr><td>Site Battery Low</td><td";
		//$FLAG_06_BATT 	= 0x0000000040;	//Site battery Low  1 Means Site Battery Low ,0 Means  Battery Ok
		if (($stats[6] & 0x80) == false)
			echo "  ><img id='led_white' src='/media/img_trans.gif'>";
		else
			echo "  ><img id='led_red' src='/media/img_trans.gif'>";
		echo "</td></tr>";


		echo "<tr><td>Room Temperature</td><td";
		//$FLAG_07_TEMP 	= 0x0000000080;	//Room Temperature  1 Means high Temperature, 0 Means Normal temp
		if (($stats[5] & 0x01) == false)
			echo "  ><img id='led_white' src='/media/img_trans.gif'>";
		else
			echo "  ><img id='led_red' src='/media/img_trans.gif'>";
		echo "</td></tr>";


		echo "<tr><td>DG 1 LLOP</td><td";
		//$FLAG_08_LLOP 	= 0x0000000100;	//LLOP
		if (($stats[5] & 0x02) == false)
			echo "  ><img id='led_white' src='/media/img_trans.gif'>";
		else
			echo "  ><img id='led_red' src='/media/img_trans.gif'>";
		echo "</td></tr>";


		echo "<tr><td>DG 1 HCT/HWT</td><td";
		//$FLAG_09_HCT 	= 0x0000000200;	//HCT/HWT
		if (($stats[5] & 0x04) == false)
			echo "  ><img id='led_white' src='/media/img_trans.gif'>";
		else
			echo "  ><img id='led_red' src='/media/img_trans.gif'>";
		echo "</td></tr>";


		echo "<tr><td>DG 1 Alternate Fault</td><td";
		//$FLAG_10_ALT 	= 0x0000000400;	//Alternate Fault
		if (($stats[5] & 0x08) == false)
			echo "  ><img id='led_white' src='/media/img_trans.gif'>";
		else
			echo "  ><img id='led_red' src='/media/img_trans.gif'>";
		echo "</td></tr>";


		echo "<tr><td>DG 1 Over Speed</td><td";
		//$FLAG_11_DG 	= 0x0000000800;	//DG Over Speed
		if (($stats[5] & 0x10) == false)
			echo "  ><img id='led_white' src='/media/img_trans.gif'>";
		else
			echo "  ><img id='led_red' src='/media/img_trans.gif'>";
		echo "</td></tr>";


		echo "<tr><td>DG 1 Over Load</td><td";
		//$FLAG_12_DG 	= 0x0000001000;	//DG Over Load
		if (($stats[5] & 0x20) == false)
			echo "  ><img id='led_white' src='/media/img_trans.gif'>";
		else
			echo "  ><img id='led_red' src='/media/img_trans.gif'>";
		echo "</td></tr>";


		echo "<tr><td>DG 1 Low Fuel</td><td";
		//$FLAG_13_DG 	= 0x0000002000;	//DG Low Fuel
		if (($stats[5] & 0x40) == false)
			echo "  ><img id='led_white' src='/media/img_trans.gif'>";
		else
			echo "  ><img id='led_red' src='/media/img_trans.gif'>";
		echo "</td></tr>";


		echo "<tr><td>DG 1 Start Fail</td><td";
		//$FLAG_14_DG 	= 0x0000004000;	//DG Start Fail
		if (($stats[5] & 0x80) == false)
			echo "  ><img id='led_white' src='/media/img_trans.gif'>";
		else
			echo "  ><img id='led_red' src='/media/img_trans.gif'>";
		echo "</td></tr>";


		echo "<tr><td>DG 1 Stop Fail</td><td";
		//$FLAG_15_DG 	= 0x0000008000;	//DG Stop Fail
		if (($stats[4] & 0x01) == false)
			echo "  ><img id='led_white' src='/media/img_trans.gif'>";
		else
			echo "  ><img id='led_red' src='/media/img_trans.gif'>";
		echo "</td></tr>";


		echo "<tr><td>DG 1 Battery</td><td";
		//$FLAG_16_DG 	= 0x0000010000;	//DG battery Low
		if (($stats[4] & 0x02) == false)
			echo "  ><img id='led_white' src='/media/img_trans.gif'>";
		else
			echo "  ><img id='led_red' src='/media/img_trans.gif'>";
		echo "</td></tr>";


		echo "<tr><td>LCU Fail</td><td";
		//$FLAG_17_LCU 	= 0x0000020000;	//LCU fail
		if (($stats[4] & 0x04) == false)
			echo "  ><img id='led_green' src='/media/img_trans.gif'>";
		else
			echo "  ><img id='led_red' src='/media/img_trans.gif'>";
		echo "</td></tr>";


		echo "<tr><td>Rectifier Fail</td><td";
		//$FLAG_18_RECT 	= 0x0000040000;	//Rectifier Fail
		if (($stats[4] & 0x08) == false)
			echo "  ><img id='led_green' src='/media/img_trans.gif'>";
		else
			echo "  ><img id='led_red' src='/media/img_trans.gif'>";
		echo "</td></tr>";


		echo "<tr><td>Multi Rectifier Fail</td><td";
		//$FLAG_19_RECT 	= 0x0000080000;	//Multi Rectifier Fail
		if (($stats[4] & 0x10) == false)
			echo "  ><img id='led_green' src='/media/img_trans.gif'>";
		else
			echo "  ><img id='led_red' src='/media/img_trans.gif'>";
		echo "</td></tr>";


		echo "<tr><td>LVD Trip</td><td";
		//$FLAG_20_LVD 	= 0x0000100000;	//LVD TRIP
		if (($stats[4] & 0x20) == false)
			echo "  ><img id='led_green' src='/media/img_trans.gif'>";
		else
			echo "  ><img id='led_red' src='/media/img_trans.gif'>";
		echo "</td></tr>";


		echo "<tr><td>LVD BY PASS</td><td";
		//$FLAG_21_LVD 	= 0x0000200000;	//LVD BY pass
		if (($stats[4] & 0x40) == false)
			echo "  ><img id='led_green' src='/media/img_trans.gif'>";
		else
			echo "  ><img id='led_red' src='/media/img_trans.gif'>";
		echo "</td></tr>";

		////////////////////////////
		////////////////////////////
		////////////////////////////

		echo "<tr><td>DG 2 LLOP</td><td";
		//$FLAG_08_LLOP 	= 0x0000000100;	//LLOP
		if (($stats[4] & 0x80) == false)
			echo "  ><img id='led_white' src='/media/img_trans.gif'>";
		else
			echo "  ><img id='led_red' src='/media/img_trans.gif'>";
		echo "</td></tr>";


		echo "<tr><td>DG 2 HCT/HWT</td><td";
		//$FLAG_09_HCT 	= 0x0000000200;	//HCT/HWT
		if (($stats[3] & 0x01) == false)
			echo "  ><img id='led_white' src='/media/img_trans.gif'>";
		else
			echo "  ><img id='led_red' src='/media/img_trans.gif'>";
		echo "</td></tr>";


		echo "<tr><td>DG 2 Alternate Fault</td><td";
		//$FLAG_10_ALT 	= 0x0000000400;	//Alternate Fault
		if (($stats[3] & 0x02) == false)
			echo "  ><img id='led_white' src='/media/img_trans.gif'>";
		else
			echo "  ><img id='led_red' src='/media/img_trans.gif'>";
		echo "</td></tr>";


		echo "<tr><td>DG 2 Over Speed</td><td";
		//$FLAG_11_DG 	= 0x0000000800;	//DG Over Speed
		if (($stats[3] & 0x04) == false)
			echo "  ><img id='led_white' src='/media/img_trans.gif'>";
		else
			echo "  ><img id='led_red' src='/media/img_trans.gif'>";
		echo "</td></tr>";


		echo "<tr><td>DG 2 Over Load</td><td";
		//$FLAG_12_DG 	= 0x0000001000;	//DG Over Load
		if (($stats[3] & 0x08) == false)
			echo "  ><img id='led_white' src='/media/img_trans.gif'>";
		else
			echo "  ><img id='led_red' src='/media/img_trans.gif'>";
		echo "</td></tr>";


		echo "<tr><td>DG 2 Low Fuel</td><td";
		//$FLAG_13_DG 	= 0x0000002000;	//DG Low Fuel
		if (($stats[3] & 0x10) == false)
			echo "  ><img id='led_white' src='/media/img_trans.gif'>";
		else
			echo "  ><img id='led_red' src='/media/img_trans.gif'>";
		echo "</td></tr>";


		echo "<tr><td>DG 2 Start Fail</td><td";
		//$FLAG_14_DG 	= 0x0000004000;	//DG Start Fail
		if (($stats[3] & 0x20) == false)
			echo "  ><img id='led_white' src='/media/img_trans.gif'>";
		else
			echo "  ><img id='led_red' src='/media/img_trans.gif'>";
		echo "</td></tr>";


		echo "<tr><td>DG 2 Stop Fail</td><td";
		//$FLAG_15_DG 	= 0x0000008000;	//DG Stop Fail
		if (($stats[3] & 0x40) == false)
			echo "  ><img id='led_white' src='/media/img_trans.gif'>";
		else
			echo "  ><img id='led_red' src='/media/img_trans.gif'>";
		echo "</td></tr>";


		echo "<tr><td>DG 2 Battery</td><td";
		//$FLAG_16_DG 	= 0x0000010000;	//DG battery Low
		if (($stats[3] & 0x80) == false)
			echo "  ><img id='led_white' src='/media/img_trans.gif'>";
		else
			echo "  ><img id='led_red' src='/media/img_trans.gif'>";
		echo "</td></tr>";


		echo "<tr><td>DG 1 OFF</td><td";
		//$FLAG_32_DG 	= 0x0100000000;	//DG OFF
		if (($stats[2] & 0x01) == false)
			echo "  ><img id='led_white' src='/media/img_trans.gif'>";
		else
			echo "  ><img id='led_green' src='/media/img_trans.gif'>";
		echo "</td></tr>";


		echo "<tr><td>DG 1 ON</td><td";
		//$FLAG_33_DG 	= 0x0200000000;	//DG ON
		if (($stats[2] & 0x02) == false)
			echo "  ><img id='led_white' src='/media/img_trans.gif'>";
		else
			echo "  ><img id='led_green' src='/media/img_trans.gif'>";
		echo "</td></tr>";


		echo "<tr><td>DG 1 Cranking</td><td";
		//$FLAG_34_DG 	= 0x0400000000;	//DG Cranking
		if (($stats[2] & 0x04) == false)
			echo "  ><img id='led_white' src='/media/img_trans.gif'>";
		else
			echo "  ><img id='led_green' src='/media/img_trans.gif'>";
		echo "</td></tr>";


		echo "<tr><td>DG 1 Start in Progress</td><td";
		//$FLAG_35_DG 	= 0x0800000000;	//DG Start in Progress
		if (($stats[2] & 0x08) == false)
			echo "  ><img id='led_white' src='/media/img_trans.gif'>";
		else
			echo "  ><img id='led_green' src='/media/img_trans.gif'>";
		echo "</td></tr>";


		echo "<tr><td>DG 1 Cool Down (Idle Running)</td><td";
		//$FLAG_36_DG 	= 0x1000000000;	//DG Cool Down (Idle Running)
		if (($stats[2] & 0x10) == false)
			echo "  ><img id='led_white' src='/media/img_trans.gif'>";
		else
			echo "  ><img id='led_green' src='/media/img_trans.gif'>";
		echo "</td></tr>";


		echo "<tr><td>DG 1 STOP  Normally</td><td";
		//$FLAG_37_DG 	= 0x2000000000;	//DG STOP  Normally
		if (($stats[2] & 0x20) == false)
			echo "  ><img id='led_white' src='/media/img_trans.gif'>";
		else
			echo "  ><img id='led_green' src='/media/img_trans.gif'>";
		echo "</td></tr>";


		echo "<tr><td>DG 1 STOP due to Fault</td><td";
		//$FLAG_38_DG 	= 0x8000000000;	//DG STOP due to Fault
		if (($stats[2] & 0x40) == false)
			echo "  ><img id='led_white' src='/media/img_trans.gif'>";
		else
			echo "  ><img id='led_green' src='/media/img_trans.gif'>";
		echo "</td></tr>";


		echo "<tr><td>DG 1 Stop Due to Maximum Run Expiry</td><td";
		//$FLAG_39_DG 	= 0x8000000000;	//DG Stop Due to Maximum Run Exipary 
		if (($stats[2] & 0x80) == false)
			echo "  ><img id='led_white' src='/media/img_trans.gif'>";
		else
			echo "  ><img id='led_green' src='/media/img_trans.gif'>";
		echo "</td></tr>";


		///
		echo "<tr><td>DG 2 OFF</td><td";
		//$FLAG_32_DG 	= 0x0100000000;	//DG OFF
		if (($stats[1] & 0x01) == false)
			echo "  ><img id='led_white' src='/media/img_trans.gif'>";
		else
			echo "  ><img id='led_green' src='/media/img_trans.gif'>";
		echo "</td></tr>";


		echo "<tr><td>DG 2 ON</td><td";
		//$FLAG_33_DG 	= 0x0200000000;	//DG ON
		if (($stats[1] & 0x02) == false)
			echo "  ><img id='led_white' src='/media/img_trans.gif'>";
		else
			echo "  ><img id='led_green' src='/media/img_trans.gif'>";
		echo "</td></tr>";


		echo "<tr><td>DG 2 Cranking</td><td";
		//$FLAG_34_DG 	= 0x0400000000;	//DG Cranking
		if (($stats[1] & 0x04) == false)
			echo "  ><img id='led_white' src='/media/img_trans.gif'>";
		else
			echo "  ><img id='led_green' src='/media/img_trans.gif'>";
		echo "</td></tr>";


		echo "<tr><td>DG 2 Start in Progress</td><td";
		//$FLAG_35_DG 	= 0x0800000000;	//DG Start in Progress
		if (($stats[1] & 0x08) == false)
			echo "  ><img id='led_white' src='/media/img_trans.gif'>";
		else
			echo "  ><img id='led_green' src='/media/img_trans.gif'>";
		echo "</td></tr>";


		echo "<tr><td>DG 2 Cool Down (Idle Running)</td><td";
		//$FLAG_36_DG 	= 0x1000000000;	//DG Cool Down (Idle Running)
		if (($stats[1] & 0x10) == false)
			echo "  ><img id='led_white' src='/media/img_trans.gif'>";
		else
			echo "  ><img id='led_green' src='/media/img_trans.gif'>";
		echo "</td></tr>";


		echo "<tr><td>DG 2 STOP  Normally</td><td";
		//$FLAG_37_DG 	= 0x2000000000;	//DG STOP  Normally
		if (($stats[1] & 0x20) == false)
			echo "  ><img id='led_white' src='/media/img_trans.gif'>";
		else
			echo "  ><img id='led_green' src='/media/img_trans.gif'>";
		echo "</td></tr>";


		echo "<tr><td>DG 2 STOP due to Fault</td><td";
		//$FLAG_38_DG 	= 0x8000000000;	//DG STOP due to Fault
		if (($stats[1] & 0x40) == false)
			echo "  ><img id='led_white' src='/media/img_trans.gif'>";
		else
			echo "  ><img id='led_green' src='/media/img_trans.gif'>";
		echo "</td></tr>";


		echo "<tr><td>DG 2 Stop Due to Maximum Run Expiry</td><td";
		//$FLAG_39_DG 	= 0x8000000000;	//DG Stop Due to Maximum Run Expiry 
		if (($stats[1] & 0x80) == false)
			echo "  ><img id='led_white' src='/media/img_trans.gif'>";
		else
			echo "  ><img id='led_green' src='/media/img_trans.gif'>";
		echo "</td></tr>";
	}

	echo ("</table>");
} else if ($data_device_id == 2)	//Solar
{
	if ($data_validated == 1) {
		echo ("<table class='table table-striped'>");

		echo ("<tr><td>Type</td><td>$data_type_str</td></tr>
				<tr><td>Site Id</td><td>$data_site_id</td></tr>
				<tr><td>Date Time</td><td>$data_datetime</td></tr>
				<tr><td>Device Id</td><td>$data_device_id</td></tr>");

		//echo ("Status: <b>$data_status</b><br>");
		//echo ("Status: <b>$data_status_hex</b></tr>");

		//$tmp = gettype($data_status);
		//echo "Var: $tmp<br>";

		$stats = unpack("C*", $data_status);
		//var_dump($stats);


		//$tmp = $stats[5] & 0x02;
		//echo "NUM: $stats[5]<br>";
		//echo "AND: $tmp<br>";

		echo "<tr><td>Smoke Fire Alarm</td><td";
		//0x0000000000 Smoke fire    0 means No alarm,  1 means Alarm
		if (($stats[5] & 0x01) == false)
			echo " ><img id='led_white' src='/media/img_trans.gif'>";
		else
			echo " ><img id='led_red' src='/media/img_trans.gif'>";

		echo "</td></tr>";


		if ($indoor_type == 0)	//Door Alarm Only Applicable if Indoor
		{
			echo "<tr><td>Door</td><td";
			//$FLAG_01_DOOR 	= 0x0000000002;	//Door Open     0  means  Door Close , 1 Means  Door open
			if (($stats[5] & 0x02) == false)
				echo "  ><img id='led_white' src='/media/img_trans.gif'><br>";
			else
				echo "  ><img id='led_red' src='/media/img_trans.gif'>";

			echo "</td></tr>";
		}

		echo "<tr><td>Site Battery1 Low</td><td";
		//$FLAG_02_BATT1 	= 0x0000000004;	//Site battery Low  1 Means Site Battery1 Low ,0 Means  Battery1 Ok
		if (($stats[5] & 0x04) == false)
			echo "  ><img id='led_white' src='/media/img_trans.gif'>";
		else
			echo "  ><img id='led_red' src='/media/img_trans.gif'>";
		echo "</td></tr>";

		echo "<tr><td>Site Battery2 Low</td><td";
		//$FLAG_03_BATT2 	= 0x0000000008;	//Site battery Low  1 Means Site Battery2 Low ,0 Means  Battery2 Ok
		if (($stats[5] & 0x08) == false)
			echo "  ><img id='led_white' src='/media/img_trans.gif'>";
		else
			echo "  ><img id='led_red' src='/media/img_trans.gif'>";
		echo "</td></tr>";


		echo "<tr><td>Room Temperature</td><td";
		//$FLAG_04_TEMP 	= 0x0000000010;	//Room Temperature  1 Means high Temperature, 0 Means Normal temp
		if (($stats[5] & 0x10) == false)
			echo "  ><img id='led_white' src='/media/img_trans.gif'>";
		else
			echo "  ><img id='led_red' src='/media/img_trans.gif'>";
		echo "</td></tr>";

		echo "<tr><td>Mains Fail</td><td";
		//$FLAG_05_MAINS 	= 0x0000000020;	//Mains fail	1 Means main Fail,  0 Means  Main healthy
		if (($stats[5] & 0x20) == false)
			echo "  ><img id='led_white' src='/media/img_trans.gif'>";
		else
			echo "  ><img id='led_red' src='/media/img_trans.gif'>";
		echo "</td></tr>";

		echo "<tr><td>Power Plant1 Fail</td><td";
		//$FLAG_06_PPLT1 	= 0x0000000040;	//Power Plant1 fail 1 Means Power Plant1 Fail, 0 Means Power Plant1 OK
		if (($stats[5] & 0x40) == false)
			echo "  ><img id='led_white' src='/media/img_trans.gif'>";
		else
			echo "  ><img id='led_red' src='/media/img_trans.gif'>";
		echo "</td></tr>";

		echo "<tr><td>Rectifier Fail</td><td";
		//$FLAG_07_RECTFR 	= 0x0000000080;	//Rectifier Fail 1 Means Rectifier Fail, 0 Means Rectifier OK
		if (($stats[5] & 0x80) == false)
			echo "  ><img id='led_white' src='/media/img_trans.gif'>";
		else
			echo "  ><img id='led_red' src='/media/img_trans.gif'>";
		echo "</td></tr>";
	} else {
		echo ("<table class='table table-striped'>");

		echo ("<tr><td>Type</td><td>$data_type_str</td></tr>
				<tr><td>Site Id</td><td>$data_site_id</td></tr>
				<tr><td>Date Time</td><td>$data_datetime</td></tr>
				<tr><td>Device Id</td><td>$data_device_id</td></tr>");

		//echo ("Status: <b>$data_status</b><br>");
		//echo ("Status: <b>$data_status_hex</b></tr>");

		//$tmp = gettype($data_status);
		//echo "Var: $tmp<br>";

		$stats = unpack("C*", $data_status);
		//var_dump($stats);


		//$tmp = $stats[5] & 0x02;
		//echo "NUM: $stats[5]<br>";
		//echo "AND: $tmp<br>";

		echo "<tr><td>Smoke Fire Alarm</td><td";
		//0x0000000000 Smoke fire    0 means No alarm,  1 means Alarm
		if (($stats[5] & 0x01) == false)
			echo " ><img id='led_white' src='/media/img_trans.gif'>";
		else
			echo " ><img id='led_red' src='/media/img_trans.gif'>";

		echo "</td></tr>";


		if ($indoor_type == 0)	//Door Alarm Only Applicable if Indoor
		{
			echo "<tr><td>Door</td><td";
			//$FLAG_01_DOOR 	= 0x0000000002;	//Door Open     0  means  Door Close , 1 Means  Door open
			if (($stats[5] & 0x02) == false)
				echo "  ><img id='led_white' src='/media/img_trans.gif'><br>";
			else
				echo "  ><img id='led_red' src='/media/img_trans.gif'>";

			echo "</td></tr>";
		}


		echo "<tr><td>Inverter Fail</td><td";
		//$FLAG_05_MAINS 	= 0x0000000020;	//Inverter fail	1 Means Inverter Fail,  0 Means  Inverter healthy
		if (($stats[5] & 0x20) == false)
			echo "  ><img id='led_white' src='/media/img_trans.gif'>";
		else
			echo "  ><img id='led_red' src='/media/img_trans.gif'>";
		echo "</td></tr>";

		//$FLAG_22_RES 	= 0x0000400000;	//Solar Araay Fault
		echo "<tr><td><B>Solar Array Fault</B></td><td";
		if (($stats[3] & 0x40) == false)
			echo "  ><img id='led_green' src='/media/img_trans.gif'>";
		else
			echo "  ><img id='led_red' src='/media/img_trans.gif'>";
		echo "</td></tr>";


		//$FLAG_23_RES 	= 0x0000800000;	//Solar PV open/theft
		echo "<tr><td><B>Solar PV open/theft</B></td><td";
		if (($stats[3] & 0x80) == false)
			echo "  ><img id='led_green' src='/media/img_trans.gif'>";
		else
			echo "  ><img id='led_red' src='/media/img_trans.gif'>";
		echo "</td></tr>";

		//$FLAG_24_RES 	= 0x0001000000;	//PV High Voltage
		echo "<tr><td><B>PV High Voltage</B></td><td";
		if (($stats[2] & 0x10) == false)
			echo "  ><img id='led_white' src='/media/img_trans.gif'>";
		else
			echo "  ><img id='led_red' src='/media/img_trans.gif'>";
		echo "</td></tr>";

		//$FLAG_25_RES 	= 0x0002000000;	//PV Low Voltage
		echo "<tr><td><B>PV Low Voltage</B></td><td";
		if (($stats[2] & 0x20) == false)
			echo "  ><img id='led_white' src='/media/img_trans.gif'>";
		else
			echo "  ><img id='led_red' src='/media/img_trans.gif'>";
		echo "</td></tr>";
	}


	echo ("</table>");
} else if ($data_device_id == 32) {
	echo ("<table class='table table-striped'>");

	echo ("<tr><td>Type</td><td>$data_type_str</td></tr>
				<tr><td>Site Id</td><td>$data_site_id</td></tr>
				<tr><td>Date Time</td><td>$data_datetime</td></tr>
				<tr><td>Device Id</td><td>$data_device_id</td></tr>");

	//echo ("Status: <b>$data_status</b><br>");
	//echo ("Status: <b>$data_status_hex</b></tr>");

	//$tmp = gettype($data_status);
	//echo "Var: $tmp<br>";

	$stats = unpack("C*", $data_status);
	//var_dump($stats);

	//        if ($data_validated != 2)
	//        {

	echo "<tr><td>Smoke Fire Alarm</td><td";
	//0x0000000000 Smoke fire    0 means No alarm,  1 means Alarm
	if (($stats[5] & 0x01) == false)
		echo " ><img id='led_white' src='/media/img_trans.gif'>";
	else
		echo " ><img id='led_red' src='/media/img_trans.gif'>";

	echo "</td></tr>";


	if ($indoor_type == 0)	//Door Alarm Only Applicable if Indoor
	{
		echo "<tr><td>Door</td><td";
		//$FLAG_01_DOOR 	= 0x0000000002;	//Door Open     0  means  Door Close , 1 Means  Door open
		if (($stats[5] & 0x02) == false)
			echo "  ><img id='led_white' src='/media/img_trans.gif'><br>";
		else
			echo "  ><img id='led_red' src='/media/img_trans.gif'>";

		echo "</td></tr>";
	}


	/* echo "<tr><td>Mode</td><td";
            //$FLAG_02_AUTO 	= 0x0000000004;	//Auto/Man Mode 0 Means  Auto Mode, 1 Means  Man Mode
            if (($stats[5] & 0x04) == false)
                echo "  >Auto";
            else
                echo "  >Manual";
            echo "</td></tr>";

            echo "<tr><td>Load</td><td>";
            //$FLAG_034_LOAD 	= 0x0000000018;	//00    :Load on EB,01: Load on DG, 10: Load on site Battery   11: Not used
            if (($stats[5] & 0x08) == false)
            {
                if (($stats[5] & 0x10) == false)
                    echo "[- Load on EB -]<br>";				//00
                else
                    echo "[- Load on site Battery -]<br>";	//10
            }
            else
            {
                if (($stats[5] & 0x10) == false)
                    echo "[- Load on DG -]<br>";				//01
                else
                    echo "[- Not Used -]<br>";	//11
            }
            echo "</td></tr>";

            echo "<tr><td>Mains Fail</td><td";
            //$FLAG_05_MAINS 	= 0x0000000020;	//Mains fail	1 Means main Fail,  0 Means  Main healthy
            if (($stats[5] & 0x20) == false)
                echo "  ><img id='led_white' src='/media/img_trans.gif'>";
            else
                echo "  ><img id='led_red' src='/media/img_trans.gif'>";
            echo "</td></tr>";*/

	echo "<tr><td>Site Battery1 Low</td><td";
	//$FLAG_02_BATT1 	= 0x0000000004;	//Site battery Low  1 Means Site Battery1 Low ,0 Means  Battery1 Ok
	if (($stats[5] & 0x04) == false)
		echo "  ><img id='led_white' src='/media/img_trans.gif'>";
	else
		echo "  ><img id='led_red' src='/media/img_trans.gif'>";
	echo "</td></tr>";

	echo "<tr><td>Site Battery2 Low</td><td";
	//$FLAG_03_BATT2 	= 0x0000000008;	//Site battery Low  1 Means Site Battery2 Low ,0 Means  Battery2 Ok
	if (($stats[5] & 0x08) == false)
		echo "  ><img id='led_white' src='/media/img_trans.gif'>";
	else
		echo "  ><img id='led_red' src='/media/img_trans.gif'>";
	echo "</td></tr>";


	echo "<tr><td>Room Temperature</td><td";
	//$FLAG_04_TEMP 	= 0x0000000010;	//Room Temperature  1 Means high Temperature, 0 Means Normal temp
	if (($stats[5] & 0x10) == false)
		echo "  ><img id='led_white' src='/media/img_trans.gif'>";
	else
		echo "  ><img id='led_red' src='/media/img_trans.gif'>";
	echo "</td></tr>";

	echo "<tr><td>Mains Fail</td><td";
	//$FLAG_05_MAINS 	= 0x0000000020;	//Mains fail	1 Means main Fail,  0 Means  Main healthy
	if (($stats[5] & 0x20) == false)
		echo "  ><img id='led_white' src='/media/img_trans.gif'>";
	else
		echo "  ><img id='led_red' src='/media/img_trans.gif'>";
	echo "</td></tr>";

	echo "<tr><td>Power Plant1 Fail</td><td";
	//$FLAG_06_PPLT1 	= 0x0000000040;	//Power Plant1 fail 1 Means Power Plant1 Fail, 0 Means Power Plant1 OK
	if (($stats[5] & 0x40) == false)
		echo "  ><img id='led_white' src='/media/img_trans.gif'>";
	else
		echo "  ><img id='led_red' src='/media/img_trans.gif'>";
	echo "</td></tr>";

	echo "<tr><td>Power Plant2 Fail</td><td";
	//$FLAG_07_PPLT2 	= 0x0000000080;	//Power Plant1 fail 1 Means Power Plant2 Fail, 0 Means Power Plant2 OK
	if (($stats[5] & 0x80) == false)
		echo "  ><img id='led_white' src='/media/img_trans.gif'>";
	else
		echo "  ><img id='led_red' src='/media/img_trans.gif'>";
	echo "</td></tr>";




	/* echo "<tr><td>LLOP</td><td";
            //$FLAG_08_LLOP 	= 0x0000000100;	//LLOP
            if (($stats[4] & 0x01) == false)
                echo "  ><img id='led_white' src='/media/img_trans.gif'>";
            else
                echo "  ><img id='led_red' src='/media/img_trans.gif'>";
            echo "</td></tr>";


            echo "<tr><td>HCT/HWT</td><td";
            //$FLAG_09_HCT 	= 0x0000000200;	//HCT/HWT
            if (($stats[4] & 0x02) == false)
                echo "  ><img id='led_white' src='/media/img_trans.gif'>";
            else
                echo "  ><img id='led_red' src='/media/img_trans.gif'>";
            echo "</td></tr>";


            echo "<tr><td>Alternator Fault</td><td";
            //$FLAG_10_ALT 	= 0x0000000400;	//Alternator Fault
            if (($stats[4] & 0x04) == false)
                echo "  ><img id='led_white' src='/media/img_trans.gif'>";
            else
                echo "  ><img id='led_red' src='/media/img_trans.gif'>";
            echo "</td></tr>";


            echo "<tr><td>DG Over Speed</td><td";
            //$FLAG_11_DG 	= 0x0000000800;	//DG Over Speed
            if (($stats[4] & 0x08) == false)
                echo "  ><img id='led_white' src='/media/img_trans.gif'>";
            else
                echo "  ><img id='led_red' src='/media/img_trans.gif'>";
            echo "</td></tr>";


            echo "<tr><td>DG Over Load</td><td";
            //$FLAG_12_DG 	= 0x0000001000;	//DG Over Load
            if (($stats[4] & 0x10) == false)
                echo "  ><img id='led_white' src='/media/img_trans.gif'>";
            else
                echo "  ><img id='led_red' src='/media/img_trans.gif'>";
            echo "</td></tr>";


            echo "<tr><td>DG Low Fuel</td><td";
            //$FLAG_13_DG 	= 0x0000002000;	//DG Low Fuel
            if (($stats[4] & 0x20) == false)
                echo "  ><img id='led_white' src='/media/img_trans.gif'>";
            else
                echo "  ><img id='led_red' src='/media/img_trans.gif'>";
            echo "</td></tr>";


            echo "<tr><td>DG Start Fail</td><td";
            //$FLAG_14_DG 	= 0x0000004000;	//DG Start Fail
            if (($stats[4] & 0x40) == false)
                echo "  ><img id='led_white' src='/media/img_trans.gif'>";
            else
                echo "  ><img id='led_red' src='/media/img_trans.gif'>";
            echo "</td></tr>";


            echo "<tr><td>DG Stop Fail</td><td";
            //$FLAG_15_DG 	= 0x0000008000;	//DG Stop Fail
            if (($stats[4] & 0x80) == false)
                echo "  ><img id='led_white' src='/media/img_trans.gif'>";
            else
                echo "  ><img id='led_red' src='/media/img_trans.gif'>";
            echo "</td></tr>";


            echo "<tr><td>DG Battery</td><td";
            //$FLAG_16_DG 	= 0x0000010000;	//DG battery Low
            if (($stats[3] & 0x01) == false)
                echo "  ><img id='led_white' src='/media/img_trans.gif'>";
            else
                echo "  ><img id='led_red' src='/media/img_trans.gif'>";
            echo "</td></tr>";


            echo "<tr><td>LCU Fail</td><td";
            //$FLAG_17_LCU 	= 0x0000020000;	//LCU fail
            if (($stats[3] & 0x02) == false)
                echo "  ><img id='led_green' src='/media/img_trans.gif'>";
            else
                echo "  ><img id='led_red' src='/media/img_trans.gif'>";
            echo "</td></tr>";


            echo "<tr><td>Rectifier Fail</td><td";
            //$FLAG_18_RECT 	= 0x0000040000;	//Rectifier Fail
            if (($stats[3] & 0x04) == false)
                echo "  ><img id='led_green' src='/media/img_trans.gif'>";
            else
                echo "  ><img id='led_red' src='/media/img_trans.gif'>";
            echo "</td></tr>";


            echo "<tr><td>Multi Rectifier Fail</td><td";
            //$FLAG_19_RECT 	= 0x0000080000;	//Multi Rectifier Fail
            if (($stats[3] & 0x08) == false)
                echo "  ><img id='led_green' src='/media/img_trans.gif'>";
            else
                echo "  ><img id='led_red' src='/media/img_trans.gif'>";
            echo "</td></tr>";


            echo "<tr><td>LVD Trip</td><td";
            //$FLAG_20_LVD 	= 0x0000100000;	//LVD TRIP
            if (($stats[3] & 0x10) == false)
                echo "  ><img id='led_green' src='/media/img_trans.gif'>";
            else
                echo "  ><img id='led_red' src='/media/img_trans.gif'>";
            echo "</td></tr>";


            echo "<tr><td>LVD BY PASS</td><td";
            //$FLAG_21_LVD 	= 0x0000200000;	//LVD BY pass
            if (($stats[3] & 0x20) == false)
                echo "  ><img id='led_green' src='/media/img_trans.gif'>";
            else
                echo "  ><img id='led_red' src='/media/img_trans.gif'>";
            echo "</td></tr>";


            //$FLAG_22_RES 	= 0x0000400000;	//Reserved
            //$FLAG_23_RES 	= 0x0000800000;	//Reserved
            //$FLAG_24_RES 	= 0x0001000000;	//Reserved
            //$FLAG_25_RES 	= 0x0002000000;	//Reserved
            //$FLAG_26_RES 	= 0x0004000000;	//Reserved
            //$FLAG_27_RES 	= 0x0008000000;	//Reserved
            //$FLAG_28_RES 	= 0x0010000000;	//Reserved
            //$FLAG_29_RES 	= 0x0020000000;	//Reserved
            //$FLAG_30_RES 	= 0x0040000000;	//Reserved
            //$FLAG_31_RES 	= 0x0080000000;	//Reserved

            echo "<tr><td>DG OFF</td><td";
            //$FLAG_32_DG 	= 0x0100000000;	//DG OFF
            if (($stats[1] & 0x01) == false)
                echo "  ><img id='led_white' src='/media/img_trans.gif'>";
            else
                echo "  ><img id='led_green' src='/media/img_trans.gif'>";
            echo "</td></tr>";


            echo "<tr><td>DG ON</td><td";
            //$FLAG_33_DG 	= 0x0200000000;	//DG ON
            if (($stats[1] & 0x02) == false)
                echo "  ><img id='led_white' src='/media/img_trans.gif'>";
            else
                echo "  ><img id='led_green' src='/media/img_trans.gif'>";
            echo "</td></tr>";


            echo "<tr><td>DG Cranking</td><td";
            //$FLAG_34_DG 	= 0x0400000000;	//DG Cranking
            if (($stats[1] & 0x04) == false)
                echo "  ><img id='led_white' src='/media/img_trans.gif'>";
            else
                echo "  ><img id='led_green' src='/media/img_trans.gif'>";
            echo "</td></tr>";


            echo "<tr><td>DG Start in Progress</td><td";
            //$FLAG_35_DG 	= 0x0800000000;	//DG Start in Progress
            if (($stats[1] & 0x08) == false)
                echo "  ><img id='led_white' src='/media/img_trans.gif'>";
            else
                echo "  ><img id='led_green' src='/media/img_trans.gif'>";
            echo "</td></tr>";


            echo "<tr><td>DG Cool Down (Idle Running)</td><td";
            //$FLAG_36_DG 	= 0x1000000000;	//DG Cool Down (Idle Running)
            if (($stats[1] & 0x10) == false)
                echo "  ><img id='led_white' src='/media/img_trans.gif'>";
            else
                echo "  ><img id='led_green' src='/media/img_trans.gif'>";
            echo "</td></tr>";


            echo "<tr><td>DG STOP  Normally</td><td";
            //$FLAG_37_DG 	= 0x2000000000;	//DG STOP  Normally
            if (($stats[1] & 0x20) == false)
                echo "  ><img id='led_white' src='/media/img_trans.gif'>";
            else
                echo "  ><img id='led_green' src='/media/img_trans.gif'>";
            echo "</td></tr>";


            echo "<tr><td>DG STOP due to Fault</td><td";
            //$FLAG_38_DG 	= 0x8000000000;	//DG STOP due to Fault
            if (($stats[1] & 0x40) == false)
                echo "  ><img id='led_white' src='/media/img_trans.gif'>";
            else
                echo "  ><img id='led_green' src='/media/img_trans.gif'>";
            echo "</td></tr>";


            echo "<tr><td>DG Stop Due to Maximum Run Expiry</td><td";
            //$FLAG_39_DG 	= 0x8000000000;	//DG Stop Due to Maximum Run Exipary
            if (($stats[1] & 0x80) == false)
                echo "  ><img id='led_white' src='/media/img_trans.gif'>";
            else
                echo "  ><img id='led_green' src='/media/img_trans.gif'>";
            echo "</td></tr>";
        */
	echo ("</table>");
}



/*if (($data_device_id == 1) && ($data_validated != 2))
	{
		echo "<h3>Data for Hybrid Controller</h3>";
		echo "<h5>Device Type 0x01</h5>";
		
		echo ("<table class='table table-striped'>");
	
		//$data_hex= bin2hex($data_raw);
		//echo $data_hex;
	
		//echo "<br><br>";
		
		//$sub_data = substr($data_raw, 41, 86);
		
		//$data_hex= bin2hex($sub_data);
		//echo $data_hex;
		
		
		$sub_data = substr($data_raw, 41, 36);
		$stats = str_split($sub_data, 2);
		//var_dump($stats);
		
		$sub_data = unpack("n",$stats[0]);
		$sub_data = $sub_data[1]/10;
		
		echo "<tr><td>Room Temperature</td><td>";
		echo("$sub_data Deg C");
		echo "</td></tr>";
		
		
		$sub_data = unpack("n",$stats[1]);
		$sub_data = $sub_data[1]/10;
		echo "<tr><td>Fuel Level</td><td>";
		echo("$sub_data %");
		echo "</td></tr>";
		
		$sub_data = unpack("n",$stats[2]);
		$sub_data = $sub_data[1]/10;
		echo "<tr><td>Fuel Data</td><td>";
		echo("$sub_data Liters");
		echo "</td></tr>";
		
		$sub_data = unpack("n",$stats[3]);
		$sub_data = $sub_data[1]/10;
		echo "<tr><td>Site Batt. bank Voltage</td><td>";
		echo("$sub_data VDC");
		echo "</td></tr>";
		
		$sub_data = unpack("n",$stats[4]);
		$sub_data = $sub_data[1]/10;
		echo "<tr><td>R-Phase current</td><td>";
		echo("$sub_data");
		echo "</td></tr>";
		
		$sub_data = unpack("n",$stats[5]);
		$sub_data = $sub_data[1]/10;
		echo "<tr><td>Y-Phase current</td><td>";
		echo("$sub_data");
		echo "</td></tr>";
		
		$sub_data = unpack("n",$stats[6]);
		$sub_data = $sub_data[1]/10;
		echo "<tr><td>B-Phase current</td><td>";
		echo("$sub_data");
		echo "</td></tr>";
		
		$sub_data = unpack("n",$stats[7]);
		$sub_data = $sub_data[1]/10;
		echo "<tr><td>Mains Frequency</td><td>";
		echo("$sub_data");
		echo "</td></tr>";
		
		$sub_data = unpack("n",$stats[8]);
		$sub_data = $sub_data[1]/10;
		echo "<tr><td>DG frequency</td><td>";
		echo("$sub_data");
		echo "</td></tr>";
		
		$sub_data = unpack("n",$stats[9]);
		$sub_data = $sub_data[1]/10;
		echo "<tr><td>DG-R phase Voltage</td><td>";
		echo("$sub_data VAC");
		echo "</td></tr>";
		
		$sub_data = unpack("n",$stats[10]);
		$sub_data = $sub_data[1]/10;
		echo "<tr><td>DG-Y phase Voltage</td><td>";
		echo("$sub_data VAC");
		echo "</td></tr>";
		
		$sub_data = unpack("n",$stats[11]);
		$sub_data = $sub_data[1]/10;
		echo "<tr><td>DG-B phase Voltage</td><td>";
		echo("$sub_data VAC");
		echo "</td></tr>";
		
		$sub_data = unpack("n",$stats[12]);
		$sub_data = $sub_data[1]/10;
		echo "<tr><td>LCU1 Output Voltage</td><td>";
		echo("$sub_data VAC");
		echo "</td></tr>";
		
		$sub_data = unpack("n",$stats[13]);
		$sub_data = $sub_data[1]/10;
		echo "<tr><td>LCU2 Output Voltage</td><td>";
		echo("$sub_data VAC");
		echo "</td></tr>";
		
		$sub_data = unpack("n",$stats[14]);
		$sub_data = $sub_data[1]/10;
		echo "<tr><td>LCU3 Output Voltage</td><td>";
		echo("$sub_data VAC");
		echo "</td></tr>";
		
		$sub_data = unpack("n",$stats[15]);
		$sub_data = $sub_data[1]/10;
		echo "<tr><td>Input Mains Voltage - R Phase</td><td>";
		echo("$sub_data VAC");
		echo "</td></tr>";
		
		$sub_data = unpack("n",$stats[16]);
		$sub_data = $sub_data[1]/10;
		echo "<tr><td>Input Mains Voltage - Y Phase</td><td>";
		echo("$sub_data VAC");
		echo "</td></tr>";
		
		$sub_data = unpack("n",$stats[17]);
		$sub_data = $sub_data[1]/10;
		echo "<tr><td>Input Mains Voltage - B Phase</td><td>";
		echo("$sub_data VAC");
		echo "</td></tr>";
		
		/////
		
		
		$sub_data = substr($data_raw, 77, 48);
		$stats = str_split($sub_data, 8);
		$stats = substr_replace($stats, ".", 7, 0);
		
		for ($i=0; $i<6; $i++)
		{
			$stats[$i] = ltrim($stats[$i], '0');
			if ($stats[$i][0] == '.')
				$stats[$i] = "0".$stats[$i];
		}
		//var_dump($stats);
		echo "<tr><td>DG Running Hours</td><td>";
		echo("$stats[0] Hrs");
		echo "</td></tr>";
		
		echo "<tr><td>Mains RUN HOURS</td><td>";
		echo("$stats[1] Hrs");
		echo "</td></tr>";
		
		echo "<tr><td>Batt RUN HOURS</td><td>";
		echo("$stats[2] Hrs");
		echo "</td></tr>";
		
		echo "<tr><td>O/P Mains Energy</td><td>";
		echo("$stats[3] Kwh");
		echo "</td></tr>";
		
		echo "<tr><td>DG Energy</td><td>";
		echo("$stats[4] Kwh");
		echo "</td></tr>";
		
		echo "<tr><td>I/P Mains Energy</td><td>";
		echo("$stats[5] Kwh");
		echo "</td></tr>";
		
		$sub_data = substr($data_raw, 125, 2);
		//var_dump($sub_data);
		
		$sub_data = unpack("n",$sub_data);
		$sub_data = $sub_data[1]/10;
		echo "<tr><td>DG Battery Voltage</td><td>";
		echo("$sub_data VDC");
		echo "</td></tr>";
		
		
		if (strlen($data_raw) >= 134)	//Rev 2
		{
			$sub_data = substr($data_raw, 127, 4);
			$stats = str_split($sub_data, 2);
			
			$sub_data = unpack("n",$stats[0]);
			$sub_data = $sub_data[1]/10;
			echo "<tr><td>Battery Charging current</td><td>";
			echo("$sub_data Amp");
			echo "</td></tr>";
			
			
			$sub_data = unpack("n",$stats[1]);
			$sub_data = $sub_data[1]/10;
			echo "<tr><td>Battery Discharging current</td><td>";
			echo("$sub_data Amp");
			echo "</td></tr>";
			
			$sub_data = substr($data_raw, 131, 2);
			$stats = str_split($sub_data, 1);
			
			$sub_data = unpack("c",$stats[0]);
			$sub_data = (hexdec(bin2hex($stats[0])));	//$sub_data[1]/1;
			echo "<tr><td>Battery status</td><td>";
			echo("$sub_data %");
			echo "</td></tr>";
			
			$sub_data = unpack("c",$stats[1]);
			$sub_data = (hexdec(bin2hex($stats[1]))/10);	//$sub_data[1]/10;
			echo "<tr><td>Battery back up time</td><td>";
			echo("$sub_data hours");
			echo "</td></tr>";
			
			
			if (strlen($data_raw) >= 150)	//Rev 3
			{
				$sub_data = substr($data_raw, 133, 16);
				$stats = str_split($sub_data, 8);
				$stats = substr_replace($stats, ".", 7, 0);
				
				for ($i=0; $i<2; $i++)
				{
					$stats[$i] = ltrim($stats[$i], '0');
					if ($stats[$i][0] == '.')
						$stats[$i] = "0".$stats[$i];
				}

				echo "<tr><td>Battery Charging Energy</td><td>";				
				echo("$stats[0] Kwh");
				echo "</td></tr>";
				
				echo "<tr><td>Battery Discharging Energy</td><td>";				
				echo("$stats[1] Kwh");
				echo "</td></tr>";
			}
		
		}
	
		echo ("</table>");*/



//$binarydata = pack("c*", 0x0, 0x5);
//var_dump(bin2hex($binarydata));
//$sub_data = unpack("n",$binarydata);
//$sub_data = $sub_data[1]/10;
//echo("Room Temperature: <b>$sub_data Deg C<br>");

//18*2	= 36
//6 *8	= 48
//1 *2	= 02
//----------
//        86
//----------

?>