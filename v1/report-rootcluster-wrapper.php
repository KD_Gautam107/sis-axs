<?php
session_start();
if (!isset($_SESSION['login_user'])) {
	header("Location: login.php");
}

include 'common.php';
include 'report-rootcluster-wrapper-data.php';

if ($_SERVER["REQUEST_METHOD"] == "GET") {
	$id = $_GET['id'];
	$_SESSION['range_start'] = $_GET['range_start'];
	$_SESSION['range_end'] = $_GET['range_end'];
	//$_SESSION['chart_type'] = $_GET['type'];

	//if ($_SESSION['chart_type'] == null) $_SESSION['chart_type'] = "Pie";

	//Temp Code
	if ($_SESSION['range_start'] == null) $_SESSION['range_start'] = "2020-11-23 18:33:18";
	if ($_SESSION['range_end'] == null) $_SESSION['range_end'] = "2021-01-04 15:23:23";
}

//$data_array = GetSiteDetailFromId($id);
//var_dump($data_array);


?>

<script>
	AsyncLoad("report-root-a.php?site_id=<?php echo $id; ?>&range_data_type=0", "#id_report_rootcluster");
</script>

<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title">
			<div class="dropdown">
				<button onclick="myFunction()" class="dropbtn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Select Report Type <span class="caret"></span></button>
				<div id="myDropdown" class="dropdown-content dropdown-menu">
					<li><a href="#" onclick='event.preventDefault(); AsyncLoad("report-root-a.php?site_id=<?php echo $id; ?>&range_data_type=0", "#id_report_rootcluster", "#id_report_rootcluster")'>Report - A</a></li>
					<li><a href="#" onclick='event.preventDefault(); AsyncLoad("report-root-b.php?site_id=<?php echo $id; ?>&range_data_type=0", "#id_report_rootcluster", "#id_report_rootcluster")'>Report - B</a></li>

				</div>
			</div>
		</h3>
	</div>

	<div id="collapse_report" class="panel-collapse collapse in">
		<div class="panel-body">
			<div id="id_report_rootcluster"></div>
		</div>
	</div>
</div>




<script>
	function myFunction() {
		document.getElementById("myDropdown").classList.toggle("show");
	}

	// Close the dropdown if the user clicks outside of it
	window.onclick = function(event) {
		if (!event.target.matches('.dropbtn')) {
			var dropdowns = document.getElementsByClassName("dropdown-content");
			var i;
			for (i = 0; i < dropdowns.length; i++) {
				var openDropdown = dropdowns[i];
				if (openDropdown.classList.contains('show')) {
					openDropdown.classList.remove('show');
				}
			}
		}
	}
</script>


<style>
	.dropbtn {

		color: white;
		padding: 13px;
		font-size: 14px;
		border: none;
		cursor: pointer;
	}

	.dropdown-menu {
		padding: 0px !important;
	}

	.dropdown-menu>li>a {
		border-bottom: 1px solid #80808075 !important;
		background: #beebff;
	}


	.dropdown {
		position: relative;
		display: inline-block;
	}

	.dropdown-content {
		display: none;
		position: absolute;

		min-width: 160px;
		overflow: auto;
		box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);
		z-index: 1;
	}

	.dropdown-content a {
		color: black;

		text-decoration: none;
		display: block;
	}

	.dropdown a:hover {
		background-color: #ddd;
	}

	.show {
		display: block;
	}
</style>