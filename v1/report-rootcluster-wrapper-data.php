<?php

//TO DELETE - NOT USED

function FetchData($strStart, $strEnd)
{
    $id = $_SESSION['login_cust_id'];
    $login_type = $_SESSION['login_type'];
    $clusters = $_SESSION['login_typeref_id'];

    include 'dbinc.php';
    include 'list.php';

    $mysqli = new mysqli("$mysql_hostname", "$mysql_user", "$mysql_password", "$mysql_database");
    if (mysqli_connect_errno()) {
        printf("Connect failed: %s\n", mysqli_connect_error());
        return null;
    }

    // echo "login_cust_id: $login_cust_id\r\n";
    // echo "login_type: $login_type\r\n";
    // var_dump($clusters);

    if ($login_type == 1)    //Head, show all clusters and sites for the customer
    {
        $sql = "SELECT 
                        a.Id, a.site_id, a.ClusterId, b.name, a.SiteName 
                    FROM siteinfo a 
                    
                        INNER JOIN clusters b 
                        ON a.ClusterId = b.id			
    
                    WHERE ((a.CustomerId = $id) AND (a.site_enabled = 1))
                    ORDER BY ClusterId 
    
                    
                    ";
    } else if ($login_type == 2)    //Cluster Head, show only the cluster of the head
    {
        $clustersid = $clusters[0];    //Only a single cluster
        $sql = "SELECT 
                        a.Id, a.site_id, a.ClusterId, b.name, a.SiteName 
                    FROM siteinfo a 
                    
                        INNER JOIN clusters b 
                        ON a.ClusterId = b.id			
    
                    WHERE ((a.CustomerId = $id) AND (a.ClusterId = $clustersid) AND (a.site_enabled = 1))
                    ORDER BY ClusterId 
    
                    
                    ";
    } else if ($login_type == 4)    //Zone Head, show all the clusters under the zone
    {
        $clustersid = join(',', $clusters);
        //echo "$clustersid <br>";
        $sql = "SELECT 
                        a.Id, a.site_id, a.ClusterId, b.name, a.SiteName 
                    FROM siteinfo a 
                    
                        INNER JOIN clusters b 
                        ON a.ClusterId = b.id			
    
                    WHERE ((a.CustomerId = $id) AND (a.ClusterId IN ($clustersid)) AND (a.site_enabled = 1))
                    ORDER BY ClusterId 
    
                    
                    ";
    } else {
        //Show no cluster/site
        $sql = null;
    }


    if ($sql != null) {

        $result = $mysqli->query($sql);

        echo "<tbody>";

        $totalDG = 0;
        $totalMains = 0;
        $totalBattery = 0;

        while ($data = $result->fetch_row()) {
            //var_dump($data);

            $sum = null;
            //if ($range_data_type == 3)
            //{
            //echo "TEST";
            //echo $data[1] ." - ". $strEnd ." - ". $strStart;
            $array = GetSiteStatsFromIdStartEnd($data[1], $strEnd, $strStart);
            //print_r($array);

            $sum = AddUp($array, $sum);

            // calculating total values.
            $totalDG += round($sum[1] + $sum[4], 1);
            $totalMains += round($sum[2], 1);
            $totalBattery += round($sum[3] + $adj_offset, 1);

            echo ("<tr>
            
          <td> $data[4]</td>
          <td> $data[1]</td>");

            echo ("<td>" . round($sum[1] + $sum[4], 1) . "</td>");
            echo (" <td>" . round($sum[2], 1) . " hours" . "</td>");
            echo (" <td>" . round($sum[3] + $adj_offset, 1) . "</td>");

            $array = GetEntryTypeData($data[1]);

            echo ("<td>$array[30]</td>
          <td> $array[29]</td>
          <td> $array[31]</td>");

            $data_status_hex = bin2hex($array[7]);

            echo ("<td><button type='button' class='btn btn-primary btn-sm'
           data-toggle='modal' OnClick='ShowDetail($array[0])'>$data_status_hex</button></td>
          <td> $array[8]</td>
          <td> $array[12]</td>
          <td> $array[13]</td>
          <td> $array[14]</td>
          <td> $array[15]</td>
          <td> $array[16]</td>
          <td> $array[17]</td>
          <td> $array[18]</td>
          <td> $array[19]</td>
          <td> $array[23]</td>
          <td> $array[24]</td>
          <td> $array[25]</td>

     </tr>");
        }

        echo "<tr style =\"background: #4bb2c547;\">
          <td></td>
          <td>Total</td>
          <td>" . $totalDG . "</td>
          <td>" . $totalMains . "</td>
          <td>" . $totalBattery . "</td>
          <td> $data[1]</td>
          <td> $data[1]</td>
          <td> $data[1]</td>
         
          <td colspan=\"24\" ></td>
        </tr>";

        echo "</tbody></table>";

        $result->close();
    }
}

function GetEntryTypeData($site_id)
{
    // Find the quanta ID 
    // Database connection
    include 'dbinc.php';

    $mysqli = new mysqli("$mysql_hostname", "$mysql_user", "$mysql_password", "$mysql_database");
    if (mysqli_connect_errno()) {
        printf("Connect failed: %s\n", mysqli_connect_error());
        exit();
    }

    $query = "SELECT ID
                FROM quanta 
                WHERE site_id = '$site_id'
                ORDER BY ID DESC 
                LIMIT 1";

    //echo "query: $query";

    $result = $mysqli->query($query);
    $data = $result->fetch_row();

    // // Fetch quanta data
    return GetDeviceRecordFromId($data[0]);
    //var_dump($array);



}
