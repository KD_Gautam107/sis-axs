<?php
session_start();
if (!isset($_SESSION['login_user']))
{
	header("Location: login.php");
}


if($_SERVER["REQUEST_METHOD"] == "GET")
{
	$id = $_GET['id'];
	if ($id[0] == 'n')
	{
		$id = ltrim($id, "n");
	}
	else
	{
		echo "Validation FAILED<br>";
		die;
	}
}


include 'common.php';
$data_array = GetSiteDetailFromId($id);
//var_dump($data_array);

?>

<script type="text/javascript">
				
	function applyUpdate()
	{
		var param = "id=<?php echo $id; ?>";
		var installDate = "";
		
		if ($('#datetimepickerInstallationDate').data("DateTimePicker").date() != null)
			installDate = encodeURIComponent($('#datetimepickerInstallationDate').data("DateTimePicker").date().format('YYYY-MM-DD'));
		
		
		if ($("#site_install_date_null").prop('checked'))
		{
			param += "&site_installation_date_present=false";
			installDate = "";
		}
		else
		{
			param += "&site_installation_date_present=true";
		}
			
		param += "&site_installation_date="+installDate;
		
		param += "&site_name="+encodeURIComponent($( "#site_name" ).val().trim());
		
		param += "&site_id="+encodeURIComponent($( "#site_id" ).val().trim());
		param += "&site_location_lat="+encodeURIComponent($( "#site_location_lat" ).val().trim());
		param += "&site_location_lon="+encodeURIComponent($( "#site_location_lon" ).val().trim());
		param += "&site_circle="+encodeURIComponent($( "#site_circle" ).val().trim());
		param += "&site_district="+encodeURIComponent($( "#site_district" ).val().trim());
		param += "&site_type="+encodeURIComponent($( "#site_type" ).val().trim());

		param += "&site_sam_name="+encodeURIComponent($( "#site_sam_name" ).val().trim());
		param += "&site_sam_num="+encodeURIComponent($( "#site_sam_num" ).val().trim());
		param += "&site_am_name="+encodeURIComponent($( "#site_am_name" ).val().trim());
		param += "&site_am_num="+encodeURIComponent($( "#site_am_num" ).val().trim());
		param += "&site_tech_name="+encodeURIComponent($( "#site_tech_name" ).val().trim());
		param += "&site_tech_num="+encodeURIComponent($( "#site_tech_num" ).val().trim());
		
		AsyncLoad("nodeupdate.php?"+param, "#id_apply_output");
	}
					
</script>

	<div class="panel panel-default">
	  <div class="panel-body">
		<div id="id_data_control"></div>
			<em>Double Click to Edit an Entry</em>
	  </div>
	</div>


	<div class="panel panel-default">
	  <div class="panel-heading">
		<h3 class="panel-title" title='<?php echo $id." / ".$data_array[20]; ?>'>Site Details</h3>
	  </div>
		<div class="panel-body">

			<h3><img src="/media/icons/ic_settings_input_antenna_black_36dp.png" title="Site Name"> <input id="site_name" name="site_name" type="text" value="<?php echo $data_array[5]; ?>" readonly="true" ondblclick="this.readOnly='';"> </h3>
			
			<div style="margin-top:20px;">
				<table class="table table-bordered table-condensed table-striped">
					<tr><td class='col-md-3'>Site Id</td><td class='col-md-9'><strong><input id="site_id" name="site_id" type="text" value="<?php echo $data_array[0]; ?>" readonly="true" ondblclick="this.readOnly='';"></strong></td></tr>
					<tr><td><img src="/media/icons/ic_place_black_24dp.png" title="Location">Location Latitude</td><td><input id="site_location_lat" name="site_location_lat" type="text" value="<?php echo $data_array[46]; ?>" readonly="true" ondblclick="this.readOnly='';"> </td></tr>
					<tr><td><img src="/media/icons/ic_place_black_24dp.png" title="Location">Location Longitude</td><td><input id="site_location_lon" name="site_location_lon" type="text" value="<?php echo $data_array[47]; ?>" readonly="true" ondblclick="this.readOnly='';"></td></tr>
				
					<tr><td><img src="/media/icons/ic_gamepad_black_24dp.png" title="Cluster">Cluster</td><td><strong><?php echo $data_array[19]; ?></strong></td></tr>
				
					<tr><td><img src="/media/icons/ic_data_usage_black_24dp.png" title="Circle">Circle</td><td><strong><input id="site_circle" name="site_circle" type="text" value="<?php echo $data_array[4]; ?>" readonly="true" ondblclick="this.readOnly='';"></strong></td></tr>
					<tr><td><img src="/media/icons/ic_location_city_black_24dp.png" title="District">District</td><td><strong><input id="site_district" name="site_district" type="text" value="<?php echo $data_array[6]; ?>" readonly="true" ondblclick="this.readOnly='';"></strong></td></tr>
					
					<tr><td><img src="/media/icons/ic_home_black_24dp.png" title="Site Type Indoor or Outdoor">Type</td><td><strong>
						
						
						<select id="site_type" name="site_type">
							<option value="0" <?php if ($data_array[10] == 0) echo "selected"; ?>>Indoor</option>
							<option value="1" <?php if ($data_array[10] == 1) echo "selected"; ?>>Outdoor</option>
						</select>
						
						</strong></td></tr>
					
					<tr><td><img src="/media/icons/ic_event_black_24dp.png" title="Date of Installation">Installation Date</td><td><strong>
					
					<input type="checkbox" id="site_install_date_null" name="site_install_date_null" 
						<?php 
								if ($data_array[21] == null) 
								{
									echo " checked='checked' "; 
								}
								//else 
								//{
									//echo date_create_from_format('Y-m-d', $data_array[21])->format('F j, Y');
									//echo $data_array[21]; 
								//}
						   ?>
						
						> No Date Set <i>(If this is checked then Installation Date will be blank)</i></input>
						
						<div class="row">
							<div class='col-sm-4'>
								<div class='input-group date' id='datetimepickerInstallationDate'>
									<input name="site_installation_date" type='text' class="form-control" />
									<span class="input-group-addon">
										<span class="glyphicon glyphicon-calendar"></span>
									</span>
								</div>
							</div>
						</div>
							</strong></td></tr>
					
					<tr><td><img src="/media/icons/ic_home_black_24dp.png" title="Senior Asset Manager Name">SAM Name</td><td><strong><input id="site_sam_name" name="site_sam_name" type="text" value="<?php echo $data_array[12]; ?>" readonly="true" ondblclick="this.readOnly='';"></strong></td></tr>
					<tr><td><img src="/media/icons/ic_home_black_24dp.png" title="Senior Asset Manager Name">SAM Number</td><td><strong><input id="site_sam_num" name="site_sam_num" type="text" value="<?php echo $data_array[13]; ?>" readonly="true" ondblclick="this.readOnly='';"></strong></td></tr>
					
					<tr><td><img src="/media/icons/ic_home_black_24dp.png" title="Asset Manager Name">AM Name</td><td><strong><input id="site_am_name" name="site_am_name" type="text" value="<?php echo $data_array[14]; ?>" readonly="true" ondblclick="this.readOnly='';"></strong></td></tr>
					<tr><td><img src="/media/icons/ic_home_black_24dp.png" title="Asset Manager Name">AM Number</td><td><strong><input id="site_am_num" name="site_am_num" type="text" value="<?php echo $data_array[15]; ?>" readonly="true" ondblclick="this.readOnly='';"></strong></td></tr>
					
					<tr><td><img src="/media/icons/ic_home_black_24dp.png" title="Tech Name">Tech Name</td><td><strong><input id="site_tech_name" name="site_tech_name" type="text" value="<?php echo $data_array[16]; ?>" readonly="true" ondblclick="this.readOnly='';"></strong></td></tr>
					<tr><td><img src="/media/icons/ic_home_black_24dp.png" title="Tech Name">Tech Number</td><td><strong><input id="site_tech_num" name="site_tech_num" type="text" value="<?php echo $data_array[17]; ?>" readonly="true" ondblclick="this.readOnly='';"></strong></td></tr>
					
					
					
				</table>
			</div>
				
			
		
			
			
			
	  </div>
	</div>

	<div class="panel panel-default">
	  <div class="panel-heading">
		<h3 class="panel-title">Equipment Detail and Control</h3>
	  </div>
	  <div class="panel-body">
	  
		<table class="table table-bordered table-condensed">
			
			<tr>
				<td class='col-md-3'>Device Name</td>
				<td class='col-md-9'><strong><?php echo $data_array[2]; ?></strong></td>
			</tr>
			<tr>
				<td class='col-md-3'>Device Make</td>
				<td class='col-md-9'><strong><?php echo $data_array[3]; ?></strong></td>
			</tr>
			<tr>
				<td class='col-md-3'>EB Capacity</td>
				<td class='col-md-9'><strong><?php if ($data_array[23] == 0) echo "-"; else echo $data_array[23]." Kwh"; ?></strong></td>
			</tr>
			<tr>
				<td class='col-md-3'>EB Phase</td>
				<td class='col-md-9'><strong><?php if ($data_array[24] == 0) echo "-"; else echo $data_array[24]." Phase"; ?></strong></td>
			</tr>
			<tr>
				<td class='col-md-3'>Battery Capacity</td>
				<td class='col-md-9'><strong><?php if ($data_array[25] == 0) echo "-"; else echo $data_array[25]." AH"; ?></strong></td>
			</tr>
			<tr>
				<td class='col-md-3'>Battery Quantity</td>
				<td class='col-md-9'><strong><?php if ($data_array[26] == 0) echo "-"; else echo $data_array[26]; ?></strong></td>
			</tr>
			<tr>
				<td class='col-md-3'>Battery Make</td>
				<td class='col-md-9'><strong><?php echo $data_array[27]; ?></strong></td>
			</tr>
			<tr>
				<td class='col-md-3'>DG Make</td>
				<td class='col-md-9'><strong><?php echo $data_array[22]; ?> </strong></td>
			</tr>
			<tr>
				<td class='col-md-3'>DG Detail</td>
				<td class='col-md-9'><strong><?php if ($data_array[11] == 0) echo "-"; else echo $data_array[11]." KVA"; ?> </strong></td>
			</tr>
			<tr>
				<td class='col-md-3'>Site Mobile #</td>
				<td class='col-md-9'><strong></strong></td>
			</tr>
			
		</table>
	 
	  </div>
	</div>
	
	<div class="panel panel-default">
	  <div class="panel-body">
		<div id="id_data_control"></div>
			<a role="button" class="btn btn-primary" href="#" onclick="event.preventDefault(); LoadContentState();">Discard Changes</a>
			<a role="button" class="btn btn-danger" href="#" onclick="event.preventDefault(); applyUpdate();">Apply Changes</a>
	  </div>
	</div>

	<div class="panel panel-default">
	  <div class="panel-body">
		<div id="id_apply_output"></div>
	  </div>
	</div>
	


<script type="text/javascript">
				
	$(function () {
		$('#datetimepickerInstallationDate').datetimepicker({
			format: 'MMMM DD, YYYY',
			date: moment('<?php echo $data_array[21]; ?>', 'YYYY-MM-DD'),
		});		
	});
	
	 
</script>
