<?php

include 'dbinc.php';


function IsNullOrEmptyString($question){
    return (!isset($question) || trim($question)==='');
}

$mysqli = new mysqli("$mysql_hostname", "$mysql_user", "$mysql_password", "$mysql_database");
if (mysqli_connect_errno())
{
	printf("Connect failed: %s\n", mysqli_connect_error());
	exit();
}

session_start();

$error=0;
if($_SERVER["REQUEST_METHOD"] == "POST")
{
	$myusername=addslashes($_POST['username']);
	$mypassword=addslashes($_POST['password']);

	$sql="SELECT * FROM authentication WHERE username='$myusername' and passcode='$mypassword'";

	$result=$mysqli->query($sql);
	//var_dump($result);
	$count=$result->num_rows;
	$data = $result->fetch_row();
	
	$usertype = $data[3]; //type
	$usercust = $data[4]; //customer
	$typeref = $data[5]; //Type Reference
	
	$result->close();
	
	// If result matched $myusername and $mypassword, table row must be 1 row
	if($count==1)
	{
		if (IsNullOrEmptyString($typeref))
		{
			$typearr = null;
			echo "NULL VALUE";
		}
		else
		{
			$typeref = trim($typeref);
			$typearr = explode(",", $typeref);
			$typearr = array_map('trim', $typearr);
			
			for ($i=0; $i<count($typearr); $i++) {
				if (is_numeric($typearr[$i])) {
					//echo "'{$element}' is numeric", PHP_EOL;
					$typearr[$i] = intval($typearr[$i]);
				} else {
					//echo "'{$element}' is NOT numeric", PHP_EOL;
					$typearr[$i] = 0;
				}
			}
			
			//var_dump($typearr);
		}
		
	
	
		$_SESSION['login_user']=$myusername;
		$_SESSION['login_cust_id']=$usercust;
		$_SESSION['login_type']=$usertype;
		$_SESSION['login_typeref_id']=$typearr;
		
		
		$sql="SELECT Name FROM customers WHERE id='$usercust'";

		$result=$mysqli->query($sql);
		$data = $result->fetch_row();
		
		$_SESSION['login_cust_name'] = $data[0]; //Customer Name
		
		$result->close();
		
		if ($usertype == 0)
		{
			header("location: /v1/listadmin.php");
		}
		else
		{
			header("location: base.php");
		}
	}
	else
	{
		$error=1;
	}
	
	$mysqli->close();
}
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Welcome</title>

    <!-- Bootstrap -->
    <!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">

	<!-- Optional theme -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css">

	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
	
	<!-- Latest compiled and minified JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
	
	<style>
		.borderless tbody tr td, .borderless tbody tr th, .borderless thead tr th {
			border: none;
		}
	</style>
	
  </head>
  <body>
	
	<?php $page_title="Welcome"; include 'header1.php'; ?>
	
	
	<div style="margin:10px; width: 300px;">

	
		<?php if ($error == 1)
			{
				echo "<p class='bg-danger'>Your Login Name or Password is invalid</p>";
			}
		?>

		
		<form action="" method="post">
		  <div class="form-group">
			<label for="exampleInputEmail1">User Name</label>
			<input type="text" class="form-control" name="username" placeholder="Enter User Name">
		  </div>
		  <div class="form-group">
			<label for="exampleInputPassword1">Password</label>
			<input type="password" class="form-control" name="password" placeholder="Password">
		  </div>

		  <button type="submit" class="btn btn-default" value=" Submit ">Submit</button>
		</form>
		
		<div style="margin-top:50px;"></div>
		<div class="panel panel-default">
			<div class="panel-heading">Need Help?</div>
			<div class="panel-body">
				<table class="table-condensed borderless">
				<tr><td>E-Mail</td><td><strong>support@sysinfra.in</strong></td></tr>
				<tr><td>Phone</td><td><strong>+91-11-47603671<br>+91-11-22025461<br>+91-7840069520</strong></td>	</tr>
			</table>
				
			</div>
		</div>
		
	</div>

	<?php include 'footer.php'; ?>  
	
</body>
</html>