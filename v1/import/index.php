<?php
	
	include('lib.php');
	
	$tmp_dir = dirname(__FILE__)."/tmp";

	$cus = GetAllCustomers();
	//var_dump($cus);
?>

<html>
<head>
	<title>Import CSV File</title>
</head>
<body>
	<h3>Select File to Upload:</h3>
	
	<form 	action="verify.php" 
			method="post"
			enctype="multipart/form-data">
			
		Select Customer: 
		<select name="customer" id="customer">
			
			<?php
			foreach ($cus as $value) {
					echo "<option selected='$value[0]'>$value[1]</option>";
				}			
			?>
			
		</select> 
		<br>
		<input type="file" name="file" size="50" />
		<br />
		<input type="submit" value="Upload File" />
	</form>
</body>
</html>