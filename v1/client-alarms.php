<?php
session_start();
if (!isset($_SESSION['login_user']))
{
    header("Location: login.php");
}
if($_SERVER["REQUEST_METHOD"] == "GET")
{
    $site_id = $_GET['site_id'];
    $indoor_type=$_GET['indoor_type'];
    $site_name=$_GET['site_name'];
}
include 'common.php';

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Equipment Data</title>

    <!-- Bootstrap -->
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css">

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>

    <script src="/libs/bootstrap-paginator.min.js"></script>
    <script>





    </script>

    <style>
        #led_white {
            width: 28px;
            height: 28px;
            background: url(/media/leds.png) 0 0;
        }

        #led_red {
            width: 28px;
            height: 28px;
            background: url(/media/leds.png) -30px 0;
        }

        #led_green {
            width: 28px;
            height: 28px;
            background: url(/media/leds.png) -124px 0;
        }

    </style>

</head>
<body>

<h3><img src="/media/icons/ic_settings_input_antenna_black_36dp.png" title="Site Name"> <?php echo $site_name; ?></h3>

<?php

include 'dbinc.php';

//$rec_limit = 10;

$mysqli = new mysqli("$mysql_hostname", "$mysql_user", "$mysql_password", "$mysql_database");
if (mysqli_connect_errno()) {
    printf("Connect failed: %s\n", mysqli_connect_error());
    exit();
}

$where_clause = "WHERE (site_id = '$site_id') ";
//$filter_data_type = 0;
//if ($_GET['filter_data_type'] == 1) {$where_clause = $where_clause." AND type=1  "; $filter_data_type = 1;}
//if ($_GET['filter_data_type'] == 2) {$where_clause = $where_clause." AND type=0  "; $filter_data_type = 2;}

/* Get total number of records */
$sql = "SELECT 
        ID, timestamp, data_validated, type, site_id, date_time, device_id, status, raw_data 
        FROM quanta 
        ".$where_clause." 
        ORDER BY date_time DESC LIMIT 1 ";
//echo "Final: $sql<br>";
$retval = $mysqli->query($sql);
if(! $retval )
{
    die('No Data' . $mysqli->error);
}

$row = $retval->fetch_array(MYSQLI_NUM);

                $id                 = $row[0];  //Id
                $timestamp          = $row[1];  //timestamp
                $data_validated     = $row[2];  //.._
                $type               = $row[3];
                $site_id            = $row[4];
                $date_time          = $row[5];
                $device_id          = $row[6];
                $status             = $row[7];
                $raw_data           = $row[8];

$base64Array = array();

//var text = '{"ID":"255","timestamp":"2015-06-17 07:28:20","data_validated":"1","type":"0","site_id":"SISPVTLTD1","date_time":"2015-06-17 13:21:00","device_id":"1","status":"AgABgEIAAAAAAA==","raw_data":"I1NQLFNJU1BWVExURDEsMTM6MjE6MDksMTcvMDYvMjAxNSwCAAGAQgEAAAAAAAAAAAAAAAAAAAHvAfMI3gjUCOgAAAAAAAAIwAiiCNQwMDAwMDAwMzAwMDAwMDAzMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAAAAAAEEQAAC4="}';


array_push($base64Array,array(  "ID"                => $id,
                                "timestamp"         => $timestamp,
                                "data_validated"    => $data_validated,
                                "type"              => $type,
                                "site_id"           => $site_id,
                                "date_time"         => $date_time,
                                "device_id"         => $device_id,
                                "status"            => base64_encode($status),
                                "raw_data"          => base64_encode($raw_data),
                                "indoor_type"       => $indoor_type)   );

    $jsonArray = json_encode($base64Array);
//var_dump($base64Array);
//var_dump($jsonArray);
?>

<h2>Equipment Data</h2>
<script src="base64.js"></script>

<script>
    var jsonArray = <?php echo $jsonArray?>;

    console.log(jsonArray);

    var text = jsonArray.map(function(e){
        return JSON.stringify(e);
    });
    console.log(text);
    var obj = JSON.parse(text);

    console.log(obj);

    var byteArray = Base64Binary.decodeArrayBuffer(obj.status);
    //document.write(byteArray);




    var int8View = new Uint8Array(byteArray);
    var dataView = new DataView(byteArray);

    document.write("<table align='center'>");
    var type_str = "";
    if(obj.type == 0)  {type_str = "Periodic";}
    if(obj.type == 1)  {type_str = "Fault";}

    document.write("<tr><td>Type</td><td>"+type_str+"</td></tr>"+
                    "<tr><td>Site Id</td><td>"+obj.site_id+"</td></tr>"+
                    "<tr><td>Date Time</td><td>"+obj.date_time+"</td></tr>"+
                    "<tr><td>Device Id</td><td>"+obj.device_id+"</td></tr>");

    //document.write(int8View);

if(obj.device_id == 1)
{
    if(obj.data_validated != 2 )
    {

//        var i = 5;
//        console.log(int8View[i]&0x01);
//        console.log(int8View[i]&0x02);
//        console.log(int8View[i]&0x04);
//        console.log(int8View[i]&0x08);
//        console.log(int8View[i]&0x10);
//        console.log(int8View[i]&0x20);
//        console.log(int8View[i]&0x40);
//        console.log(int8View[i]&0x80);

        document.write ("<tr><td>Smoke Fire Alarm</td><td");
        //0x0000000000 Smoke fire    0 means No alarm,  1 means Alarm
        if ((int8View[4] & 0x01) == false)
            document.write( " ><img id='led_white' src='/media/img_trans.gif'>");

    else
        document.write( " ><img id='led_red' src='/media/img_trans.gif'>");

        document.write( "</td></tr>");


        if (obj.indoor_type == 0)	//Door Alarm Only Applicable if Indoor
        {
            document.write( "<tr><td>Door</td><td");
            //$FLAG_01_DOOR 	= 0x0000000002;	//Door Open     0  means  Door Close , 1 Means  Door open  
            if ((int8View[4] & 0x02) == false)
                document.write( "  ><img id='led_white' src='/media/img_trans.gif'><br>");
        else
            document.write( "  ><img id='led_red' src='/media/img_trans.gif'>");

            document.write( "</td></tr>");
        }


        document.write( "<tr><td>Mode</td><td");
        //$FLAG_02_AUTO 	= 0x0000000004;	//Auto/Man Mode 0 Means  Auto Mode, 1 Means  Man Mode
        if ((int8View[4] & 0x04) == false)
            document.write( "  >Auto");
    else
        document.write( "  >Manual");
        document.write( "</td></tr>");

        document.write( "<tr><td>Load</td><td>");
        //$FLAG_034_LOAD 	= 0x0000000018;	//00    :Load on EB,01: Load on DG, 10: Load on site Battery   11: Not used
        if ((int8View[4] & 0x08) == false)
        {
            if ((int8View[4] & 0x10) == false)
                document.write( "[- Load on EB -]<br>");				//00
        else
            document.write( "[- Load on site Battery -]<br>");	//10
        }
        else
        {
            if ((int8View[4] & 0x10) == false)
                document.write( "[- Load on DG -]<br>");				//01
        else
            document.write( "[- Not Used -]<br>");	//11
        }
        document.write( "</td></tr>");

        document.write( "<tr><td>Mains Fail</td><td");
        //$FLAG_05_MAINS 	= 0x0000000020;	//Mains fail	1 Means main Fail,  0 Means  Main healthy
        if ((int8View[4] & 0x20) == false)
            document.write( "  ><img id='led_white' src='/media/img_trans.gif'>");
    else
        document.write( "  ><img id='led_red' src='/media/img_trans.gif'>");
        document.write( "</td></tr>");

        document.write( "<tr><td>Site Battery Low</td><td");
        //$FLAG_06_BATT 	= 0x0000000040;	//Site battery Low  1 Means Site Battery Low ,0 Means  Battery Ok
        if ((int8View[4] & 0x40) == false)
            document.write( "  ><img id='led_white' src='/media/img_trans.gif'>");
    else
        document.write( "  ><img id='led_red' src='/media/img_trans.gif'>");
        document.write( "</td></tr>");


        document.write( "<tr><td>Room Temperature</td><td");
        //$FLAG_07_TEMP 	= 0x0000000080;	//Room Temperature  1 Means high Temperature, 0 Means Normal temp
        if ((int8View[4] & 0x80) == false)
            document.write( "  ><img id='led_white' src='/media/img_trans.gif'>");
    else
        document.write( "  ><img id='led_red' src='/media/img_trans.gif'>");
        document.write( "</td></tr>");


        document.write( "<tr><td>LLOP</td><td");
        //$FLAG_08_LLOP 	= 0x0000000100;	//LLOP
        if ((int8View[3] & 0x01) == false)
            document.write( "  ><img id='led_white' src='/media/img_trans.gif'>");
    else
        document.write( "  ><img id='led_red' src='/media/img_trans.gif'>");
        document.write( "</td></tr>");


        document.write( "<tr><td>HCT/HWT</td><td");
        //$FLAG_09_HCT 	= 0x0000000200;	//HCT/HWT
        if ((int8View[3] & 0x02) == false)
            document.write( "  ><img id='led_white' src='/media/img_trans.gif'>");
    else
        document.write( "  ><img id='led_red' src='/media/img_trans.gif'>");
        document.write( "</td></tr>");


        document.write( "<tr><td>Alternator Fault</td><td");
        //$FLAG_10_ALT 	= 0x0000000400;	//Alternator Fault
        if ((int8View[3] & 0x04) == false)
            document.write( "  ><img id='led_white' src='/media/img_trans.gif'>");
    else
        document.write( "  ><img id='led_red' src='/media/img_trans.gif'>");
        document.write( "</td></tr>");


        document.write( "<tr><td>DG Over Speed</td><td");
        //$FLAG_11_DG 	= 0x0000000800;	//DG Over Speed
        if ((int8View[3] & 0x08) == false)
            document.write( "  ><img id='led_white' src='/media/img_trans.gif'>");
    else
        document.write( "  ><img id='led_red' src='/media/img_trans.gif'>");
        document.write( "</td></tr>");


        document.write( "<tr><td>DG Over Load</td><td");
        //$FLAG_12_DG 	= 0x0000001000;	//DG Over Load
        if ((int8View[3] & 0x10) == false)
            document.write( "  ><img id='led_white' src='/media/img_trans.gif'>");
    else
        document.write( "  ><img id='led_red' src='/media/img_trans.gif'>");
        document.write( "</td></tr>");


        document.write( "<tr><td>DG Low Fuel</td><td");
        //$FLAG_13_DG 	= 0x0000002000;	//DG Low Fuel
        if ((int8View[3] & 0x20) == false)
            document.write( "  ><img id='led_white' src='/media/img_trans.gif'>");
    else
        document.write( "  ><img id='led_red' src='/media/img_trans.gif'>");
        document.write( "</td></tr>");


        document.write( "<tr><td>DG Start Fail</td><td");
        //$FLAG_14_DG 	= 0x0000004000;	//DG Start Fail
        if ((int8View[3] & 0x40) == false)
            document.write( "  ><img id='led_white' src='/media/img_trans.gif'>");
    else
        document.write( "  ><img id='led_red' src='/media/img_trans.gif'>");
        document.write( "</td></tr>");


        document.write( "<tr><td>DG Stop Fail</td><td");
        //$FLAG_15_DG 	= 0x0000008000;	//DG Stop Fail
        if ((int8View[3] & 0x80) == false)
            document.write( "  ><img id='led_white' src='/media/img_trans.gif'>");
    else
        document.write( "  ><img id='led_red' src='/media/img_trans.gif'>");
        document.write( "</td></tr>");


        document.write( "<tr><td>DG Battery</td><td");
        //$FLAG_16_DG 	= 0x0000010000;	//DG battery Low
        if ((int8View[2] & 0x01) == false)
            document.write( "  ><img id='led_white' src='/media/img_trans.gif'>");
    else
        document.write( "  ><img id='led_red' src='/media/img_trans.gif'>");
        document.write( "</td></tr>");


        document.write( "<tr><td>LCU Fail</td><td");
        //$FLAG_17_LCU 	= 0x0000020000;	//LCU fail
        if ((int8View[2] & 0x02) == false)
            document.write( "  ><img id='led_green' src='/media/img_trans.gif'>");
    else
        document.write( "  ><img id='led_red' src='/media/img_trans.gif'>");
        document.write( "</td></tr>");


        document.write( "<tr><td>Rectifier Fail</td><td");
        //$FLAG_18_RECT 	= 0x0000040000;	//Rectifier Fail
        if ((int8View[2] & 0x04) == false)
            document.write( "  ><img id='led_green' src='/media/img_trans.gif'>");
    else
        document.write( "  ><img id='led_red' src='/media/img_trans.gif'>");
        document.write( "</td></tr>");


        document.write( "<tr><td>Multi Rectifier Fail</td><td");
        //$FLAG_19_RECT 	= 0x0000080000;	//Multi Rectifier Fail
        if ((int8View[2] & 0x08) == false)
            document.write( "  ><img id='led_green' src='/media/img_trans.gif'>");
    else
        document.write( "  ><img id='led_red' src='/media/img_trans.gif'>");
        document.write( "</td></tr>");


        document.write( "<tr><td>LVD Trip</td><td");
        //$FLAG_20_LVD 	= 0x0000100000;	//LVD TRIP
        if ((int8View[2] & 0x10) == false)
            document.write( "  ><img id='led_green' src='/media/img_trans.gif'>");
    else
        document.write( "  ><img id='led_red' src='/media/img_trans.gif'>");
        document.write( "</td></tr>");


        document.write( "<tr><td>LVD BY PASS</td><td");
        //$FLAG_21_LVD 	= 0x0000200000;	//LVD BY pass
        if ((int8View[2] & 0x20) == false)
            document.write( "  ><img id='led_green' src='/media/img_trans.gif'>");
    else
        document.write( "  ><img id='led_red' src='/media/img_trans.gif'>");
        document.write( "</td></tr>");


        //$FLAG_22_RES 	= 0x0000400000;	//Reserved
        //$FLAG_23_RES 	= 0x0000800000;	//Reserved
        //$FLAG_24_RES 	= 0x0001000000;	//Reserved
        //$FLAG_25_RES 	= 0x0002000000;	//Reserved
        //$FLAG_26_RES 	= 0x0004000000;	//Reserved
        //$FLAG_27_RES 	= 0x0008000000;	//Reserved
        //$FLAG_28_RES 	= 0x0010000000;	//Reserved
        //$FLAG_29_RES 	= 0x0020000000;	//Reserved
        //$FLAG_30_RES 	= 0x0040000000;	//Reserved
        //$FLAG_31_RES 	= 0x0080000000;	//Reserved

        document.write( "<tr><td>DG OFF</td><td");
        //$FLAG_32_DG 	= 0x0100000000;	//DG OFF
        if ((int8View[0] & 0x01) == false)
            document.write( "  ><img id='led_white' src='/media/img_trans.gif'>");
    else
        document.write( "  ><img id='led_green' src='/media/img_trans.gif'>");
        document.write( "</td></tr>");


        document.write( "<tr><td>DG ON</td><td");
        //$FLAG_33_DG 	= 0x0200000000;	//DG ON
        if ((int8View[0] & 0x02) == false)
            document.write( "  ><img id='led_white' src='/media/img_trans.gif'>");
    else
        document.write( "  ><img id='led_green' src='/media/img_trans.gif'>");
        document.write( "</td></tr>");


        document.write( "<tr><td>DG Cranking</td><td");
        //$FLAG_34_DG 	= 0x0400000000;	//DG Cranking
        if ((int8View[0] & 0x04) == false)
            document.write( "  ><img id='led_white' src='/media/img_trans.gif'>");
    else
        document.write( "  ><img id='led_green' src='/media/img_trans.gif'>");
        document.write( "</td></tr>");


        document.write( "<tr><td>DG Start in Progress</td><td");
        //$FLAG_35_DG 	= 0x0800000000;	//DG Start in Progress
        if ((int8View[0] & 0x08) == false)
            document.write( "  ><img id='led_white' src='/media/img_trans.gif'>");
    else
        document.write( "  ><img id='led_green' src='/media/img_trans.gif'>");
        document.write( "</td></tr>");


        document.write( "<tr><td>DG Cool Down (Idle Running)</td><td");
        //$FLAG_36_DG 	= 0x1000000000;	//DG Cool Down (Idle Running)
        if ((int8View[0] & 0x10) == false)
            document.write( "  ><img id='led_white' src='/media/img_trans.gif'>");
    else
        document.write( "  ><img id='led_green' src='/media/img_trans.gif'>");
        document.write( "</td></tr>");


        document.write( "<tr><td>DG STOP  Normally</td><td");
        //$FLAG_37_DG 	= 0x2000000000;	//DG STOP  Normally
        if ((int8View[0] & 0x20) == false)
            document.write( "  ><img id='led_white' src='/media/img_trans.gif'>");
    else
        document.write( "  ><img id='led_green' src='/media/img_trans.gif'>");
        document.write( "</td></tr>");


        document.write( "<tr><td>DG STOP due to Fault</td><td");
        //$FLAG_38_DG 	= 0x8000000000;	//DG STOP due to Fault
        if ((int8View[0] & 0x40) == false)
            document.write( "  ><img id='led_white' src='/media/img_trans.gif'>");
    else
        document.write( "  ><img id='led_green' src='/media/img_trans.gif'>");
        document.write( "</td></tr>");


        document.write( "<tr><td>DG Stop Due to Maximum Run Expiry</td><td");
        //$FLAG_39_DG 	= 0x8000000000;	//DG Stop Due to Maximum Run Exipary 
        if ((int8View[0] & 0x80) == false)
            document.write( "  ><img id='led_white' src='/media/img_trans.gif'>");
    else
        document.write( "  ><img id='led_green' src='/media/img_trans.gif'>");
        document.write( "</td></tr>");
        
        
        
//        console.log("hello");

//        document.getElementById("demo").innerHTML =
//            obj.ID + "<br>" +
//            obj.status + "<br>x<br>";



        

        //console.log(base64toHEX(obj.status));

    }
    else
    {
        //Double DG

        document.write( "<tr><td>Smoke Fire Alarm</td><td");
        //0x0000000000 Smoke fire    0 means No alarm,  1 means Alarm
        if ((int8View[5] & 0x01) == false)
            document.write( " ><img id='led_white' src='/media/img_trans.gif'>");
    else
        document.write( " ><img id='led_red' src='/media/img_trans.gif'>");

        document.write( "</td></tr>");

        document.write( "<tr><td>Door</td><td");
        //$FLAG_01_DOOR 	= 0x0000000002;	//Door Open     0  means  Door Close , 1 Means  Door open  
        if ((int8View[5] & 0x02) == false)
            document.write( "  ><img id='led_white' src='/media/img_trans.gif'><br>");
    else
        document.write( "  ><img id='led_red' src='/media/img_trans.gif'>");

        document.write( "</td></tr>");


        document.write( "<tr><td>Mode</td><td");
        //$FLAG_02_AUTO 	= 0x0000000004;	//Auto/Man Mode 0 Means  Auto Mode, 1 Means  Man Mode
        if ((int8View[5] & 0x04) == false)
            document.write( "  >Auto");
    else
        document.write( "  >Manual");
        document.write( "</td></tr>");

        document.write( "<tr><td>Load</td><td>");
        //$FLAG_034_LOAD 	= 0x0000000018;	
        //000    :Load on EB,001: Load on DG1, 011: Load on site Battery   010: Load on DG2, 111 ( site in battery with emg case) other’s Not used

      var  sub_data = (int8View[5] & 0x38) / 8;

        if (sub_data == 0)
            document.write( "[- Load on EB -]<br>");			//000
    else if (sub_data == 1)
        document.write( "[- Load on DG 1 -]<br>");			//001
    else if (sub_data == 2)
        document.write( "[- Load on DG 2 -]<br>");			//010
    else if (sub_data == 3)
        document.write( "[- Load on site Battery -]<br>");	//011
    else if (sub_data == 7)
        document.write( "[- Load on site Battery (emg case) -]<br>");	//111
    else
        document.write( "[- Not Used -]<br>");				//remaining

        document.write( "</td></tr>");

        document.write( "<tr><td>Mains Fail</td><td");
        //$FLAG_05_MAINS 	= 0x0000000020;	//Mains fail	1 Means main Fail,  0 Means  Main healthy
        if ((int8View[5] & 0x40) == false)
            document.write( "  ><img id='led_white' src='/media/img_trans.gif'>");
    else
        document.write( "  ><img id='led_red' src='/media/img_trans.gif'>");
        document.write( "</td></tr>");

        document.write( "<tr><td>Site Battery Low</td><td");
        //$FLAG_06_BATT 	= 0x0000000040;	//Site battery Low  1 Means Site Battery Low ,0 Means  Battery Ok
        if ((int8View[5] & 0x80) == false)
            document.write( "  ><img id='led_white' src='/media/img_trans.gif'>");
    else
        document.write( "  ><img id='led_red' src='/media/img_trans.gif'>");
        document.write( "</td></tr>");


        document.write( "<tr><td>Room Temperature</td><td");
        //$FLAG_07_TEMP 	= 0x0000000080;	//Room Temperature  1 Means high Temperature, 0 Means Normal temp
        if ((int8View[4] & 0x01) == false)
            document.write( "  ><img id='led_white' src='/media/img_trans.gif'>");
    else
        document.write( "  ><img id='led_red' src='/media/img_trans.gif'>");
        document.write( "</td></tr>");


        document.write( "<tr><td>DG 1 LLOP</td><td");
        //$FLAG_08_LLOP 	= 0x0000000100;	//LLOP
        if ((int8View[4] & 0x02) == false)
            document.write( "  ><img id='led_white' src='/media/img_trans.gif'>");
    else
        document.write( "  ><img id='led_red' src='/media/img_trans.gif'>");
        document.write( "</td></tr>");


        document.write( "<tr><td>DG 1 HCT/HWT</td><td");
        //$FLAG_09_HCT 	= 0x0000000200;	//HCT/HWT
        if ((int8View[4] & 0x04) == false)
            document.write( "  ><img id='led_white' src='/media/img_trans.gif'>");
    else
        document.write( "  ><img id='led_red' src='/media/img_trans.gif'>");
        document.write( "</td></tr>");


        document.write( "<tr><td>DG 1 Alternate Fault</td><td");
        //$FLAG_10_ALT 	= 0x0000000400;	//Alternate Fault
        if ((int8View[4] & 0x08) == false)
            document.write( "  ><img id='led_white' src='/media/img_trans.gif'>");
    else
        document.write( "  ><img id='led_red' src='/media/img_trans.gif'>");
        document.write( "</td></tr>");


        document.write( "<tr><td>DG 1 Over Speed</td><td");
        //$FLAG_11_DG 	= 0x0000000800;	//DG Over Speed
        if ((int8View[4] & 0x10) == false)
            document.write( "  ><img id='led_white' src='/media/img_trans.gif'>");
    else
        document.write( "  ><img id='led_red' src='/media/img_trans.gif'>");
        document.write( "</td></tr>");


        document.write( "<tr><td>DG 1 Over Load</td><td");
        //$FLAG_12_DG 	= 0x0000001000;	//DG Over Load
        if ((int8View[4] & 0x20) == false)
            document.write( "  ><img id='led_white' src='/media/img_trans.gif'>");
    else
        document.write( "  ><img id='led_red' src='/media/img_trans.gif'>");
        document.write( "</td></tr>");


        document.write( "<tr><td>DG 1 Low Fuel</td><td");
        //$FLAG_13_DG 	= 0x0000002000;	//DG Low Fuel
        if ((int8View[4] & 0x40) == false)
            document.write( "  ><img id='led_white' src='/media/img_trans.gif'>");
    else
        document.write( "  ><img id='led_red' src='/media/img_trans.gif'>");
        document.write( "</td></tr>");


        document.write( "<tr><td>DG 1 Start Fail</td><td");
        //$FLAG_14_DG 	= 0x0000004000;	//DG Start Fail
        if ((int8View[4] & 0x80) == false)
            document.write( "  ><img id='led_white' src='/media/img_trans.gif'>");
    else
        document.write( "  ><img id='led_red' src='/media/img_trans.gif'>");
        document.write( "</td></tr>");


        document.write( "<tr><td>DG 1 Stop Fail</td><td");
        //$FLAG_15_DG 	= 0x0000008000;	//DG Stop Fail
        if ((int8View[3] & 0x01) == false)
            document.write( "  ><img id='led_white' src='/media/img_trans.gif'>");
    else
        document.write( "  ><img id='led_red' src='/media/img_trans.gif'>");
        document.write( "</td></tr>");


        document.write( "<tr><td>DG 1 Battery</td><td");
        //$FLAG_16_DG 	= 0x0000010000;	//DG battery Low
        if ((int8View[3] & 0x02) == false)
            document.write( "  ><img id='led_white' src='/media/img_trans.gif'>");
    else
        document.write( "  ><img id='led_red' src='/media/img_trans.gif'>");
        document.write( "</td></tr>");


        document.write( "<tr><td>LCU Fail</td><td");
        //$FLAG_17_LCU 	= 0x0000020000;	//LCU fail
        if ((int8View[3] & 0x04) == false)
            document.write( "  ><img id='led_green' src='/media/img_trans.gif'>");
    else
        document.write( "  ><img id='led_red' src='/media/img_trans.gif'>");
        document.write( "</td></tr>");


        document.write( "<tr><td>Rectifier Fail</td><td");
        //$FLAG_18_RECT 	= 0x0000040000;	//Rectifier Fail
        if ((int8View[3] & 0x08) == false)
            document.write( "  ><img id='led_green' src='/media/img_trans.gif'>");
    else
        document.write( "  ><img id='led_red' src='/media/img_trans.gif'>");
        document.write( "</td></tr>");


        document.write( "<tr><td>Multi Rectifier Fail</td><td");
        //$FLAG_19_RECT 	= 0x0000080000;	//Multi Rectifier Fail
        if ((int8View[3] & 0x10) == false)
            document.write( "  ><img id='led_green' src='/media/img_trans.gif'>");
    else
        document.write( "  ><img id='led_red' src='/media/img_trans.gif'>");
        document.write( "</td></tr>");


        document.write( "<tr><td>LVD Trip</td><td");
        //$FLAG_20_LVD 	= 0x0000100000;	//LVD TRIP
        if ((int8View[3] & 0x20) == false)
            document.write( "  ><img id='led_green' src='/media/img_trans.gif'>");
    else
        document.write( "  ><img id='led_red' src='/media/img_trans.gif'>");
        document.write( "</td></tr>");


        document.write( "<tr><td>LVD BY PASS</td><td");
        //$FLAG_21_LVD 	= 0x0000200000;	//LVD BY pass
        if ((int8View[3] & 0x40) == false)
            document.write( "  ><img id='led_green' src='/media/img_trans.gif'>");
    else
        document.write( "  ><img id='led_red' src='/media/img_trans.gif'>");
        document.write( "</td></tr>");

        ////////////////////////////
        ////////////////////////////
        ////////////////////////////

        document.write( "<tr><td>DG 2 LLOP</td><td");
        //$FLAG_08_LLOP 	= 0x0000000100;	//LLOP
        if ((int8View[3] & 0x80) == false)
            document.write( "  ><img id='led_white' src='/media/img_trans.gif'>");
    else
        document.write( "  ><img id='led_red' src='/media/img_trans.gif'>");
        document.write( "</td></tr>");


        document.write( "<tr><td>DG 2 HCT/HWT</td><td");
        //$FLAG_09_HCT 	= 0x0000000200;	//HCT/HWT
        if ((int8View[2] & 0x01) == false)
            document.write( "  ><img id='led_white' src='/media/img_trans.gif'>");
    else
        document.write( "  ><img id='led_red' src='/media/img_trans.gif'>");
        document.write( "</td></tr>");


        document.write( "<tr><td>DG 2 Alternate Fault</td><td");
        //$FLAG_10_ALT 	= 0x0000000400;	//Alternate Fault
        if ((int8View[2] & 0x02) == false)
            document.write( "  ><img id='led_white' src='/media/img_trans.gif'>");
    else
        document.write( "  ><img id='led_red' src='/media/img_trans.gif'>");
        document.write( "</td></tr>");


        document.write( "<tr><td>DG 2 Over Speed</td><td");
        //$FLAG_11_DG 	= 0x0000000800;	//DG Over Speed
        if ((int8View[2] & 0x04) == false)
            document.write( "  ><img id='led_white' src='/media/img_trans.gif'>");
    else
        document.write( "  ><img id='led_red' src='/media/img_trans.gif'>");
        document.write( "</td></tr>");


        document.write( "<tr><td>DG 2 Over Load</td><td");
        //$FLAG_12_DG 	= 0x0000001000;	//DG Over Load
        if ((int8View[2] & 0x08) == false)
            document.write( "  ><img id='led_white' src='/media/img_trans.gif'>");
    else
        document.write( "  ><img id='led_red' src='/media/img_trans.gif'>");
        document.write( "</td></tr>");


        document.write( "<tr><td>DG 2 Low Fuel</td><td");
        //$FLAG_13_DG 	= 0x0000002000;	//DG Low Fuel
        if ((int8View[2] & 0x10) == false)
            document.write( "  ><img id='led_white' src='/media/img_trans.gif'>");
    else
        document.write( "  ><img id='led_red' src='/media/img_trans.gif'>");
        document.write( "</td></tr>");


        document.write( "<tr><td>DG 2 Start Fail</td><td");
        //$FLAG_14_DG 	= 0x0000004000;	//DG Start Fail
        if ((int8View[2] & 0x20) == false)
            document.write( "  ><img id='led_white' src='/media/img_trans.gif'>");
    else
        document.write( "  ><img id='led_red' src='/media/img_trans.gif'>");
        document.write( "</td></tr>");


        document.write( "<tr><td>DG 2 Stop Fail</td><td");
        //$FLAG_15_DG 	= 0x0000008000;	//DG Stop Fail
        if ((int8View[2] & 0x40) == false)
            document.write( "  ><img id='led_white' src='/media/img_trans.gif'>");
    else
        document.write( "  ><img id='led_red' src='/media/img_trans.gif'>");
        document.write( "</td></tr>");


        document.write( "<tr><td>DG 2 Battery</td><td");
        //$FLAG_16_DG 	= 0x0000010000;	//DG battery Low
        if ((int8View[2] & 0x80) == false)
            document.write( "  ><img id='led_white' src='/media/img_trans.gif'>");
    else
        document.write( "  ><img id='led_red' src='/media/img_trans.gif'>");
        document.write( "</td></tr>");


        document.write( "<tr><td>DG 1 OFF</td><td");
        //$FLAG_32_DG 	= 0x0100000000;	//DG OFF
        if ((int8View[1] & 0x01) == false)
            document.write( "  ><img id='led_white' src='/media/img_trans.gif'>");
    else
        document.write( "  ><img id='led_green' src='/media/img_trans.gif'>");
        document.write( "</td></tr>");


        document.write( "<tr><td>DG 1 ON</td><td");
        //$FLAG_33_DG 	= 0x0200000000;	//DG ON
        if ((int8View[1] & 0x02) == false)
            document.write( "  ><img id='led_white' src='/media/img_trans.gif'>");
    else
        document.write( "  ><img id='led_green' src='/media/img_trans.gif'>");
        document.write( "</td></tr>");


        document.write( "<tr><td>DG 1 Cranking</td><td");
        //$FLAG_34_DG 	= 0x0400000000;	//DG Cranking
        if ((int8View[1] & 0x04) == false)
            document.write( "  ><img id='led_white' src='/media/img_trans.gif'>");
    else
        document.write( "  ><img id='led_green' src='/media/img_trans.gif'>");
        document.write( "</td></tr>");


        document.write( "<tr><td>DG 1 Start in Progress</td><td");
        //$FLAG_35_DG 	= 0x0800000000;	//DG Start in Progress
        if ((int8View[1] & 0x08) == false)
            document.write( "  ><img id='led_white' src='/media/img_trans.gif'>");
    else
        document.write( "  ><img id='led_green' src='/media/img_trans.gif'>");
        document.write( "</td></tr>");


        document.write( "<tr><td>DG 1 Cool Down (Idle Running)</td><td");
        //$FLAG_36_DG 	= 0x1000000000;	//DG Cool Down (Idle Running)
        if ((int8View[1] & 0x10) == false)
            document.write( "  ><img id='led_white' src='/media/img_trans.gif'>");
    else
        document.write( "  ><img id='led_green' src='/media/img_trans.gif'>");
        document.write( "</td></tr>");


        document.write( "<tr><td>DG 1 STOP  Normally</td><td");
        //$FLAG_37_DG 	= 0x2000000000;	//DG STOP  Normally
        if ((int8View[1] & 0x20) == false)
            document.write( "  ><img id='led_white' src='/media/img_trans.gif'>");
    else
        document.write( "  ><img id='led_green' src='/media/img_trans.gif'>");
        document.write( "</td></tr>");


        document.write( "<tr><td>DG 1 STOP due to Fault</td><td");
        //$FLAG_38_DG 	= 0x8000000000;	//DG STOP due to Fault
        if ((int8View[1] & 0x40) == false)
            document.write( "  ><img id='led_white' src='/media/img_trans.gif'>");
    else
        document.write( "  ><img id='led_green' src='/media/img_trans.gif'>");
        document.write( "</td></tr>");


        document.write( "<tr><td>DG 1 Stop Due to Maximum Run Expiry</td><td");
        //$FLAG_39_DG 	= 0x8000000000;	//DG Stop Due to Maximum Run Exipary 
        if ((int8View[1] & 0x80) == false)
            document.write( "  ><img id='led_white' src='/media/img_trans.gif'>");
    else
        document.write( "  ><img id='led_green' src='/media/img_trans.gif'>");
        document.write( "</td></tr>");


        ///
        document.write( "<tr><td>DG 2 OFF</td><td");
        //$FLAG_32_DG 	= 0x0100000000;	//DG OFF
        if ((int8View[0] & 0x01) == false)
            document.write( "  ><img id='led_white' src='/media/img_trans.gif'>");
    else
        document.write( "  ><img id='led_green' src='/media/img_trans.gif'>");
        document.write( "</td></tr>");


        document.write( "<tr><td>DG 2 ON</td><td");
        //$FLAG_33_DG 	= 0x0200000000;	//DG ON
        if ((int8View[0] & 0x02) == false)
            document.write( "  ><img id='led_white' src='/media/img_trans.gif'>");
    else
        document.write( "  ><img id='led_green' src='/media/img_trans.gif'>");
        document.write( "</td></tr>");


        document.write( "<tr><td>DG 2 Cranking</td><td");
        //$FLAG_34_DG 	= 0x0400000000;	//DG Cranking
        if ((int8View[0] & 0x04) == false)
            document.write( "  ><img id='led_white' src='/media/img_trans.gif'>");
    else
        document.write( "  ><img id='led_green' src='/media/img_trans.gif'>");
        document.write( "</td></tr>");


        document.write( "<tr><td>DG 2 Start in Progress</td><td");
        //$FLAG_35_DG 	= 0x0800000000;	//DG Start in Progress
        if ((int8View[0] & 0x08) == false)
            document.write( "  ><img id='led_white' src='/media/img_trans.gif'>");
    else
        document.write( "  ><img id='led_green' src='/media/img_trans.gif'>");
        document.write( "</td></tr>");


        document.write( "<tr><td>DG 2 Cool Down (Idle Running)</td><td");
        //$FLAG_36_DG 	= 0x1000000000;	//DG Cool Down (Idle Running)
        if ((int8View[0] & 0x10) == false)
            document.write( "  ><img id='led_white' src='/media/img_trans.gif'>");
    else
        document.write( "  ><img id='led_green' src='/media/img_trans.gif'>");
        document.write( "</td></tr>");


        document.write( "<tr><td>DG 2 STOP  Normally</td><td");
        //$FLAG_37_DG 	= 0x2000000000;	//DG STOP  Normally
        if ((int8View[0] & 0x20) == false)
            document.write( "  ><img id='led_white' src='/media/img_trans.gif'>");
    else
        document.write( "  ><img id='led_green' src='/media/img_trans.gif'>");
        document.write( "</td></tr>");


        document.write( "<tr><td>DG 2 STOP due to Fault</td><td");
        //$FLAG_38_DG 	= 0x8000000000;	//DG STOP due to Fault
        if ((int8View[0] & 0x40) == false)
            document.write( "  ><img id='led_white' src='/media/img_trans.gif'>");
    else
        document.write( "  ><img id='led_green' src='/media/img_trans.gif'>");
        document.write( "</td></tr>");


        document.write( "<tr><td>DG 2 Stop Due to Maximum Run Expiry</td><td");
        //$FLAG_39_DG 	= 0x8000000000;	//DG Stop Due to Maximum Run Expiry 
        if ((int8View[0] & 0x80) == false)
            document.write( "  ><img id='led_white' src='/media/img_trans.gif'>");
    else
        document.write( "  ><img id='led_green' src='/media/img_trans.gif'>");
        document.write( "</td></tr>");

    }

    document.write( "</table>");
    }


//else if(obj.device_id == )


</script>





</body>