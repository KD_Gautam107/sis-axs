 <?php 
 	
	include 'common-alarms.php';
	
	function ReportAlarmSiteHistory($site_id_to_report)
	{
		include 'dbinc.php';
		
		$mysqli = new mysqli("$mysql_hostname", "$mysql_user", "$mysql_password", "$mysql_database");
		if (mysqli_connect_errno()) {
			printf("Connect failed: %s\n", mysqli_connect_error());
			exit();
		}

		$sql = "SELECT `id`, `site`, `quanta_id`, `alarm_flag`, `alarm_flag_version`, `start_time`, `last_message_level`, `last_message_time` 
					FROM `sitealarmhistory` WHERE site=\"$site_id_to_report\"";

		//echo "SQL: $sql<br>";	
		$result = $mysqli->query($sql);
		//var_dump($result);
		
		$site_in_alarm = 0;
		$site_alarms_text = "";
		
		//printf("\nSQL ERROR: %s\n", $mysqli->error);
		
		if ($result !== false)
		{
			$row_count = $result->num_rows;
			
			if ($row_count == 0)
			{
				
			}
			else
			{
				while($data = $result->fetch_row())
				{
					$site_in_alarm++;
					
					$site_details = GetDetailsFromSiteId($data[1]);
					//$alarms = GetAlarmBits($data[3]);
					$alarms = GetAlarmBitsEx($data[3], $site_details[11], $site_details[29]);
					
					//($alarms, $site_id, $site_name, $alarm_time)
					$text = GetAlarmText($alarms, $data[1], "", $data[5]);
					
					$site_alarms_text .= $text."<br>";
				}
			}
		}

				
		$result->close();	
		$mysqli->close();
		
		$retval = "";
		
		if ($site_in_alarm == 0)
		{
			$retval = "<p>No Alarm History</p>";
		}
		else
		{
			$retval = $site_in_alarm." <h3>Alarm History</h3>".$site_alarms_text;
		}
	
		return $retval;
	}
	
	function ReportSiteAlarmHistoryForDay($site_id_to_report, $date, $date1)
	{
		include 'dbinc.php';
		
		$sql_sdt = "\"".$date1." 18:30:00"."\"";
		$sql_edt = "\"".$date." 18:29:59"."\"";
		
		$mysqli = new mysqli("$mysql_hostname", "$mysql_user", "$mysql_password", "$mysql_database");
		if (mysqli_connect_errno()) {
			printf("Connect failed: %s\n", mysqli_connect_error());
			exit();
		}

		$sql = "SELECT `id`, `date_time`, `site`, `level`, `mobile`, `message` 
					FROM `sitealarmmessagetracking` 
					WHERE (site=\"$site_id_to_report\") AND (date_time between $sql_sdt and $sql_edt)";

		//echo "SQL: $sql<br>";	
		$result = $mysqli->query($sql);
		//var_dump($result);
		
		$site_in_alarm = 0;
		$site_alarms_text = "";
		
		//printf("\nSQL ERROR: %s\n", $mysqli->error);
		
		if ($result !== false)
		{
			$row_count = $result->num_rows;
			
			if ($row_count == 0)
			{
				
			}
			else
			{
			
				$site_alarms_text .= '<table rules="all" style="border-color: #666;" cellpadding="10">';
				$site_alarms_text .= "<tr><td><strong>Date</strong> </td><td><strong>Status</strong> </td></tr>";
			
				while($data = $result->fetch_row())
				{
					$site_in_alarm++;
					
					$newtimestamp = strtotime($data[1].' + 5 hours 30 minute');
					$localdate = date('Y-m-d H:i', $newtimestamp);
					
					if ($data[3] == 0)
						//$text = $data[1]." Alarms Cleared";
						$text = "<tr style='background-color:#dff0d8'><td>$localdate</td><td>Alarm Cleared</td></tr>";
					else if ($data[3] == 1)
						//$text = $data[1]." ".$data[5];
						$text = "<tr style='background-color:#fcf8e3'><td>$localdate</td><td>$data[5]</td></tr>";
					else
						continue;
						
					$site_alarms_text .= $text;
				}
				
				$site_alarms_text .= "</table>";
			}
		}

				
		$result->close();	
		$mysqli->close();
		
		$retval = "";
		
		if ($site_in_alarm == 0)
		{
			$retval = "<p>No Alarm triggered on this day.</p>";
		}
		else
		{
			$retval = "<h3>Alarm History</h3>".$site_alarms_text;
		}
	
		return $retval;
	}
	
	
	function ReportPowerSupply($site_id_to_report, $date)
	{
		$hours_interval = 24;
		$message = "";
		
		$message .= '<table rules="all" style="border-color: #666;" cellpadding="10">';
		$message .= "<tr><td><strong>Date Range</strong> </td><td><strong>Mains Running</strong> </td><td><strong>DG Running</strong> </td><td><strong>Battery Running</strong> </td></tr>";
		
		$site_id = $site_id_to_report;
		//$site_name = "TODO";
		
		//echo $site_id."<br>";
		$sum = null;
		
		$array = GetDaySiteStatsFromId($site_id, $date);
		
		if ($array == null)
			return "Insufficient data.";
		$sum = AddUp($array, $sum);
		
		$adj_time_total = abs($sum[2])+abs($sum[1])+abs($sum[3])+abs($sum[4]);
		$adj_time_diff = abs($array[10]) - $adj_time_total;
		
		$adj_four_percent = abs($array[10]) * 0.04;
		
		$adj_offset = 0;
		$adj_style = "";
		if (($adj_time_diff > 0) && ($adj_time_diff < $adj_four_percent))
		{
			$adj_offset = $adj_time_diff;
			$adj_style = "style='font-style: italic;'";
		}
		
		if (($sum[1] > 27.0) || ($sum[2] > 27.0) || ($sum[3] > 27.0) || ($sum[1] < 0.0) || ($sum[2] < 0.0) || ($sum[3] < 0.0))
		{
			//Some issue with calculation, do not send the mail
			$message = null;
		}
		else
		{
		
			$site_from = $array[7]->format('Y/m/d H:i');
			$site_to = $array[8]->format('Y/m/d H:i');
			
			$message .= "<tr><td ".$adj_style." >".$site_from." - ".$site_to."</td><td>".number_format($sum[2], 1)."</td><td>".number_format(($sum[1]+$sum[4]), 1)."</td><td>".number_format($sum[3]+$adj_offset, 1)."</td></tr>";
			
			$message .= "</table>";
		
		}
		
		return $message;
	}
	
	
	
	

	
	function SingleSiteReport($site_id_to_report, $date, $date1, $email, $email_cc)
	{
		$site_array = GetDetailsFromSiteId($site_id_to_report);
		//var_dump($site_array);
		
		$subject = "Site Report for $site_array[6] ($site_id_to_report) Date $date";
		
		$email_message = "<h2>".$subject."</h2>";
	
		//Get Alarm Site History, and delete it
		$alarm_site_txt = ReportSiteAlarmHistoryForDay($site_id_to_report, $date, $date1);
		$email_message .= $alarm_site_txt;
		
		$power_report = ReportPowerSupply($site_id_to_report, $date);
		
		if ($power_report == null)
		{
			//Do not send the mail
			echo "Mail not Sent, as calculation problem";
			$result = CommSendMailHtmlWithCC("toakhilesh@gmail.com", "Ram.sharma@sysinfra.in", "admin@sysinfra.in", "support@sysinfra.in", $subject, "Mail not sent as Power data incorrect.");
			return;
		}
		
		$email_message .= "<br><br><br><br><h3>Site Power Report </h3>".$power_report;
		
		
		$sig = file_get_contents('./mailsignature.htm', FILE_USE_INCLUDE_PATH);
		$email_message .= $sig;
		
		//For Testing
		//echo "$email_message";
		
		//Production
		$result = CommSendMailHtmlWithCC($email, $email_cc, "admin@sysinfra.in", "support@sysinfra.in", $subject, $email_message);
		echo "Mail Send Result: $result";

		
	}
	
	
	
	
	
	$date = date('Y-m-d',strtotime("-1 days"));
	$date1 = date('Y-m-d',strtotime("-2 days"));
	//$date = date('Y-m-d');
	//echo $date1." - ".$date."<br>";
	
	
	//"toakhilesh@gmail.com, Ram.sharma@sysinfra.in, dushyant@sysinfra.in"
	//SingleSiteReport("SISPMH2289", $date, $date1, "toakhilesh@gmail.com", "akhilesh.teraltd@gmail.com");
	//SingleSiteReport("ATCRJPANYL", $date);  NANOOWALI0
	//SingleSiteReport("ATCTIKARA1", $date);
	echo "<h1>Test</h1>";
	
	echo ReportPowerSupply("SISPLTD001", $date);
	echo "<p><hr><p>";
	echo ReportPowerSupply("SISPLTD001", $date1);
	
 
 ?>