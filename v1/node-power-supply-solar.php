<?php
session_start();
if (!isset($_SESSION['login_user']))
{
	header("Location: login.php");
}

if($_SERVER["REQUEST_METHOD"] == "GET")
{
	$site_id = $_GET['site_id'];	
	$hours_interval = 24;
	$hours_interval_txt = "24 Hours";
	$hours_interval_txt_adj = "last ";
	$range_data_type = 0;
}
	
	include 'common.php';
	
	function Solar_GetSiteStatsFromIdStartInterval($site_id, $start_date_time, $hours)
	{
		include 'dbinc.php'; 
		
		$sql_dt = "";
		if ($start_date_time == null)
		{
			$ist = gmmktime() + (330 * 60);	//+5:30 for IST
			$str_ist = date('Y-m-d H:i:s', $ist);
			//$sql_dt = "NOW()";
			$sql_dt = "\"".$str_ist."\"";
		}
		else
		{
			//$sql_dt = "\"".date_create_from_format('Y-m-d H:i:s', $start_date_time)."\"";
			$sql_dt = "\"".$start_date_time."\"";
		}
		

		$mysqli = new mysqli("$mysql_hostname", "$mysql_user", "$mysql_password", "$mysql_database");
		if (mysqli_connect_errno())
		{
			printf("Connect failed: %s\n", mysqli_connect_error());
			return null;
		}
		
		//Get first Id
		$sql = "SELECT
					id,
					date_time
				FROM
				  quanta
				WHERE
					site_id=\"$site_id\" 
				ORDER BY
				  (ABS(TIMESTAMPDIFF(SECOND, date_time, ($sql_dt)))) ASC
				LIMIT 1";

		//echo "$sql <br>";
				
		$result = $mysqli->query($sql);
		//var_dump($result);
		if ($result !== false)
		{
			$data = $result->fetch_row();
			$id1		= $data[0];	//First Id	
			//echo "<br>ID1: $id1";
			
			$new_dt = date_create_from_format('Y-m-d H:i:s', $data[1]);
			$new_dt->sub(new DateInterval('PT'.$hours.'H'));
			$sql_dt = "\"".$new_dt->format('Y-m-d H:i:s')."\"";
			
			$result->close();
			
			
			//Get 2nd Id
			$sql = "SELECT
						id
					FROM
					  quanta
					WHERE
						site_id=\"$site_id\"  					  
					ORDER BY
					  (ABS(TIMESTAMPDIFF(SECOND, date_time, ($sql_dt)))) ASC
					LIMIT 2";

			//echo "$sql <br>";
					
			$result = $mysqli->query($sql);	
			if ($result !== false)
			{
				$data1 = $result->fetch_row();
				$id2		= $data1[0];	//Second Id	
				//echo "<br>ID2: $id2";
				
				if ($id1 == $id2)
				{
					//echo "<br>Same Id $id1 and $id2, We have results more than given time interval apart<br>";
					//Find the next record
				
					$data1 = $result->fetch_row();
					if ($data1 !== false)
					{
						$id2 = $data1[0];	//Second Id
					}
					else
					{
						//We have not enough records
						$result->close();
						mysqli_close($mysqli);
						echo "Not Enough Records";
						return null;
					}
				}
				
				$result->close();
				
				//echo "<br>Comparison between $id1 and $id2<br>";
				$array = Solar_GetComparisonFromIds($id1, $id2);
				
				mysqli_close($mysqli);
				return $array;
			}
		}
		
		mysqli_close($mysqli);
		echo "NULL";
		
		return null;
	
	}
	
	function Solar_GetComparisonFromIds($id1, $id2)
	{
		$array = array();
		
		$arr1 = GetDeviceRecordFromId($id1);	//New
		$arr2 = GetDeviceRecordFromId($id2);	//Old
		//var_dump($arr1);
		//var_dump($arr2);
		
		$array[] = $arr1[4];	//0 Site Id
		$array[] = $arr1[39];	//1 Circle
		$array[] = $arr1[38];	//2 Culster
		$array[] = $arr1[37];	//3 Make
		$array[] = $arr1[41];	//4 AM NAME
		$array[] = $arr1[42];	//5 Mob no.
		$array[] = $arr1[43]; 	//6 DG Mobile
		
		
		//What Diffs to Retrieve
		//$array[] = new DateTime(arr2[5]);	//7 From Time
		$array[] = date_create_from_format('Y-m-d H:i:s', $arr2[5]);
		//$array[] = new DateTime(arr1[5]);	//8 To Time
		$array[] = date_create_from_format('Y-m-d H:i:s', $arr1[5]);
		
		//echo "F: $arr2[5]". " > ".$array[0]->format('Y-m-d H:i:s')."<br>";
		//echo "T: $arr1[5]". " > ".$array[1]->format('Y-m-d H:i:s')."<br>";
		
		$array[] = $array[7]->diff($array[8]);	//9 Interval
		//echo "Interval: ".$array[2]->format("%d %h %i")."<br>";
		//var_dump($array[2]);
		
		$delta_h = ($array[8]->getTimestamp() - $array[7]->getTimestamp()) / 3600;
		$array[] = $delta_h;	//10 Hours
		//echo "Hours: $array[3]<br>";
		
		$array[] = $arr1[22]-$arr2[22]; //11 System On Solar
		$array[] = $arr1[23]-$arr2[23]; //12 System On Inverter
		$array[] = $arr1[26]-$arr2[26]; //13 Energy Solar
		$array[] = $arr1[25]-$arr2[25]; //14 Energy Inverter
		
		//echo "DG: ".$arr1[26]." - ".$arr2[26]." = ".$array[4]."<br>";
		//echo "Mains: ".$arr1[27]." - ".$arr2[27]." = ".$array[5]."<br>";
		//echo "Batt: ".$arr1[28]." - ".$arr2[28]." = ".$array[6]."<br>";
		
		$array[] = $arr1[43];	//14 Site Name
		
		return $array;
	}
	
	
	
	$sum = null;
	$array = Solar_GetSiteStatsFromIdStartInterval($site_id, null, $hours_interval);
	//$sum = AddUp($array, $sum);		
	
	//var_dump($array);
	//var_dump( $array[7]->format('Y/m/d H:i:s'));
	//var_dump ($array[7]->$date);
	//var_dump( $array[9]);
	//echo $array[9]->y." ".$array[9]->m." ".$array[9]->d;

	if ($array[9]->days == 0)
	{
		$hours_interval_txt = $array[9]->h." hours";
	}
	else
	{
		$hours_interval_txt = $array[9]->days." days";
	}
	
?>
	<style>
		.color-box {
			width: 14px;
			height: 14px;
			display: inline-block;
			left: 5px;
			top: 5px;
		}
	</style>
	
    <div style="margin-top:20px;"><center><h3>Power Statistics</h3><h4>(<?php echo $array[7]->format('Y/m/d H:i:s')." - ".$array[8]->format('Y/m/d H:i:s');?>)</h4></center></div>

	<div style="margin-top:20px;">
	<table class="table table-bordered table-condensed">
		<tr><thead><th></th><th>Hours Run</th><th>Energy Produced</th></thead></tr>
		<tr><td  class='col-md-3' style="color: #990099"><div class="color-box" style="background-color: #990099;"></div> Site on Solar </td><td  class='col-md-3'><strong><?php echo round($array[11], 1)." hours"; ?></strong></td>
				<td  class='col-md-3'><strong><?php echo round($array[13], 1)." "; ?></strong></td></tr>
		<tr><td style="color: #3366cc"><div class="color-box" style="background-color: #3366cc;"></div> Site on Inverter </td><td><strong><?php echo round($array[12], 1)." hours"; ?></strong></td>
				<td><strong><?php echo round($array[14], 1)." "; ?></strong></td></tr>
		<tr><td style="color: #dc3912"><div class="color-box" style="background-color: #dc3912;"></div> Loss </td><td><strong><?php echo round(100-(abs($array[12]/$array[11])*100), 1)." %"; ?></strong></td>
				<td><strong><?php echo round(100-(abs($array[14]/$array[13])*100), 1)." %"; ?></strong></td></tr>
	</table>
	</div>
	
	<div id="pie1" style="margin-top:20px;"></div>
	
	
	
	<script class="code" type="text/javascript">$(document).ready(function(){
		var plot1 = $.jqplot('pie1', [[['Inverter Energy',<?php echo abs($array[14]); ?>],['Loss',<?php echo abs($array[14]-$array[13]); ?>]]], {
			gridPadding: {top:0, bottom:38, left:0, right:0},
			seriesDefaults:{
				renderer:$.jqplot.PieRenderer, 
				trendline:{ show:false }, 
				rendererOptions: { padding: 8, showDataLabels: true }
			},
			legend:{
				show:true, 
				placement: 'inside', 
				rendererOptions: {
					numberRows: 3
				}, 
				location:'s',
				marginTop: '15px'
			},
			seriesColors: [ "#3366cc", "#dc3912", "#dc3912", "#ff9900", "#109618"],
			grid: {
				drawBorder: false, 
				drawGridlines: false,
				background: '#ffffff',
				shadow:false
			},
			highlighter: {
				//tooltipContentEditor: function (str, seriesIndex, pointIndex) {
				//	return str + "<br/> additional data";
				//},

				// other options just for completeness
				show: true,
				showTooltip: true,
				tooltipFade: true,
				sizeAdjust: 10,
				formatString: '%s',
				tooltipLocation: 'n',
				useAxesFormatters: false,
			},
		});
	});</script>



