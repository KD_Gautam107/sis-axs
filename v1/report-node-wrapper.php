<?php
session_start();
if (!isset($_SESSION['login_user']))
{
	header("Location: login.php");
}

if($_SERVER["REQUEST_METHOD"] == "GET")
{
	$id = $_GET['id'];
	if ($id[0] == 'n')
	{
		$id = ltrim($id, "n");
	}
	else
	{
		echo "Validation FAILED<br>";
		die;
	}
}

include 'common.php';


$data_array = GetSiteDetailFromId($id);
//var_dump($data_array);


?>


<script>AsyncLoad("report-node-power.php?site_id=<?php echo $data_array[0]; ?>&range_data_type=0", "#id_power_supply");</script>

<div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title">
		<div class="dropdown">
		  <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Select Report Type
		  <span class="caret"></span></button>
		  <ul class="dropdown-menu">
			<li><a href="#" onclick='event.preventDefault(); AsyncLoad("report-node-power.php?site_id=<?php echo $data_array[0]; ?>&range_data_type=0", "#id_power_supply")'>Power Statistics</a></li>
			<li><a href="#" onclick='event.preventDefault(); AsyncLoad("report-node-custom.php?site_id=<?php echo $data_array[0]; ?>&range_data_type=0", "#id_power_supply")'>Custom Report</a></li>
		  </ul>
		</div>
	</h3>
  </div>
  <div id="collapsePowerSupply" class="panel-collapse collapse in">
	  <div class="panel-body">
		<div id="id_power_supply"></div>
	  </div>
  </div>
</div>





