<?php

function DeviceGetStatusBits($data_status)
{
    $array = array();
    $stats = unpack("C*", $data_status);

    //Smoke Fire Alarm
    $array[] = ($stats[5] & 0x01);

    //Door
    $array[] = ($stats[5] & 0x02);

    //Auto/Man Mode 0 Means  Auto Mode, 1 Means  Man Mode
    $array[] = ($stats[5] & 0x04);

    //00    :Load on EB,01: Load on DG, 10: Load on site Battery   11: Not used
    if (($stats[5] & 0x08) == false) {
        if (($stats[5] & 0x10) == false) {
            $array[] =  "Load on EB";
        } else {
            $array[] =  "Load on site Battery";
        }
    } else {
        if (($stats[5] & 0x10) == false) {
            $array[] =  "Load on DG";
        } else {
            $array[] =  "Not Used";
        }
    }

    //Mains Fail
    $array[] = ($stats[5] & 0x20);

    //Site Battery Low
    $array[] = ($stats[5] & 0x40);


    //Room Temperature
    $array[] = ($stats[5] & 0x80);


    //LLOP
    $array[] = ($stats[4] & 0x01);


    //HCT/HWT
    $array[] = ($stats[4] & 0x02);

    //Alternator Fault
    $array[] = ($stats[4] & 0x04);

    //DG Over Speed
    $array[] = ($stats[4] & 0x08);

    //DG Over Load
    $array[] = ($stats[4] & 0x10);

    //DG Low Fuel
    $array[] = ($stats[4] & 0x20);

    //DG Start Fail
    $array[] = ($stats[4] & 0x40);

    //DG Stop Fail
    $array[] = ($stats[4] & 0x80);

    //DG battery Low
    $array[] = ($stats[3] & 0x01);

    //LCU fail
    $array[] = ($stats[3] & 0x02);

    //Rectifier Fail
    $array[] = ($stats[3] & 0x04);

    //Multi Rectifier Fail
    $array[] = ($stats[3] & 0x08);

    //LVD TRIP
    $array[] = ($stats[3] & 0x10);

    //LVD BY pass
    $array[] = ($stats[3] & 0x20);


    //$FLAG_22_RES 	= 0x0000400000;	//Reserved
    //$FLAG_23_RES 	= 0x0000800000;	//Reserved
    //$FLAG_24_RES 	= 0x0001000000;	//Reserved
    //$FLAG_25_RES 	= 0x0002000000;	//Reserved
    //$FLAG_26_RES 	= 0x0004000000;	//Reserved
    //$FLAG_27_RES 	= 0x0008000000;	//Reserved
    //$FLAG_28_RES 	= 0x0010000000;	//Reserved
    //$FLAG_29_RES 	= 0x0020000000;	//Reserved
    //$FLAG_30_RES 	= 0x0040000000;	//Reserved
    //$FLAG_31_RES 	= 0x0080000000;	//Reserved

    //DG OFF
    $array[] = ($stats[1] & 0x01);

    //DG ON
    $array[] = ($stats[1] & 0x02);

    //DG Cranking
    $array[] = ($stats[1] & 0x04);

    //DG Start in Progress
    $array[] = ($stats[1] & 0x08);

    //DG Cool Down (Idle Running)
    $array[] = ($stats[1] & 0x10);

    //DG STOP  Normally
    $array[] = ($stats[1] & 0x20);

    //DG STOP due to Fault
    $array[] = ($stats[1] & 0x40);

    //DG Stop Due to Maximum Run Exipary
    $array[] = ($stats[1] & 0x80);

    return $array;
}



function DeviceGetData($data_raw)
{
    $array = array();
    $sub_data = substr($data_raw, 41, 36);
    $stats = str_split($sub_data, 2);
    //var_dump($stats);

    $sub_data = unpack("n", $stats[0]);
    $sub_data = $sub_data[1] / 10;
    //echo "<tr><td>Room Temperature</td><td>";
    $array[] = $sub_data;    //8


    $sub_data = unpack("n", $stats[1]);
    $sub_data = $sub_data[1] / 10;
    //echo "<tr><td>Fuel Level</td><td>";
    $array[] = $sub_data;    //9

    $sub_data = unpack("n", $stats[2]);
    $sub_data = $sub_data[1] / 10;
    //echo "<tr><td>Fuel Data</td><td>";
    $array[] = $sub_data;    //10

    $sub_data = unpack("n", $stats[3]);
    $sub_data = $sub_data[1] / 10;
    //echo "<tr><td>Site Batt. bank Voltage</td><td>";
    $array[] = $sub_data;    //11

    $sub_data = unpack("n", $stats[4]);
    $sub_data = $sub_data[1] / 10;
    //echo "<tr><td>R-Phase current</td><td>";
    $array[] = $sub_data;    //12

    $sub_data = unpack("n", $stats[5]);
    $sub_data = $sub_data[1] / 10;
    //echo "<tr><td>Y-Phase current</td><td>";
    $array[] = $sub_data;    //13

    $sub_data = unpack("n", $stats[6]);
    $sub_data = $sub_data[1] / 10;
    //echo "<tr><td>B-Phase current</td><td>";
    $array[] = $sub_data;    //14

    $sub_data = unpack("n", $stats[7]);
    $sub_data = $sub_data[1] / 10;
    //echo "<tr><td>Mains Frequency</td><td>";
    $array[] = $sub_data;    //15

    $sub_data = unpack("n", $stats[8]);
    $sub_data = $sub_data[1] / 10;
    //echo "<tr><td>DG frequency</td><td>";
    $array[] = $sub_data;    //16

    $sub_data = unpack("n", $stats[9]);
    $sub_data = $sub_data[1] / 10;
    //echo "<tr><td>DG-R phase Voltage</td><td>";
    $array[] = $sub_data;    //17

    $sub_data = unpack("n", $stats[10]);
    $sub_data = $sub_data[1] / 10;
    //echo "<tr><td>DG-Y phase Voltage</td><td>";
    $array[] = $sub_data;    //18

    $sub_data = unpack("n", $stats[11]);
    $sub_data = $sub_data[1] / 10;
    //echo "<tr><td>DG-B phase Voltage</td><td>";
    $array[] = $sub_data;    //19

    $sub_data = unpack("n", $stats[12]);
    $sub_data = $sub_data[1] / 10;
    //echo "<tr><td>LCU1 Output Voltage</td><td>";
    $array[] = $sub_data;    //20

    $sub_data = unpack("n", $stats[13]);
    $sub_data = $sub_data[1] / 10;
    //echo "<tr><td>LCU2 Output Voltage</td><td>";
    $array[] = $sub_data;    //21

    $sub_data = unpack("n", $stats[14]);
    $sub_data = $sub_data[1] / 10;
    //echo "<tr><td>LCU3 Output Voltage</td><td>";
    $array[] = $sub_data;    //22

    $sub_data = unpack("n", $stats[15]);
    $sub_data = $sub_data[1] / 10;
    //echo "<tr><td>Input Mains Voltage - R Phase</td><td>";
    $array[] = $sub_data;    //23

    $sub_data = unpack("n", $stats[16]);
    $sub_data = $sub_data[1] / 10;
    //echo "<tr><td>Input Mains Voltage - Y Phase</td><td>";
    $array[] = $sub_data;    //24

    $sub_data = unpack("n", $stats[17]);
    $sub_data = $sub_data[1] / 10;
    //echo "<tr><td>Input Mains Voltage - B Phase</td><td>";
    $array[] = $sub_data;    //25

    /////


    $sub_data = substr($data_raw, 77, 48);
    $stats = str_split($sub_data, 8);
    $stats = substr_replace($stats, ".", 7, 0);

    for ($i = 0; $i < 6; $i++) {
        $stats[$i] = ltrim($stats[$i], '0');
        if ($stats[$i][0] == '.') {
            $stats[$i] = "0" . $stats[$i];
        }
    }

    //echo "<tr><td>DG Running Hours</td><td>";
    $array[] = $stats[0];    //26

    //echo "<tr><td>Mains RUN HOURS</td><td>";
    $array[] = $stats[1];    //27

    //echo "<tr><td>Batt RUN HOURS</td><td>";
    $array[] = $stats[2];    //28

    //echo "<tr><td>O/P Mains Energy</td><td>";
    $array[] = $stats[3];    //29

    //echo "<tr><td>DG Energy</td><td>";
    $array[] = $stats[4];    //30

    //echo "<tr><td>I/P Mains Energy</td><td>";
    $array[] = $stats[5];    //31

    $sub_data = substr($data_raw, 125, 2);

    $sub_data = unpack("n", $sub_data);
    $sub_data = $sub_data[1] / 10;
    //echo "<tr><td>DG Battery Voltage</td><td>";
    $array[] = $sub_data;    //32


    //18*2	= 36
    //6 *8	= 48
    //1 *2	= 02
    //----------
    //        86
    //----------


    //Updated Data - Updated Protocol

    //18*2	= 36
    //6 *8	= 48
    //1 *2	= 02
    //2 *2	= 04	*
    //2 *1	= 02	*
    //----------
    //        92
    //----------

    //REV2 Total 134 Bytes
    if (strlen($data_raw) >= 134) {
        $sub_data = substr($data_raw, 127, 4);
        $stats = str_split($sub_data, 2);

        $sub_data = unpack("n", $stats[0]);
        $sub_data = $sub_data[1] / 10;
        //echo "<tr><td>Battery Charging current</td><td>";
        $array[] = $sub_data;    //33


        $sub_data = unpack("n", $stats[1]);
        $sub_data = $sub_data[1] / 10;
        //echo "<tr><td>Battery Discharging current</td><td>";
        $array[] = $sub_data;    //34

        $sub_data = substr($data_raw, 131, 2);
        $stats = str_split($sub_data, 1);

        $sub_data = unpack("c", $stats[0]);
        $sub_data = (hexdec(bin2hex($stats[0])));    //$sub_data[1]/1;
        //echo "<tr><td>Battery status</td><td>";
        $array[] = $sub_data;    //35

        $sub_data = unpack("c", $stats[1]);
        $sub_data = (hexdec(bin2hex($stats[1])) / 10);    //$sub_data[1]/10;
        //echo "<tr><td>Battery back up time</td><td>";
        $array[] = $sub_data;    //36


        //REV3	Total 150 Bytes
        if (strlen($data_raw) >= 150) {
            $sub_data = substr($data_raw, 133, 16);
            $stats = str_split($sub_data, 8);
            $stats = substr_replace($stats, ".", 7, 0);

            for ($i = 0; $i < 2; $i++) {
                $stats[$i] = ltrim($stats[$i], '0');
                if ($stats[$i][0] == '.') {
                    $stats[$i] = "0" . $stats[$i];
                }
            }

            //echo "<tr><td>Battery Charging Energy (Kwh)</td><td>";
            $array[] = $stats[0];    //37

            //echo "<tr><td>Battery Discharging Energy (Kwh)</td><td>";
            $array[] = $stats[1];    //38
        } else {
            $array[] = 0;    //37
            $array[] = 0;    //38
        }
    } else {
        $array[] = 0;    //33
        $array[] = 0;    //34
        $array[] = 0;    //35
        $array[] = 0;    //36
        $array[] = 0;    //37
        $array[] = 0;    //38
    }

    return $array;
}

function DeviceGetDataDevice2($data_raw)
{
    $array = array();

    //14 * 2 Bytes
    $sub_data = substr($data_raw, 41, 28);
    $stats = str_split($sub_data, 2);
    //var_dump($stats);

    $sub_data = unpack("n", $stats[0]);
    $sub_data = $sub_data[1] / 10;
    //echo "<tr><td>Room Temperature</td><td>";
    $array[] = $sub_data;    //8


    $sub_data = unpack("n", $stats[1]);
    $sub_data = $sub_data[1] / 10;
    //echo "<tr><td>Solar array 1 Voltage</td><td>";
    $array[] = $sub_data;    //9

    $sub_data = unpack("n", $stats[2]);
    $sub_data = $sub_data[1] / 10;
    //echo "<tr><td>Solar array 2 Voltage</td><td>";
    $array[] = $sub_data;    //10

    $sub_data = unpack("n", $stats[3]);
    $sub_data = $sub_data[1] / 10;
    //echo "<tr><td>Solar array 3 Voltage</td><td>";
    $array[] = $sub_data;    //11

    $sub_data = unpack("n", $stats[4]);
    $sub_data = $sub_data[1] / 10;
    //echo "<tr><td>Array1 current</td><td>";
    $array[] = $sub_data;    //12

    $sub_data = unpack("n", $stats[5]);
    $sub_data = $sub_data[1] / 10;
    //echo "<tr><td>Array2 current</td><td>";
    $array[] = $sub_data;    //13

    $sub_data = unpack("n", $stats[6]);
    $sub_data = $sub_data[1] / 10;
    //echo "<tr><td>Array3 current</td><td>";
    $array[] = $sub_data;    //14

    $sub_data = unpack("n", $stats[7]);
    $sub_data = $sub_data[1] / 10;
    //echo "<tr><td>R-Phase current</td><td>";
    $array[] = $sub_data;    //15

    $sub_data = unpack("n", $stats[8]);
    $sub_data = $sub_data[1] / 10;
    //echo "<tr><td>Y-Phase current</td><td>";
    $array[] = $sub_data;    //16

    $sub_data = unpack("n", $stats[9]);
    $sub_data = $sub_data[1] / 10;
    //echo "<tr><td>B-Phase current</td><td>";
    $array[] = $sub_data;    //17

    $sub_data = unpack("n", $stats[10]);
    $sub_data = $sub_data[1] / 10;
    //echo "<tr><td>Mains Frequency</td><td>";
    $array[] = $sub_data;    //18

    $sub_data = unpack("n", $stats[11]);
    $sub_data = $sub_data[1] / 10;
    //echo "<tr><td>Input Mains Voltage – R Phase</td><td>";
    $array[] = $sub_data;    //19

    $sub_data = unpack("n", $stats[12]);
    $sub_data = $sub_data[1] / 10;
    //echo "<tr><td>Input Mains Voltage – Y Phase</td><td>";
    $array[] = $sub_data;    //20

    $sub_data = unpack("n", $stats[13]);
    $sub_data = $sub_data[1] / 10;
    //echo "<tr><td>Input Mains Voltage – B Phase</td><td>";
    $array[] = $sub_data;    //21


    //5 * 8 Bytes

    $sub_data = substr($data_raw, 69, 40);
    $stats = str_split($sub_data, 8);
    $stats = substr_replace($stats, ".", 7, 0);

    for ($i = 0; $i < 5; $i++) {
        $stats[$i] = ltrim($stats[$i], '0');
        if ($stats[$i][0] == '.') {
            $stats[$i] = "0" . $stats[$i];
        }
    }

    //echo "<tr><td>Solar Run Hours</td><td>";
    $array[] = $stats[0];    //22

    //echo "<tr><td>Mains RUN HOURS</td><td>";
    $array[] = $stats[1];    //23

    //echo "<tr><td>Solar + mains  RUN HOURS</td><td>";
    $array[] = $stats[2];    //24

    //echo "<tr><td>Mains Energy</td><td>";
    $array[] = $stats[3];    //25

    //echo "<tr><td>Solar Energy</td><td>";
    $array[] = $stats[4];    //26



    //3 * 2 Bytes

    $sub_data = substr($data_raw, 109, 6);
    $stats = str_split($sub_data, 2);

    $sub_data = unpack("n", $stats[0]);
    $sub_data = $sub_data[1] / 10;
    //echo "<tr><td>Battery bank Voltage</td><td>";
    $array[] = $sub_data;    //27

    $sub_data = unpack("n", $stats[1]);
    $sub_data = $sub_data[1] / 10;
    //echo "<tr><td>Battery Charging current</td><td>";
    $array[] = $sub_data;    //28

    $sub_data = unpack("n", $stats[2]);
    $sub_data = $sub_data[1] / 10;
    //echo "<tr><td>Battery Discharging current</td><td>";
    $array[] = $sub_data;    //29


    //1 * 1 Byte

    $sub_data = substr($data_raw, 115, 1);
    $sub_data = (hexdec(bin2hex($sub_data)));
    //echo "<tr><td>Battery status</td><td>";
    $array[] = $sub_data;    //30


    //2 * 8 Bytes

    $sub_data = substr($data_raw, 116, 16);
    $stats = str_split($sub_data, 8);
    $stats = substr_replace($stats, ".", 7, 0);

    for ($i = 0; $i < 2; $i++) {
        $stats[$i] = ltrim($stats[$i], '0');
        if ($stats[$i][0] == '.') {
            $stats[$i] = "0" . $stats[$i];
        }
    }

    //echo "<tr><td>Batt CHG energy</td><td>";
    $array[] = $stats[0];    //31

    //echo "<tr><td>BAT Dischg Energy</td><td>";
    $array[] = $stats[1];    //32


    //Padding to make same size as for Device Type 1
    $array[] = null;    //33
    $array[] = null;    //34
    $array[] = null;    //35
    $array[] = null;    //36
    $array[] = null;    //37
    $array[] = null;    //38


    return $array;
}

//Data for Double DG
function DeviceGetDataDouble($data_raw)
{
    $array = array();
    $sub_data = substr($data_raw, 42, 2);
    $stats = str_split($sub_data, 2);
    //var_dump($stats);

    $sub_data = unpack("n", $stats[0]);
    $sub_data = $sub_data[1] / 10;
    //echo "<tr><td>Room Temperature</td><td>";
    $array[] = $sub_data;    //8


    $sub_data = substr($data_raw, 44, 1);
    $stats = str_split($sub_data, 1);
    $sub_data = (hexdec(bin2hex($stats[0])));    //$sub_data[1]/1;
    //echo "<tr><td>DG1 Fuel Level (0-100 %)</td><td>";
    $array[] = $sub_data;    //9



    $sub_data = substr($data_raw, 45, 32);
    $stats = str_split($sub_data, 2);

    $sub_data = unpack("n", $stats[0]);
    $sub_data = $sub_data[1] / 10;
    //echo "<tr><td>DG1 Fuel Data</td><td>";
    $array[] = $sub_data;    //10


    $sub_data = unpack("n", $stats[1]);
    $sub_data = $sub_data[1] / 10;
    //echo "<tr><td>DG1 Site Batt. bank Voltage</td><td>";
    $array[] = $sub_data;    //11

    $sub_data = unpack("n", $stats[2]);
    $sub_data = $sub_data[1] / 10;
    //echo "<tr><td>DG1 R-Phase current</td><td>";
    $array[] = $sub_data;    //12

    $sub_data = unpack("n", $stats[3]);
    $sub_data = $sub_data[1] / 10;
    //echo "<tr><td>DG1 Y-Phase current</td><td>";
    $array[] = $sub_data;    //13

    $sub_data = unpack("n", $stats[4]);
    $sub_data = $sub_data[1] / 10;
    //echo "<tr><td>DG1 B-Phase current</td><td>";
    $array[] = $sub_data;    //14

    $sub_data = unpack("n", $stats[5]);
    $sub_data = $sub_data[1] / 10;
    //echo "<tr><td>Mains Frequency</td><td>";
    $array[] = $sub_data;    //15

    $sub_data = unpack("n", $stats[6]);
    $sub_data = $sub_data[1] / 10;
    //echo "<tr><td>DG1 frequency</td><td>";
    $array[] = $sub_data;    //16

    $sub_data = unpack("n", $stats[7]);
    $sub_data = $sub_data[1] / 10;
    //echo "<tr><td>DG1-R phase Voltage</td><td>";
    $array[] = $sub_data;    //17

    $sub_data = unpack("n", $stats[8]);
    $sub_data = $sub_data[1] / 10;
    //echo "<tr><td>DG1-Y phase Voltage</td><td>";
    $array[] = $sub_data;    //18

    $sub_data = unpack("n", $stats[9]);
    $sub_data = $sub_data[1] / 10;
    //echo "<tr><td>DG1-B phase Voltage</td><td>";
    $array[] = $sub_data;    //19

    $sub_data = unpack("n", $stats[10]);
    $sub_data = $sub_data[1] / 10;
    //echo "<tr><td>LCU1 Output Voltage</td><td>";
    $array[] = $sub_data;    //20

    $sub_data = unpack("n", $stats[11]);
    $sub_data = $sub_data[1] / 10;
    //echo "<tr><td>LCU2 Output Voltage</td><td>";
    $array[] = $sub_data;    //21

    $sub_data = unpack("n", $stats[12]);
    $sub_data = $sub_data[1] / 10;
    //echo "<tr><td>LCU3 Output Voltage</td><td>";
    $array[] = $sub_data;    //22

    $sub_data = unpack("n", $stats[13]);
    $sub_data = $sub_data[1] / 10;
    //echo "<tr><td>Input Mains Voltage - R Phase</td><td>";
    $array[] = $sub_data;    //23

    $sub_data = unpack("n", $stats[14]);
    $sub_data = $sub_data[1] / 10;
    //echo "<tr><td>Input Mains Voltage - Y Phase</td><td>";
    $array[] = $sub_data;    //24

    $sub_data = unpack("n", $stats[15]);
    $sub_data = $sub_data[1] / 10;
    //echo "<tr><td>Input Mains Voltage - B Phase</td><td>";
    $array[] = $sub_data;    //25

    /////


    $sub_data = substr($data_raw, 77, 48);
    $stats = str_split($sub_data, 8);
    $stats = substr_replace($stats, ".", 7, 0);

    for ($i = 0; $i < 6; $i++) {
        $stats[$i] = ltrim($stats[$i], '0');
        if ($stats[$i][0] == '.') {
            $stats[$i] = "0" . $stats[$i];
        }
    }

    //echo "<tr><td>DG1 Running Hours</td><td>";
    $array[] = $stats[0];    //26

    //echo "<tr><td>Mains RUN HOURS</td><td>";
    $array[] = $stats[1];    //27

    //echo "<tr><td>Batt RUN HOURS</td><td>";
    $array[] = $stats[2];    //28

    //echo "<tr><td>O/P Mains Energy</td><td>";
    $array[] = $stats[3];    //29

    //echo "<tr><td>DG1 Energy</td><td>";
    $array[] = $stats[4];    //30

    //echo "<tr><td>I/P Mains Energy</td><td>";
    $array[] = $stats[5];    //31

    $sub_data = substr($data_raw, 125, 6);
    $stats = str_split($sub_data, 2);

    $sub_data = unpack("n", $stats[0]);
    $sub_data = $sub_data[1] / 10;
    //echo "<tr><td>DG1 Battery Voltage</td><td>";
    $array[] = $sub_data;    //32

    $sub_data = unpack("n", $stats[1]);
    $sub_data = $sub_data[1] / 10;
    //echo "<tr><td>Battery Charging current</td><td>";
    $array[] = $sub_data;    //33

    $sub_data = unpack("n", $stats[2]);
    $sub_data = $sub_data[1] / 10;
    //echo "<tr><td>Battery Discharging current</td><td>";
    $array[] = $sub_data;    //34


    $sub_data = substr($data_raw, 131, 2);
    $stats = str_split($sub_data, 1);

    $sub_data = (hexdec(bin2hex($stats[0])));    //$sub_data[1]/1;
    //echo "<tr><td>Battery status</td><td>";
    $array[] = $sub_data;    //35

    $sub_data = (hexdec(bin2hex($stats[1])) / 10);    //$sub_data[1]/10;
    //echo "<tr><td>Battery back up time</td><td>";
    $array[] = $sub_data;    //36



    $sub_data = substr($data_raw, 133, 16);
    $stats = str_split($sub_data, 8);
    $stats = substr_replace($stats, ".", 7, 0);

    for ($i = 0; $i < 2; $i++) {
        $stats[$i] = ltrim($stats[$i], '0');
        if ($stats[$i][0] == '.') {
            $stats[$i] = "0" . $stats[$i];
        }
    }

    //echo "<tr><td>Battery Charging Energy (Kwh)</td><td>";
    $array[] = $stats[0];    //37

    $embedded_array = array();

    //echo "<tr><td>Battery Discharging Energy (Kwh)</td><td>";
    $embedded_array[] = $stats[1];    //38 [0]



    $sub_data = substr($data_raw, 149, 10);
    $stats = str_split($sub_data, 2);

    $sub_data = unpack("n", $stats[0]);
    $sub_data = $sub_data[1] / 10;
    //echo "<tr><td>DG2 frequency</td><td>";
    $embedded_array[] = $sub_data;    //38 [1]

    $sub_data = unpack("n", $stats[1]);
    $sub_data = $sub_data[1] / 10;
    //echo "<tr><td>DG2-R phase Voltage</td><td>";
    $embedded_array[] = $sub_data;    //38 [2]

    $sub_data = unpack("n", $stats[2]);
    $sub_data = $sub_data[1] / 10;
    //echo "<tr><td>DG2-Y phase Voltage</td><td>";
    $embedded_array[] = $sub_data;    //38 [3]

    $sub_data = unpack("n", $stats[3]);
    $sub_data = $sub_data[1] / 10;
    //echo "<tr><td>DG2-B phase Voltage</td><td>";
    $embedded_array[] = $sub_data;    //38 [4]


    $sub_data = unpack("n", $stats[4]);
    $sub_data = $sub_data[1] / 10;
    //echo "<tr><td>DG2 Battery Voltage</td><td>";
    $embedded_array[] = $sub_data;    //38 [5]


    $sub_data = substr($data_raw, 159, 16);
    $stats = str_split($sub_data, 8);
    $stats = substr_replace($stats, ".", 7, 0);

    for ($i = 0; $i < 2; $i++) {
        $stats[$i] = ltrim($stats[$i], '0');
        if ($stats[$i][0] == '.') {
            $stats[$i] = "0" . $stats[$i];
        }
    }

    //echo "<tr><td>DG2 Running Hours (Hrs)</td><td>";
    $embedded_array[] = $stats[0];    //38 [6]

    //echo "<tr><td>DG2 Energy (Kwh)</td><td>";
    $embedded_array[] = $stats[1];    //38 [7]


    $sub_data = substr($data_raw, 175, 1);
    $sub_data = (hexdec(bin2hex($sub_data)));
    //echo "<tr><td>DG2 Fuel Level %</td><td>";
    $embedded_array[] = $sub_data;    //38 [8]

    $sub_data = substr($data_raw, 176, 2);
    $sub_data = unpack("n", $sub_data);
    $sub_data = $sub_data[1] / 10;
    //echo "<tr><td>DG2 Fuel Data (Litres)</td><td>";
    $embedded_array[] = $sub_data;    //38 [9]


    $array[] = $embedded_array;    //38 -- Array of Extra elements beyond 37

    //var_dump($array);

    return $array;
}


function DeviceGetDataDevicePhase2($data_raw)
{
    $array = array();

    $sub_data = substr($data_raw, 44, 36);
    $stats = str_split($sub_data, 2);
    //var_dump($stats);

    $sub_data = unpack("n", $stats[0]);
    $sub_data = $sub_data[1] / 10;

    //echo "<tr><td>Room Temperature</td><td>";
    //echo("$sub_data Deg C");
    //echo "</td></tr>";
    $array[] = $sub_data;    //8


    $sub_data = unpack("n", $stats[1]);
    $sub_data = $sub_data[1] / 10;
    //echo "<tr><td>Fuel Level</td><td>";
    //echo("$sub_data %");
    //echo "</td></tr>";
    $array[] = $sub_data;    //9

    $sub_data = unpack("n", $stats[2]);
    $sub_data = $sub_data[1] / 10;
    //echo "<tr><td>Fuel Data</td><td>";
    //echo("$sub_data Liters");
    //echo "</td></tr>";
    $array[] = $sub_data;    //10

    $sub_data = unpack("n", $stats[3]);
    $sub_data = $sub_data[1] / 10;
    //echo "<tr><td>Site Batt. bank Voltage</td><td>";
    //echo("$sub_data VDC");
    //echo "</td></tr>";
    $array[] = $sub_data;    //11

    $sub_data = unpack("n", $stats[4]);
    $sub_data = $sub_data[1] / 10;
    //echo "<tr><td>R-Phase current</td><td>";
    //echo("$sub_data");
    //echo "</td></tr>";
    $array[] = $sub_data;    //12

    $sub_data = unpack("n", $stats[5]);
    $sub_data = $sub_data[1] / 10;
    //echo "<tr><td>Y-Phase current</td><td>";
    //echo("$sub_data");
    //echo "</td></tr>";
    $array[] = $sub_data;    //13


    $sub_data = unpack("n", $stats[6]);
    $sub_data = $sub_data[1] / 10;
    //echo "<tr><td>B-Phase current</td><td>";
    //echo("$sub_data");
    //echo "</td></tr>";
    $array[] = $sub_data;    //14

    $sub_data = unpack("n", $stats[7]);
    $sub_data = $sub_data[1] / 10;
    //echo "<tr><td>Mains Frequency</td><td>";
    //echo("$sub_data");
    //echo "</td></tr>";
    $array[] = $sub_data;    //15

    $sub_data = unpack("n", $stats[8]);
    $sub_data = $sub_data[1] / 10;
    //echo "<tr><td>DG frequency</td><td>";
    //echo("$sub_data");
    //echo "</td></tr>";
    $array[] = $sub_data;    //16

    $sub_data = unpack("n", $stats[9]);
    $sub_data = $sub_data[1] / 10;
    //echo "<tr><td>DG-R phase Voltage</td><td>";
    //echo("$sub_data VAC");
    //echo "</td></tr>";
    $array[] = $sub_data;    //17

    $sub_data = unpack("n", $stats[10]);
    $sub_data = $sub_data[1] / 10;
    //echo "<tr><td>DG-Y phase Voltage</td><td>";
    //echo("$sub_data VAC");
    //echo "</td></tr>";
    $array[] = $sub_data;    //18

    $sub_data = unpack("n", $stats[11]);
    $sub_data = $sub_data[1] / 10;
    //echo "<tr><td>DG-B phase Voltage</td><td>";
    //echo("$sub_data VAC");
    //echo "</td></tr>";
    $array[] = $sub_data;    //19

    $sub_data = unpack("n", $stats[12]);
    $sub_data = $sub_data[1] / 10;
    //echo "<tr><td>LCU1 Output Voltage</td><td>";
    //echo("$sub_data VAC");
    //echo "</td></tr>";
    $array[] = $sub_data;    //20

    $sub_data = unpack("n", $stats[13]);
    $sub_data = $sub_data[1] / 10;
    //echo "<tr><td>LCU2 Output Voltage</td><td>";
    //echo("$sub_data VAC");
    //echo "</td></tr>";
    $array[] = $sub_data;    //21

    $sub_data = unpack("n", $stats[14]);
    $sub_data = $sub_data[1] / 10;
    //echo "<tr><td>LCU3 Output Voltage</td><td>";
    //echo("$sub_data VAC");
    //echo "</td></tr>";
    $array[] = $sub_data;    //22

    $sub_data = unpack("n", $stats[15]);
    $sub_data = $sub_data[1] / 10;
    //echo "<tr><td>Input Mains Voltage - R Phase</td><td>";
    //echo("$sub_data VAC");
    //echo "</td></tr>";
    $array[] = $sub_data;    //23

    $sub_data = unpack("n", $stats[16]);
    $sub_data = $sub_data[1] / 10;
    //echo "<tr><td>Input Mains Voltage - Y Phase</td><td>";
    //echo("$sub_data VAC");
    //echo "</td></tr>";
    $array[] = $sub_data;    //24

    $sub_data = unpack("n", $stats[17]);
    $sub_data = $sub_data[1] / 10;
    //echo "<tr><td>Input Mains Voltage - B Phase</td><td>";
    //echo("$sub_data VAC");
    //echo "</td></tr>";
    $array[] = $sub_data;    //25

    /////


    $sub_data = substr($data_raw, 80, 48);
    $stats = str_split($sub_data, 8);
    $stats = substr_replace($stats, ".", 7, 0);

    for ($i = 0; $i < 6; $i++) {
        $stats[$i] = ltrim($stats[$i], '0');
        if ($stats[$i][0] == '.') {
            $stats[$i] = "0" . $stats[$i];
        }
    }
    //var_dump($stats);
    //echo "<tr><td>DG Running Hours</td><td>";
    //echo("$stats[0] Hrs");
    //echo "</td></tr>";
    $array[] = $stats[0];    //26

    //echo "<tr><td>Mains RUN HOURS</td><td>";
    //echo("$stats[1] Hrs");
    //echo "</td></tr>";
    $array[] = $stats[1];    //27

    //echo "<tr><td>Batt RUN HOURS</td><td>";
    //echo("$stats[2] Hrs");
    //echo "</td></tr>";
    $array[] = $stats[2];    //28

    //echo "<tr><td>O/P Mains Energy</td><td>";
    //echo("$stats[3] Kwh");
    //echo "</td></tr>";
    $array[] = $stats[3];    //29

    //echo "<tr><td>DG Energy</td><td>";
    //echo("$stats[4] Kwh");
    //echo "</td></tr>";
    $array[] = $stats[4];    //30

    //echo "<tr><td>I/P Mains Energy</td><td>";
    //echo("$stats[5] Kwh");
    //echo "</td></tr>";
    $array[] = $stats[5];    //31

    $sub_data = substr($data_raw, 128, 6);
    $stats = str_split($sub_data, 2);

    $sub_data = unpack("n", $stats[0]);
    $sub_data = $sub_data[1] / 10;
    //echo "<tr><td>DG Battery Voltage</td><td>";
    //echo("$sub_data VDC");
    //echo "</td></tr>";
    $array[] = $sub_data;    //32

    $sub_data = unpack("n", $stats[1]);
    $sub_data = $sub_data[1] / 10;
    //echo "<tr><td>Battery Charging current</td><td>";
    //echo("$sub_data Amp");
    //echo "</td></tr>";
    $array[] = $sub_data;    //33

    $sub_data = unpack("n", $stats[2]);
    $sub_data = $sub_data[1] / 10;
    //echo "<tr><td>Battery Discharging current</td><td>";
    //echo("$sub_data Amp");
    //echo "</td></tr>";
    $array[] = $sub_data;    //34

    $sub_data = substr($data_raw, 134, 2);
    $stats = str_split($sub_data, 1);

    $sub_data = (hexdec(bin2hex($stats[0])));
    //echo "<tr><td>Battery status</td><td>";
    //echo("$sub_data %");
    //echo "</td></tr>";
    $array[] = $sub_data;    //35

    $sub_data = (hexdec(bin2hex($stats[1])) / 10);
    //echo "<tr><td>Battery back up time</td><td>";
    //echo("$sub_data hours");
    //echo "</td></tr>";
    $array[] = $sub_data;    //36


    $sub_data = substr($data_raw, 136, 16);
    $stats = str_split($sub_data, 8);
    $stats = substr_replace($stats, ".", 7, 0);

    for ($i = 0; $i < 2; $i++) {
        $stats[$i] = ltrim($stats[$i], '0');
        if ($stats[$i][0] == '.') {
            $stats[$i] = "0" . $stats[$i];
        }
    }

    //echo "<tr><td>Battery Charging Energy</td><td>";
    //echo("$stats[0] Kwh");
    //echo "</td></tr>";
    $array[] = $stats[0];    //37


    $embedded_array = array();

    //echo "<tr><td>Battery Discharging Energy</td><td>";
    //echo("$stats[1] Kwh");
    //echo "</td></tr>";
    $embedded_array[] = $stats[1];    //38[0]

    //echo "<tr><td>CRC LOWER BYTE</td><td>";
    //echo(hexdec(bin2hex($data_raw[152])));
    //echo "</td></tr>";
    //$array[] = $stats[2];

    //echo "<tr><td>CRC HIGHER BYTE</td><td>";
    //echo(hexdec(bin2hex($data_raw[153])));
    //echo "</td></tr>";
    //$array[] = $stats[3];




    //echo "<tr><td></td><td>";
    //echo "DC Energy Meter Data";
    //echo "</td></tr>";




    //echo "<tr><td>Device 2 Id</td><td>";
    $embedded_array[] = (hexdec(bin2hex($data_raw[154])));    //38[1]
    //echo "</td></tr>";


    //echo "<tr><td>Function Code</td><td>";
    $embedded_array[] = (hexdec(bin2hex($data_raw[155])));    //38[2]
    //echo "</td></tr>";

    //echo "<tr><td># bytes</td><td>";
    $embedded_array[] = (hexdec(bin2hex($data_raw[156])));    //38[3]
    //echo "</td></tr>";



    //9 * 4 IEEE-754 32-bit float value
    $sub_data = substr($data_raw, 157, 36);
    $stats = str_split($sub_data, 4);

    for ($i = 0; $i < 9; $i++) {
        $bin = "\x4A\x5B\x1B\x05";
        $tmp = unpack('f', strrev($stats[$i]));
        //$tmp = unpack('f', $stats[$i]);
        //echo $tmp[1];  // 3589825.25
        $stats[$i] = $tmp[1];
    }

    //echo "<tr><td>KWH of Channel 1</td><td>";
    $embedded_array[] = $stats[0];                    //38[4]
    //echo "</td></tr>";

    //echo "<tr><td>Current of Channel 1</td><td>";
    $embedded_array[] = $stats[1];                    //38[5]
    //echo "</td></tr>";

    //echo "<tr><td>KWH of Channel 2</td><td>";
    $embedded_array[] = $stats[2];                    //38[6]
    //echo "</td></tr>";

    //echo "<tr><td>Current of Channel 2</td><td>";
    $embedded_array[] = $stats[3];                    //38[7]
    //echo "</td></tr>";

    //echo "<tr><td>KWH of Channel 3</td><td>";
    $embedded_array[] = $stats[4];                    //38[8]
    //echo "</td></tr>";

    //echo "<tr><td>Current of Channel 3</td><td>";
    $embedded_array[] = $stats[5];                    //38[9]
    //echo "</td></tr>";

    //echo "<tr><td>KWH of Channel 4</td><td>";
    $embedded_array[] = $stats[6];                    //38[10]
    //echo "</td></tr>";

    //echo "<tr><td>Current of Channel 4</td><td>";
    $embedded_array[] = $stats[7];                    //38[11]
    //echo "</td></tr>";

    //echo "<tr><td>Voltage</td><td>";
    $embedded_array[] = $stats[8];                    //38[12]
    //echo "</td></tr>";


    //echo "<tr><td>CRC LOWER BYTE</td><td>";
    //echo(hexdec(bin2hex($data_raw[193])));
    //echo "</td></tr>";

    //echo "<tr><td>CRC HIGHER BYTE</td><td>";
    //echo(hexdec(bin2hex($data_raw[194])));
    //echo "</td></tr>";

    $array[] = $embedded_array;    //38 -- Array of Extra elements beyond 37

    //var_dump($array);

    return $array;
}


function DeviceGetDataDoubleBattery($data_raw)
{
    $array = array();
    $sub_data = substr($data_raw, 41, 22);
    $stats = str_split($sub_data, 2);
    //var_dump($stats);

    $sub_data = unpack("n", $stats[0]);
    $sub_data = $sub_data[1] / 10;
    //echo "<tr><td>Room Temperature</td><td>";
    $array[] = $sub_data;    //8


    $sub_data = unpack("n", $stats[1]);
    $sub_data = $sub_data[1] / 10;
    //echo "<tr><td>Site Battery Bank Voltage</td><td>";
    $array[] = $sub_data;    //9

    $sub_data = unpack("n", $stats[2]);
    $sub_data = $sub_data[1] / 10;
    //echo "<tr><td>Site Batt. bank Voltage</td><td>";
    $array[] = $sub_data;    //10

    $sub_data = unpack("n", $stats[3]);
    $sub_data = $sub_data[1] / 10;
    //echo "<tr><td>Load Current current</td><td>";
    $array[] = $sub_data;    //11

    $sub_data = unpack("n", $stats[4]);
    $sub_data = $sub_data[1] / 10;
    //echo "<tr><td>Mains Voltage</td><td>";
    $array[] = $sub_data;    //12

    $sub_data = unpack("n", $stats[5]);
    $sub_data = $sub_data[1] / 10;
    //echo "<tr><td>DG Voltage</td><td>";
    $array[] = $sub_data;    //13

    $sub_data = unpack("n", $stats[6]);
    $sub_data = $sub_data[1] / 10;
    //echo "<tr><td>Input Mains Voltage B Phase</td><td>";
    $array[] = $sub_data;    //14

    $sub_data = unpack("n", $stats[7]);
    $sub_data = $sub_data[1] / 10;
    //echo "<tr><td>Battery1 Charging Current</td><td>";
    $array[] = $sub_data;    //15

    $sub_data = unpack("n", $stats[8]);
    $sub_data = $sub_data[1] / 10;
    //echo "<tr><td>Battery1 Discharging Current</td><td>";
    $array[] = $sub_data;    //16

    $sub_data = unpack("n", $stats[9]);
    $sub_data = $sub_data[1] / 10;
    //echo "<tr><td>Battery2 Charging Current</td><td>";
    $array[] = $sub_data;    //17

    $sub_data = unpack("n", $stats[10]);
    $sub_data = $sub_data[1] / 10;
    //echo "<tr><td>Battery2 Discharging Current</td><td>";
    $array[] = $sub_data;    //18


    $sub_data = substr($data_raw, 63, 2);
    $stats = str_split($sub_data, 1);

    //$sub_data = unpack("C",$stats[0]);
    $sub_data = hexdec(bin2hex($stats[0]));
    //echo "<tr><td>Battery1 Status</td><td>";
    $array[] = $sub_data;    //19

    //$sub_data = unpack("C",$stats[1]);
    $sub_data = hexdec(bin2hex($stats[1]));
    // echo "<tr><td>Battery2 Status</td><td>";
    $array[] = $sub_data;    //20


    /////

    //11*2	= 22
    //2 *1	= 02
    //----------
    //        66
    //----------

    //REV2 Total 66 Bytes


    return $array;
}

function DeviceGetDataDevice3($data_raw)
{
    $array = array();
    $sub_data = substr($data_raw, 41, 22);
    $stats = str_split($sub_data, 2);
    //var_dump($stats);

    $sub_data = unpack("n", $stats[0]);
    $sub_data = $sub_data[1] / 10;
    //echo "<tr><td>Room Temperature</td><td>";
    $array[] = $sub_data;    //8


    $sub_data = unpack("n", $stats[1]);
    $sub_data = $sub_data[1] / 10;
    //echo "<tr><td>Site Battery Bank Voltage</td><td>";
    $array[] = $sub_data;    //9

    $sub_data = unpack("n", $stats[2]);
    $sub_data = $sub_data[1] / 10;
    //echo "<tr><td>Site Batt. bank Voltage</td><td>";
    $array[] = $sub_data;    //10

    $sub_data = unpack("n", $stats[3]);
    $sub_data = $sub_data[1] / 10;
    //echo "<tr><td>Total Load Current current</td><td>";
    $array[] = $sub_data;    //11

    $sub_data = unpack("n", $stats[4]);
    $sub_data = $sub_data[1] / 10;
    //echo "<tr><td>Mains Voltage-R Phase</td><td>";
    $array[] = $sub_data;    //12

    $sub_data = unpack("n", $stats[5]);
    $sub_data = $sub_data[1] / 10;
    //echo "<tr><td>DG Voltage-Y Phase</td><td>";
    $array[] = $sub_data;    //13

    $sub_data = unpack("n", $stats[6]);
    $sub_data = $sub_data[1] / 10;
    //echo "<tr><td>Input Mains Voltage-B Phase Phase</td><td>";
    $array[] = $sub_data;    //14

    $sub_data = unpack("n", $stats[7]);
    $sub_data = $sub_data[1] / 10;
    //echo "<tr><td>Battery1 Charging Current</td><td>";
    $array[] = $sub_data;    //15

    $sub_data = unpack("n", $stats[8]);
    $sub_data = $sub_data[1] / 10;
    //echo "<tr><td>Battery1 Discharging Current</td><td>";
    $array[] = $sub_data;    //16

    $sub_data = unpack("n", $stats[9]);
    $sub_data = $sub_data[1] / 10;
    //echo "<tr><td>Battery2 Charging Current</td><td>";
    $array[] = $sub_data;    //17

    $sub_data = unpack("n", $stats[10]);
    $sub_data = $sub_data[1] / 10;
    //echo "<tr><td>Battery2 Discharging Current</td><td>";
    $array[] = $sub_data;    //18


    $sub_data = substr($data_raw, 63, 4);
    $stats = str_split($sub_data, 2);

    //$sub_data = unpack("C",$stats[0]);
    $sub_data = hexdec(bin2hex($stats[0]));
    //echo "<tr><td>Battery1 Status</td><td>";
    $array[] = $sub_data;    //19

    //$sub_data = unpack("C",$stats[1]);
    $sub_data = hexdec(bin2hex($stats[1]));
    // echo "<tr><td>Battery2 Status</td><td>";
    $array[] = $sub_data;    //20

    $sub_data = substr($data_raw, 67, 40);
    $stats = str_split($sub_data, 8);
    $stats = substr_replace($stats, ".", 7, 0);

    for ($i = 0; $i < 6; $i++) {
        $stats[$i] = ltrim($stats[$i], '0');
        if ($stats[$i][0] == '.') {
            $stats[$i] = "0" . $stats[$i];
        }
    }

    //echo "<tr><td>Mains Energy</td><td>";
    $array[] = $stats[0];    //21

    //echo "<tr><td>Battery#1 CHG Energy</td><td>";
    $array[] = $stats[1];    //22

    //echo "<tr><td>Battery#1 DISCHG Energy </td><td>";
    $array[] = $stats[2];    //23

    //echo "<tr><td>Battery#2 CHG Energy </td><td>";
    $array[] = $stats[3];    //24

    //echo "<tr><td>Battery#2 DISCHG Energy </td><td>";
    $array[] = $stats[4];    //25


    /////
    //41
    //11*2	= 22
    //2 *2	= 04
    //5*8   = 40
    //----------
    //       107
    //----------

    //REV2 Total 66 Bytes


    return $array;
}

//////////**// */
function GetDeviceRecordFromId($id)
{
    $array = array();

    include 'dbinc.php';


    $mysqli = new mysqli("$mysql_hostname", "$mysql_user", "$mysql_password", "$mysql_database");
    if (mysqli_connect_errno()) {
        printf("Connect failed: %s\n", mysqli_connect_error());
        return null;
    }

    //$sql = 'SELECT a.tutorial_id, a.tutorial_author, b.tutorial_count
    //	FROM tutorials_tbl a, tcount_tbl b
    //	WHERE a.tutorial_author = b.tutorial_author';

    $order = "SELECT 
			a.ID, a.timestamp , a.data_validated , a.type , a.site_id , a.date_time , a.device_id , a.status , a.raw_data,  
			b.ClusterId, b.DeviceName, b.DeviceMake, b.Circle, b.SiteName, b.District, b.mobile, b.Longitude, b.Latitude, b.SiteIndoorType,  b.DG_KVA, b.SAM_Name, b.SAM_Number, b.AM_Name, b.AM_Number, b.Tech_Name,  b.Tech_Number, b.Comments,
			c.name,
			b.date_of_installation, b.DG_make, b.EB_capacity, b.EB_phase, b.battery_AH, b.battery_qty, b.battery_make, 
			b.DG2_KVA, b.DG2_MAKE, b.site_type
			
		FROM quanta a, siteinfo b, clusters c
		WHERE (a.site_id = b.site_id) AND (b.ClusterId = c.Id) AND a.id=" . $id;

    //echo $order;

    //$order = "SELECT * FROM quanta where id=".$id;
    $result = $mysqli->query($order);
    $data = $result->fetch_row();

    //var_dump($data);

    $array[]     = $data[0];        //0Unique Id
    $array[]     = $data[1];        //1Time Stamp by Server
    $array[]     = $data[2];        //2Validated by Server
    $array[]     = $data[3];        //3Type 0 - Periodic / 1 - Fault
    $array[]     = $data[4];        //4String Site Id
    $array[]     = $data[5];        //5Date Time by Device
    $array[]     = $data[6];        //6Device Id/ Only 1, 2 & 32 known for now
    $array[]     = $data[7];        //7Status Bits - To Expand
    $data_raw    = $data[8];        //Raw Data, for device specific information

    //var_dump($array);
    //$array = array_merge($array, DeviceGetStatusBits($data[7]));
    //var_dump($array);


    if ($data[6] == 1) {
        //$data_validated
        //	0 - Not Validated
        //	1 - Single DG
        //	2 - Double DG
        if ($data[2] == 2) {    //Double DG
            $array = array_merge($array, DeviceGetDataDouble($data_raw));
        } else {
            $array = array_merge($array, DeviceGetData($data_raw));
        }
    } elseif ($data[6] == 2) {    //2 = Solar
        if ($data[2] == 1) {
            //if($data[37] == 3)
            //{
            $array = array_merge($array, DeviceGetDataDevice3($data_raw));
            for ($i = 26; $i < 39; $i++) {
                $array[$i] = "";
            }
            //}
        } else {
            $array = array_merge($array, DeviceGetDataDevice2($data_raw));
        }
    } elseif ($data[6] == 16) {    //Phase 2
        $array = array_merge($array, DeviceGetData($data_raw)); //DeviceGetDataDevicePhase2($data_raw));
    } elseif ($data[6] == 32) {
        $array = array_merge($array, DeviceGetDataDoubleBattery($data_raw));
        for ($i = 21; $i < 39; $i++) {
            $array[$i] = "";
        }
    }

    $array[]    = $data[9];        //39 ClusterId
    $array[]    = $data[10];    //40 DeviceName
    $array[]    = $data[11];    //41 DeviceMake
    $array[]    = $data[12];    //42 Circle
    $array[]    = $data[13];    //43 SiteName
    $array[]    = $data[14];    //44 District
    $array[]    = $data[15];    //45 Site Mobile
    $array[]    = $data[16];    //46 Longitude

    $array[]    = $data[17];    //47 Latitude
    $array[]    = $data[18];    //48 SiteIndoorType
    $array[]    = $data[19];    //49 DG_KVA
    $array[]    = $data[20];    //50 SAM_Name
    $array[]    = $data[21];    //51 SAM_Number
    $array[]    = $data[22];    //52 AM_Name
    $array[]    = $data[23];    //53 AM_Number
    $array[]    = $data[24];    //54 Tech_Name
    $array[]    = $data[25];    //55 Tech_Number

    $array[]    = $data[28];    //56 date_of_installation
    $array[]    = $data[29];    //57 DG_make
    $array[]    = $data[30];    //58 EB_capacity
    $array[]    = $data[31];    //59 EB_phase
    $array[]    = $data[32];    //60 battery_AH
    $array[]    = $data[33];    //61 battery_qty
    $array[]    = $data[34];    //62 battery_make

    $array[]    = $data[35];    //63 DG2_KVA
    $array[]    = $data[36];    //64 DG2_MAKE
    $array[]    = $data[37];    //65 site_type


    $result->close();
    mysqli_close($mysqli);

    //var_dump($array);

    return $array;
}

function AddUp($array, $sum)
{
    $array1 = array();

    if ($sum == null) {
        $array1[]    = abs($array[10]);
        $array1[]    = abs($array[11]);
        $array1[]    = abs($array[12]);
        $array1[]    = abs($array[13]);
        $array1[]    = abs($array[17]);

        //var_dump($array);
    } else {
        $array1[]    = abs($array[10]) + $sum[0];
        $array1[]    = abs($array[11]) + $sum[1];
        $array1[]    = abs($array[12]) + $sum[2];
        $array1[]    = abs($array[13]) + $sum[3];
        $array1[]    = abs($array[17]) + $sum[4];

        //var_dump($array);
        //var_dump($sum);
    }


    return $array1;
}


function DisplayEntryType1Header()
{
    echo ("<table class='table table-striped table-condensed table-responsive'>");

    echo ("<tr><thead>
					
					<th>Type</th>
					<th>Date Time</th>
					<th>Status</th>
					<th>Device Id</th>
					
					<th>Room/Ambient Temperature</th>
					<th>Fuel Level</th>
					<th>Fuel Data</th>
					<th>Site Battery bank Voltage</th>
					<th>R-Phase current</th>
					<th>Y-Phase current</th>
					<th>B-Phase current</th>
					<th>Mains Frequency</th>
					<th>DG frequency</th>
					<th>DG-R phase Voltage</th>
					<th>DG-Y phase Voltage</th>
					<th>DG-B phase Voltage</th>
					<th>LCU1 Output Voltage</th>
					<th>LCU2 Output Voltage</th>
					<th>LCU3 Output Voltage</th>
					<th>Input Mains Voltage - R Phase</th>
					<th>Input Mains Voltage - Y Phase</th>
					<th>Input Mains Voltage - B Phase</th>
					<th>DG Running Hours</th>
					<th>Mains RUN HOURS</th>
					<th>Battery RUN HOURS</th>
					<th>O/P Mains Energy</th>
					<th>DG Energy</th>
					<th>I/P Mains Energy</th>
					<th>DG Battery Voltage</th>
					<th>Battery Charging Current</th>
					<th>Battery Discharging Current</th>
					<th>Battery Status</th>
					<th>Battery Back Up Time</th>
					<th>Battery Charging Energy</th>
					<th>Battery Discharging Energy</th>
				</thead></tr><tbody>");
}

function DisplayEntryTypeSolarHeader()
{
    echo ("<table class='table table-striped table-condensed table-responsive'>");

    echo ("<tr><thead>
					
					<th>Type</th>
					<th>Date Time</th>
					<th>Status</th>
					<th>Device Id</th>
					
					<th>Room/Ambient Temperature</th>
					<th>Solar array 1 Voltage</th>
					<th>Solar array 2 Voltage</th>
					<th>Solar array 3 Voltage</th>
					<th>Array 1 current</th>
					<th>Array 2 current</th>
					<th>Array 3 current</th>
					<th>R-Phase current</th>
					<th>Y-Phase current</th>
					<th>B-Phase current</th>
					<th>Inverter Frequency</th>
					<th>Inverter R-Phase Voltage</th>
					<th>Inverter Y-Phase Voltage</th>
					<th>Inverter B-Phase Voltage</th>
					<th>Solar Run Hours</th>
					<th>Inverter Run Hours</th>
					<th>Solar + Inverter Run Hours</th>
					<th>Inverter Energy</th>
					<th>Solar Energy</th>
					
					<th>Battery Bank Voltage</th>
					<th>Battery Charging Current</th>
					<th>Battery Discharging Current</th>
					<th>Battery Status</th>
					<th>Battery Charging Energy</th>
					<th>Battery Discharging Energy</th>
				</thead></tr><tbody>");
}

function DisplayEntryType1DoubleDGHeader()
{
    echo ("<table class='table table-striped table-condensed table-responsive'>");

    echo ("<tr><thead>
					<th>#</th>
					<th>Site Id</th>

					<th>Type</th>
					<th>Date Time</th>
					<th>Status</th>
					<th>Device Id</th>
					
					<th>Room/Ambient Temperature</th>
					<th>Fuel Level</th>
					<th>Fuel Data</th>
					<th>Site Battery bank Voltage</th>
					<th>R-Phase current</th>
					<th>Y-Phase current</th>
					<th>B-Phase current</th>
					<th>Mains Frequency</th>
					<th>DG1 frequency</th>
					<th>DG1-R phase Voltage</th>
					<th>DG1-Y phase Voltage</th>
					<th>DG1-B phase Voltage</th>
					<th>LCU1 Output Voltage</th>
					<th>LCU2 Output Voltage</th>
					<th>LCU3 Output Voltage</th>
					<th>Input Mains Voltage - R Phase</th>
					<th>Input Mains Voltage - Y Phase</th>
					<th>Input Mains Voltage - B Phase</th>
					<th>DG1 Running Hours</th>
					<th>Mains RUN HOURS</th>
					<th>Battery RUN HOURS</th>
					<th>O/P Mains Energy</th>
					<th>DG1 Energy</th>
					<th>I/P Mains Energy</th>
					<th>DG1 Battery Voltage</th>
					<th>Battery Charging Current</th>
					<th>Battery Discharging Current</th>
					<th>Battery Status</th>
					<th>Battery Back Up Time</th>
					<th>Battery Charging Energy</th>
					<th>Battery Discharging Energy</th>
					
					<th>DG2 frequency</th>
					<th>DG2-R phase Voltage VAC</th>
					<th>DG2-Y phase Voltage VAC</th>
					<th>DG2-B phase Voltage VAC</th>
					<th>DG2 Running Hours Hrs</th>
					<th>DG2 Energy Kwh</th>
					<th>DG2 Battery Voltage VDC</th>
					<th>Fuel Level of DG2 %</th>
					<th>Fuel data of DG2 liter</th>
					
				</thead></tr><tbody>");
}

function DisplayEntryType2Header()
{
    echo ("<table class='table table-hover table-condensed table-responsive'>");

    echo ("<tr><thead>
					<th>SITE NAME</th>
					<th>SITE ID</th>
					<th>Circle</th>
					<th>Cluster</th>
					
					<th>Make</th>
					<th>AM NAME</th>
					<th>Mob no.</th>
					
					<th>Date Time</th>
					<th>Interval</th>
					

					<th>DG Running Hours</th>
					<th>Mains RUN HOURS</th>
					<th>Battery RUN HOURS</th>

					<th>Generator Start</th>
					<th>Generator Stop</td>
				</thead></tr><tbody>");
}
function DisplayEntryTypeDoubleBatteryHeader()
{
    echo ("<table class='table table-striped table-condensed table-responsive'>");

    echo ("<tr><thead>
					<th>#</th>
					<th>Site Id</th>

					<th>Type</th>
					<th>Date Time</th>
					<th>Status</th>
					<th>Device Id</th>
					<th>Room/Ambient Temperature</th>
					<th>Site Battery bank Voltage</th>
					<th>Site Battery bank Voltage</th>
					<th>Load Current</th>
					<th>Mains Voltage</th>
					
					<th>DG Voltage</th>
					<th>Input Mains Voltage B Phase</th>
					<th>Battery1 Charging Current</th>
					<th>Battery1 Discharging Current</th>
					<th>Battery2 Charging Current</th>
					<th>Battery2 Discharging Current</th>
					<th>Battery1 Status</th>
					<th>Battery2 Status</th>
					
				</thead></tr><tbody>");
}

function DisplayEntryType3Header()
{
    echo ("<table class='table table-striped table-condensed table-responsive'>");

    echo ("<tr><thead>
					<th>#</th>
					<th>Site Id</th>
					<th>Type</th>
					<th>Date Time</th>
					<th>Status</th>
					<th>Device Id</th>
					<th>Room/Ambient Temperature</th>
					<th>Site Battery bank Voltage</th>
					<th>Site Battery bank Voltage</th>
					<th>Total Load Current</th>
					<th>Mains Voltage-R Phase</th>
					<th>Mains Voltage-Y Phase</th>
					<th>Mains Voltage-B Phase</th>
					<th>Battery 1 Charging Current</th>
					<th>Battery 1 Discharging Current</th>
					<th>Battery 2 Charging Current</th>
					<th>Battery 2 Discharging Current</th>
					<th>Battery 1 Status</th>
					<th>Battery 2 Status</th>					
					<th>Mains Energy</th>
					<th>Battery#1 CHG Energy</th>
					<th>Battery#1 DISCHG Energy</th>
					<th>Battery#2 CHG Energy</th>
					<th>Battery#2 DISCHG Energy</th>
				</thead></tr><tbody>");
}

function DisplayEntryType1HeaderEnd()
{
    echo ("</tbody></table>");
}



function DisplayEntryType1($array)
{
    $color = "";
    if ($array[3] == 1) {
        $color = " class='danger' ";
    }
    echo ("<tr $color>");


    $data_type_str = "";
    if ($array[3] == 0) {
        $data_type_str = "P";
    }
    if ($array[3] == 1) {
        $data_type_str = "F";
    }
    echo ("<td>" . $data_type_str . "</td>"); //Type

    echo ("<td>" . $array[5] . "</td>"); //Date Time



    $data_status_hex = bin2hex($array[7]);
    echo ("<td><button type='button' class='btn btn-primary btn-sm' data-toggle='modal' OnClick='ShowDetail($array[0])'>$data_status_hex</button></td>"); //Status
    echo ("<td>" . $array[6] . "</td>"); //Device Id

    echo ("<td>" . $array[8] . "</td>"); //Room Temperature
    echo ("<td>" . $array[9] . "</td>"); //Fuel Level
    echo ("<td>" . $array[10] . "</td>"); //Fuel Data
    echo ("<td>" . $array[11] . "</td>"); //Site Batt. bank Voltage
    echo ("<td>" . $array[12] . "</td>"); //R-Phase current
    echo ("<td>" . $array[13] . "</td>"); //Y-Phase current
    echo ("<td>" . $array[14] . "</td>"); //B-Phase current
    echo ("<td>" . $array[15] . "</td>"); //Mains Frequency
    echo ("<td>" . $array[16] . "</td>"); //DG frequency
    echo ("<td>" . $array[17] . "</td>"); //DG-R phase Voltage
    echo ("<td>" . $array[18] . "</td>"); //DG-Y phase Voltage
    echo ("<td>" . $array[19] . "</td>"); //DG-B phase Voltage
    echo ("<td>" . $array[20] . "</td>"); //LCU1 Output Voltage
    echo ("<td>" . $array[21] . "</td>"); //LCU2 Output Voltage
    echo ("<td>" . $array[22] . "</td>"); //LCU3 Output Voltage
    echo ("<td>" . $array[23] . "</td>"); //Input Mains Voltage - R Phase
    echo ("<td>" . $array[24] . "</td>"); //Input Mains Voltage - Y Phase
    echo ("<td>" . $array[25] . "</td>"); //Input Mains Voltage - B Phase
    echo ("<td>" . $array[26] . "</td>"); //DG Running Hours
    echo ("<td>" . $array[27] . "</td>"); //Mains RUN HOURS
    echo ("<td>" . $array[28] . "</td>"); //Batt RUN HOURS
    echo ("<td>" . $array[29] . "</td>"); //O/P Mains Energy
    echo ("<td>" . $array[30] . "</td>"); //DG Energy
    echo ("<td>" . $array[31] . "</td>"); //I/P Mains Energy
    echo ("<td>" . $array[32] . "</td>"); //DG Battery Voltage
    echo ("<td>" . $array[33] . "</td>"); //Battery Charging current
    echo ("<td>" . $array[34] . "</td>"); //Battery Discharging current
    echo ("<td>" . $array[35] . "</td>"); //Battery status
    echo ("<td>" . $array[36] . "</td>"); //Battery back up time
    echo ("<td>" . $array[37] . "</td>"); //Battery Charging Energy
    echo ("<td>" . $array[38] . "</td>"); //Battery Disharging Energy
}

function DisplayEntryTypeSolar($array)
{
    $color = "";
    if ($array[3] == 1) {
        $color = " class='danger' ";
    }
    echo ("<tr $color>");

    $data_type_str = "";
    if ($array[3] == 0) {
        $data_type_str = "P";
    }
    if ($array[3] == 1) {
        $data_type_str = "F";
    }
    echo ("<td>" . $data_type_str . "</td>"); //Type

    echo ("<td>" . $array[5] . "</td>"); //Date Time



    $data_status_hex = bin2hex($array[7]);
    echo ("<td><button type='button' class='btn btn-primary btn-sm' data-toggle='modal' OnClick='ShowDetail($array[0])'>$data_status_hex</button></td>"); //Status
    echo ("<td>" . $array[6] . "</td>"); //Device Id

    echo ("<td>" . $array[8] . "</td>"); //Room Temperature
    echo ("<td>" . $array[9] . "</td>"); //Solar array 1 Voltage
    echo ("<td>" . $array[10] . "</td>"); //Solar array 2 Voltage
    echo ("<td>" . $array[11] . "</td>"); //Solar array 3 Voltage
    echo ("<td>" . $array[12] . "</td>"); //Array1 current
    echo ("<td>" . $array[13] . "</td>"); //Array2 current
    echo ("<td>" . $array[14] . "</td>"); //Array3 current
    echo ("<td>" . $array[15] . "</td>"); //R-Phase current
    echo ("<td>" . $array[16] . "</td>"); //Y-Phase current
    echo ("<td>" . $array[17] . "</td>"); //B-Phase current
    echo ("<td>" . $array[18] . "</td>"); //Mains Frequency
    echo ("<td>" . $array[19] . "</td>"); //Input Mains Voltage – R Phase
    echo ("<td>" . $array[20] . "</td>"); //Input Mains Voltage – Y Phase
    echo ("<td>" . $array[21] . "</td>"); //Input Mains Voltage – B Phase
    echo ("<td>" . $array[22] . "</td>"); //Solar Run Hours
    echo ("<td>" . $array[23] . "</td>"); //Mains RUN HOURS
    echo ("<td>" . $array[24] . "</td>"); //Solar + mains  RUN HOURS
    echo ("<td>" . $array[25] . "</td>"); //Mains Energy
    echo ("<td>" . $array[26] . "</td>"); //Solar Energy

    echo ("<td>" . $array[27] . "</td>"); // Battery bank Voltage  Voltage
    echo ("<td>" . $array[28] . "</td>"); //Battery Charging current
    echo ("<td>" . $array[29] . "</td>"); //Battery Discharging current

    echo ("<td>" . $array[30] . "</td>"); //Battery status

    echo ("<td>" . $array[31] . "</td>"); //Battery Charging Energy
    echo ("<td>" . $array[32] . "</td>"); //Battery Disharging Energy
}

function DisplayEntryType1DoubleDG($array)
{
    $color = "";
    if ($array[3] == 1) {
        $color = " class='danger' ";
    }
    echo ("<tr $color>");
    echo ("<td>" . $array[0] . "</td>");    //#
    echo ("<td>" . $array[4] . "</td>");    //Site Id

    $data_type_str = "";
    if ($array[3] == 0) {
        $data_type_str = "P";
    }
    if ($array[3] == 1) {
        $data_type_str = "F";
    }
    echo ("<td>" . $data_type_str . "</td>"); //Type

    echo ("<td>" . $array[5] . "</td>"); //Date Time



    $data_status_hex = bin2hex($array[7]);
    echo ("<td><button type='button' class='btn btn-primary btn-sm' data-toggle='modal' OnClick='ShowDetail($array[0], 0)'>$data_status_hex</button></td>"); //Status
    echo ("<td>" . $array[6] . "</td>"); //Device Id

    echo ("<td>" . $array[8] . "</td>"); //Room Temperature
    echo ("<td>" . $array[9] . "</td>"); //Fuel Level
    echo ("<td>" . $array[10] . "</td>"); //Fuel Data
    echo ("<td>" . $array[11] . "</td>"); //Site Batt. bank Voltage
    echo ("<td>" . $array[12] . "</td>"); //R-Phase current
    echo ("<td>" . $array[13] . "</td>"); //Y-Phase current
    echo ("<td>" . $array[14] . "</td>"); //B-Phase current
    echo ("<td>" . $array[15] . "</td>"); //Mains Frequency
    echo ("<td>" . $array[16] . "</td>"); //DG frequency
    echo ("<td>" . $array[17] . "</td>"); //DG-R phase Voltage
    echo ("<td>" . $array[18] . "</td>"); //DG-Y phase Voltage
    echo ("<td>" . $array[19] . "</td>"); //DG-B phase Voltage
    echo ("<td>" . $array[20] . "</td>"); //LCU1 Output Voltage
    echo ("<td>" . $array[21] . "</td>"); //LCU2 Output Voltage
    echo ("<td>" . $array[22] . "</td>"); //LCU3 Output Voltage
    echo ("<td>" . $array[23] . "</td>"); //Input Mains Voltage - R Phase
    echo ("<td>" . $array[24] . "</td>"); //Input Mains Voltage - Y Phase
    echo ("<td>" . $array[25] . "</td>"); //Input Mains Voltage - B Phase
    echo ("<td>" . $array[26] . "</td>"); //DG Running Hours
    echo ("<td>" . $array[27] . "</td>"); //Mains RUN HOURS
    echo ("<td>" . $array[28] . "</td>"); //Batt RUN HOURS
    echo ("<td>" . $array[29] . "</td>"); //O/P Mains Energy
    echo ("<td>" . $array[30] . "</td>"); //DG Energy
    echo ("<td>" . $array[31] . "</td>"); //I/P Mains Energy
    echo ("<td>" . $array[32] . "</td>"); //DG Battery Voltage
    echo ("<td>" . $array[33] . "</td>"); //Battery Charging current
    echo ("<td>" . $array[34] . "</td>"); //Battery Discharging current
    echo ("<td>" . $array[35] . "</td>"); //Battery status
    echo ("<td>" . $array[36] . "</td>"); //Battery back up time
    echo ("<td>" . $array[37] . "</td>"); //Battery Charging Energy
    echo ("<td>" . $array[38][0] . "</td>"); //Battery Disharging Energy

    if ($array[2] == 2) {
        echo ("<td>" . $array[38][1] . "</td>"); //DG2 frequency
        echo ("<td>" . $array[38][2] . "</td>"); //DG2-R phase Voltage VAC
        echo ("<td>" . $array[38][3] . "</td>"); //DG2-Y phase Voltage VAC
        echo ("<td>" . $array[38][4] . "</td>"); //DG2-B phase Voltage VAC
        echo ("<td>" . $array[38][5] . "</td>"); //DG2 Battery Voltage VDC
        echo ("<td>" . $array[38][6] . "</td>"); //DG2 Running Hours Hrs
        echo ("<td>" . $array[38][7] . "</td>"); //DG2 Energy Kwh
        echo ("<td>" . $array[38][8] . "</td>"); //Fuel Level of DG2 %
        echo ("<td>" . $array[38][9] . "</td>"); //Fuel data of DG2 liter
    } else {
        echo ("<td></td>");
        echo ("<td></td>");
        echo ("<td></td>");
        echo ("<td></td>");
        echo ("<td></td>");
        echo ("<td></td>");
        echo ("<td></td>");
        echo ("<td></td>");
        echo ("<td></td>");
    }
}


function DisplayEntryType2($array)
{
    echo "<tr>";
    echo ("<td>" . $array[14] . "</td>"); //Site Name
    echo ("<td>" . $array[0] . "</td>");    //Site Id


    echo ("<td>" . $array[1] . "</td>"); //Circle
    echo ("<td>" . $array[2] . "</td>"); //Culster
    echo ("<td>" . $array[3] . "</td>"); //Make
    echo ("<td>" . $array[4] . "</td>"); //AM NAME
    echo ("<td>" . $array[5] . "</td>"); //Mob no.

    echo ("<td>" . $array[7]->format('Y-m-d H:i:s') . "</td>"); //Date Time
    echo ("<td>" . round($array[10], 2) . "</td>"); //Interval

    echo ("<td>" . abs(round($array[11], 2)) . "</td>"); //DG Running Hours
    echo ("<td>" . abs(round($array[12], 2)) . "</td>"); //Mains RUN HOURS
    echo ("<td>" . abs(round($array[13], 2)) . "</td>"); //Batt RUN HOURS


    if (ctype_space($array[6]) || $array[6] == '') { //Mobile
        echo ("<td></td>");
        echo ("<td></td>");
    } else {
        echo ("<td><button type='button' class='btn-success btn-primary btn-sm' data-toggle='modal' OnClick='GenStartStop(1, $array[40])'>START</button></td>"); //START
        echo ("<td><button type='button' class='btn-danger btn-primary btn-sm' data-toggle='modal' OnClick='GenStartStop(0, $array[40])'>STOP</button></td>"); //STOP
    }
    echo "</tr>";
}

function DisplayEntryType2Sum($array)
{
    echo "<tr class='info'>";
    echo ("<td><strong>Total</strong></td>"); //Site Name
    echo ("<td></td>");    //Site Id


    echo ("<td></td>"); //Circle
    echo ("<td></td>"); //Culster
    echo ("<td></td>"); //Make
    echo ("<td></td>"); //AM NAME
    echo ("<td></td>"); //Mob no.

    echo ("<td></td>"); //Date Time
    echo ("<td>" . round($array[0], 2) . "</td>"); //Interval

    echo ("<td>" . abs(round($array[1], 2)) . "</td>"); //DG Running Hours
    echo ("<td>" . abs(round($array[2], 2)) . "</td>"); //Mains RUN HOURS
    echo ("<td>" . abs(round($array[3], 2)) . "</td>"); //Batt RUN HOURS


    echo ("<td></td>");
    echo ("<td></td>");

    echo "</tr>";
}


function DisplayEntryTypeDoubleBattery($array)
{
    $color = "";
    if ($array[3] == 1) {
        $color = " class='danger' ";
    }
    echo ("<tr $color>");
    echo ("<td>" . $array[0] . "</td>");    //#
    echo ("<td>" . $array[4] . "</td>");    //Site Id

    $data_type_str = "";
    if ($array[3] == 0) {
        $data_type_str = "P";
    }
    if ($array[3] == 1) {
        $data_type_str = "F";
    }
    echo ("<td>" . $data_type_str . "</td>"); //Type

    echo ("<td>" . $array[5] . "</td>"); //Date Time


    $data_status_hex = bin2hex($array[7]);
    echo ("<td><button type='button' class='btn btn-primary btn-sm' data-toggle='modal' OnClick='ShowDetail($array[0], 0)'>$data_status_hex</button></td>"); //Status
    echo ("<td>" . $array[6] . "</td>"); //Device Id

    echo ("<td>" . $array[8] . "</td>"); //Room Temperature
    echo ("<td>" . $array[9] . "</td>"); //Site Batt. Banl Voltage
    echo ("<td>" . $array[10] . "</td>"); //Site Batt. Bank Voltage
    echo ("<td>" . $array[11] . "</td>"); //Load Current
    echo ("<td>" . $array[12] . "</td>"); //Mains Voltage
    echo ("<td>" . $array[13] . "</td>"); //DG Voltage
    echo ("<td>" . $array[14] . "</td>"); //Input Mains Voltage B Phase
    echo ("<td>" . $array[15] . "</td>"); //Battery1 Charging Current
    echo ("<td>" . $array[16] . "</td>"); //Battery1 Discharging Current
    echo ("<td>" . $array[17] . "</td>"); //Battery2 Charging Current
    echo ("<td>" . $array[18] . "</td>"); //Battery2 Discharging Current
    echo ("<td>" . $array[19] . "</td>"); //Battery1 Status
    echo ("<td>" . $array[20] . "</td>"); //Battery2 Status
}

function DisplayEntryType3($array)
{
    $color = "";
    if ($array[3] == 1) {
        $color = " class='danger' ";
    }
    echo ("<tr $color>");
    echo ("<td>" . $array[0] . "</td>");    //#
    echo ("<td>" . $array[4] . "</td>");    //Site Id

    $data_type_str = "";
    if ($array[3] == 0) {
        $data_type_str = "P";
    }
    if ($array[3] == 1) {
        $data_type_str = "F";
    }
    echo ("<td>" . $data_type_str . "</td>"); //Type

    echo ("<td>" . $array[5] . "</td>"); //Date Time


    $data_status_hex = bin2hex($array[7]);
    echo ("<td><button type='button' class='btn btn-primary btn-sm' data-toggle='modal' OnClick='ShowDetail($array[0], 0)'>$data_status_hex</button></td>"); //Status
    echo ("<td>" . $array[6] . "</td>"); //Device Id

    echo ("<td>" . $array[8] . "</td>"); //Room Temperature
    echo ("<td>" . $array[9] . "</td>"); //Site Batt. Banl Voltage
    echo ("<td>" . $array[10] . "</td>"); //Site Batt. Bank Voltage
    echo ("<td>" . $array[11] . "</td>"); //Total Load Current
    echo ("<td>" . $array[12] . "</td>"); //Mains Voltage-R Phase
    echo ("<td>" . $array[13] . "</td>"); //Mains Voltage-Y Phase
    echo ("<td>" . $array[14] . "</td>"); //Mains Voltage-B Phase
    echo ("<td>" . $array[15] . "</td>"); //Battery1 Charging Current
    echo ("<td>" . $array[16] . "</td>"); //Battery1 Discharging Current
    echo ("<td>" . $array[17] . "</td>"); //Battery2 Charging Current
    echo ("<td>" . $array[18] . "</td>"); //Battery2 Discharging Current
    echo ("<td>" . $array[19] . "</td>"); //Battery1 Status
    echo ("<td>" . $array[20] . "</td>"); //Battery2 Status
    echo ("<td>" . $array[21] . "</td>"); //Mains Energy
    echo ("<td>" . $array[22] . "</td>"); //Battery#1 CHG Energy
    echo ("<td>" . $array[23] . "</td>"); //Battery#1 DISCHG Energy
    echo ("<td>" . $array[24] . "</td>"); //Battery#2 CHG Energy
    echo ("<td>" . $array[25] . "</td>"); //Battery#2 DISCHG Energy
}


function CalculateGeneratorFailedToStopTime($site_id, $dateStart, $dateEnd)
{
    $array = array();
    include 'dbinc.php';

    $mysqli = new mysqli("$mysql_hostname", "$mysql_user", "$mysql_password", "$mysql_database");
    if (mysqli_connect_errno()) {
        printf("Connect failed: %s\n", mysqli_connect_error());
        exit();
    }

    $sqlquery = "SELECT date_time, MID(HEX(`status`),7,1) & 8 " .
        "FROM quanta WHERE `site_id`='$site_id' AND `date_time` >= '$dateStart' AND `date_time` <= '$dateEnd' " .
        "ORDER BY date_time ASC ";


    //echo ": $sqlquery<br>";
    //var_dump($sqlquery);
    $result = $mysqli->query($sqlquery);

    $intervals = null;

    if ($result !== false) {
        $genError = false;
        $alarmFlag = false;


        while ($data = $result->fetch_row()) {
            //var_dump($data);

            //If first data is in error then assume ??????
            //if (firstTime && !genError && ($data[1] == '8'))
            //	genError = true;

            $alarmFlag = ($data[1] == '8');
            if ($genError) {
                if (!$alarmFlag) {
                    //Yes Alarm --> No Alarm
                    $timeE = $data[0];
                    $genError = false;

                    $tmpInterval = date_create_from_format('Y-m-d H:i:s', $timeE)->getTimestamp() - date_create_from_format('Y-m-d H:i:s', $timeS)->getTimestamp();

                    if ($intervals == null) {
                        $intervals = $tmpInterval;
                    } else {
                        $intervals += $tmpInterval;
                    }
                }
            } else {
                if ($alarmFlag) {
                    //No Alarm --> Yes Alarm
                    $timeS = $data[0];
                    $genError = true;
                }
            }

            $lastTime = $data[0];
            //var_dump($intervals);
        }

        //Error flag true even at end. Diff it from the last time.
        if ($genError) {
            $timeE = $lastTime;
            $genError = false;

            $tmpInterval = date_create_from_format('Y-m-d H:i:s', $timeE)->getTimestamp() - date_create_from_format('Y-m-d H:i:s', $timeS)->getTimestamp();

            if ($intervals == null) {
                $intervals = $tmpInterval;
            } else {
                $intervals += $tmpInterval;
            }
        }


        $result->close();
    }

    mysqli_close($mysqli);

    if ($intervals == null) {
        $intervals = 0;
    } else {
        $intervals = $intervals / 3600;
    }
    return $intervals;
}


function GetComparisonFromIds($id1, $id2)
{
    $array = array();

    $arr1 = GetDeviceRecordFromId($id1);    //New
    $arr2 = GetDeviceRecordFromId($id2);    //Old

    $array[] = $arr1[4];    //0 Site Id
    $array[] = $arr1[39];    //1 Circle
    $array[] = $arr1[38];    //2 Culster
    $array[] = $arr1[37];    //3 Make
    $array[] = $arr1[41];    //4 AM NAME
    $array[] = $arr1[42];    //5 Mob no.
    $array[] = $arr1[43];     //6 DG Mobile

    //What Diffs to Retrieve
    //$array[] = new DateTime(arr2[5]);	//7 From Time
    $array[] = date_create_from_format('Y-m-d H:i:s', $arr2[5]);
    //$array[] = new DateTime(arr1[5]);	//8 To Time
    $array[] = date_create_from_format('Y-m-d H:i:s', $arr1[5]);

    //echo "F: $arr2[5]". " > ".$array[0]->format('Y-m-d H:i:s')."<br>";
    //echo "T: $arr1[5]". " > ".$array[1]->format('Y-m-d H:i:s')."<br>";

    $array[] = $array[7]->diff($array[8]);    //9 Interval
    //echo "Interval: ".$array[2]->format("%d %h %i")."<br>";
    //var_dump($array[2]);

    $delta_h = ($array[8]->getTimestamp() - $array[7]->getTimestamp()) / 3600;
    $array[] = $delta_h;    //10 Hours
    //echo "Hours: $array[3]<br>";

    $array[] = $arr1[26] - $arr2[26]; //11 System On DG
    $array[] = $arr1[27] - $arr2[27]; //12 System On Mains
    $array[] = $arr1[28] - $arr2[28]; //13 System On Batt

    //echo "DG: ".$arr1[26]." - ".$arr2[26]." = ".$array[4]."<br>";
    //echo "Mains: ".$arr1[27]." - ".$arr2[27]." = ".$array[5]."<br>";
    //echo "Batt: ".$arr1[28]." - ".$arr2[28]." = ".$array[6]."<br>";

    $array[] = $arr1[43];    //14 Site Name

    if ($arr1[5] < $arr2[5]) {
        $array[] = CalculateGeneratorFailedToStopTime($arr1[4], $arr1[5], $arr2[5]);
    } else {
        $array[] = CalculateGeneratorFailedToStopTime($arr1[4], $arr2[5], $arr1[5]);
    }

    if (($arr1[6] == 1) && ($arr1[2] == 2) && ($arr2[6] == 1) && ($arr2[2] == 2)) {    //Double DG
        $array[] = 1;    //16 Double DG Case
        $array[] = $arr1[38][6] - $arr2[38][6]; //17 System On DG2
    } else {
        $array[] = 0;    //16 Double DG Case
        $array[] = 0;     //17 System On DG2 - 0
    }

    return $array;
}


function GetSiteDetailFromId($id)
{
    $array = array();

    include 'dbinc.php';


    $mysqli = new mysqli("$mysql_hostname", "$mysql_user", "$mysql_password", "$mysql_database");
    if (mysqli_connect_errno()) {
        printf("Connect failed: %s\n", mysqli_connect_error());
        return null;
    }


    $order = "SELECT 
			b.site_id, b.ClusterId, b.DeviceName, b.DeviceMake, b.Circle, b.SiteName, b.District, b.mobile, b.Longitude, b.Latitude, b.SiteIndoorType,  b.DG_KVA, b.SAM_Name, b.SAM_Number, b.AM_Name, b.AM_Number, b.Tech_Name,  b.Tech_Number, b.Comments,
			c.Name, 
			b.date_of_installation, b.DG_make, b.EB_capacity, b.EB_phase, b.battery_AH, b.battery_qty, b.battery_make, 
			b.DG2_KVA, b.DG2_MAKE, b.site_type 
		FROM siteinfo b, clusters c 
		WHERE (b.ClusterId = c.Id) AND b.id=" . $id;

    //print($order);
    //print("<br>");

    $result = $mysqli->query($order);

    $data = $result->fetch_row();

    $array[]    = $data[0];    //0 Site ID String
    $array[]    = $data[1];    //1 ClusterId
    $array[]    = $data[2];    //2 DeviceName
    $array[]    = $data[3];    //3 DeviceMake
    $array[]    = $data[4];    //4 Circle
    $array[]    = $data[5];    //5 SiteName
    $array[]    = $data[6];    //6 District
    $array[]    = $data[7];    //7 mobile
    $array[]    = $data[8];    //8 Longitude
    $array[]    = $data[9];    //9 Latitude
    $array[]    = $data[10];    //10 SiteIndoorType
    $array[]    = $data[11];    //11 DG_KVA
    $array[]    = $data[12];    //12 SAM_Name
    $array[]    = $data[13];    //13 SAM_Number
    $array[]    = $data[14];    //14 AM_Name
    $array[]    = $data[15];    //15 AM_Number
    $array[]    = $data[16];    //16 Tech_Name
    $array[]    = $data[17];    //17 Tech_Number
    $array[]    = $data[18];    //18 Comments
    $array[]    = $data[19];    //19 Cluster Name

    $quanta_id = GetLatestQuantaIdFromSiteId($data[0]);

    $array[]    = $quanta_id;    //20 Latest Data Quanta Id

    $array[]    = $data[20];    //21 date_of_installation
    $array[]    = $data[21];    //22 DG_make
    $array[]    = $data[22];    //23 EB_capacity
    $array[]    = $data[23];    //24 EB_phase
    $array[]    = $data[24];    //25 battery_AH
    $array[]    = $data[25];    //26 battery_qty
    $array[]    = $data[26];    //27 battery_make
    $array[]    = $data[27];    //28 DG2_KVA
    $array[]    = $data[28];    //29 DG2_MAKE
    $array[]    = $data[29];    //30 site_type

    $result->close();
    mysqli_close($mysqli);

    //var_dump($array);

    return $array;
}

function GetLatestQuantaIdFromSiteId($site_id)
{
    include 'dbinc.php';

    $retval = null;

    $mysqli = new mysqli("$mysql_hostname", "$mysql_user", "$mysql_password", "$mysql_database");
    if (mysqli_connect_errno()) {
        printf("Connect failed: %s\n", mysqli_connect_error());
        return null;
    }

    //Get first Id
    $sql = "SELECT
					id
				FROM
				  quanta
				WHERE
					site_id=\"$site_id\" 
				ORDER BY 
					date_time DESC 
				LIMIT 1";

    //echo "$sql <br>";

    $result = $mysqli->query($sql);
    //var_dump($result);
    if ($result !== false) {
        $data = $result->fetch_row();
        $id1        = $data[0];    //First Id
        //echo "<br>ID1: $id1";

        $retval = $id1;

        $result->close();
    }

    mysqli_close($mysqli);

    return $retval;
}


function GetSiteStatsFromId($site_id)
{
    //return GetSiteStatsFromIdStartInterval($site_id, null, 24);
    //return GetSiteStatsFromIdStartInterval($site_id, "2015-05-16 16:02:04", 24);
    return GetSiteStatsFromIdStartInterval($site_id, "2015-05-02 15:16:26", 24);
}

function GetSiteStatsFromIdStartInterval($site_id, $start_date_time, $hours)
{
    include 'dbinc.php';

    $sql_dt = "";
    if ($start_date_time == null) {
        $ist = gmmktime() + (330 * 60);    //+5:30 for IST
        $str_ist = date('Y-m-d H:i:s', $ist);
        //$sql_dt = "NOW()";
        $sql_dt = "\"" . $str_ist . "\"";
    } else {
        //$sql_dt = "\"".date_create_from_format('Y-m-d H:i:s', $start_date_time)."\"";
        $sql_dt = "\"" . $start_date_time . "\"";
    }


    $mysqli = new mysqli("$mysql_hostname", "$mysql_user", "$mysql_password", "$mysql_database");
    if (mysqli_connect_errno()) {
        printf("Connect failed: %s\n", mysqli_connect_error());
        return null;
    }

    //Get first Id
    $sql = "SELECT
					id,
					date_time
				FROM
				  quanta
				WHERE
					site_id=\"$site_id\" 
				ORDER BY
				  (ABS(TIMESTAMPDIFF(SECOND, date_time, ($sql_dt)))) ASC
				LIMIT 1";

    //echo "$sql <br>";

    $result = $mysqli->query($sql);
    //var_dump($result);
    if ($result !== false) {
        $data = $result->fetch_row();
        $id1        = $data[0];    //First Id
        //echo "<br>ID1: $id1";

        $new_dt = date_create_from_format('Y-m-d H:i:s', $data[1]);
        $new_dt->sub(new DateInterval('PT' . $hours . 'H'));
        $sql_dt = "\"" . $new_dt->format('Y-m-d H:i:s') . "\"";

        $result->close();


        //Get 2nd Id
        $sql = "SELECT
						id
					FROM
					  quanta
					WHERE
						site_id=\"$site_id\"  					  
					ORDER BY
					  (ABS(TIMESTAMPDIFF(SECOND, date_time, ($sql_dt)))) ASC
					LIMIT 2";

        //echo "$sql <br>";

        $result = $mysqli->query($sql);
        if ($result !== false) {
            $data1 = $result->fetch_row();
            $id2        = $data1[0];    //Second Id
            //echo "<br>ID2: $id2";

            if ($id1 == $id2) {
                //echo "<br>Same Id $id1 and $id2, We have results more than given time interval apart<br>";
                //Find the next record

                $data1 = $result->fetch_row();
                if ($data1 !== false) {
                    $id2 = $data1[0];    //Second Id
                } else {
                    //We have not enough records
                    $result->close();
                    mysqli_close($mysqli);
                    echo "Not Enough Records";
                    return null;
                }
            }

            $result->close();

            //echo "<br>Comparison between $id1 and $id2<br>";
            $array = GetComparisonFromIds($id1, $id2);

            mysqli_close($mysqli);
            return $array;
        }
    }

    mysqli_close($mysqli);
    echo "NULL";

    return null;
}

function GetSiteStatsFromIdStartEnd($site_id, $start_date_time, $end_date_time)
{
    include 'dbinc.php';

    if ($start_date_time == null) {
        return null;
    }
    if ($end_date_time == null) {
        return null;
    }

    $sql_sdt = "\"" . $start_date_time . "\"";
    $sql_edt = "\"" . $end_date_time . "\"";



    $mysqli = new mysqli("$mysql_hostname", "$mysql_user", "$mysql_password", "$mysql_database");
    if (mysqli_connect_errno()) {
        printf("Connect failed: %s\n", mysqli_connect_error());
        return null;
    }
    //Get first Id
    $sql = "SELECT
					id,
					date_time
				FROM
				  quanta
				WHERE
					site_id=\"$site_id\" 
				ORDER BY
				  (ABS(TIMESTAMPDIFF(SECOND, date_time, ($sql_edt)))) ASC
				LIMIT 1";


    $result = $mysqli->query($sql);
    //var_dump($result);

    if ($result !== false) {
        if ($result->num_rows > 0) {

            $data = $result->fetch_row();
            $id1        = $data[0];    //First Id
            //echo "<br>ID1: $id1";
            //var_dump( $id1);
            $result->close();


            //Get 2nd Id
            $sql = "SELECT
                            id
                        FROM
                        quanta
                        WHERE
                            site_id=\"$site_id\"  					  
                        ORDER BY
                        (ABS(TIMESTAMPDIFF(SECOND, date_time, ($sql_sdt)))) ASC
                        LIMIT 2";

            //echo "$sql <br>";

            $result = $mysqli->query($sql);
            if ($result !== false) {
                $data1 = $result->fetch_row();
                $id2        = $data1[0];    //Second Id
                //echo "<br>ID2: $id2";
                //var_dump( $id2);

                if ($id1 == $id2) {
                    //echo "<br>Same Id $id1 and $id2, We have results more than given time interval apart<br>";
                    //Find the next record

                    $data1 = $result->fetch_row();
                    if ($data1 !== false) {
                        $id2 = $data1[0];    //Second Id
                    } else {
                        //We have not enough records
                        $result->close();
                        mysqli_close($mysqli);
                        echo "Not Enough Records";
                        return null;
                    }
                }

                $result->close();
                //echo "<br>Comparison between $id1 and $id2<br>";
                $array = GetComparisonFromIds($id1, $id2);

                mysqli_close($mysqli);
                return $array;
            }
        } else {
            //No Data

        }
    }

    mysqli_close($mysqli);
    //echo "NULL";

    return null;
}

function GetDaySiteStatsFromId($site_id, $date)
{
    include 'dbinc.php';

    if ($site_id == null) {
        return null;
    }
    if ($date == null) {
        return null;
    }

    $sql_sdt = "\"" . $date . " 00:00:00" . "\"";
    $sql_edt = "\"" . $date . " 23:59:59" . "\"";



    $mysqli = new mysqli("$mysql_hostname", "$mysql_user", "$mysql_password", "$mysql_database");
    if (mysqli_connect_errno()) {
        printf("Connect failed: %s\n", mysqli_connect_error());
        return null;
    }

    //Get first Id
    $sql = "SELECT
					id
				FROM
				  quanta
				WHERE
					(site_id=\"$site_id\") AND (date_time between $sql_sdt and $sql_edt) 
				ORDER BY
				  date_time ASC
				LIMIT 1";

    //echo "$sql <br>";

    $result = $mysqli->query($sql);
    //var_dump($result);
    if ($result !== false) {
        $data = $result->fetch_row();
        $id1        = $data[0];    //First Id
        //echo "<br>ID1: $id1";

        $result->close();


        //Get 2nd Id
        $sql = "SELECT
						id
					FROM
					  quanta
					WHERE
					  (site_id=\"$site_id\") AND (date_time between $sql_sdt and $sql_edt)  					  
					ORDER BY
					  date_time DESC
					LIMIT 2";

        //echo "$sql <br>";

        $result = $mysqli->query($sql);
        if ($result !== false) {
            $data1 = $result->fetch_row();
            $id2        = $data1[0];    //Second Id
            //echo "<br>ID2: $id2";

            if ($id1 == $id2) {
                //Only one record
                return null;
            }

            $result->close();

            //echo "<br>Comparison between $id1 and $id2<br>";
            $array = GetComparisonFromIds($id1, $id2);

            mysqli_close($mysqli);
            return $array;
        }
    }

    mysqli_close($mysqli);
    echo "NULL";

    return null;
}

function GetDailySiteStatsFromIdStartEnd($site_id, $start_date_time, $end_date_time)
{
    include 'dbinc.php';

    if ($start_date_time == null) {
        return null;
    }
    if ($end_date_time == null) {
        return null;
    }

    $sql_sdt = "\"" . $start_date_time . "\"";
    $sql_edt = "\"" . $end_date_time . "\"";

    $array = array();



    $mysqli = new mysqli("$mysql_hostname", "$mysql_user", "$mysql_password", "$mysql_database");
    if (mysqli_connect_errno()) {
        printf("Connect failed: %s\n", mysqli_connect_error());
        return null;
    }

    //Get first Id


    $sql = "SELECT vm.id
					FROM quanta AS vm 
					JOIN  
					( 
						SELECT MIN( id ) AS id 
						FROM 
							quanta 
						WHERE 
							(site_id=\"$site_id\") and (date_time<=\"$start_date_time\") and (date_time>=\"$end_date_time\")  
						GROUP BY DATE( date_time ) 
					) AS vi  
						ON vi.id = vm.id
					";

    //echo "$sql <br>";

    $result = $mysqli->query($sql);

    if ($result !== false) {
        while ($data = $result->fetch_row()) {
            $arr1 = GetDeviceRecordFromId($data[0]);
            //var_dump($arr1);
            $arr2 = array();
            $arr2[] = date_create_from_format('Y-m-d H:i:s', $arr1[5]);    //0 Date Time
            $arr2[] = $arr1[26]; //1 DG
            $arr2[] = $arr1[27]; //2 Mains
            $arr2[] = $arr1[28]; //3 Batt
            $array[] = $arr2;
        }
    }

    //var_dump($array);

    $result->close();

    mysqli_close($mysqli);
    //echo "NULL";

    return $array;
}


function GetQuantaStatusFromId($id)
{
    include 'dbinc.php';

    $mysqli = new mysqli("$mysql_hostname", "$mysql_user", "$mysql_password", "$mysql_database");
    if (mysqli_connect_errno()) {
        printf("Connect failed: %s\n", mysqli_connect_error());
        exit();
    }

    $order = "SELECT 
					date_time, status
				FROM quanta 
				WHERE id=" . $id;

    //echo "order: $order<br>";
    $result = $mysqli->query($order);

    $data = $result->fetch_row();

    $array = array();
    $array[] = $data[0];
    $array[] = $data[1];

    $result->close();

    return $array;
}


//Shift through the quanta's for the site and get originating time of the given alarm(s)
function GetAlarmHistory($site_id, $history)
{
    //var_dump($history);
    include 'dbinc.php';

    $mysqli = new mysqli("$mysql_hostname", "$mysql_user", "$mysql_password", "$mysql_database");
    if (mysqli_connect_errno()) {
        printf("Connect failed: %s\n", mysqli_connect_error());
        exit();
    }
    $order = "SELECT ID " .
        "FROM quanta WHERE site_id='$site_id' " .
        "ORDER BY date_time DESC ";

    //echo "order: $order<br>";
    $result = $mysqli->query($order);
    //var_dump($result);

    while ($data = $result->fetch_row()) {
        $array = GetQuantaStatusFromId($data[0]);
        //var_dump($array);
        $stats = unpack("C*", $array[1]);

        $done = true;
        for ($i = 0; $i < count($history); $i++) {
            if ($history[$i][2] == false) {    //False == last date not found
                if (($stats[$history[$i][0]] & $history[$i][1]) == false) {
                    //Alarm No Longer Present.
                    //Last entry was having last alert
                    $history[$i][2] = true;
                } else {
                    //Alarm Present, just update time
                    $history[$i][3] = $array[0];
                }

                $done = false;
            }
        }

        if ($done == true) {
            //No more alert remaining, just exit loop
            break;
        }
    }

    //var_dump($history);

    $result->close();
    mysqli_close($mysqli);

    return $history;
}

function GetSiteDateRange($site_id)
{
    include 'dbinc.php';

    $array = array();

    $mysqli = new mysqli("$mysql_hostname", "$mysql_user", "$mysql_password", "$mysql_database");
    if (mysqli_connect_errno()) {
        printf("Connect failed: %s\n", mysqli_connect_error());
        exit();
    }
    $order = "SELECT ID, date_time " .
        "FROM quanta WHERE site_id='$site_id' " .
        "ORDER BY date_time ASC LIMIT 1";

    //echo "order: $order<br>";
    $result = $mysqli->query($order);
    //var_dump($result);

    $data = $result->fetch_row();
    $array[] = $data[1];

    $result->close();

    $order = "SELECT ID, date_time " .
        "FROM quanta WHERE site_id='$site_id' " .
        "ORDER BY date_time DESC LIMIT 1";

    //echo "order: $order<br>";
    $result = $mysqli->query($order);


    $data = $result->fetch_row();
    $array[] = $data[1];

    $result->close();



    mysqli_close($mysqli);

    //var_dump($site_id);
    //var_dump($array);

    return $array;
}


function GetDetailsFromSiteId($site_id)
{
    $array = array();

    include 'dbinc.php';


    $mysqli = new mysqli("$mysql_hostname", "$mysql_user", "$mysql_password", "$mysql_database");
    if (mysqli_connect_errno()) {
        printf("Connect failed: %s\n", mysqli_connect_error());
        return null;
    }

    $sql = "SELECT 
			b.ClusterId, c.name, b.site_id, b.DeviceName, b.DeviceMake, b.Circle, b.SiteName, b.District, 
			b.mobile, b.Longitude, b.Latitude, b.SiteIndoorType,  b.DG_KVA, 
			b.SAM_Name, b.SAM_Number, b.AM_Name, b.AM_Number, b.Tech_Name,  b.Tech_Number, b.Comments, 
			b.date_of_installation, b.DG_make, b.EB_capacity, b.EB_phase, b.battery_AH, b.battery_qty, b.battery_make, 
			b.DG2_KVA, b.DG2_MAKE, b.site_type 
			
			FROM siteinfo b, clusters c
			WHERE (b.ClusterId = c.Id) AND b.site_id='" . $site_id . "'";

    //echo $sql;

    $result = $mysqli->query($sql);

    if ($result !== false) {
        $data = $result->fetch_row();

        //var_dump($data);

        $array[]    = $data[0];        // ClusterId
        $array[]    = $data[1];        // Cluster Name
        $array[]    = $data[2];        // site_id
        $array[]    = $data[3];        // DeviceName
        $array[]    = $data[4];        // DeviceMake
        $array[]    = $data[5];        // Circle
        $array[]    = $data[6];        // SiteName
        $array[]    = $data[7];        // District
        $array[]    = $data[8];        // Site Mobile
        $array[]    = $data[9];        // Longitude

        $array[]    = $data[10];    // Latitude
        $array[]    = $data[11];    // SiteIndoorType
        $array[]    = $data[12];    // DG_KVA
        $array[]    = $data[13];    // SAM_Name
        $array[]    = $data[14];    // SAM_Number
        $array[]    = $data[15];    // AM_Name
        $array[]    = $data[16];    // AM_Number
        $array[]    = $data[17];    // Tech_Name
        $array[]    = $data[18];    // Tech_Number

        $array[]    = $data[19];    // Comments

        $array[]    = $data[20];    // date_of_installation
        $array[]    = $data[21];    // DG_make
        $array[]    = $data[22];    // EB_capacity
        $array[]    = $data[23];    // EB_phase
        $array[]    = $data[24];    // battery_AH
        $array[]    = $data[25];    // battery_qty
        $array[]    = $data[26];    // battery_make

        $array[]    = $data[27];    // DG2_KVA
        $array[]    = $data[28];    // DG2_MAKE
        $array[]    = $data[29];    // site_type
    } else {
        printf("\nSQL ERROR: %s\n", $mysqli->error);
    }

    $result->close();
    mysqli_close($mysqli);

    //var_dump($array);

    return $array;
}

function DataExport($site_id, $export_page_no, $excel_format)
{
    //var_dump($site_id);
    include 'dbinc.php';

    $rec_limit = 5000;
    $rec_start = ($export_page_no - 1) * $rec_limit;

    $mysqli = new mysqli("$mysql_hostname", "$mysql_user", "$mysql_password", "$mysql_database");
    if (mysqli_connect_errno()) {
        printf("Connect failed: %s\n", mysqli_connect_error());
        exit();
    }

    $where_clause = "WHERE (site_id = '$site_id') ";
    //		$filter_data_type = 0;
    //		if ($_GET['filter_data_type'] == 1) {$where_clause = $where_clause." AND type=1  "; $filter_data_type = 1;}
    //		if ($_GET['filter_data_type'] == 2) {$where_clause = $where_clause." AND type=0  "; $filter_data_type = 2;}

    /* Get total number of records */
    $sql = "SELECT count(ID) FROM quanta " . $where_clause;
    //echo "Final: $sql<br>";
    $retval = $mysqli->query($sql);
    if (!$retval) {
        die('No Data' . $mysqli->error);
    }

    $row = $retval->fetch_array(MYSQLI_NUM);
    $rec_count = $row[0];

    $order = "SELECT ID " .
        "FROM quanta " . $where_clause .
        "ORDER BY date_time DESC " .
        "LIMIT $rec_limit OFFSET $rec_start ";

    //echo "order: $order<br>";
    $result = $mysqli->query($order);


    // output headers so that the file is downloaded rather than displayed
    header('Content-Type: text/csv; charset=utf-8');
    header('Content-Disposition: attachment; filename=exported_data.csv');

    // create a file pointer connected to the output stream
    $output = fopen('php://output', 'w');

    // Heading and other information

    $date = new DateTime("now", new DateTimeZone('Asia/Kolkata'));
    $localtime_str = $date->format('Y-m-d H:i:s');

    fputcsv($output, array('Site Data Exported on ' . $localtime_str));
    //fputcsv($output, array('Filter Details -- Data Type: '.$filter_data_type_str.', Site: '.$site_list_data_type_str));
    fputcsv($output, array(''));

    // output the column headings
    $first_time = true;
    $count = 0;
    while ($data = $result->fetch_row()) {
        $array = GetDeviceRecordFromIdExport($data[0], true);

        if ($first_time) {
            if ($array[6] == 1) {
                if ($array[2] == 2) {
                    fputcsv($output, array(
                        'Id', 'Periodic / Fault', 'Site Id', 'Date Time', 'Device Code', 'Status Bits', 'Temperature',
                        'Fuel Level', 'Fuel Data', 'Batt Voltage', 'R-Phase Current', 'Y-Phase Current', 'B-Phase Current', 'Mains Frequency', 'DG Frequency',
                        'DG-R Phase Voltage', 'DG-Y Phase Voltage', 'DG-B Phase Voltage', 'LCU1 Output Voltage', 'LCU2 Output Voltage', 'LCU3 Output Voltage', 'Mains Voltage - R Phase', 'Mains Voltage - Y Phase', 'Mains Voltage - B Phase', 'DG Running Hours', 'Mains Run Hours', 'Batt Run Hours', 'O/P Mains Energy', 'DG Energy', 'I/P Mains Energy', 'DG Battery Voltage', 'Battery Charging Current', 'Battery Discharging Current',
                        'Battery Status', 'Battery Backup Time', 'Battery Charging Energy', 'Battery Discharging Energy', 'DG2 frequency', 'DG2-R phase Voltage VAC', 'DG2-Y phase Voltage VAC', 'DG2-B phase Voltage VAC', 'DG2 Running Hours Hrs', 'DG2 Energy Kwh', 'DG2 Battery Voltage VDC', 'Fuel Level of DG2 %', 'Fuel data of DG2 liter'
                    ));


                    //					$size1 = sizeof($array);
                    //					$size2 = sizeof($array[$size1-1]);
                    //					fputcsv($output,array("$size1.","$size2.","$size"));
                } else {
                    fputcsv($output, array(
                        'Id', 'Periodic / Fault', 'Site Id', 'Date Time', 'Device Code', 'Status Bits', 'Temperature',
                        'Fuel Level', 'Fuel Data', 'Batt Voltage', 'R-Phase Current', 'Y-Phase Current', 'B-Phase Current', 'Mains Frequency', 'DG Frequency',
                        'DG-R Phase Voltage', 'DG-Y Phase Voltage', 'DG-B Phase Voltage', 'LCU1 Output Voltage', 'LCU2 Output Voltage', 'LCU3 Output Voltage', 'Mains Voltage - R Phase', 'Mains Voltage - Y Phase', 'Mains Voltage - B Phase', 'DG Running Hours', 'Mains Run Hours', 'Batt Run Hours', 'O/P Mains Energy', 'DG Energy', 'I/P Mains Energy', 'DG Battery Voltage', 'Battery Charging Current', 'Battery Discharging Current',
                        'Battery Status', 'Battery Backup Time', 'Battery Charging Energy', 'Battery Discharging Energy'
                    ));
                }
            } elseif ($array[6] == 2) {
                fputcsv($output, array(
                    'Id', 'Periodic / Fault', 'Site Id', 'Date Time', 'Device Code', 'Status Bits', 'Temperature',
                    'Solar array 1 Voltage', 'Solar array 2 Voltage', 'Solar array 3 Voltage', 'Array 1 current', 'Array 2 current', 'Array 3 current', 'R-Phase current', 'Y-Phase current', 'B-Phase current', 'Inverter Frequency', 'Inverter R-Phase Voltage', 'Inverter Y-Phase Voltage', 'Inverter B-Phase Voltage', 'Solar Run Hours', 'Inverter Run Hours', 'Solar + Inverter Run Hours', 'Inverter Energy', 'Solar Energy', 'Battery Bank Voltage',
                    'Battery Charging Current', 'Battery Discharging Current', 'Battery Status', 'Battery Charging Energy', 'Battery Discharging Energy'
                ));
            } elseif ($array[6] == 32) {
                fputcsv($output, array(
                    'Id', 'Periodic / Fault', 'Site Id', 'Date Time', 'Device Code', 'Status Bits', 'Temperature',
                    'Site Batt.bank Voltage', 'Site Batt.bank Voltage', 'Load  current', 'Mains Voltage', 'DG Voltage', 'Input Mains Voltage –B Phase', 'Battery1 Charging current', 'Battery1 Discharging current', 'Battery2 Charging current', 'Battery2 Discharging current', 'Battery1 status', 'Battery2 status'
                ));
            }

            $first_time = false;
        }

        if ($array[6] == 1) {
            if ($array[2] == 2) {
                $size = sizeof($array);
                $array = array_merge($array, $array[$size - 1]);
                unset($array[$size - 1]);
            }
        }
        //deleting the $array[1] and $array[2] values
        $array = array_diff_key($array, [1 => "xy", 2 => "xy"]);
        //var_dump($array);

        //For excel put status bits like ="xxxxxxxxxx"
        if ($excel_format == 1) {
            $array[7] = '="' . $array[7] . '"';
        }


        fputcsv($output, $array);

        $count++;
    }


    $result->close();
    mysqli_close($mysqli);

    fclose($output);
}

// Data export for reports-root
function DataExportReportRoot($site_id, $export_page_no, $excel_format)
{
    //var_dump($site_id);
    include 'dbinc.php';

    $rec_limit = 5000;
    $rec_start = ($export_page_no - 1) * $rec_limit;

    $mysqli = new mysqli("$mysql_hostname", "$mysql_user", "$mysql_password", "$mysql_database");
    if (mysqli_connect_errno()) {
        printf("Connect failed: %s\n", mysqli_connect_error());
        exit();
    }

    $where_clause = "WHERE (site_id = '$site_id') ";
    //		$filter_data_type = 0;
    //		if ($_GET['filter_data_type'] == 1) {$where_clause = $where_clause." AND type=1  "; $filter_data_type = 1;}
    //		if ($_GET['filter_data_type'] == 2) {$where_clause = $where_clause." AND type=0  "; $filter_data_type = 2;}

    /* Get total number of records */
    $sql = "SELECT count(ID) FROM quanta " . $where_clause;
    //echo "Final: $sql<br>";
    $retval = $mysqli->query($sql);
    if (!$retval) {
        die('No Data' . $mysqli->error);
    }

    $row = $retval->fetch_array(MYSQLI_NUM);
    $rec_count = $row[0];

    $order = "SELECT ID " .
        "FROM quanta " . $where_clause .
        "ORDER BY date_time DESC " .
        "LIMIT $rec_limit OFFSET $rec_start ";

    //echo "order: $order<br>";
    $result = $mysqli->query($order);


    // output headers so that the file is downloaded rather than displayed
    header('Content-Type: text/csv; charset=utf-8');
    header('Content-Disposition: attachment; filename=exported_data.csv');

    // create a file pointer connected to the output stream
    $output = fopen('php://output', 'w');

    // Heading and other information

    $date = new DateTime("now", new DateTimeZone('Asia/Kolkata'));
    $localtime_str = $date->format('Y-m-d H:i:s');

    fputcsv($output, array('Site Data Exported on ' . $localtime_str));
    //fputcsv($output, array('Filter Details -- Data Type: '.$filter_data_type_str.', Site: '.$site_list_data_type_str));
    fputcsv($output, array(''));

    // output the column headings
    $first_time = true;
    $count = 0;
    while ($data = $result->fetch_row()) {
        $array = GetDeviceRecordFromIdExport($data[0], true);

        if ($first_time) {
            if ($array[6] == 1) {
                if ($array[2] == 2) {
                    fputcsv($output, array(
                        'Id', 'Periodic / Fault', 'Site Id', 'Date Time', 'Device Code', 'Status Bits', 'Temperature',
                        'Fuel Level', 'Fuel Data', 'Batt Voltage', 'R-Phase Current', 'Y-Phase Current', 'B-Phase Current', 'Mains Frequency', 'DG Frequency',
                        'DG-R Phase Voltage', 'DG-Y Phase Voltage', 'DG-B Phase Voltage', 'LCU1 Output Voltage', 'LCU2 Output Voltage', 'LCU3 Output Voltage', 'Mains Voltage - R Phase', 'Mains Voltage - Y Phase', 'Mains Voltage - B Phase', 'DG Running Hours', 'Mains Run Hours', 'Batt Run Hours', 'O/P Mains Energy', 'DG Energy', 'I/P Mains Energy', 'DG Battery Voltage', 'Battery Charging Current', 'Battery Discharging Current',
                        'Battery Status', 'Battery Backup Time', 'Battery Charging Energy', 'Battery Discharging Energy', 'DG2 frequency', 'DG2-R phase Voltage VAC', 'DG2-Y phase Voltage VAC', 'DG2-B phase Voltage VAC', 'DG2 Running Hours Hrs', 'DG2 Energy Kwh', 'DG2 Battery Voltage VDC', 'Fuel Level of DG2 %', 'Fuel data of DG2 liter'
                    ));


                    //					$size1 = sizeof($array);
                    //					$size2 = sizeof($array[$size1-1]);
                    //					fputcsv($output,array("$size1.","$size2.","$size"));
                } else {
                    fputcsv($output, array(
                        'Id', 'Periodic / Fault', 'Site Id', 'Date Time', 'Device Code', 'Status Bits', 'Temperature',
                        'Fuel Level', 'Fuel Data', 'Batt Voltage', 'R-Phase Current', 'Y-Phase Current', 'B-Phase Current', 'Mains Frequency', 'DG Frequency',
                        'DG-R Phase Voltage', 'DG-Y Phase Voltage', 'DG-B Phase Voltage', 'LCU1 Output Voltage', 'LCU2 Output Voltage', 'LCU3 Output Voltage', 'Mains Voltage - R Phase', 'Mains Voltage - Y Phase', 'Mains Voltage - B Phase', 'DG Running Hours', 'Mains Run Hours', 'Batt Run Hours', 'O/P Mains Energy', 'DG Energy', 'I/P Mains Energy', 'DG Battery Voltage', 'Battery Charging Current', 'Battery Discharging Current',
                        'Battery Status', 'Battery Backup Time', 'Battery Charging Energy', 'Battery Discharging Energy'
                    ));
                }
            } elseif ($array[6] == 2) {
                fputcsv($output, array(
                    'Id', 'Periodic / Fault', 'Site Id', 'Date Time', 'Device Code', 'Status Bits', 'Temperature',
                    'Solar array 1 Voltage', 'Solar array 2 Voltage', 'Solar array 3 Voltage', 'Array 1 current', 'Array 2 current', 'Array 3 current', 'R-Phase current', 'Y-Phase current', 'B-Phase current', 'Inverter Frequency', 'Inverter R-Phase Voltage', 'Inverter Y-Phase Voltage', 'Inverter B-Phase Voltage', 'Solar Run Hours', 'Inverter Run Hours', 'Solar + Inverter Run Hours', 'Inverter Energy', 'Solar Energy', 'Battery Bank Voltage',
                    'Battery Charging Current', 'Battery Discharging Current', 'Battery Status', 'Battery Charging Energy', 'Battery Discharging Energy'
                ));
            } elseif ($array[6] == 32) {
                fputcsv($output, array(
                    'Id', 'Periodic / Fault', 'Site Id', 'Date Time', 'Device Code', 'Status Bits', 'Temperature',
                    'Site Batt.bank Voltage', 'Site Batt.bank Voltage', 'Load  current', 'Mains Voltage', 'DG Voltage', 'Input Mains Voltage –B Phase', 'Battery1 Charging current', 'Battery1 Discharging current', 'Battery2 Charging current', 'Battery2 Discharging current', 'Battery1 status', 'Battery2 status'
                ));
            }

            $first_time = false;
        }

        if ($array[6] == 1) {
            if ($array[2] == 2) {
                $size = sizeof($array);
                $array = array_merge($array, $array[$size - 1]);
                unset($array[$size - 1]);
            }
        }
        //deleting the $array[1] and $array[2] values
        $array = array_diff_key($array, [1 => "xy", 2 => "xy"]);
        //var_dump($array);

        //For excel put status bits like ="xxxxxxxxxx"
        if ($excel_format == 1) {
            $array[7] = '="' . $array[7] . '"';
        }


        fputcsv($output, $array);

        $count++;
    }


    $result->close();
    mysqli_close($mysqli);

    fclose($output);
}

function GetDeviceRecordFromIdExport($id, $isExport)
{
    $array = array();

    include 'dbinc.php';


    $mysqli = new mysqli("$mysql_hostname", "$mysql_user", "$mysql_password", "$mysql_database");
    if (mysqli_connect_errno()) {
        printf("Connect failed: %s\n", mysqli_connect_error());
        return null;
    }

    $order = "SELECT 
			ID, timestamp , data_validated , type , site_id , date_time , device_id , status , raw_data
			FROM quanta 
			WHERE id=" . $id;


    //echo $order;

    //$order = "SELECT * FROM quanta where id=".$id;
    $result = $mysqli->query($order);
    $data = $result->fetch_row();

    $array[]     = $data[0];        //0Unique Id
    if ($isExport) {
        $array[]     = $data[1];
    }        //1Time Stamp by Server
    if ($isExport) {
        $array[]    = $data[2];
    }        //2Validated by Server
    $array[]     = $data[3];        //3Type 0 - Periodic / 1 - Fault
    $array[]    = $data[4];        //4String Site Id
    $array[]    = $data[5];        //5Date Time by Device
    $array[]    = $data[6];        //6Device Id/ Only 1 & 2 known for now
    if (!$isExport) {
        $array[]    = $data[7];
    }        //7Status Bits - To Expand
    else {
        $array[]    = bin2hex($data[7]);
    }        //7Status Bits in Hex for Export
    $data_raw    = $data[8];        //Raw Data, for device specific information

    //var_dump($array);
    //$array = array_merge($array, DeviceGetStatusBits($data[7]));
    //var_dump($array);


    if ($data[6] == 1) {
        if ($data[2] == 2) {
            $array = array_merge($array, DeviceGetDataDouble($data_raw));
        } else {
            $array = array_merge($array, DeviceGetData($data_raw));
        }
    } elseif ($data[6] == 2) {
        $array = array_merge($array, DeviceGetData($data_raw));
    } elseif ($data[6] == 32) {
        $array = array_merge($array, DeviceGetDataDoubleBattery($data_raw));
    }

    $result->close();
    mysqli_close($mysqli);

    //var_dump($array);

    return $array;
}
function GetRecordCount($site_id)
{
    include 'dbinc.php';

    $mysqli = new mysqli("$mysql_hostname", "$mysql_user", "$mysql_password", "$mysql_database");
    if (mysqli_connect_errno()) {
        printf("Connect failed: %s\n", mysqli_connect_error());
        exit();
    }
    $where_clause = "WHERE (site_id = '$site_id') ";
    //		$filter_data_type = 0;
    //		if ($_GET['filter_data_type'] == 1) {$where_clause = $where_clause." AND type=1  "; $filter_data_type = 1;}
    //		if ($_GET['filter_data_type'] == 2) {$where_clause = $where_clause." AND type=0  "; $filter_data_type = 2;}

    /* Get total number of records */
    $sql = "SELECT count(ID) FROM quanta " . $where_clause;
    //echo "Final: $sql<br>";
    $retval = $mysqli->query($sql);
    if (!$retval) {
        die('No Data' . $mysqli->error);
    }

    $row = $retval->fetch_array(MYSQLI_NUM);
    $rec_count = $row[0];
    mysqli_close($mysqli);
    return $rec_count;
}

function GetLatesQuantaForDatatable($site_id)
{
    $resp = array();
    include 'dbinc.php';

    $mysqli = new mysqli("$mysql_hostname", "$mysql_user", "$mysql_password", "$mysql_database");
    if (mysqli_connect_errno()) {
        printf("Connect failed: %s\n", mysqli_connect_error());
        exit();
    }

    $sql = "SELECT ID,timestamp,status,raw_data FROM quanta WHERE site_id = " . $site_id . "ORDER BY timestamp DESC LIMIT 1";

    $result = $mysqli->query($sql);
    if (!$result) {
        die('No Data' . $mysqli->error);
    }
    $data = $result->fetch_row();
    $resp[] = $data[2];
    return $resp;
}

function processNotifications($siteNo, $message)
{
    //var_dump($siteNo);
    //var_dump($message);
    $msg = array(
        'title'    => $siteNo,
        'body'     => $message,
        'data' => 'Background Data String',
        'sound' => 'default',
    );

    $tokens = getTokens($siteNo);

    //var_dump($tokens);
    foreach ($tokens as $token) {
        quantaNotifications($msg, $token);
    }
}

function getTokens($siteNo)
{
    //sql:...

    include "dbinc.php";
    $mysqli = new mysqli("$mysql_hostname", "$mysql_user", "$mysql_password", "$mysql_database");
    if (mysqli_connect_errno()) {
        printf("Connect failed: %s\n", mysqli_connect_error());
        exit();
    }
    //Check if token already exists.
    $sql = "  SELECT pushNotifications.token
			FROM  `pushNotifications` 
			JOIN authentication ON pushNotifications.userId = authentication.Id
			JOIN siteinfo ON siteinfo.customerId = authentication.Customer
			WHERE siteinfo.site_id =  '$siteNo'
			LIMIT 0 , 30";
    //echo $sql;
    // prepare query statement
    $result = $mysqli->query($sql);

    $tokens = array();

    if ($result) {
        while ($row = $result->fetch_row()) {
            $tokens[] = $row[0];
        }
    }
    $result->close();
    $mysqli->close();

    return $tokens;
}

function quantaNotifications($msg, $token)
{

    //var_dump($msg);
    //var_dump($token);
    // call api...
    //echo 'Hello';
    define('API_ACCESS_KEY', 'AAAAosM1N1c:APA91bEi6S_yfaNPHh_oLxe-5464OynYMxoky7wj0XSK_CE3UugpRzx0CUBtRxfVk1ZEJSwY7OzzupEnF0W_qPrAZhemW6DKuVN1h5Rst7sawTtKgR603cNdQB_69179MrCbLN8KLC7p');
    //   $registrationIds = ;
    #prep the bundle
    // $msg = array(
    //     'title'	=> 'SISPL00001',
    //     'body' 	=> 'An Alarm Has Triggered On Site ID SISPL00001',
    //     'data' => 'Background Data String'
    //       );
    $fields = array(
        'to'        => $token,
        'priority' => 'high',
        'data'    => $msg
    );
    echo json_encode($fields);
    $headers = array(
        'Authorization: key=' . API_ACCESS_KEY,
        'Content-Type: application/json'
    );
    #Send Reponse To FireBase Server
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
    $result = curl_exec($ch);
    echo $result;
    curl_close($ch);
}

        #now try your stuff
        # ok
        # ty kudos
        //:)
        // :-D
        // :-
        // thik ab kkijie kijieyountoo go ahread <yup class=""></yup>
