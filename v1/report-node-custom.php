<?php
session_start();
if (!isset($_SESSION['login_user']))
{
	header("Location: login.php");
}

if($_SERVER["REQUEST_METHOD"] == "GET")
{
	$site_id = $_GET['site_id'];
	$_SESSION['range_data_type'] = $_GET['range_data_type'];
	$_SESSION['range_start'] = $_GET['range_start'];
	$_SESSION['range_end'] = $_GET['range_end'];
	$_SESSION['chart_type'] = $_GET['type'];
	
	if($_SESSION['chart_type'] == null) $_SESSION['chart_type']="Pie";
	
	//var_dump($_SESSION['chart_type']);
	
	$hours_interval = 24;
	$hours_interval_txt = "24 Hours";
	$hours_interval_txt_adj = "last ";
	$range_data_type = 0;
	if ($_SESSION['range_data_type'] == 1) {$range_data_type = 1; $hours_interval=24*7; $hours_interval_txt = "One Week";}
	if ($_SESSION['range_data_type'] == 2) {$range_data_type = 2; $hours_interval=24*30; $hours_interval_txt = "One Month";}
	if ($_SESSION['range_data_type'] == 3) 
	{
		$range_data_type = 3; 
		$hours_interval_txt = "Custom Range";
		$hours_interval_txt_adj = "";
		
		//echo "<br>S: ".$_SESSION['range_start'];
		//echo "<br>E: ".$_SESSION['range_end'];
		
		$startSec = strtotime($_SESSION['range_start']);
		$endSec = strtotime($_SESSION['range_end']);
		
		$hours_interval = abs(round(($endSec - $startSec) / 3600));
		//echo "<br>H: ".$hours_interval;
	}
}
	
	include 'common.php';
	
	$daterange = GetSiteDateRange($site_id);

	$sum = null;
	if ($range_data_type == 3)
	{
		$array = GetSiteStatsFromIdStartEnd($site_id, $_SESSION['range_end'], $_SESSION['range_start']);	
	}
	else
	{
		$array = GetSiteStatsFromIdStartInterval($site_id, null, $hours_interval);
	}
	$sum = AddUp($array, $sum);		
	
	//var_dump($array);
	//var_dump( $array[7]->format('Y/m/d H:i:s'));
	//var_dump ($array[7]->$date);
	//var_dump( $array[9]);
	//echo $array[9]->y." ".$array[9]->m." ".$array[9]->d;

	if ($array[9]->days == 0)
	{
		$hours_interval_txt = $array[9]->h." hours";
	}
	else
	{
		$hours_interval_txt = $array[9]->days." days";
	}
	
	
	
	if (($range_data_type == 3) && ($_SESSION['chart_type'] == "Bar"))
	{
		//Bar Chart
		$arrayBar = GetDailySiteStatsFromIdStartEnd($site_id, $_SESSION['range_end'], $_SESSION['range_start']);
		//var_dump($arrayBar);
	}
	
	//var_dump($array);
	
?>
	<style>
		.color-box {
			width: 14px;
			height: 14px;
			display: inline-block;
			left: 5px;
			top: 5px;
		}
	</style>
	
	<div style="margin-top:20px;"><center><h3>Custom Report</h3><h4>(<?php echo $array[7]->format('Y/m/d H:i:s')." - ".$array[8]->format('Y/m/d H:i:s');?>)</h4></center></div>
	
	<div class="btn-toolbar" role="toolbar" aria-label="time-frame-buttons">
	  <a role="button" class="btn <?php  if ($range_data_type==0) echo "btn-primary"; else echo "btn-default"; ?>" href="#" onclick='event.preventDefault(); AsyncLoad("node-power-supply.php?site_id=<?php echo $site_id; ?>&range_data_type=0", "#id_power_supply")'>One Day</a>
	  <a role="button" class="btn <?php  if ($range_data_type==1) echo "btn-primary"; else echo "btn-default"; ?>" href="#" onclick='event.preventDefault(); AsyncLoad("node-power-supply.php?site_id=<?php echo $site_id; ?>&range_data_type=1", "#id_power_supply")'>One Week</a>
	  <a role="button" class="btn <?php  if ($range_data_type==2) echo "btn-primary"; else echo "btn-default"; ?>" href="#" onclick='event.preventDefault(); AsyncLoad("node-power-supply.php?site_id=<?php echo $site_id; ?>&range_data_type=2", "#id_power_supply")'>One Month</a>
	  <a role="button" class="btn <?php  if ($range_data_type==3) echo "btn-primary"; else echo "btn-default"; ?>" href="#myRange" data-toggle="collapse">Custom Range</a>
	</div>
	
	<div id="myRange" class="collapse">
		<p class='bg-info' style="margin-top:10px;">&nbsp;&nbsp;Select date and time between <strong><?php echo $daterange[0]; ?></strong> and <strong><?php echo $daterange[1]; ?></strong></p>
		<div class="container">
			<div class="row">
				<div class='col-sm-3'>
					<div class="form-group">
						
						<div class='input-group date' id='datetimepickerFrom'>
							<input type='text' class="form-control" />
							<span class="input-group-addon">
								<span class="glyphicon glyphicon-calendar"></span>
							</span>
						</div>
					</div>
				</div>
				<div class='col-sm-3'>
					<div class="form-group">
						<div class='input-group date' id='datetimepickerTo'>
							<input type='text' class="form-control" />
							<span class="input-group-addon">
								<span class="glyphicon glyphicon-calendar"></span>
							</span>
						</div>
					</div>
				</div>
				<div class='col-sm-2'>
				
					<select class="form-control" id="Chart">
					  <option value="Pie">Pie Chart</option>
					  <option value="Bar">Bar Chart</option>
					</select>
				
					
				</div>
				<div class='col-sm-1'>
					<button href="#" class="btn btn-default" onclick="applyRangeString()">Show</button>
				</div>
				<script type="text/javascript">
				
					function applyRangeString()
					{
						var strStart = encodeURIComponent($('#datetimepickerFrom').data("DateTimePicker").date().format('YYYY-MM-DD HH:mm:ss'));
						var strEnd = encodeURIComponent($('#datetimepickerTo').data("DateTimePicker").date().format('YYYY-MM-DD HH:mm:ss'));
						
						AsyncLoad("node-power-supply.php?site_id=<?php echo $site_id; ?>&range_data_type=3&range_start="+strStart+"&range_end="+strEnd+"&type="+$('#Chart').val(), "#id_power_supply");
						
					
					}
					
					
					$(function () {
						$('#datetimepickerFrom').datetimepicker({
							format: 'YYYY-MM-DD HH:mm:ss',
							minDate: moment('<?php echo $daterange[0]; ?>', 'YYYY-MM-DD HH:mm:ss'),
							maxDate: moment('<?php echo $daterange[1]; ?>', 'YYYY-MM-DD HH:mm:ss'),
							date: moment('<?php echo $daterange[0]; ?>', 'YYYY-MM-DD HH:mm:ss'),
						});
						$('#datetimepickerTo').datetimepicker({
							format: 'YYYY-MM-DD HH:mm:ss',
							minDate: moment('<?php echo $daterange[0]; ?>', 'YYYY-MM-DD HH:mm:ss'),
							maxDate: moment('<?php echo $daterange[1]; ?>', 'YYYY-MM-DD HH:mm:ss'),
							date: moment('<?php echo $daterange[1]; ?>', 'YYYY-MM-DD HH:mm:ss'),
						});
						$("#datetimepickerFrom").on("dp.change", function (e) {
							$('#datetimepickerTo').data("DateTimePicker").minDate(e.date);
							$('#displayFrom').html(e.date.format('YYYY-MM-DD HH:mm:ss'));
						});
						$("#datetimepickerTo").on("dp.change", function (e) {
							$('#datetimepickerFrom').data("DateTimePicker").maxDate(e.date);
						});
						
					});
					
					 
				</script>
			</div>
		</div>
	</div>
	

	<div style="margin-top:20px;">
	<table class="table table-bordered table-condensed">
		<tr><td  class='col-md-3' style="color: #990099"><div class="color-box" style="background-color: #990099;"></div> Site on Mains </td><td  class='col-md-9'><strong><?php echo round($sum[2], 1)." hours"; ?></strong></tr>
		<tr><td style="color: #3366cc"><div class="color-box" style="background-color: #3366cc;"></div> Site on DG </td><td><strong><?php echo round($sum[1], 1)." hours"; ?></strong></tr>
		<tr><td style="color: #dc3912"><div class="color-box" style="background-color: #dc3912;"></div> Site on Battery </td><td><strong><?php echo round($sum[3], 1)." hours"; ?></strong></tr>
		<tr><td style="color: #4bb2c5"><div class="color-box" style="background-color: #4bb2c5;"></div> DG Stop Fail Time </td><td><strong><?php echo round($array[15], 1)." hours"; ?></strong></tr>
	</table>
	</div>
	
	<div id="pie1" style="margin-top:20px;"></div>
	
	
	
	
	
<?php
	
	if (($range_data_type == 3) && ($_SESSION['chart_type'] == "Bar"))
	{
		//BAR CHART
?>
	
		<script class="code" type="text/javascript">$(document).ready(function(){
			var s1 = [<?php for ($i=1; $i<count($arrayBar); $i++) print ($arrayBar[$i][1]-$arrayBar[$i-1][1]).","; ?>];
			var s2 = [<?php for ($i=1; $i<count($arrayBar); $i++) print ($arrayBar[$i][2]-$arrayBar[$i-1][2]).","; ?>];
			var s3 = [<?php for ($i=1; $i<count($arrayBar); $i++) print ($arrayBar[$i][3]-$arrayBar[$i-1][3]).","; ?>];
			
			var t1 = [<?php for ($i=1; $i<count($arrayBar); $i++) print "'".$arrayBar[$i][0]->diff($arrayBar[$i-1][0])->format('%a day %H hrs')."',"; ?>];
			
			// Can specify a custom tick Array.
			// Ticks should match up one for each y value (category) in the series.
			var ticks = [<?php for ($i=1; $i<count($arrayBar); $i++) print "'".$arrayBar[$i][0]->format('d/m')."',"; ?>];
			
			//console.log(s1);
			//console.log(s2);
			//console.log(s3);
			//console.log(t1);
			//console.log(ticks);

			var plot1 = $.jqplot('pie1', [s1, s2, s3], {
				// The "seriesDefaults" option is an options object that will
				// be applied to all series in the chart.
				seriesDefaults:{
					renderer:$.jqplot.BarRenderer,
					rendererOptions: {fillToZero: true}
				},
				// Custom labels for the series are specified with the "label"
				// option on the series option.  Here a series option object
				// is specified for each series.
				series:[
					{label:'DG'},
					{label:'Mains'},
					{label:'Battery'}
				],
				seriesColors: [ "#990099", "#3366cc", "#dc3912", "#ff9900", "#109618"],
				axes: {
					// Use a category axis on the x axis and use our custom ticks.
					xaxis: {
						renderer: $.jqplot.CategoryAxisRenderer,
						ticks: ticks
					},
					// Pad the y axis just a little so bars can get close to, but
					// not touch, the grid boundaries.  1.2 is the default padding.
					yaxis: {
						pad: 1.0,
						tickOptions: {formatString: '%d hrs'}
					}
				},
				highlighter: {
					tooltipContentEditor: function (str, seriesIndex, pointIndex) {

						tip = "";
						if (seriesIndex == 0)
							tip = "Mains<br>" + parseFloat(s1[str-1]).toFixed(1);
						else if (seriesIndex == 1)
							tip = "DG<br>" + parseFloat(s2[str-1]).toFixed(1);
						else if (seriesIndex == 2)
							tip = "Battery<br>" + parseFloat(s3[str-1]).toFixed(1);
						
						tip = tip + " hrs /<br>(" + t1[str-1] + ")";
						
						return tip;
						//return s1[str] + "<br/> additional data" + seriesIndex + " " + pointIndex;
					},

					// other options just for completeness
					show: true,
					showTooltip: true,
					tooltipFade: true,
					sizeAdjust: 10,
					formatString: '%s',
					tooltipLocation: 'n',
					useAxesFormatters: false,
				},
				
			});
		});</script>
	
<?php
	
	}
	else
	{
		//PIE CHART

?>	
	
	<script class="code" type="text/javascript">$(document).ready(function(){
		var plot1 = $.jqplot('pie1', [[['Mains Running',<?php echo $sum[2]; ?>],['DG Running',<?php echo $sum[1]; ?>],['Battery Running',<?php echo $sum[3]; ?>]]], {
			gridPadding: {top:0, bottom:38, left:0, right:0},
			seriesDefaults:{
				renderer:$.jqplot.PieRenderer, 
				trendline:{ show:false }, 
				rendererOptions: { padding: 8, showDataLabels: true }
			},
			legend:{
				show:false, 
				placement: 'inside', 
				rendererOptions: {
					numberRows: 3
				}, 
				location:'s',
				marginTop: '15px'
			},
			seriesColors: [ "#990099", "#3366cc", "#dc3912", "#ff9900", "#109618"],
			grid: {
				drawBorder: false, 
				drawGridlines: false,
				background: '#ffffff',
				shadow:false
			},
			highlighter: {
				//tooltipContentEditor: function (str, seriesIndex, pointIndex) {
				//	return str + "<br/> additional data";
				//},

				// other options just for completeness
				show: true,
				showTooltip: true,
				tooltipFade: true,
				sizeAdjust: 10,
				formatString: '%s',
				tooltipLocation: 'n',
				useAxesFormatters: false,
			},
		});
	});</script>

<?php
	}
?>

