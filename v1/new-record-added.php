 <?php 
	include 'common-alarms.php';
	include 'insert-alarm-test.php';
	
	$id = $_GET["id"];
	//echo "\nGetting Device Detail from Id: $id"; 
	
	//insertAlarmTest($id);

	$array1 = GetDeviceRecordFromId($id);
	
	//var_dump($array1);
	
	if ($array1[5] == "0000-00-00 00:00:00")
	{
		//print "<<<< NULL DATE >>>>><br><br>";
	}
	else
	{
		//print "<<<< OK DATE >>>>><br><br>";
	
		//var_dump($array1);
		
		//print "\nSite Id: $array1[4]";
		
		//$alarms = GetAlarmBits($array1[7]);			
		$alarms = GetAlarmBitsEx($array1[7], $array1[48], $array1[65]);
		
		$text = GetAlarmText($alarms, $array1[4], $array1[43], $array1[5]);
		
		
		//print("\nText: $text\n\n");

		
		if ($text === false)
		{
			//Clear alarm if any for the site
			//print("\n\n No Alarm - Clear DB of Alarms, if any \n\n");
			ResetAlarm($array1[4]);
		}
		else
		{
			//ADD/UPDATE ALARM DB
			//print("\n\n Alarm Found - process \n\n");
			ProcessAlarm($array1[4], $id, $array1[7], 1, $array1[5], $text, $alarms, $array1[48], $array1[65]);
		}
		
		
		
		
		//Check if Site Mode (Auto or Manual) has changed
		
		//Get previous record for the site
		$last_id = null;
		$last_status = null;
		$found_state = 0;
		$send_email = false;
		$subject = "";
		$email_message = "";
		
		include 'dbinc.php';
			
		$mysqli = new mysqli("$mysql_hostname", "$mysql_user", "$mysql_password", "$mysql_database");
		if (mysqli_connect_errno()) {
			printf("Connect failed: %s\n", mysqli_connect_error());
			exit();
		}
		$order = "SELECT ID, status ".
					"FROM quanta WHERE site_id='$array1[4]' ".
					"ORDER BY date_time DESC LIMIT 5";

		//echo "order: $order<br>";	
		$result = $mysqli->query($order);
		//var_dump($result);
		
		while($data = $result->fetch_row()){
			//$array = GetQuantaStatusFromId($data[0]);
			//var_dump($data);
			
			if ($found_state == 0)
			{
				if ($data[0] == $id)
					$found_state = 1;
			}
			else if ($found_state == 1)
			{
				$last_status = $data[1];
				$found_state = 2;
				break;
			}
			
		}
		
		$result->close();	
		mysqli_close($mysqli);
		
		
		if ($found_state == 2)
		{
			//Found
			$now_in_manual = IsManualMode($array1[7]);
			$last_in_manual = IsManualMode($last_status);
			
			if ($now_in_manual != $last_in_manual)
			{
				//Mode has changed, send mail accordingly
				if ($now_in_manual)
				{
					$subject = "Warning: Site changed to Manual Mode (".$array1[4].")";
					$email_message = "Site <b>".$array1[4]."</b> changed to <b>Manual</b> Mode at <b>".$array1[5]."</b>.";
					$send_email = true;
				}
				else
				{
					$subject = "Site changed to Auto Mode (".$array1[4].")";
					$email_message = "Site <b>".$array1[4]."</b> changed to <b>Auto</b> Mode at <b>".$array1[5]."</b>.";
					$send_email = true;
				}
				
			}
			else
			{
				if ($now_in_manual)
					$email_message =  "Same State: Site in Manual Mode";
				else
					$email_message =  "Same State: Site in Auto Mode";
			}

			
		
		}
		else
		{
			//Not Found
			//Send Mail if in Manual Mode
			$now_in_manual = IsManualMode($array1[7]);
			//echo "Last State Not Found: ";
			if ($now_in_manual)
			{
				$subject = "Warning: Site is in Manual Mode (".$array1[4].")";
				$email_message = "Site <b>".$array1[4]."</b> is in <b>Manual</b> Mode at <b>".$array1[5]."</b>.";
				$send_email = true;
			}
			else
			{
				$email_message =  "Site in Auto Mode";
			}
		
		}
		
		
		if ($send_email)
		{
			//Send Mail
			$email_cc = "Ram.sharma@sysinfra.in";
			
			if ((strcasecmp($array1[4], "ATCRJPANYL") == 0)
					|| (strcasecmp($array1[4], "TAUSAR0001") == 0)
					|| (strcasecmp($array1[4], "PHARDOD402") == 0)
					|| (strcasecmp($array1[4], "BABTARA000") == 0)
					|| (strcasecmp($array1[4], "SALWAKHURD") == 0))
			{
				$email = "Parvat.Shekhawat@atctower.in, Milind.Ghawghawe@atctower.in";
			}
			else if ((strcasecmp($array1[4], "ATCTIKARA1") == 0)
					|| (strcasecmp($array1[4], "SISPLTD007") == 0)
					|| (strcasecmp($array1[4], "SAROO00001") == 0))
			{
				$email = "Sunil.Sikarwal@americantower.com, Milind.Ghawghawe@atctower.in";
			}
			else if (strcasecmp($array1[4], "GOBARDHANP") == 0)
			{
				$email = "Biswajit.Mal@atctower.in, Mithun.Ghosh@atctower.in";
			}
			else if (strcasecmp($array1[4], "ATCSRDONGR") == 0)
			{
				$email = "Ravbharat.Singh@atctower.in, Milind.Ghawghawe@atctower.in";
			}
			else if ((strcasecmp($array1[4], "NANOOWALI0") == 0)
					|| (strcasecmp($array1[4], "DILDHANI01") == 0)
					|| (strcasecmp($array1[4], "SHIMLA3043") == 0)
					|| (strcasecmp($array1[4], "SOOPKA0001") == 0))
			{
				$email = "Rajesh.Goyal@AmericanTower.com, Milind.Ghawghawe@atctower.in";
			}
			//else if (strcasecmp($array1[4], "BHAGOMBTS1") == 0)
			//{
			//	//$email = "munna.pandey@idea.adityabirla.com, Ashish.Goyal@idea.adityabirla.com, psridhar.rao@idea.adityabirla.com";
			//	$email = "toakhilesh@gmail.com";
			//}
			else if (strcasecmp($array1[4], "RAJASANSI1") == 0)
			{
				$email = "sanjeev.sachdeva@idea.adityabirla.com, Ashish.Goyal@idea.adityabirla.com, psridhar.rao@idea.adityabirla.com";
			}
			else if (strcasecmp($array1[4], "BPLVODA001") == 0)
			{
				$email = "vipinsharma305@gmail.com, manojbarua.00@gmail.com, ChandraShekhar.Malviya@vodafone.com, Gyan.Rajpoot@vodafone.com, anil.indolia2@vodafone.com, Neeraj.Tiwari2@vodafone.com, Basant.Tiwari@vodafone.com";
			}
			else if (strcasecmp($array1[4], "RAIHSIIDC1") == 0)
			{
				$email = "sdekdlspt@bsnl.co.in, derrlspt@bsnl.co.in";
			}
			else if (strcasecmp($array1[4], "MPCG153650") == 0)
			{
				$email = "Akhil1.Gupta@Bharti-Infratel.in, Anshu.Sachdeva@Bharti-Infratel.in, Manish.Baghel@Bharti-Infratel.in, Manish.Khare@bharti-infratel.in, Nilesh.Gosawami@Bharti-Infratel.in, Rajeev.Nigam@Bharti-Infratel.in, Suresh.Kumar@Bharti-Infratel.in, Harendra.shukla@bharti-infratel.in, sudhir.kumar@bharti-infrarel.in";
			}
			else if (strcasecmp($array1[4], "BILPMP2670") == 0)
			{
				$email = "Akhil1.Gupta@Bharti-Infratel.in, Anshu.Sachdeva@Bharti-Infratel.in, Manish.Baghel@Bharti-Infratel.in, Manish.Khare@bharti-infratel.in, Nilesh.Gosawami@Bharti-Infratel.in, Rajeev.Nigam@Bharti-Infratel.in, Suresh.Kumar@Bharti-Infratel.in, Harendra.shukla@bharti-infratel.in, sudhir.kumar@bharti-infrarel.in, Jagdish1.Dashore@Bharti-Infratel.in, Munendra.Mishra@Bharti-Infratel.in";
			}
			else if ((strcasecmp($array1[4], "SMHVO11088") == 0)
					|| (strcasecmp($array1[4], "SISPMH2289") == 0))
			{
				$email = "Siddhesh.Borkar@americantower.com, Kiran.Patil@atctower.in, Aishvarya.Ratnaparkhi@atctower.in, Gaffar.Mulla@atctower.in";
			}
			else if ((strcasecmp($array1[4], "TVINOIDA15") == 0)
					|| (strcasecmp($array1[4], "TVINOIDO66") == 0))
			{
				$email = "vizitk@tower-vision.com, rajeshkr@tower-vision.com, mukeshk@tower-vision.com, amits@tower-vision.com, sanjaym@tower-vision.com";
			}
			else if ((strcasecmp($array1[4], "FULIANBSNL") == 0)
					|| (strcasecmp($array1[4], "DIDWARA001") == 0)
					|| (strcasecmp($array1[4], "SINDHVI002") == 0))
			{
				$email = "dgmcfajd@gmail.com, sdetransnwa@gmail.com";
			}
			else
			{
				$email = "Ram.sharma@sysinfra.in";
			}
			
			
			//Add Salutation and Signature 
			$email_message = "Dear Sir,<br><br><br>".$email_message;
			$sig = file_get_contents('./mailsignature.htm', FILE_USE_INCLUDE_PATH);
			$email_message .= $sig;
			
			$result = CommSendMailHtmlWithCC($email, $email_cc, "admin@sysinfra.in", "support@sysinfra.in", $subject, $email_message);
			
			//$result = "";
			//echo $subject."<br><br>".$email_message."<br>".$result;
		}
	
	}
	
	
	
	
	
	
	function IsManualMode($data_status) 
	{
		//Return only the applicable alarm bits
		
		$array = array();
		$stats = unpack ( "C*" , $data_status );
    		
		//Auto/Man Mode 0 Means  Auto Mode, 1 Means  Man Mode
		$mode = ($stats[5] & 0x04);
		
		if ($mode == 0)
			return false;
		else
			return true;
	}

 ?>
 
 
 
 