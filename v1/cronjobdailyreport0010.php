 <?php 
 
	// Cron Job to be Run every Day at 00:10 HRS
	//	40 18 * * * /usr/bin/php-cgi -f /var/www/html/v1/cronjobdailyreport0010.php >/dev/null 2>&1
	
	include 'common-alarms.php';
	
	function ReportAlarmSiteHistory()
	{
		include 'dbinc.php';
		
		$mysqli = new mysqli("$mysql_hostname", "$mysql_user", "$mysql_password", "$mysql_database");
		if (mysqli_connect_errno()) {
			printf("Connect failed: %s\n", mysqli_connect_error());
			exit();
		}

		$sql = "SELECT `id`, `site`, `quanta_id`, `alarm_flag`, `alarm_flag_version`, `start_time`, `last_message_level`, `last_message_time` 
					FROM `sitealarmhistory` WHERE 1";

		//echo "SQL: $sql<br>";	
		$result = $mysqli->query($sql);
		//var_dump($result);
		
		$site_in_alarm = 0;
		$site_alarms_text = "";
		
		//printf("\nSQL ERROR: %s\n", $mysqli->error);
		
		if ($result !== false)
		{
			$row_count = $result->num_rows;
			
			if ($row_count == 0)
			{
				
			}
			else
			{
				while($data = $result->fetch_row())
				{
					$site_in_alarm++;
					
					$site_details = GetDetailsFromSiteId($data[1]);
					//$alarms = GetAlarmBits($data[3]);
					$alarms = GetAlarmBitsEx($data[3], $site_details[11], $site_details[29]);
					
					//($alarms, $site_id, $site_name, $alarm_time)
					$text = GetAlarmText($alarms, $data[1], "", $data[5]);
					
					$site_alarms_text .= $text."<br>";
					
					
					//delete alarm with id $data[0]
					
					$del_sql = "DELETE FROM `sitealarmhistory` WHERE id=".$data[0];
					$mysqli->query($del_sql);
				}
			}
		}

				
		$result->close();	
		$mysqli->close();
		
		$retval = "";
		
		if ($site_in_alarm == 0)
		{
			$retval = "<p>No Alarm History</p>";
		}
		else
		{
			$retval = $site_in_alarm." <h3>Alarm History</h3>".$site_alarms_text;
		}
	
		return $retval;
	}
	
	//Duplicate Method
	function GetAllSiteList()
	{
		include 'dbinc.php'; 
		
		
		$array = array();
		
		

		$mysqli = new mysqli("$mysql_hostname", "$mysql_user", "$mysql_password", "$mysql_database");
		if (mysqli_connect_errno())
		{
			printf("Connect failed: %s\n", mysqli_connect_error());
			return null;
		}
		
		$sql = "SELECT site_id, SiteName 
					FROM siteinfo ";

		//echo "$sql <br>";				
		$result = $mysqli->query($sql);

		if ($result !== false)
		{
			while($data = $result->fetch_row())
			{
				
				$arr2 = array();
				$arr2[] = $data[0]; 
				$arr2[] = $data[1]; 
				
				$array[] = $arr2;
			}
			
		}
		
		//var_dump($array);
		
		$result->close();
		
		mysqli_close($mysqli);
		//echo "NULL";
		
		return $array;
	
	}
	
	function ReportPowerSupply()
	{
		$hours_interval = 24;
		$message = "";
		
		$message .= '<table rules="all" style="border-color: #666;" cellpadding="10">';
		$message .= "<tr><td><strong>Site</strong> </td><td><strong>Date Range</strong> </td><td><strong>Mains Running</strong> </td><td><strong>DG Running</strong> </td><td><strong>Battery Running</strong> </td></tr>";
		
		$site_list = GetAllSiteList();
		for ($i=0; $i<count($site_list); $i++)
		{
			
			$site_id = $site_list[$i][0];
			$site_name = $site_list[$i][1];
			
			//echo $site_id."<br>";
			$sum = null;
			
			$array = GetSiteStatsFromIdStartInterval($site_id, null, $hours_interval);
			$sum = AddUp($array, $sum);
			
			$site_from = $array[7]->format('Y/m/d H:i');
			$site_to = $array[8]->format('Y/m/d H:i');
			
			$message .= "<tr><td>".$site_name."</td><td>".$site_from." - ".$site_to."</td><td>".number_format($sum[2], 1)."</td><td>".number_format($sum[1], 1)."</td><td>".number_format($sum[3], 1)."</td></tr>";
			//$message .= '<b>'.$site_name.'</b>: Mains Running: <b>'.$sum[2].'</b>  DG Running: <b>'.$sum[1].'</b>  Battery Running: <b>'.$sum[3]."</b><br>";
			
		}
		
		$message .= "</table>";
		
		return $message;
	}
	

	
	
	
	//Get Alarm Site History, and delete it
	$alarm_site_txt = ReportAlarmSiteHistory();
	$email_message .= "<h3>Site Report for Alarm History</h3>".$alarm_site_txt;
	
	$power_report = ReportPowerSupply();
	$email_message .= "<h3>Site Power Report</h3>".$power_report;
	
	//For Testing
	//echo "Mail Send Result: $email_message";
	
	//Production
	$result = CommSendMailHtml("toakhilesh@gmail.com, Ram.sharma@sysinfra.in, dushyant@sysinfra.in", "admin@sysinfra.in", "support@sysinfra.in", "SIS-AXS Daily Site Report", $email_message);
	echo "Mail Send Result: $result";

	

 
 ?>