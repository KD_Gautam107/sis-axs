
<?php	
	if ($data_quanta[6] == 1)
	{
		if ($data_array[30] == 1) 
		{ 
			echo "<h3>Hybrid Controller with Double DG (0x01)</h3>";
			
			echo ("<table class='table table-striped'>");
		
				echo "<tr><td class='col-md-4'>Room Temperature</td><td class='col-md-8'><strong>";
				echo("$data_quanta[8] Deg C");
				echo "</strong></td></tr>";

				echo "<tr><td>DG 1 Fuel Level</td><td><strong>";
				echo("$data_quanta[9] %");
				echo "</strong></td></tr>";
				
				echo "<tr><td>DG 1 Fuel Data</td><td><strong>";
				echo("$data_quanta[10] Liters");
				echo "</strong></td></tr>";
				
				echo "<tr><td>Site Batt. bank Voltage</td><td><strong>";
				echo("$data_quanta[11] VDC");
				echo "</strong></td></tr>";
				
				echo "<tr><td>DG 1 R-Phase current</td><td><strong>";
				echo("$data_quanta[12] A");
				echo "</strong></td></tr>";

				echo "<tr><td>DG 1 Y-Phase current</td><td><strong>";
				echo("$data_quanta[13] A");
				echo "</strong></td></tr>";

				echo "<tr><td>DG 1 B-Phase current</td><td><strong>";
				echo("$data_quanta[14] A");
				echo "</strong></td></tr>";

				echo "<tr><td>Mains Frequency</td><td><strong>";
				echo("$data_quanta[15] Hz");
				echo "</strong></td></tr>";

				echo "<tr><td>DG 1 frequency</td><td><strong>";
				echo("$data_quanta[16] Hz");
				echo "</strong></td></tr>";

				echo "<tr><td>DG 1-R phase Voltage</td><td><strong>";
				echo("$data_quanta[17] VAC");
				echo "</strong></td></tr>";

				echo "<tr><td>DG 1-Y phase Voltage</td><td><strong>";
				echo("$data_quanta[18] VAC");
				echo "</strong></td></tr>";

				echo "<tr><td>DG 1-B phase Voltage</td><td><strong>";
				echo("$data_quanta[19] VAC");
				echo "</strong></td></tr>";

				echo "<tr><td>LCU1 Output Voltage</td><td><strong>";
				echo("$data_quanta[20] VAC");
				echo "</strong></td></tr>";

				echo "<tr><td>LCU2 Output Voltage</td><td><strong>";
				echo("$data_quanta[21] VAC");
				echo "</strong></td></tr>";

				echo "<tr><td>LCU3 Output Voltage</td><td><strong>";
				echo("$data_quanta[22] VAC");
				echo "</strong></td></tr>";

				echo "<tr><td>Input Mains Voltage - R Phase</td><td><strong>";
				echo("$data_quanta[23] VAC");
				echo "</strong></td></tr>";

				echo "<tr><td>Input Mains Voltage - Y Phase</td><td><strong>";
				echo("$data_quanta[24] VAC");
				echo "</strong></td></tr>";

				echo "<tr><td>Input Mains Voltage - B Phase</td><td><strong>";
				echo("$data_quanta[25] VAC");
				echo "</strong></td></tr>";
				

				echo "<tr><td>DG 1 Running Hours</td><td><strong>";
				echo("$data_quanta[26] Hrs");
				echo "</strong></td></tr>";
				
				echo "<tr><td>DG 2 Running Hours</td><td><strong>";
				echo($data_quanta[38][6]." Hrs");
				echo "</strong></td></tr>";
				
				echo "<tr><td>Mains RUN HOURS</td><td><strong>";
				echo("$data_quanta[27] Hrs");
				echo "</strong></td></tr>";
				
				echo "<tr><td>Batt RUN HOURS</td><td><strong>";
				echo("$data_quanta[28] Hrs");
				echo "</strong></td></tr>";
				
				echo "<tr><td>O/P Mains Energy</td><td><strong>";
				echo("$data_quanta[29] Kwh");
				echo "</strong></td></tr>";
				
				echo "<tr><td>DG 1 Energy</td><td><strong>";
				echo("$data_quanta[30] Kwh");
				echo "</strong></td></tr>";
				
				echo "<tr><td>I/P Mains Energy</td><td><strong>";
				echo("$data_quanta[31] Kwh");
				echo "</strong></td></tr>";

				echo "<tr><td>DG 1 Battery Voltage</td><td><strong>";
				echo("$data_quanta[32] VDC");
				echo "</strong></td></tr>";

				echo "<tr><td>Battery Charging current</td><td><strong>";
				echo("$data_quanta[33] Amp");
				echo "</strong></td></tr>";

				echo "<tr><td>Battery Discharging current</td><td><strong>";
				echo("$data_quanta[34] Amp");
				echo "</strong></td></tr>";

				echo "<tr><td>Battery status</td><td><strong>";
				echo("$data_quanta[35] %");
				echo "</strong></td></tr>";

				echo "<tr><td>Battery back up time</td><td><strong>";
				echo("$data_quanta[36] hours");
				echo "</strong></td></tr>";
					
					
				echo "<tr><td>Battery Charging Energy</td><td><strong>";				
				echo("$data_quanta[37] Kwh");
				echo "</strong></td></tr>";
				
				echo "<tr><td>Battery Discharging Energy</td><td><strong>";				
				echo($data_quanta[38][0]." Kwh");
				echo "</strong></td></tr>";

				echo "<tr><td>DG 2 frequency</td><td><strong>";
				echo($data_quanta[38][1]." Hz");
				echo "</strong></td></tr>";
		
				echo "<tr><td>DG 2-R phase Voltage</td><td><strong>";
				echo($data_quanta[38][2]." VAC");
				echo "</strong></td></tr>";

				echo "<tr><td>DG 2-Y phase Voltage</td><td><strong>";
				echo($data_quanta[38][3]." VAC");
				echo "</strong></td></tr>";

				echo "<tr><td>DG 2-B phase Voltage</td><td><strong>";
				echo($data_quanta[38][4]." VAC");
				echo "</strong></td></tr>";
				
				echo "<tr><td>DG 2 Energy</td><td><strong>";
				echo($data_quanta[38][7]." Kwh");
				echo "</strong></td></tr>";
				
				echo "<tr><td>DG 2 Battery Voltage</td><td><strong>";
				echo($data_quanta[38][5]." VDC");
				echo "</strong></td></tr>";
				
				echo "<tr><td>DG 2 Fuel Level</td><td><strong>";
				echo($data_quanta[38][8]." %");
				echo "</strong></td></tr>";
				
				echo "<tr><td>DG 2 Fuel Data</td><td><strong>";
				echo($data_quanta[38][9]." Liters");
				echo "</strong></td></tr>";
		
		
			echo ("</table>");
		}
		else
		{
			echo "<h3>Hybrid Controller (0x01)</h3>";
			
			echo ("<table class='table table-striped'>");
		
				echo "<tr><td class='col-md-4'>Room Temperature</td><td class='col-md-8'><strong>";
				echo("$data_quanta[8] Deg C");
				echo "</strong></td></tr>";

				echo "<tr><td>Fuel Level</td><td><strong>";
				echo("$data_quanta[9] %");
				echo "</strong></td></tr>";
				
				echo "<tr><td>Fuel Data</td><td><strong>";
				echo("$data_quanta[10] Liters");
				echo "</strong></td></tr>";
				
				echo "<tr><td>Site Batt. bank Voltage</td><td><strong>";
				echo("$data_quanta[11] VDC");
				echo "</strong></td></tr>";
				
				echo "<tr><td>R-Phase current</td><td><strong>";
				echo("$data_quanta[12] A");
				echo "</strong></td></tr>";

				echo "<tr><td>Y-Phase current</td><td><strong>";
				echo("$data_quanta[13] A");
				echo "</strong></td></tr>";

				echo "<tr><td>B-Phase current</td><td><strong>";
				echo("$data_quanta[14] A");
				echo "</strong></td></tr>";

				echo "<tr><td>Mains Frequency</td><td><strong>";
				echo("$data_quanta[15] Hz");
				echo "</strong></td></tr>";

				echo "<tr><td>DG frequency</td><td><strong>";
				echo("$data_quanta[16] Hz");
				echo "</strong></td></tr>";

				echo "<tr><td>DG-R phase Voltage</td><td><strong>";
				echo("$data_quanta[17] VAC");
				echo "</strong></td></tr>";

				echo "<tr><td>DG-Y phase Voltage</td><td><strong>";
				echo("$data_quanta[18] VAC");
				echo "</strong></td></tr>";

				echo "<tr><td>DG-B phase Voltage</td><td><strong>";
				echo("$data_quanta[19] VAC");
				echo "</strong></td></tr>";

				echo "<tr><td>LCU1 Output Voltage</td><td><strong>";
				echo("$data_quanta[20] VAC");
				echo "</strong></td></tr>";

				echo "<tr><td>LCU2 Output Voltage</td><td><strong>";
				echo("$data_quanta[21] VAC");
				echo "</strong></td></tr>";

				echo "<tr><td>LCU3 Output Voltage</td><td><strong>";
				echo("$data_quanta[22] VAC");
				echo "</strong></td></tr>";

				echo "<tr><td>Input Mains Voltage - R Phase</td><td><strong>";
				echo("$data_quanta[23] VAC");
				echo "</strong></td></tr>";

				echo "<tr><td>Input Mains Voltage - Y Phase</td><td><strong>";
				echo("$data_quanta[24] VAC");
				echo "</strong></td></tr>";

				echo "<tr><td>Input Mains Voltage - B Phase</td><td><strong>";
				echo("$data_quanta[25] VAC");
				echo "</strong></td></tr>";
				

				echo "<tr><td>DG Running Hours</td><td><strong>";
				echo("$data_quanta[26] Hrs");
				echo "</strong></td></tr>";
				
				echo "<tr><td>Mains RUN HOURS</td><td><strong>";
				echo("$data_quanta[27] Hrs");
				echo "</strong></td></tr>";
				
				echo "<tr><td>Batt RUN HOURS</td><td><strong>";
				echo("$data_quanta[28] Hrs");
				echo "</strong></td></tr>";
				
				echo "<tr><td>O/P Mains Energy</td><td><strong>";
				echo("$data_quanta[29] Kwh");
				echo "</strong></td></tr>";
				
				echo "<tr><td>DG Energy</td><td><strong>";
				echo("$data_quanta[30] Kwh");
				echo "</strong></td></tr>";
				
				echo "<tr><td>I/P Mains Energy</td><td><strong>";
				echo("$data_quanta[31] Kwh");
				echo "</strong></td></tr>";

				echo "<tr><td>DG Battery Voltage</td><td><strong>";
				echo("$data_quanta[32] VDC");
				echo "</strong></td></tr>";

				echo "<tr><td>Battery Charging current</td><td><strong>";
				echo("$data_quanta[33] Amp");
				echo "</strong></td></tr>";

				echo "<tr><td>Battery Discharging current</td><td><strong>";
				echo("$data_quanta[34] Amp");
				echo "</strong></td></tr>";

				echo "<tr><td>Battery status</td><td><strong>";
				echo("$data_quanta[35] %");
				echo "</strong></td></tr>";

				echo "<tr><td>Battery back up time</td><td><strong>";
				echo("$data_quanta[36] hours");
				echo "</strong></td></tr>";
					
					
				echo "<tr><td>Battery Charging Energy</td><td><strong>";				
				echo("$data_quanta[37] Kwh");
				echo "</strong></td></tr>";
				
				echo "<tr><td>Battery Discharging Energy</td><td><strong>";				
				echo("$data_quanta[38] Kwh");
				echo "</strong></td></tr>";

		
			echo ("</table>");
		}
	}
	else if ($data_quanta[6] == 2)
	{
		if($data_array[30] == 3)
		{
			echo "<h3>Hybrid Controller with [ 'TYPE DEMO' ] (0x01)</h3>";

			echo ("<table class='table table-striped'>");

			echo "<tr><td class='col-md-4'>Room/Ambient Temperature</td><td class='col-md-8'><strong>";
			echo("$data_quanta[8] Deg C");
			echo "</strong></td></tr>";

			echo "<tr><td>Site Batt.bank Voltage</td><td><strong>";
			echo("$data_quanta[9] VDC");
			echo "</strong></td></tr>";

			echo "<tr><td>Site Batt.bank Voltage</td><td><strong>";
			echo("$data_quanta[10] VDC");
			echo "</strong></td></tr>";

			echo "<tr><td>Total Load Current</td><td><strong>";
			echo("$data_quanta[11] Amp");
			echo "</strong></td></tr>";

			echo "<tr><td>Mains Voltage-R Phase</td><td><strong>";
			echo("$data_quanta[12] VAC");
			echo "</strong></td></tr>";

			echo "<tr><td>Mains Voltage-Y Phase</td><td><strong>";
			echo("$data_quanta[13] VAC");
			echo "</strong></td></tr>";

			echo "<tr><td>Mains Voltage-B Phase</td><td><strong>";
			echo("$data_quanta[14] VAC");
			echo "</strong></td></tr>";

			echo "<tr><td>Battery1 Charging current</td><td><strong>";
			echo("$data_quanta[15] Amp");
			echo "</strong></td></tr>";

			echo "<tr><td>Battery1 Discharging current</td><td><strong>";
			echo("$data_quanta[16] Amp");
			echo "</strong></td></tr>";

			echo "<tr><td>Battery2 Charging current</td><td><strong>";
			echo("$data_quanta[17] Amp");
			echo "</strong></td></tr>";

			echo "<tr><td>Battery2 Discharging current</td><td><strong>";
			echo("$data_quanta[18] Amp");
			echo "</strong></td></tr>";

			echo "<tr><td>Battery1 status</td><td><strong>";
			echo("$data_quanta[19] %");
			echo "</strong></td></tr>";

			echo "<tr><td>Battery2 status</td><td><strong>";
			echo("$data_quanta[20] %");
			echo "</strong></td></tr>";

			echo "<tr><td>Mains Energy</td><td><strong>";
			echo("$data_quanta[21] Kwh");
			echo "</strong></td></tr>";

			echo "<tr><td>Batter#1 CHG Energy</td><td><strong>";
			echo("$data_quanta[22] Kwh");
			echo "</strong></td></tr>";

			echo "<tr><td>Batter#1 DISCHG Energy</td><td><strong>";
			echo("$data_quanta[23] Kwh");
			echo "</strong></td></tr>";

			echo "<tr><td>Batter#2 CHG Energy</td><td><strong>";
			echo("$data_quanta[24] Kwh");
			echo "</strong></td></tr>";

			echo "<tr><td>Batter#2 DISCHG Energy</td><td><strong>";
			echo("$data_quanta[25] Kwh");
			echo "</strong></td></tr>";
		}
		else
		{
			echo "<h3>Hybrid Controller (0x02) - Solar</h3>";

			echo ("<table class='table table-striped'>");

			echo "<tr><td class='col-md-4'>Room Temperature</td><td class='col-md-8'><strong>";
			echo("$data_quanta[8] Deg C");
			echo "</strong></td></tr>";

			echo "<tr><td>Solar array 1 Voltage</td><td><strong>";
			echo("$data_quanta[9] V");
			echo "</strong></td></tr>";

			echo "<tr><td>Solar array 2 Voltage</td><td><strong>";
			echo("$data_quanta[10] V");
			echo "</strong></td></tr>";

			echo "<tr><td>Solar array 3 Voltage</td><td><strong>";
			echo("$data_quanta[11] V");
			echo "</strong></td></tr>";

			echo "<tr><td>Array 1 Current</td><td><strong>";
			echo("$data_quanta[12] A");
			echo "</strong></td></tr>";

			echo "<tr><td>Array 2 Current</td><td><strong>";
			echo("$data_quanta[13] A");
			echo "</strong></td></tr>";

			echo "<tr><td>Array 3 Current</td><td><strong>";
			echo("$data_quanta[14] A");
			echo "</strong></td></tr>";

			echo "<tr><td>Inverter R-Phase Load</td><td><strong>";
			echo("$data_quanta[15] A");
			echo "</strong></td></tr>";

			echo "<tr><td>Inverter Y-Phase Load</td><td><strong>";
			echo("$data_quanta[16] A");
			echo "</strong></td></tr>";

			echo "<tr><td>Inverter B-Phase Load</td><td><strong>";
			echo("$data_quanta[17] A");
			echo "</strong></td></tr>";

			echo "<tr><td>Inverter Frequency</td><td><strong>";
			echo("$data_quanta[18] Hz");
			echo "</strong></td></tr>";

			echo "<tr><td>Inverter R-Phase Voltage</td><td><strong>";
			echo("$data_quanta[19] V");
			echo "</strong></td></tr>";

			echo "<tr><td>Inverter Y-Phase Voltage</td><td><strong>";
			echo("$data_quanta[20] V");
			echo "</strong></td></tr>";

			echo "<tr><td>Inverter B-Phase Voltage</td><td><strong>";
			echo("$data_quanta[21] V");
			echo "</strong></td></tr>";

			echo "<tr><td>Solar Run Hours</td><td><strong>";
			echo("$data_quanta[22] Hrs");
			echo "</strong></td></tr>";

			echo "<tr><td>Inverter Run Hours</td><td><strong>";
			echo("$data_quanta[23] Hrs");
			echo "</strong></td></tr>";

			echo "<tr><td>Inverter Energy</td><td><strong>";
			echo("$data_quanta[25]");
			echo "</strong></td></tr>";

			echo "<tr><td>Solar Energy</td><td><strong>";
			echo("$data_quanta[26]");
			echo "</strong></td></tr>";

			echo "<tr><td>Battery Bank Voltage</td><td><strong>";
			echo("$data_quanta[27] V");
			echo "</strong></td></tr>";

			echo "<tr><td>Battery Charging Current</td><td><strong>";
			echo("$data_quanta[28] A");
			echo "</strong></td></tr>";

			echo "<tr><td>Battery Discharging Current</td><td><strong>";
			echo("$data_quanta[29] A");
			echo "</strong></td></tr>";

			echo "<tr><td>Battery Status</td><td><strong>";
			echo("$data_quanta[30] %");
			echo "</strong></td></tr>";

			echo "<tr><td>Battery Charging Energy</td><td><strong>";
			echo("$data_quanta[31]");
			echo "</strong></td></tr>";

			echo "<tr><td>Battery Discharging Energy</td><td><strong>";
			echo("$data_quanta[32]");
			echo "</strong></td></tr>";
		}


		echo ("</table>");

	}
	else if ($data_quanta[6] == 16)
	{
		echo "<h3>Hybrid Controller (0x16)</h3>";
			
		echo ("<table class='table table-striped'>");
	
			echo "<tr><td class='col-md-4'>Room Temperature</td><td class='col-md-8'><strong>";
			echo("$data_quanta[8] Deg C");
			echo "</strong></td></tr>";

			echo "<tr><td>Fuel Level</td><td><strong>";
			echo("$data_quanta[9] %");
			echo "</strong></td></tr>";
			
			echo "<tr><td>Fuel Data</td><td><strong>";
			echo("$data_quanta[10] Liters");
			echo "</strong></td></tr>";
			
			echo "<tr><td>Site Batt. bank Voltage</td><td><strong>";
			echo("$data_quanta[11] VDC");
			echo "</strong></td></tr>";
			
			echo "<tr><td>R-Phase current</td><td><strong>";
			echo("$data_quanta[12] A");
			echo "</strong></td></tr>";

			echo "<tr><td>Y-Phase current</td><td><strong>";
			echo("$data_quanta[13] A");
			echo "</strong></td></tr>";

			echo "<tr><td>B-Phase current</td><td><strong>";
			echo("$data_quanta[14] A");
			echo "</strong></td></tr>";

			echo "<tr><td>Mains Frequency</td><td><strong>";
			echo("$data_quanta[15] Hz");
			echo "</strong></td></tr>";

			echo "<tr><td>DG frequency</td><td><strong>";
			echo("$data_quanta[16] Hz");
			echo "</strong></td></tr>";

			echo "<tr><td>DG-R phase Voltage</td><td><strong>";
			echo("$data_quanta[17] VAC");
			echo "</strong></td></tr>";

			echo "<tr><td>DG-Y phase Voltage</td><td><strong>";
			echo("$data_quanta[18] VAC");
			echo "</strong></td></tr>";

			echo "<tr><td>DG-B phase Voltage</td><td><strong>";
			echo("$data_quanta[19] VAC");
			echo "</strong></td></tr>";

			echo "<tr><td>LCU1 Output Voltage</td><td><strong>";
			echo("$data_quanta[20] VAC");
			echo "</strong></td></tr>";

			echo "<tr><td>LCU2 Output Voltage</td><td><strong>";
			echo("$data_quanta[21] VAC");
			echo "</strong></td></tr>";

			echo "<tr><td>LCU3 Output Voltage</td><td><strong>";
			echo("$data_quanta[22] VAC");
			echo "</strong></td></tr>";

			echo "<tr><td>Input Mains Voltage - R Phase</td><td><strong>";
			echo("$data_quanta[23] VAC");
			echo "</strong></td></tr>";

			echo "<tr><td>Input Mains Voltage - Y Phase</td><td><strong>";
			echo("$data_quanta[24] VAC");
			echo "</strong></td></tr>";

			echo "<tr><td>Input Mains Voltage - B Phase</td><td><strong>";
			echo("$data_quanta[25] VAC");
			echo "</strong></td></tr>";
			

			echo "<tr><td>DG Running Hours</td><td><strong>";
			echo("$data_quanta[26] Hrs");
			echo "</strong></td></tr>";
			
			echo "<tr><td>Mains RUN HOURS</td><td><strong>";
			echo("$data_quanta[27] Hrs");
			echo "</strong></td></tr>";
			
			echo "<tr><td>Batt RUN HOURS</td><td><strong>";
			echo("$data_quanta[28] Hrs");
			echo "</strong></td></tr>";
			
			echo "<tr><td>O/P Mains Energy</td><td><strong>";
			echo("$data_quanta[29] Kwh");
			echo "</strong></td></tr>";
			
			echo "<tr><td>DG Energy</td><td><strong>";
			echo("$data_quanta[30] Kwh");
			echo "</strong></td></tr>";
			
			echo "<tr><td>I/P Mains Energy</td><td><strong>";
			echo("$data_quanta[31] Kwh");
			echo "</strong></td></tr>";

			echo "<tr><td>DG Battery Voltage</td><td><strong>";
			echo("$data_quanta[32] VDC");
			echo "</strong></td></tr>";

			echo "<tr><td>Battery Charging current</td><td><strong>";
			echo("$data_quanta[33] Amp");
			echo "</strong></td></tr>";

			echo "<tr><td>Battery Discharging current</td><td><strong>";
			echo("$data_quanta[34] Amp");
			echo "</strong></td></tr>";

			echo "<tr><td>Battery status</td><td><strong>";
			echo("$data_quanta[35] %");
			echo "</strong></td></tr>";

			echo "<tr><td>Battery back up time</td><td><strong>";
			echo("$data_quanta[36] hours");
			echo "</strong></td></tr>";
				
				
			echo "<tr><td>Battery Charging Energy</td><td><strong>";				
			echo("$data_quanta[37] Kwh");
			echo "</strong></td></tr>";
			
			echo "<tr><td>Battery Discharging Energy</td><td><strong>";				
			echo($data_quanta[38][0]." Kwh");
			echo "</strong></td></tr>";
			
			
		echo ("</table>");
		
		echo "<h3>DC Energy Meter (0x".dechex($data_quanta[38][1])." / 0x".dechex($data_quanta[38][2])." )</h3>";
			
		echo ("<table class='table table-striped'>");
	
			echo "<tr><td class='col-md-4'>Energy Channel 1</td><td class='col-md-8'><strong>";
			echo(round($data_quanta[38][4], 1)." Kwh");
			echo "</strong></td></tr>";
			
			echo "<tr><td>Current Channel 1</td><td><strong>";				
			echo(round($data_quanta[38][5], 1)." A");
			echo "</strong></td></tr>";
			
			echo "<tr><td>Energy Channel 2</td><td><strong>";				
			echo(round($data_quanta[38][6], 1)." Kwh");
			echo "</strong></td></tr>";
			
			echo "<tr><td>Current Channel 2</td><td><strong>";				
			echo(round($data_quanta[38][7], 1)." A");
			echo "</strong></td></tr>";
			
			echo "<tr><td>Energy Channel 3</td><td><strong>";				
			echo(round($data_quanta[38][8], 1)." Kwh");
			echo "</strong></td></tr>";
			
			echo "<tr><td>Current Channel 3</td><td><strong>";				
			echo(round($data_quanta[38][9], 1)." A");
			echo "</strong></td></tr>";
			
			echo "<tr><td>Energy Channel 4</td><td><strong>";				
			echo(round($data_quanta[38][10], 1)." Kwh");
			echo "</strong></td></tr>";
			
			echo "<tr><td>Current Channel 4</td><td><strong>";				
			echo(round($data_quanta[38][11], 1)." A");
			echo "</strong></td></tr>";
			
			echo "<tr><td>Voltage</td><td><strong>";				
			echo(round($data_quanta[38][12], 1)." V");
			echo "</strong></td></tr>";

	
		echo ("</table>");
	}
	else if($data_quanta[6] == 32)
	{
		if($data_array[30] == 0)
		{
			echo "<h3>Hybrid Controller with Double Battery (0x01)</h3>";

			echo ("<table class='table table-striped'>");

			echo "<tr><td class='col-md-4'>Room/Ambient Temperature</td><td class='col-md-8'><strong>";
			echo("$data_quanta[8] Deg C");
			echo "</strong></td></tr>";

			echo "<tr><td>Site Batt.bank Voltage</td><td><strong>";
			echo("$data_quanta[9] VDC");
			echo "</strong></td></tr>";

			echo "<tr><td>Site Batt.bank Voltage</td><td><strong>";
			echo("$data_quanta[10] VDC");
			echo "</strong></td></tr>";

			echo "<tr><td>Load Current</td><td><strong>";
			echo("$data_quanta[11] Amp");
			echo "</strong></td></tr>";

			echo "<tr><td>Mains Voltage</td><td><strong>";
			echo("$data_quanta[12] VAC");
			echo "</strong></td></tr>";

			echo "<tr><td>DG Voltage</td><td><strong>";
			echo("$data_quanta[13] VAC");
			echo "</strong></td></tr>";

			echo "<tr><td>Input Mains Voltage – B Phase</td><td><strong>";
			echo("$data_quanta[14] VAC");
			echo "</strong></td></tr>";

			echo "<tr><td>Battery1 Charging current</td><td><strong>";
			echo("$data_quanta[15] Amp");
			echo "</strong></td></tr>";

			echo "<tr><td>Battery1 Discharging current</td><td><strong>";
			echo("$data_quanta[16] Amp");
			echo "</strong></td></tr>";

			echo "<tr><td>Battery2 Charging current</td><td><strong>";
			echo("$data_quanta[17] Amp");
			echo "</strong></td></tr>";

			echo "<tr><td>Battery2 Discharging current</td><td><strong>";
			echo("$data_quanta[18] Amp");
			echo "</strong></td></tr>";

			echo "<tr><td>Battery1 status</td><td><strong>";
			echo("$data_quanta[19] %");
			echo "</strong></td></tr>";

			echo "<tr><td>Battery2 status</td><td><strong>";
			echo("$data_quanta[20] %");
			echo "</strong></td></tr>";

			echo ("</table>");

		}
	}
	
	
	
	
	


?>
