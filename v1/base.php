<?php
session_start();
if (!isset($_SESSION['login_user'])) {
	header("Location: login.php");
}

include 'common.php';

?>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
	<title>SIS-AXS</title>

	<link rel="stylesheet" href="/libs/jstree/themes/default/style.min.css">
	<!-- Bootstrap Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
	<!-- Bootstrap Optional theme -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css">

	<!-- jQuery  -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>

	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>

	<script src="/libs/bootstrap-paginator.min.js"></script>

	<!-- Latest compiled and minified JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
	<script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
	<script src="/libs/jstree/jstree.min.js"></script>

	<!-- NOT USED
	<script src="/libs/store.js/store.min.js"></script>
	<script src="/libs/jquery-resizable-columns/jquery.resizableColumns.min.js"></script>
	-->

	<link class="include" rel="stylesheet" type="text/css" href="/charts/jquery.jqplot.min.css" />
	<!--[if lt IE 9]><script language="javascript" type="text/javascript" src="charts/excanvas.js"></script><![endif]-->

	<script class="include" type="text/javascript" src="/libs/charts/jquery.jqplot.min.js"></script>
	<script class="include" type="text/javascript" src="/libs/charts/plugins/jqplot.pieRenderer.min.js"></script>
	<script type="text/javascript" src="/libs/charts/plugins/jqplot.highlighter.js"></script>
	<script type="text/javascript" src="/libs/charts/plugins/jqplot.barRenderer.min.js"></script>
	<script type="text/javascript" src="/libs/charts/plugins/jqplot.categoryAxisRenderer.min.js"></script>
	<script type="text/javascript" src="/libs/charts/plugins/jqplot.pointLabels.min.js"></script>

	<script type="text/javascript" src="/libs/moment.min.js"></script>

	<script type="text/javascript" src="/libs/datetimepicker/js/bootstrap-datetimepicker.min.js"></script>
	<link rel="stylesheet" href="/libs/datetimepicker/css/bootstrap-datetimepicker.min.css" />

	<link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css" />

	<style>
		.jqplot-data-label {
			color: #ffffff !important;
		}

		.jqplot-highlighter-tooltip {
			background: #ffffff;
		}

		.tree {
			overflow: auto;
			border: 1px solid silver;
			width: 200px;
			min-height: 600px;
		}
	</style>

	<script>
		var gMenuState = 0;
		//REAL TIME DATA 	0
		//REPORTS 			1
		//ALARMS VIEW 		2
		//MAINTENANCE		3

		var gTreeId = 0;

		function SetMenuState(state) {
			gMenuState = state;
		}

		function SetTreeId(id) {
			gTreeId = id;
		}


		function LoadContentState() {
			var url = "default.php";

			var subUrl = "";

			if (gTreeId[0] == 'r') {
				subUrl = "-root";
			} else if (gTreeId[0] == 'c') {
				subUrl = "-cluster";
			} else if (gTreeId[0] == 'n') {
				subUrl = "-node";
			}



			switch (gMenuState) {
				case 0:
					//REAL TIME DATA
					url = "rtd" + subUrl + ".php?id=" + gTreeId;
					break;

				case 1:
					//ALL DATA
					//url = "wip.php";
					if (gTreeId[0] == 'n')
						url = "data" + subUrl + ".php?id=" + gTreeId;
					else
						url = "wip.php";
					break;

				case 2:
					//REPORTS
					if (gTreeId[0] == 'n')
						url = "report" + subUrl + "-wrapper.php?id=" + gTreeId;
					else if ((gTreeId[0] == 'r') || (gTreeId[0] == 'c'))
						url = "report-rootcluster-wrapper.php?id=" + gTreeId;
					else
						url = "wip.php";
					break;
					break;

				case 3:
					//MAINTENANCE
					//url = "maint"+subUrl+".php?id="+gTreeId;
					url = "wip.php";
					break;


			}

			AsyncLoad(url, "#context");
		}

		function AsyncLoad(url2load, outputLocation) {

			var request = $.ajax({
				url: url2load
			});

			$(outputLocation).html("<img src='/libs/jstree/themes/default/throbber.gif'>");

			request.done(function(msg) {
				$(outputLocation).html(msg);
			});

			request.fail(function(jqXHR, textStatus) {
				$(outputLocation).html("Request failed: " + textStatus);
			});

		}

		function switchtab(tabnumber) {
			if (tabnumber != gMenuState) {
				$('#tab0').removeClass('active');
				$('#tab1').removeClass('active');
				$('#tab2').removeClass('active');
				$('#tab3').removeClass('active');

				var activetab = "#tab" + tabnumber;

				$(activetab).addClass('active');

				SetMenuState(tabnumber);
				LoadContentState();
			}
		}


		function TreeRefresh() {
			var instance = $('#tree').jstree(true);
			instance.refresh();
		}


		function TreeExpand() {
			$('#tree').jstree('open_all');
		}

		function TreeCollapse() {
			$('#tree').jstree('close_all');
		}

		//MAP
		var mapDisplayed = false;

		function ToggleMapLocation(lon, lat) {
			if (mapDisplayed) {
				mapDisplayed = false;
				$("#locationMapWrapper").html("");
				$("#MapToggleButton").html("Show on Map");

			} else {
				mapDisplayed = true;
				var url = "mapwrapper.php?lat=" + lat + "&lon=" + lon;
				//alert(url);
				AsyncLoad(url, "#locationMapWrapper");
				$("#MapToggleButton").html("Hide Map");
			}

		}
	</script>

</head>

<body>


	<div class="container-fluid">
		<?php include 'header1.php'; ?>
	</div>
	<div class="container-fluid">
		<div class="row">
			<div class="col-sm-2">
				<div style="width: 200px; border-left:1px solid silver; border-top:1px solid silver; border-right:1px solid silver; padding-left: 20px; padding-top: 5px; padding-bottom: 5px;">
					<a class="btn btn-default" href="#" onclick="event.preventDefault(); TreeRefresh();"><img src="/media/icons/ic_refresh_black_24dp.png" title="Refresh Sites" /></a>
					<a class="btn btn-default" href="#" onclick="event.preventDefault(); TreeExpand();"><img src="/media/icons/ic_unfold_more_black_24dp.png" title="Expand All Sites" /></a>
					<a class="btn btn-default" href="#" onclick="event.preventDefault(); TreeCollapse();"><img src="/media/icons/ic_unfold_less_black_24dp.png" title="Collapse All Sites" /></a>
				</div>
				<div id="tree" class="tree"></div>

			</div>
			<div class="col-sm-10">
				<div id="context">
				</div>
			</div>
		</div>

		<?php include 'footer.php'; ?>




		<script>
			$('#tree')
				.bind("loaded.jstree", function(event, data) {
					data.instance.select_node('ul > li:first');
				})
				.on("changed.jstree", function(e, data) {
					if (data.selected.length) {
						var id = data.instance.get_node(data.selected[0]).id;
						SetTreeId(id);
						LoadContentState();
					}
				})
				.jstree({
					'core': {
						'data': {
							"url": "/v1/gettree.php",
							//"url" : "./tmp.txt",
							"dataType": "json" // needed only if you do not supply JSON headers
						}
					}
				});


			// interaction and events
			/*$('#evts_button').on("click", function () {
				var instance = $('#tree').jstree(true);
				instance.deselect_all();
				instance.select_node('2');
			});*/
		</script>

</body>

</html>