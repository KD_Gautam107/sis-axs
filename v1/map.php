<?php
if($_SERVER["REQUEST_METHOD"] == "GET")
{
	$site_lat = $_GET['lat'];
	$site_lon = $_GET['lon'];
}
?>

<!DOCTYPE html>
<html>
<head>
<title>Locpal</title>
  
  <script src='https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyCncDlIhRlxRYnV3UwWa1oqorWgUTd93AI&sensor=false&' >
  </script>
  
  <script>
	var x, y;
	var map;
	
	function setLocation(a, b) {
		x = a;
		y = b;
		initializemap();
	}
	
	function initializemap() {
	  var mapOptions = {
		zoom: 15,
		center: new google.maps.LatLng(x, y)
	  };

	  map = new google.maps.Map(document.getElementById('map-canvas'),
		  mapOptions);
	}
	
	var marker = null;

	
	function ShowPosition(x, y) {


		setLocation(x, y);
		var myLatlng = new google.maps.LatLng(x, y);
		
		if (marker != null) 
			marker.setMap(null);
			
		marker = new google.maps.Marker({
			position: myLatlng,
			title:"Location",
			icon: {
				path: google.maps.SymbolPath.BACKWARD_CLOSED_ARROW,
				strokeColor: '#FF0000',
				strokeOpacity: 1.0,
				strokeWeight: 2,
				scale: 2
			},
			map: map
		});

	}


    </script>

</head>

<body>

	<div id="map-canvas" style="height: 200px; width: 100%; margin: 0px; padding: 0px;"></div>
	

	<script>
		ShowPosition(<?php echo $site_lat.", ".$site_lon; ?>);	
	</script>

</body>