<?php
/**
 * Created by PhpStorm.
 * User: Shoaib
 * Date: 11/22/2016
 * Time: 2:52 PM
 */
$site_id = $_GET['siteid'];
if (isset($_GET['export_page_no']))
{
    $export_page_no = $_GET['export_page_no'];
}
else
{
    $export_page_no = 1;
}

if (isset($_GET['excel_format']))
{
    $excel_format = $_GET['excel_format'];
}
else
{
    $excel_format = 0;
}

include 'common.php';
DataExport($site_id,$export_page_no,$excel_format);
