<?php

	include 'common.php';
	include 'communications.php';

	function SendAlarmCommMessage($sms_text, $site, $level)
	{
		//echo "\n\nSendAlarmCommMessage - START $sms_text \n $site \n $level\n\n";
		$site_details = GetDetailsFromSiteId($site);
		//var_dump($site_details);
		
		$name = "";
		$mob = "";
		
		if ($level == 1)
		{
			$name = $site_details[17];
			$mob = $site_details[18];		// Tech_Number
		}
		else if ($level == 2)
		{
			$name = $site_details[15];
			$mob = $site_details[16];		// AM_Number
		} 
		else if ($level == 3)
		{
			$name = $site_details[13];
			$mob = $site_details[14];		// SAM_Number
		} 
	
		$txt = "Send SMS to\n$name: $mob (Level $level).\n\n";
		//$sms_text = $txt.$sms_text;
		//$sms_text .= " _L_$level";

		
	
	
		//print("\nSend SMS/Mail: $txt");
		//$res = CommSendMail("root@localhost", $site."(".$site_details[6]."), Level: ".$level."  Mob: ".$txt, $sms_text);
		//print ("\n\nComm Resp: $res\n");
		
		//Production Case
		//if ($level == 1)	//Send only for Level 1 SMS
		//	$res1 = CommSendSMS("9811418925", $sms_text);				//Ram's Mobile
		$res2 = CommSendSMS($mob, $sms_text);							//Mobile for the Site
		processNotifications($site, $sms_text);
		//$res3 = CommSendSMS("9996895534", $sms_text . " " . "Mobiles$mob L$level");	//Shoaib's Mobile
		
		//print ("\n\nComm Resp: $res1 -- $res2\n");
		return $res2;
	}
	
	

	
	/////////////
	
	
	// This method will be called only from Cron Job
	function UpdateAndSendAlarmMessage($arr, $level, $site_name, $indoorOrOutdoor, $site_type)
	{
		//arr = `id`, `site`, `quanta_id`, `alarm_flag`, `alarm_flag_version`, `start_time`, `last_message_level`, `last_message_time`
		
		//$site_details = GetDetailsFromSiteId($arr[1]);
		///$alarms = GetAlarmBits($arr[3]);
		$alarms = GetAlarmBitsEx($arr[3], $indoorOrOutdoor, $site_type);
		//$text = GetAlarmText($alarms, $site_details);
		$text = GetAlarmText($alarms, $arr[1], $site_name, $arr[5]);
		
		if ($text === false)
		{
			//Clear alarm if any for the site, this case should not come
			//print("\n\n No Alarm - Clear DB of Alarms, if any \n\n");
			ResetAlarm($arr[1]);
		}
		else
		{
			//Update DB 
			UpdateAlarmMini($arr[0], $level);
			
			// Send message to specified Level
			$resp = SendAlarmCommMessage($text, $arr[1], $level);
			
			if ($resp == null) $resp = "";
			InsertMessageTracking($arr[1], $level, "", $text, $resp);
		}
	
	}
	
	
	function TimeDiffInMin($start_date, $end_date)
	{
		//$start_date = new DateTime('2007-09-01 04:10:58');
		$since_start = $start_date->diff($end_date);
		//echo $since_start->days.' days total<br>';
		//echo $since_start->y.' years<br>';
		//echo $since_start->m.' months<br>';
		//echo $since_start->d.' days<br>';
		//echo $since_start->h.' hours<br>';
		//echo $since_start->i.' minutes<br>';
		//echo $since_start->s.' seconds<br>';

		$minutes = $since_start->days * 24 * 60;
		$minutes += $since_start->h * 60;
		$minutes += $since_start->i;
		
		//echo $minutes.' minutes';
		
		return $minutes;
	
	}
	
	
	
	//T + 0		L1
	//T + 30	L2
	//T + 40	L3
	//T + 60/120/180..	Repeat to all L3 (or all L ?)
	function ProcessSiteForRepeatAlarms($arr)
	{
		//echo "\nProcessSiteForRepeatAlarms - $arr[1]\n";
		$site_details = GetDetailsFromSiteId($arr[1]);
		
		$last_message_level = $arr[6];	//Last Message Level
		$last_message_time = new DateTime($arr[7]);	//Last Message Time
		
		$current_time =  new DateTime();	//Current Time
		
		$deltaInMin = TimeDiffInMin($last_message_time, $current_time);

		//print ("Delta: $deltaInMin\n");
		
		//echo $last_message_level;
		if ($last_message_level == 1)
		{
			if ($deltaInMin >= 30)
			{
				//Send L2 Message and update last level sent, last message time 
				UpdateAndSendAlarmMessage($arr, 2, $site_details[6], $site_details[11], $site_details[29]);
			}
		}
		else if ($last_message_level == 2)
		{
			if ($deltaInMin >= 10)
			{
				//Send L3 Message and update last level sent, last message time 
				UpdateAndSendAlarmMessage($arr, 3, $site_details[6], $site_details[11], $site_details[29]);
			}
		}
		else if ($last_message_level == 3)
		{
			$deltaInMin = 60;
			if ($deltaInMin >= 60)
			{
				//Send L3 Message and update last level sent, last message time
				UpdateAndSendAlarmMessage($arr, 3, $site_details[6], $site_details[11], $site_details[29]);
			}
		}
	}
	
	function ProcessAlarmSitesForRepeatAlarms()
	{
		include 'dbinc.php';
		
		$mysqli = new mysqli("$mysql_hostname", "$mysql_user", "$mysql_password", "$mysql_database");
		if (mysqli_connect_errno()) {
			printf("Connect failed: %s\n", mysqli_connect_error());
			exit();
		}

		$sql = "SELECT `id`, `site`, `quanta_id`, `alarm_flag`, `alarm_flag_version`, `start_time`, `last_message_level`, `last_message_time` 
					FROM `siteinalarm` WHERE 1";

		//echo "SQL: $sql<br>";	
		$result = $mysqli->query($sql);
		//var_dump($result);
		
		//printf("\nSQL ERROR: %s\n", $mysqli->error);
		
		if ($result !== false)
		{
			$row_count = $result->num_rows;
			//var_dump($row_count);
			
			if ($row_count == 0)
			{
				$array = null;
			}
			else
			{
				while($data = $result->fetch_row())
				{
					//var_dump($data);
					$array = array();
					$array[] = $data[0];
					$array[] = $data[1];
					$array[] = $data[2];
					$array[] = $data[3];
					$array[] = $data[4];
					$array[] = $data[5];
					$array[] = $data[6];
					$array[] = $data[7];
					
					ProcessSiteForRepeatAlarms($array);
				}
			}
		}
		else
		{
			$array = null;
		}
				
		$result->close();	
		$mysqli->close();
	
		return $array;
	}
	
	
	//////////////////////////
	

	function GetAlarmForSite($site)
	{
		include 'dbinc.php';
		
		$mysqli = new mysqli("$mysql_hostname", "$mysql_user", "$mysql_password", "$mysql_database");
		if (mysqli_connect_errno()) {
			printf("Connect failed: %s\n", mysqli_connect_error());
			exit();
		}

		$sql = "SELECT `id`, `site`, `quanta_id`, `alarm_flag`, `alarm_flag_version`, `start_time`, `last_message_level`, `last_message_time` 
					FROM `siteinalarm` 
					WHERE site='$site'";

		//echo "SQL: $sql<br>";	
		$result = $mysqli->query($sql);
		//var_dump($result);
		
		//printf("\nSQL ERROR: %s\n", $mysqli->error);
		
		if ($result !== false)
		{
			$row_count = $result->num_rows;
			
			if ($row_count == 0)
			{
				$array = null;
			}
			else
			{
				$data = $result->fetch_row();
				
				//var_dump($data);
				
				$array = array();
				$array[] = $data[0];
				$array[] = $data[1];
				$array[] = $data[2];
				$array[] = $data[3];
				$array[] = $data[4];
				$array[] = $data[5];
				$array[] = $data[6];
				$array[] = $data[7];
			}
		}
		else
		{
			$array = null;
		}
				
		$result->close();	
		$mysqli->close();
	
		return $array;
	}
	
	function InsertMessageTracking($site, $level, $mob, $message, $status)
	{
		include 'dbinc.php';
		
		$ret = -1;
		
		$mysqli = new mysqli("$mysql_hostname", "$mysql_user", "$mysql_password", "$mysql_database");
		if (mysqli_connect_errno()) {
			//printf("Connect failed: %s\n", mysqli_connect_error());
			exit();
		}
		
		
		$sql = "INSERT INTO `devices`.`sitealarmmessagetracking` (`site`, `level`, `mobile`, `message`, `status`) VALUES ('$site', $level,'$mob', '$message', '$status');";
		

		//echo "\nSql: $sql\n\n";	
		$result = $mysqli->query($sql);
		
		if ($result === true)
		{
			//echo "\nInsert Successful";
			$ret = 0;
			
		}
		else
		{
			//echo "\nInsert Failed";
			//printf("\nSQL ERROR: %s\n", $mysqli->error);
			$ret = -2;
		}
		
		$mysqli->close();
	
		return $ret;
	}
	
	function InsertAlarm($site, $quanta_id, $alarm_flag, $alarm_flag_version, $start_time, $last_message_level, $last_message_time)
	{
		include 'dbinc.php';
		
		$ret = -1;
		
		$mysqli = new mysqli("$mysql_hostname", "$mysql_user", "$mysql_password", "$mysql_database");
		if (mysqli_connect_errno()) {
			printf("Connect failed: %s\n", mysqli_connect_error());
			exit();
		}
		
		$hexalarm = bin2hex($alarm_flag);
		
		if ($last_message_time == null)
			$last_message_time = "NOW()";

		$sql = "INSERT INTO `devices`.`siteinalarm` (`id`, `site`, `quanta_id`, `alarm_flag`, `alarm_flag_version`, `start_time`, `last_message_level`, `last_message_time`) VALUES (NULL, '$site', '$quanta_id', UNHEX('$hexalarm'), $alarm_flag_version, '$start_time', $last_message_level, $last_message_time);";
		$sql1 = "INSERT INTO `devices`.`sitealarmhistory` (`id`, `site`, `quanta_id`, `alarm_flag`, `alarm_flag_version`, `start_time`, `last_message_level`, `last_message_time`) VALUES (NULL, '$site', '$quanta_id', UNHEX('$hexalarm'), $alarm_flag_version, '$start_time', $last_message_level, $last_message_time);";

		//echo "\nSql: $sql\n\n";	
		$result1 = $mysqli->query($sql1);
		$result = $mysqli->query($sql);
		
		if ($result === true)
		{
			//echo "\nInsert Successful";
			$ret = 0;
			
		}
		else
		{
			//echo "\nInsert Failed";
			//printf("\nSQL ERROR: %s\n", $mysqli->error);
			$ret = -2;
		}
		
		$mysqli->close();
	
		return $ret;
	}
	
	function UpdateAlarm($id, $site, $quanta_id, $alarm_flag, $alarm_flag_version/*, $start_time, $last_message_level, $last_message_time*/)
	{
		include 'dbinc.php';
		
		$ret = -1;
		
		$mysqli = new mysqli("$mysql_hostname", "$mysql_user", "$mysql_password", "$mysql_database");
		if (mysqli_connect_errno()) {
			printf("Connect failed: %s\n", mysqli_connect_error());
			exit();
		}
		
		$hexalarm = bin2hex($alarm_flag);
		
			
		$sql = "UPDATE `devices`.`siteinalarm` 
			SET `quanta_id` = '$quanta_id', `alarm_flag` =  UNHEX('$hexalarm'),  `alarm_flag_version` = $alarm_flag_version 
			WHERE `siteinalarm`.`id` = $id;";

		//echo "Sql: $sql<br>";	
		$result = $mysqli->query($sql);
		
		if ($result === true)
		{
			//echo "\nUpdate Successful";
			$ret = 0;
		}
		else
		{
			//echo "\nUpdate Failed";
			//printf("\nSQL ERROR: %s\n", $mysqli->error);
			$ret = -2;
		}
				
		$mysqli->close();
	
		return $ret;
	}
	
	function UpdateAlarmMini($id, $last_message_level)
	{
		include 'dbinc.php';
		
		$ret = -1;
		
		$mysqli = new mysqli("$mysql_hostname", "$mysql_user", "$mysql_password", "$mysql_database");
		if (mysqli_connect_errno()) {
			printf("Connect failed: %s\n", mysqli_connect_error());
			exit();
		}
		
		
			
		$sql = "UPDATE `devices`.`siteinalarm` 
			SET `last_message_level` = $last_message_level, `last_message_time` =  NOW() 
			WHERE `siteinalarm`.`id` = $id;";

		//echo "Sql: $sql<br>";	
		$result = $mysqli->query($sql);
		
		if ($result === true)
		{
			//echo "\nUpdate Successful";
			$ret = 0;
		}
		else
		{
			//echo "\nUpdate Failed";
			//printf("\nSQL ERROR: %s\n", $mysqli->error);
			$ret = -2;
		}
				
		$mysqli->close();
	
		return $ret;
	}
	
	function DeleteAlarm($id)
	{
		include 'dbinc.php';
		
		$ret = -1;
		
		$mysqli = new mysqli("$mysql_hostname", "$mysql_user", "$mysql_password", "$mysql_database");
		if (mysqli_connect_errno()) {
			printf("Connect failed: %s\n", mysqli_connect_error());
			exit();
		}
		
		
		$sql = "DELETE FROM `devices`.`siteinalarm` WHERE `id` = $id;";

		//echo "\nSql: $sql\n\n";	
		$result = $mysqli->query($sql);
		
		if ($result === true)
		{
			//echo "\nDelete Successful";
			$ret = 0;
		}
		else
		{
			//echo "\Delete Failed";
			//printf("\nSQL ERROR: %s\n", $mysqli->error);
			$ret = -2;
		}
				
		$mysqli->close();
	
		return $ret;
	}
	
	function CompareAlarm($alarms_db, $alarms_current)
	{
		//if alarm 1 	--> SMS 	(New Alarm)						
		//if alarm 1, 2 --> SMS 	(One more Alarm)				1
		//if alarm 2, 3 --> SMS 	(Some or all change in Alarm) 	2, -2
		//if alarm 3 	--> TODO 	(One alarm gone)				-1
	
		//if exactly same			=	0
		//if exactly same + more 	=	1
		//if entirely different set	=	2
		//if fewer alarms than db	=	-1
		//if fewer alarms than db + different alarms	= -2
		
		$same = 0;
		$different = 0;
		$tot_db = 0;
		$tot_cur = 0;
		
		for ($i=0; $i<count($alarms_db); $i++)
		{
			if ($alarms_db[$i][1] == true)
				$tot_db++;
				
			if ($alarms_current[$i][1] == true)
				$tot_cur++;
				
			if (($alarms_db[$i][1] == true) && ($alarms_current[$i][1] == true))
			{
				$same++;
			}
			
			if (($alarms_db[$i][1] != true) && ($alarms_current[$i][1] == true))
			{
				$different++;
			}
			if (($alarms_db[$i][1] == true) && ($alarms_current[$i][1] != true))
			{
				$different++;
			}
		}
		
		
		if ($different == 0)
		{
			if ($same == $tot_db)
				return 0;
			else
			{
				if ($same > $tot_db)
					return 0;	//Not possible case
				else
					return -1;	//$same < $tot_db -- fewer alarms than db (-1)
			}
		
		}
		else //$different > 0
		{
			if ($same == 0)
				return 2;	//Entirely different set (2)
				
			if ($same == $tot_db)
				return 1;		//$alarms_current has all $alarms_db alarms + some new (1)
			else if ($same > $tot_db)
				return 0;	//Not possible case
			else //if ($same < $tot_db)
				return -2;	//$same < $tot_db -- fewer alarms than db, and some extra different alarm (-2)
		}
		
	
	}
	
	
	function ProcessAlarm($site, $quanta_id, $alarm_flag, $alarm_flag_version, $start_time, $sms_text, $alarms_current, $indoorOrOutdoor, $site_type) 
	{
		$arr = GetAlarmForSite($site);
		
		if ($arr == null)
		{
			//No previous alarm for this site
			//Insert in DB
			InsertAlarm($site, $quanta_id, $alarm_flag, $alarm_flag_version, $start_time, 1, null);
			
			//Send SMS Level 1
			$resp = SendAlarmCommMessage($sms_text, $site, 1);
			
			if ($resp == null) $resp = "";
			InsertMessageTracking($site, 1, "", $sms_text, $resp);
		}
		else
		{
			//Already site is in Alarm state
			
			//Update DB 
			// NOTE: SMS sending to be done by the Cron job (10 min interval)
			
			//if alarm 1 	--> SMS 	(New Alarm)
			//if alarm 1, 2 	--> SMS 	(One more Alarm)
			//if alarm 2, 3 	--> SMS 	(Some change in Alarm)
			//if alarm 3 	--> TODO 	(One alarm gone)
			
			$alarms_db = GetAlarmBitsEx($arr[3], $indoorOrOutdoor, $site_type);
			
			//var_dump($alarms_db);
			//var_dump($alarms_current);
			
			$compare = CompareAlarm($alarms_db, $alarms_current);
			//print("\nComparison Result: $compare\n");
			
			switch ($compare)
			{
				case -2:	//Some Different
				case 1:		//Some added
				case 2:		//Different
					
					//Update DB
					UpdateAlarm($arr[0], $site, $quanta_id, $alarm_flag, $alarm_flag_version/*, $start_time, $last_message_level, $last_message_time*/);
					
					//Send Message
					$resp = SendAlarmCommMessage($sms_text, $site, 1);
					
                    InsertMessageTracking($site, 1, "", $sms_text, $resp);
                    				
					break;
					
				
				case -1:	//Some alarm Gone
					
					//Only update DB
					UpdateAlarm($arr[0], $site, $quanta_id, $alarm_flag, $alarm_flag_version/*, $start_time, $last_message_level, $last_message_time*/);
				
					break;
				
				
				default:
				case 0:		//Same as before
				
					// Not to Do Anything
					break;
			}
			
		}
	
	}
	
	function ResetAlarm($site)
	{
		//Reset all alarms by deleting site record 
		print("\nReset Alarm for site: $site\n\n");
		
		$arr = GetAlarmForSite($site);
		
		if ($arr == null)
		{
			//No previous alarm for this site
			//printf("\nNo Previous Alarm, ignore.");
		
		}
		else
		{
			//Already site is in Alarm state, Delete the record
			
			DeleteAlarm($arr[0]);
			
			InsertMessageTracking($site, 0, "", "", "");
			
			//Send SMS of Alarm Clearing
			//SMS will go upto the last level
			$alarm_level = $arr[6];
			$site_details = GetDetailsFromSiteId($arr[1]);
			
			//Send Current Time (IST) as the clear time
			$d = new DateTime("now", new DateTimeZone("Asia/Kolkata"));
			$alarm_clear_time = $d->format('Y-m-d H:i'); 
			$sms_text = GetAlarmClearText($arr[1], $site_details[6], $alarm_clear_time);
			
			
			// Send message to specified Level(s) 3, 2, 1
			for ($i=$alarm_level; $i>=1; $i--)
			{
				$resp = SendAlarmCommMessage($sms_text, $arr[1], $i);
				
				if ($resp == null) $resp = "";
				InsertMessageTracking($arr[1], $i, "", $sms_text, $resp);
			}
		}
	
	}

//TODO REMOVE	
//	function GetAlarmBits($data_status)
//	{
//		return GetAlarmBitsEx($data_status, 0);
//	}
	
	function GetAlarmBitsEx($data_status, $indoorOrOutdoor, $site_type) 
	{
		// -- 0 Indoor, 1 -- Outdoor
		// For Outdoor ignore, temperature
		//Return only the applicable alarm bits
		
		$array = array();
		$stats = unpack ( "C*" , $data_status );
		
		//echo "<br>ite Type: $site_type<br>";
		if ($site_type == 0)
		{
		
			//Smoke Fire Alarm
			$sub = array();
			$sub[] = "Smoke Fire";
			$sub[] = ($stats[5] & 0x01);
			//var_dump($stats);
			$array[] = $sub;
			
			//Door
			//$sub = array(); 	
			//$sub[] = "Door Open";
			//$sub[] = ($stats[5] & 0x02);
			//$array[] = $sub;
			
			//Auto/Man Mode 0 Means  Auto Mode, 1 Means  Man Mode
			//$array[] = ($stats[5] & 0x04);

			//00    :Load on EB,01: Load on DG, 10: Load on site Battery   11: Not used
			
			$sub = array(); 	
			$sub[] = "Site on Battery";
			$sub[] = 0;
			
			$sub1 = array(); 	
			$sub1[] = "Site on DG";
			$sub1[] = 0;

			
			if (($stats[5] & 0x08) == false) 
			{
				if (($stats[5] & 0x10) == false) 
				{
					//$array[] =  "Load on EB";
				}
				else
				{
					//$array[] =  "Load on site Battery";
					$sub[1] = 1;
				}
			}
			else
			{
				if (($stats[5] & 0x10) == false)
				{			
					//$array[] =  "Load on DG";
					$sub1[1] = 1;
				}
				else
				{
					//$array[] =  "Not Used";
				}
			}
			
			$array[] = $sub;
			$array[] = $sub1;
			
			//Mains Fail
			$sub = array();
			$sub[] = "Mains Fail";
			$sub[] = ($stats[5] & 0x20) ; 
			$array[] = $sub;
			
			//Site Battery Low
			$sub = array();
			$sub[] = "Site Battery Low";
			$sub[] = ($stats[5] & 0x40) ; 
			$array[] = $sub;
				
			
			if ($indoorOrOutdoor == 0)
			{		
				//Room Temperature, only for Indoor location
				$sub = array();
				$sub[] = "Room Temperature";
				$sub[] = ($stats[5] & 0x80) ; 
				$array[] = $sub;
			}	
			
			//LLOP
			$sub = array();
			$sub[] = "LLOP";
			$sub[] = ($stats[4] & 0x01) ; 
			$array[] = $sub;
				
			
			//HCT/HWT
			$sub = array();
			$sub[] = "HCT/HWT";
			$sub[] = ($stats[4] & 0x02) ; 
			$array[] = $sub;
				
			//Alternator Fault
			$sub = array();
			$sub[] = "Alternator Fault";
			$sub[] = ($stats[4] & 0x04) ; 
			$array[] = $sub;

			//DG Over Speed
			$sub = array();
			$sub[] = "DG Over Speed";
			$sub[] = ($stats[4] & 0x08) ; 
			$array[] = $sub;

			//DG Over Load
			$sub = array();
			$sub[] = "DG Over Load";
			$sub[] = ($stats[4] & 0x10) ; 
			$array[] = $sub;

			//DG Low Fuel
			$sub = array();
			$sub[] = "DG Low Fuel";
			$sub[] = ($stats[4] & 0x20) ;
			$array[] = $sub;		

			//DG Start Fail
			$sub = array();
			$sub[] = "DG Start Fail";
			$sub[] = ($stats[4] & 0x40) ; 
			$array[] = $sub;

			//DG Stop Fail
			$sub = array();
			$sub[] = "DG Stop Fail";
			$sub[] = ($stats[4] & 0x80) ; 
			$array[] = $sub;

			//DG battery Low
			$sub = array();
			$sub[] = "DG battery Low";
			$sub[] = ($stats[3] & 0x01) ; 
			$array[] = $sub;

			//LCU fail
			$sub = array();
			$sub[] = "LCU fail";
			$sub[] = ($stats[3] & 0x02) ; 
			$array[] = $sub;

			//Rectifier Fail
			//$sub = array();
			//$sub[] = "Rectifier Fail";
			//$sub[] = ($stats[3] & 0x04) ;
			//$array[] = $sub;		

			//Multi Rectifier Fail
			//$sub = array();
			//$sub[] = "Multi Rectifier Fail";
			//$sub[] = ($stats[3] & 0x08) ; 
			//$array[] = $sub;

			//LVD TRIP
			//$sub = array();
			//$sub[] = "LVD TRIP";
			//$sub[] = ($stats[3] & 0x10) ; 
			//$array[] = $sub;

			//LVD BY pass
			//$sub = array();
			//$sub[] = "LVD BY pass";
			//$sub[] = ($stats[3] & 0x20) ; 
			//$array[] = $sub;

			
			//$FLAG_22_RES 	= 0x0000400000;	//Reserved
			//$FLAG_23_RES 	= 0x0000800000;	//Reserved
			//$FLAG_24_RES 	= 0x0001000000;	//Reserved
			//$FLAG_25_RES 	= 0x0002000000;	//Reserved
			//$FLAG_26_RES 	= 0x0004000000;	//Reserved
			//$FLAG_27_RES 	= 0x0008000000;	//Reserved
			//$FLAG_28_RES 	= 0x0010000000;	//Reserved
			//$FLAG_29_RES 	= 0x0020000000;	//Reserved
			//$FLAG_30_RES 	= 0x0040000000;	//Reserved
			//$FLAG_31_RES 	= 0x0080000000;	//Reserved
			
			//DG OFF
			//$array[] = ($stats[1] & 0x01) ; 

			//DG ON
			//$array[] = ($stats[1] & 0x02) ; 

			//DG Cranking
			//$array[] = ($stats[1] & 0x04) ; 
			
			//DG Start in Progress
			//$array[] = ($stats[1] & 0x08) ; 

			//DG Cool Down (Idle Running)
			//$array[] = ($stats[1] & 0x10) ; 
			
			//DG STOP  Normally
			//$array[] = ($stats[1] & 0x20) ; 
			
			//DG STOP due to Fault
			$sub = array();
			$sub[] = "DG STOP due to Fault";
			$sub[] = ($stats[1] & 0x40) ;
			$array[] = $sub;		
			
			//DG Stop Due to Maximum Run Exipary 
			//$array[] = ($stats[1] & 0x80) ; 
		
		
		}
		else if ($site_type == 1)
		{
			//Double DG Alarms
			
			//Smoke Fire Alarm
			$sub = array();
			$sub[] = "Smoke Fire";
			$sub[] = ($stats[6] & 0x01);
			$array[] = $sub;
			
						
			//if ($indoorOrOutdoor == 0)	//Door Alarm Only Applicable if Indoor
			//{
				//Door
				//$sub = array(); 	
				//$sub[] = "Door Open";
				//$sub[] = ($stats[6] & 0x02);
				//$array[] = $sub;
			//}
			
			//Auto/Man Mode 0 Means  Auto Mode, 1 Means  Man Mode
			//$sub = array(); 	
			//$sub[] = "Manual Mode";
			//$sub[] = ($stats[6] & 0x04);
			//$array[] = $sub;



			$sub = array(); 	
			$sub[] = "Site on Battery";
			$sub[] = 0;
			
			$sub1 = array(); 	
			$sub1[] = "Site on DG";
			$sub1[] = 0;
			
			//000    :Load on EB,	001: Load on DG1, 011: Load on site Battery   
			//010	: Load on DG2, 111 ( site in battery with emg case) 
			// other’s Not used
			
			//$FLAG_034_LOAD 	= 0x0000000018;	
			
			$flg_data = ($stats[6] & 0x38) / 8;
			if ($flg_data == 0)
			{
				//echo "[- Load on EB -]<br>";			//000
			}
			else if ($flg_data == 1)
			{
				//echo "[- Load on DG 1 -]<br>";			//001
				$sub1[1] = 1;
			}
			else if ($flg_data == 2)
			{
				//echo "[- Load on DG 2 -]<br>";			//010
				$sub1[1] = 1;
			}
			else if ($flg_data == 3)
			{
				//echo "[- Load on site Battery -]<br>";	//011
				$sub[1] = 1;
			}
			else if ($flg_data == 7)
			{
				//echo "[- Load on site Battery (emg case) -]<br>";	//111
				$sub[1] = 1;
			}	
			else
			{
				//echo "[- Not Used -]<br>";				//remaining
			}

			$array[] = $sub;
			$array[] = $sub1;
			
				
			//Mains Fail
			$sub = array();
			$sub[] = "Mains Fail";
			$sub[] = ($stats[6] & 0x40) ; 
			$array[] = $sub;
			
			//Site Battery Low
			$sub = array();
			$sub[] = "Site Battery Low";
			$sub[] = ($stats[6] & 0x80); 
			$array[] = $sub;


			if ($indoorOrOutdoor == 0)
			{		
				//Room Temperature, only for Indoor location
				$sub = array();
				$sub[] = "Room Temperature";
				$sub[] = ($stats[5] & 0x01) ; 
				$array[] = $sub;
			}	

			//LLOP
			$sub = array();
			$sub[] = "LLOP";
			$sub[] = ($stats[5] & 0x02) ; 
			$array[] = $sub;
				
			
			//HCT/HWT
			$sub = array();
			$sub[] = "HCT/HWT";
			$sub[] = ($stats[5] & 0x04) ; 
			$array[] = $sub;
				
			//Alternator Fault
			$sub = array();
			$sub[] = "Alternator Fault";
			$sub[] = ($stats[5] & 0x08) ; 
			$array[] = $sub;


			//DG Over Speed
			$sub = array();
			$sub[] = "DG Over Speed";
			$sub[] = ($stats[5] & 0x10) ; 
			$array[] = $sub;

			//DG Over Load
			$sub = array();
			$sub[] = "DG Over Load";
			$sub[] = ($stats[5] & 0x20) ; 
			$array[] = $sub;

			//DG Low Fuel
			$sub = array();
			$sub[] = "DG Low Fuel";
			$sub[] = ($stats[5] & 0x40) ;
			$array[] = $sub;		

			//DG Start Fail
			$sub = array();
			$sub[] = "DG Start Fail";
			$sub[] = ($stats[5] & 0x80) ; 
			$array[] = $sub;

			//DG Stop Fail
			$sub = array();
			$sub[] = "DG Stop Fail";
			$sub[] = ($stats[4] & 0x01) ; 
			$array[] = $sub;

			//DG battery Low
			$sub = array();
			$sub[] = "DG battery Low";
			$sub[] = ($stats[4] & 0x02) ; 
			$array[] = $sub;

			//LCU fail
			$sub = array();
			$sub[] = "LCU fail";
			$sub[] = ($stats[4] & 0x04) ; 
			$array[] = $sub;
			
			//Rectifier Fail
			//$sub = array();
			//$sub[] = "Rectifier Fail";
			//$sub[] = ($stats[4] & 0x08) ;
			//$array[] = $sub;		

			//Multi Rectifier Fail
			//$sub = array();
			//$sub[] = "Multi Rectifier Fail";
			//$sub[] = ($stats[4] & 0x10) ; 
			//$array[] = $sub;

			//LVD TRIP
			//$sub = array();
			//$sub[] = "LVD TRIP";
			//$sub[] = ($stats[4] & 0x20) ; 
			//$array[] = $sub;

			//LVD BY pass
			//$sub = array();
			//$sub[] = "LVD BY pass";
			//$sub[] = ($stats[4] & 0x40) ; 
			//$array[] = $sub;


			//DG2 LLOP
			$sub = array();
			$sub[] = "DG2 LLOP";
			$sub[] = ($stats[4] & 0x80) ; 
			$array[] = $sub;
				
			
			//HCT/HWT
			$sub = array();
			$sub[] = "DG2 HCT/HWT";
			$sub[] = ($stats[3] & 0x01) ; 
			$array[] = $sub;
				
			//Alternator Fault
			$sub = array();
			$sub[] = "DG2 Alternator Fault";
			$sub[] = ($stats[3] & 0x02) ; 
			$array[] = $sub;


			//DG Over Speed
			$sub = array();
			$sub[] = "DG2 Over Speed";
			$sub[] = ($stats[3] & 0x04) ; 
			$array[] = $sub;

			//DG Over Load
			$sub = array();
			$sub[] = "DG2 Over Load";
			$sub[] = ($stats[3] & 0x08) ; 
			$array[] = $sub;

			//DG Low Fuel
			$sub = array();
			$sub[] = "DG2 Low Fuel";
			$sub[] = ($stats[3] & 0x10) ;
			$array[] = $sub;		

			//DG Start Fail
			$sub = array();
			$sub[] = "DG2 Start Fail";
			$sub[] = ($stats[3] & 0x20) ; 
			$array[] = $sub;

			//DG Stop Fail
			$sub = array();
			$sub[] = "DG2 Stop Fail";
			$sub[] = ($stats[3] & 0x40) ; 
			$array[] = $sub;

			//DG battery Low
			$sub = array();
			$sub[] = "DG2 battery Low";
			$sub[] = ($stats[3] & 0x80) ; 
			$array[] = $sub;
			
			////DG STOP Normally
			//if (($stats[2] & 0x20) == true)	


			//DG STOP due to Fault
			$sub = array();
			$sub[] = "DG STOP due to Fault";
			$sub[] = ($stats[2] & 0x40) ; 
			$array[] = $sub;
				
			
			//DG Stop Due to Maximum Run Exipary 
			//if (($stats[2] & 0x80) == true) 
			
			//DG2 STOP Normally
			//if (($stats[1] & 0x20) == true)	
			
			//DG2 STOP due to Fault
			$sub = array();
			$sub[] = "DG2 STOP due to Fault";
			$sub[] = ($stats[1] & 0x40) ; 
			$array[] = $sub;

			//DG2 Stop Due to Maximum Run Exipary 
			//if (($stats[1] & 0x80) == true) 
			
		}
		
		//var_dump($array);
		
		return $array;
	}
	
	
	//function GetAlarmText($alarms, $array1)
	function GetAlarmText($alarms, $site_id, $site_name, $alarm_time)
	{
		$anyalarm = false;
		$allalarms = "";
		for ($i=0; $i<count($alarms); $i++)
		{
			if ($alarms[$i][1] == true)
			{
				
				$name = $alarms[$i][0];
				//print("\nAlarm: $name");
				
				if ($anyalarm == true) $allalarms = $allalarms.", ";
				$allalarms = $allalarms.$name;
				
				$anyalarm = true;
			}
		}
		
		if ($anyalarm == true)
		{
			//$allalarms = "ALARM ".$allalarms." TRIGGERED AT SITE ".$array1[4]."-".$array1[43]." ON ".$array1[5];
			$allalarms = "ALARM ".$allalarms." TRIGGERED AT SITE ".$site_id."-".$site_name." ON ".$alarm_time;
			
			return $allalarms;
		}
		else
			return false;
	}
	
	function GetAlarmClearText($site_id, $site_name, $alarm_clear_time)
	{
		return "ALARM CLEARED THAT WAS TRIGGERED AT SITE ".$site_id."-".$site_name." ON ".$alarm_clear_time;
	}
?>
