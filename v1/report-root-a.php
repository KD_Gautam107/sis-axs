<?php
session_start();
if (!isset($_SESSION['login_user'])) {
	header("Location: login.php");
}

include 'common.php';




//include 'report-rootcluster-wrapper-data.php'; -- Fetchdata() used inline

if ($_SERVER["REQUEST_METHOD"] == "GET") {
	$id = $_GET['id'];
	$_SESSION['range_start'] = $_GET['range_start'];
	$_SESSION['range_end'] = $_GET['range_end'];
	//$_SESSION['chart_type'] = $_GET['type'];

	//if ($_SESSION['chart_type'] == null) $_SESSION['chart_type'] = "Pie";

	//Temp Code
	if ($_SESSION['range_start'] == null) $_SESSION['range_start'] = "2020-11-23 18:33:18";
	if ($_SESSION['range_end'] == null) $_SESSION['range_end'] = "2021-01-04 15:23:23";
}

//$data_array = GetSiteDetailFromId($id);
//var_dump($data_array);

function FetchData($strStart, $strEnd)
{
	$id = $_SESSION['login_cust_id'];
	$login_type = $_SESSION['login_type'];
	$clusters = $_SESSION['login_typeref_id'];

	include 'dbinc.php';
	include 'list.php';

	$mysqli = new mysqli("$mysql_hostname", "$mysql_user", "$mysql_password", "$mysql_database");
	if (mysqli_connect_errno()) {
		printf("Connect failed: %s\n", mysqli_connect_error());
		return null;
	}

	// echo "login_cust_id: $login_cust_id\r\n";
	// echo "login_type: $login_type\r\n";
	// var_dump($clusters);

	if ($login_type == 1)    //Head, show all clusters and sites for the customer
	{
		$sql = "SELECT 
                        a.Id, a.site_id, a.ClusterId, b.name, a.SiteName 
                    FROM siteinfo a 
                    
                        INNER JOIN clusters b 
                        ON a.ClusterId = b.id			
    
                    WHERE ((a.CustomerId = $id) AND (a.site_enabled = 1))
                    ORDER BY ClusterId 
    
                    
                    ";
	} else if ($login_type == 2)    //Cluster Head, show only the cluster of the head
	{
		$clustersid = $clusters[0];    //Only a single cluster
		$sql = "SELECT 
                        a.Id, a.site_id, a.ClusterId, b.name, a.SiteName 
                    FROM siteinfo a 
                    
                        INNER JOIN clusters b 
                        ON a.ClusterId = b.id			
    
                    WHERE ((a.CustomerId = $id) AND (a.ClusterId = $clustersid) AND (a.site_enabled = 1))
                    ORDER BY ClusterId 
    
                    
                    ";
	} else if ($login_type == 4)    //Zone Head, show all the clusters under the zone
	{
		$clustersid = join(',', $clusters);
		//echo "$clustersid <br>";
		$sql = "SELECT 
                        a.Id, a.site_id, a.ClusterId, b.name, a.SiteName 
                    FROM siteinfo a 
                    
                        INNER JOIN clusters b 
                        ON a.ClusterId = b.id			
    
                    WHERE ((a.CustomerId = $id) AND (a.ClusterId IN ($clustersid)) AND (a.site_enabled = 1))
                    ORDER BY ClusterId 
    
                    
                    ";
	} else {
		//Show no cluster/site
		$sql = null;
	}


	if ($sql != null) {

		$result = $mysqli->query($sql);

		echo "<tbody>";

		$totalDG = 0;
		$totalMains = 0;
		$totalBattery = 0;

		while ($data = $result->fetch_row()) {
			//var_dump($data);

			$sum = null;
			//if ($range_data_type == 3)
			//{
			//echo "TEST";
			//echo $data[1] ." - ". $strEnd ." - ". $strStart;
			//echo "Test" . $data[1] . $strEnd . $strStart;
			$array = GetSiteStatsFromIdStartEnd($data[1], $strEnd, $strStart);
			//print_r($array);
			if ($array == NULL)
				continue;

			$sum = AddUp($array, $sum);

			// calculating total values.
			$totalDG += round($sum[1] + $sum[4], 1);
			$totalMains += round($sum[2], 1);
			$totalBattery += round($sum[3] + $adj_offset, 1);

			echo ("<tr>
            
          <td> $data[4]</td>
          <td> $data[1]</td>");

			echo ("<td>" . round($sum[1] + $sum[4], 1) . "</td>");
			echo (" <td>" . round($sum[2], 1) . " hours2" . "</td>");
			echo (" <td>" . round($sum[3] + $adj_offset, 1) . "</td>");

			$array = GetEntryTypeData($data[1]);

			echo ("<td>$array[30]</td>
                   <td> $array[29]</td>
                   <td> $array[31]</td>");

			echo ("</tr>");
		}



		echo "<tr style =\"background: #4bb2c547;\">
          <td></td>
          <td>Total2</td>
          <td>" . $totalDG . "</td>
          <td>" . $totalMains . "</td>
          <td>" . $totalBattery . "</td>
          <td> </td>
          <td> </td>
          <td> </td>
         
          <td colspan=\"\" ></td>
        </tr>";

		echo "</tbody></table>";

		$result->close();
	}
}

function GetEntryTypeData($site_id)
{
	// Find the quanta ID 
	// Database connection
	include 'dbinc.php';

	$mysqli = new mysqli("$mysql_hostname", "$mysql_user", "$mysql_password", "$mysql_database");
	if (mysqli_connect_errno()) {
		printf("Connect failed: %s\n", mysqli_connect_error());
		exit();
	}

	$query = "SELECT ID
                FROM quanta 
                WHERE site_id = '$site_id'
                ORDER BY ID DESC 
                LIMIT 1";

	//echo "query: $query";

	$result = $mysqli->query($query);
	$data = $result->fetch_row();

	// // Fetch quanta data
	return GetDeviceRecordFromId($data[0]);
	//var_dump($array);



}


?>


<style>
	.color-box {
		width: 14px;
		height: 14px;
		display: inline-block;
		left: 5px;
		top: 5px;
	}

	.downloadbutton>div {
		display: inline-block;
		padding: 4px;
	}

	.downloadbutton>div>button {
		border: 1px solid #bebebe;
		padding: 2px 13px;
		border-radius: 3px;
		background: #f4f4f4;
	}

	.downloadbutton>div>button>a {
		color: #4f4f4f;
	}

	.downloadbutton>div>button>a:hover {
		text-decoration: none;
	}
</style>

<div style="margin-top:20px;">
	<center>
		<h3>Report - A</h3>
		<h4>(<?php echo $_SESSION['range_start'] . " - " . $_SESSION['range_end']; ?>)</h4>
	</center>
</div>

<div id="myRange">
	<p class='bg-info text-center' style="margin-top:10px;">Select date and time between <strong><?php echo $_SESSION['range_start']; ?></strong> <strong><?php echo $_SESSION['range_end']; ?></strong></p>
	<div class="container">
		<div class="row">
			<div class='col-lg-3 col-md-3 col-sm-3 col-sm-12'></div>
			<div class='col-lg-3 col-md-3 col-sm-3 col-sm-12'>
				<div class="form-group">

					<div class='input-group date' id='datetimepickerFrom'>
						<input type='text' class="form-control" />
						<span class="input-group-addon">
							<span class="glyphicon glyphicon-calendar"></span>
						</span>
					</div>
				</div>
			</div>
			<div class='col-lg-3 col-md-3 col-sm-3 col-sm-12'>
				<div class="form-group">
					<div class='input-group date' id='datetimepickerTo'>
						<input type='text' class="form-control" />
						<span class="input-group-addon">
							<span class="glyphicon glyphicon-calendar"></span>
						</span>
					</div>
				</div>
			</div>
			<!-- <div class='col-sm-2'>

				<select class="form-control" id="Chart">
					<option value="Pie">Pie Chart</option>
					<option value="Bar">Bar Chart</option>
				</select>


			</div> -->
			<div class='col-lg-3 col-md-3 col-sm-3 col-sm-12'>
				<button href="#" class="btn btn-default" onclick="applyRangeString()">Show</button>
			</div>
		</div>
		<script type="text/javascript">
			function applyRangeString() {
				var strStart = encodeURIComponent($('#datetimepickerFrom').data("DateTimePicker").date().format('YYYY-MM-DD HH:mm:ss'));
				var strEnd = encodeURIComponent($('#datetimepickerTo').data("DateTimePicker").date().format('YYYY-MM-DD HH:mm:ss'));

				AsyncLoad("report-root-a.php?id=<?php echo $id; ?>&range_data_type=3&range_start=" + strStart + "&range_end=" + strEnd, "#id_report_rootcluster");

			}


			$(function() {

				$('#datetimepickerFrom').datetimepicker({
					format: 'YYYY-MM-DD HH:mm:ss',
					//minDate: moment('<?php echo $daterange[0]; ?>', 'YYYY-MM-DD HH:mm:ss'),
					//maxDate: moment('<?php echo $daterange[1]; ?>', 'YYYY-MM-DD HH:mm:ss'),
					date: moment('<?php echo $_SESSION['range_start']; ?>', 'YYYY-MM-DD HH:mm:ss'),
				});
				$('#datetimepickerTo').datetimepicker({
					format: 'YYYY-MM-DD HH:mm:ss',
					//minDate: moment('<?php echo $daterange[0]; ?>', 'YYYY-MM-DD HH:mm:ss'),
					//maxDate: moment('<?php echo $daterange[1]; ?>', 'YYYY-MM-DD HH:mm:ss'),
					date: moment('<?php echo $_SESSION['range_end']; ?>', 'YYYY-MM-DD HH:mm:ss'),
				});
				$("#datetimepickerFrom").on("dp.change", function(e) {
					$('#datetimepickerTo').data("DateTimePicker").minDate(e.date);
					$('#displayFrom').html(e.date.format('YYYY-MM-DD HH:mm:ss'));
				});
				$("#datetimepickerTo").on("dp.change", function(e) {
					$('#datetimepickerFrom').data("DateTimePicker").maxDate(e.date);
				});

			});
		</script>
	</div>
</div>



<div style="margin-top:20px;">

	<table id="datatable" class='table table-striped table-condensed table-responsive'>
		<tr>
			<thead class="site-name">
				<th>Site Name</th>
				<th>Site ID</th>
				<th>DG (Run hours)</th>
				<th>Mains (Run hours)</th>
				<th>Battery (Run hours)</th>
				<th>DG (kwh)</th>
				<th>Msins (kwh)</th>
				<th>Battery(kwh)</th>
			</thead>
		</tr>

		<?php

		FetchData($_SESSION['range_start'], $_SESSION['range_end']);

		?>
</div>


<!-- 
<script type='text/javascript'>
	var options = {
		bootstrapMajorVersion: 3,
		numberOfPages: 10,
		currentPage: <?php echo $disp; ?>,
		totalPages: <?php echo $total; ?>,
		onPageClicked: function(e, originalEvent, type, page) {
			var newpage = page - 2;

			window.location.replace("<?php echo $_PHP_SELF . '?page='; ?>" + newpage + "<?php echo '&site_id=' . $site_id . '&indoor_type=' . $indoor_type . '&site_name=' . $site_name . ''; ?>");
		}
	}

	$('#id_pages').bootstrapPaginator(options);

	function isInt(value) {
		if (isNaN(value)) {
			return false;
		}
		var x = parseFloat(value);
		return (x | 0) === x;
	}

	function Data_ExportA() {

		//var url = "exportcsv.php?filter_data_type="+filter_data_type+"&site_list_data_type="+site_list_data_type+"&export_page_no="+export_page_no+"&excel_format="+excel_format;
		var site_id = "<?php echo $site_id; ?>";
		var export_page_no = $("#ExportPage").val();
		var excel_format = $('#ExcelFormat').is(":checked") ? 1 : 0;

		if (total < 1) {
			alert("No Data Available!");
		} else if ((!isInt(export_page_no) || (export_page_no < 1) || (export_page_no > total))) {
			alert("Invalid Page Number");
		} else {
			var url = "data-export.php?siteid=" + site_id + "&export_page_no=" + export_page_no + "&excel_format=" + excel_format;
			//alert(url);
			//var request = $.ajax({url: url});
			window.open(url);
		}
	}
</script> -->

<!-- <script type='text/javascript'>
	var options = {
		bootstrapMajorVersion: 3,
		numberOfPages: 10,
		currentPage: <?php echo $disp; ?>,
		totalPages: <?php echo $total; ?>,
		onPageClicked: function(e, originalEvent, type, page) {
			var newpage = page - 2;

			window.location.replace("<?php echo $_PHP_SELF . '?page='; ?>" + newpage + "<?php echo '&site_id=' . $site_id . '&indoor_type=' . $indoor_type . '&site_name=' . $site_name . ''; ?>");
		}
	}

	$('#id_pages').bootstrapPaginator(options);

	function isInt(value) {
		if (isNaN(value)) {
			return false;
		}
		var x = parseFloat(value);
		return (x | 0) === x;
	}

	function Data_Export(total) {

		//var url = "exportcsv.php?filter_data_type="+filter_data_type+"&site_list_data_type="+site_list_data_type+"&export_page_no="+export_page_no+"&excel_format="+excel_format;
		var site_id = "<?php echo $site_id; ?>";
		var export_page_no = $("#ExportPage").val();
		var excel_format = $('#ExcelFormat').is(":checked") ? 1 : 0;

		if (total < 1) {
			alert("No Data Available!");
		} else if ((!isInt(export_page_no) || (export_page_no < 1) || (export_page_no > total))) {
			alert("Invalid Page Number");
		} else {
			var url = "data-export.php?siteid=" + site_id + "&export_page_no=" + export_page_no + "&excel_format=" + excel_format;
			//alert(url);
			//var request = $.ajax({url: url});
			window.open(url);
		}
	}
</script> -->



<script>
	var characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",
		fromCharCode = String.fromCharCode,
		INVALID_CHARACTER_ERR = function() {
			try {
				document.createElement("$")
			} catch (a) {
				return a
			}
		}();
	window.btoa || (window.btoa = function(a) {
		for (var b, c, d, e, f, g, h, i = 0, j = a.length, k = Math.max, l = ""; j > i;) {
			if (b = a.charCodeAt(i++) || 0, c = a.charCodeAt(i++) || 0, h = a.charCodeAt(i++) || 0, k(b, c, h) > 255) throw INVALID_CHARACTER_ERR;
			d = b >> 2 & 63, e = (3 & b) << 4 | c >> 4 & 15, f = (15 & c) << 2 | h >> 6 & 3, g = 63 & h, c ? h || (g = 64) : f = g = 64, l += characters.charAt(d) + characters.charAt(e) + characters.charAt(f) + characters.charAt(g)
		}
		return l
	}), window.atob || (window.atob = function(a) {
		a = a.replace(/=+$/, "");
		var b, c, d, e, f, g, h, i = 0,
			j = a.length,
			k = [];
		if (j % 4 === 1) throw INVALID_CHARACTER_ERR;
		for (; j > i;) d = characters.indexOf(a.charAt(i++)), e = characters.indexOf(a.charAt(i++)), f = characters.indexOf(a.charAt(i++)), g = characters.indexOf(a.charAt(i++)), b = (63 & d) << 2 | e >> 4 & 3, c = (15 & e) << 4 | f >> 2 & 15, h = (3 & f) << 6 | 63 & g, k.push(fromCharCode(b)), c && k.push(fromCharCode(c)), h && k.push(fromCharCode(h));
		return k.join("")
	}), ExcellentExport = function() {
		var a = {
				excel: "data:application/vnd.ms-excel;base64,",
				csv: "data:application/csv;base64,"
			},
			b = {
				excel: '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8"><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>'
			},
			c = function(a) {
				return window.btoa(unescape(encodeURIComponent(a)))
			},
			d = function(a, b) {
				return a.replace(/{(\w+)}/g, function(a, c) {
					return b[c]
				})
			},
			e = function(a) {
				return a.nodeType ? a : document.getElementById(a)
			},
			f = function(a) {
				var b = a,
					c = -1 !== a.indexOf(",") || -1 !== a.indexOf("\r") || -1 !== a.indexOf("\n"),
					d = -1 !== a.indexOf('"');
				return d && (b = b.replace(/"/g, '""')), (c || d) && (b = '"' + b + '"'), b
			},
			g = function(a) {
				for (var b, c = "", d = 0; b = a.rows[d]; d++) {
					for (var e, g = 0; e = b.cells[g]; g++) c = c + (g ? "," : "") + f(e.innerHTML);
					c += "\r\n"
				}
				return c
			},
			h = {
				excel: function(f, g, h) {
					g = e(g);
					var i = {
							worksheet: h || "Worksheet",
							table: g.innerHTML
						},
						j = a.excel + c(d(b.excel, i));
					return f.href = j, !0
				},
				csv: function(b, d) {
					d = e(d);
					var f = g(d),
						h = a.csv + c(f);
					b.href = h
				}
			};
		return h
	}();
</script>

<div class="downloadbutton">
	<div><button><a download="somedata.xls" href="#" onclick="return ExcellentExport.excel(this, 'datatable', 'Sheet Name Here');">Export to Excel</a></button></div>

	<div><button><a download="somedata.csv" href="#" onclick="return ExcellentExport.csv(this, 'datatable');">Export to CSV</a></button></div>
</div>
<!-- 


<br>
<form method="post" role="form" class="form-inline">
	<a role="button" href="#" onclick="event.preventDefault(); Data_ExportA();" class="btn btn-default">Export as CSV</a>

	<input class="form-control" type="checkbox" name="ExcelFormat" id="ExcelFormat" checked="true">Excel Format</input>
</form> -->