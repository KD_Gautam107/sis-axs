<?php
session_start();
if (!isset($_SESSION['login_user'])) {
	header("Location: login.php");
}

include 'common.php';


$id = $_SESSION['login_cust_id'];
$login_type = $_SESSION['login_type'];
$clusters = $_SESSION['login_typeref_id'];
$array = array();

//var_dump($clusters);

include 'dbinc.php';


$mysqli = new mysqli("$mysql_hostname", "$mysql_user", "$mysql_password", "$mysql_database");
if (mysqli_connect_errno()) {
	printf("Connect failed: %s\n", mysqli_connect_error());
	return null;
}


$sql = "SELECT logo 
					FROM customers 
					WHERE (id = $id) ";
$result = $mysqli->query($sql);
$data = $result->fetch_row();
$icon = "/media/tree/" . $data[0];
$result->close();


if ($login_type == 1)	//Head, show all clusters and sites for the customer
{
	$sql = "SELECT 
					a.Id, a.site_id, a.ClusterId, b.name, a.SiteName 
				FROM siteinfo a 
				
					INNER JOIN clusters b 
					ON a.ClusterId = b.id			

				WHERE ((a.CustomerId = $id) AND (a.site_enabled = 1))
				ORDER BY ClusterId 

				
				";
} else if ($login_type == 2)	//Cluster Head, show only the cluster of the head
{
	$clustersid = $clusters[0];	//Only a single cluster
	$sql = "SELECT 
					a.Id, a.site_id, a.ClusterId, b.name, a.SiteName 
				FROM siteinfo a 
				
					INNER JOIN clusters b 
					ON a.ClusterId = b.id			

				WHERE ((a.CustomerId = $id) AND (a.ClusterId = $clustersid) AND (a.site_enabled = 1))
				ORDER BY ClusterId 

				
				";
} else if ($login_type == 4)	//Zone Head, show all the clusters under the zone
{
	$clustersid = join(',', $clusters);
	//echo "$clustersid <br>";
	$sql = "SELECT 
					a.Id, a.site_id, a.ClusterId, b.name, a.SiteName 
				FROM siteinfo a 
				
					INNER JOIN clusters b 
					ON a.ClusterId = b.id			

				WHERE ((a.CustomerId = $id) AND (a.ClusterId IN ($clustersid)) AND (a.site_enabled = 1))
				ORDER BY ClusterId 

				
				";
} else {
	//Show no cluster/site
	$sql = null;
}

//print($sql);
//print("<br>");

print('[{"id":"r0","text":"' . $_SESSION['login_cust_name'] . '","icon":"' . $icon . '","state":{"opened":"true"},"children":[');

if ($sql != null) {

	$result = $mysqli->query($sql);
	$last_cluster = 0;
	while ($data = $result->fetch_row()) {
		//var_dump($data);

		if ($last_cluster != $data[2]) {
			//New Cluster

			if ($last_cluster != 0) {
				//Not First Cluster
				print(']},');
			}

			$last_cluster = $data[2];

			print('{"id":"c' . $last_cluster . '","text":"' . trim($data[3]) . '","icon":"/media/tree/ic_gamepad_black_24dp.png","children":[');
			print('{"id":"n' . $data[0] . '","text":"' . trim($data[4]) . '","icon":"/media/tree/ic_settings_input_antenna_black_24dp.png"}');
		} else {
			print(',{"id":"n' . $data[0] . '","text":"' . trim($data[4]) . '","icon":"/media/tree/ic_settings_input_antenna_black_24dp.png"}');
		}
	}

	$result->close();
}

print(']}]}]');




mysqli_close($mysqli);
