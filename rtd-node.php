<?php
if($_SERVER["REQUEST_METHOD"] == "GET")
{
	$id = $_GET['id'];
	if ($id[0] == 'n')
	{
		$id = ltrim($id, "n");
	}
	else
	{
		echo "Validation FAILED<br>";
		die;
	}
}


include 'common.php';
$data_array = GetSiteDetailFromId($id);
$data_status = $data_array[7];
$stats = unpack ( "C*" , $data_status );

?>

<div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title">Site Details (<?php echo $id; ?>)</h3>
  </div>
  <div class="panel-body">
    Site Name: <?php echo $data_array[4]; ?><br>
	Site ID: <?php echo $data_array[0]; ?><br>
	Circle: <?php echo $data_array[3]; ?><br>
	Cluster: <?php echo $data_array[2]; ?><br>
	Operation Mode: <?php 
	
		/*echo "<tr class='info'><td><img src='/media/icons/ic_info_outline_black_24dp.png'></td><td>Mode</td><td";
		//$FLAG_02_AUTO 	= 0x0000000004;	//Auto/Man Mode 0 Means  Auto Mode, 1 Means  Man Mode
		if (($stats[5] & 0x04) == false) 
			echo "  >Auto";
		else
			echo "  >Manual";
		echo "</td></tr>";*/
	
		if (($stats[5] & 0x04) == false) 
		{
			echo "Automatic";
		}
		else
		{
			echo "Manual";
		}
	?><br>
	
	Power Load is on: <?php
		//$FLAG_034_LOAD 	= 0x0000000018;	//00    :Load on EB,01: Load on DG, 10: Load on site Battery   11: Not used
		if (($stats[5] & 0x08) == false) 
		{
			if (($stats[5] & 0x10) == false) 
				echo "EB";				//00
			else
				echo "Site Battery";	//10
		}
		else
		{
			if (($stats[5] & 0x10) == false) 
				echo "DG";				//01
			else
				echo "Not Used";	//11
		}
		?>
		<br>
		
		Generator Status: <?php
			
			if (($stats[1] & 0x01) == true) 		//$FLAG_32_DG 	= 0x0100000000;	//DG OFF
				echo "OFF";
			else if (($stats[1] & 0x02) == true) 	//$FLAG_33_DG 	= 0x0200000000;	//DG ON
				echo "ON";
			else if (($stats[1] & 0x04) == true) 	//$FLAG_34_DG 	= 0x0400000000;	//DG Cranking
				echo "Cranking";
			else if (($stats[1] & 0x08) == true) 	//$FLAG_35_DG 	= 0x0800000000;	//DG Start in Progress
				echo "Start in Progress";
			else if (($stats[1] & 0x10) == true) 		//$FLAG_36_DG 	= 0x1000000000;	//DG Cool Down (Idle Running)
				echo "Cool Down (Idle Running)";
			else 
				echo "Alarm";
		?>
		<br>
	
  </div>
</div>

<div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title">Equipment Details</h3>
  </div>
  <div class="panel-body">
    Equipment: <?php echo $data_array[4]; ?><br>

  </div>
</div>

<script>AsyncLoad("node-power-supply.php?site_id=<?php echo $data_array[0]; ?>&range_data_type=0", "#id_power_supply");</script>
<div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title">Power Supply</h3>
  </div>
  <div class="panel-body">
    <div id="id_power_supply"></div>
  </div>
</div>


<div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title">Site Stats</h3>
  </div>
  <div class="panel-body">
    

  </div>
</div>

<div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title">Alarm Details</h3>
  </div>
  <div class="panel-body">
	
	<table id="alarms-table" class="table table-bordered" data-resizable-columns-id="demo-table">
	  <thead>
		<tr>
		  <th data-resizable-column-id="Severity">Sev</th>
		  <th data-resizable-column-id="alarm_name">Alarm Name</th>
		  <th data-resizable-column-id="alarm_description">Description</th>
		</tr>
	  </thead>
	  <tbody>

	
	<?php
		
		
		if (($stats[5] & 0x01) == true)
		{
			echo "<tr class='danger'><td><img src='/media/icons/ic_info_black_24dp.png'></td><td>Smoke Fire Alarm</td><td";
			//0x0000000000 Smoke fire    0 means No alarm,  1 means Alarm
			echo "</td></tr>";
		}
		
		if (($stats[5] & 0x02) == true)
		{		
			echo "<tr class='warning'><td><img src='/media/icons/ic_report_problem_black_24dp.png'></td><td>Door</td><td";
			//$FLAG_01_DOOR 	= 0x0000000002;	//Door Open     0  means  Door Close , 1 Means  Door open  
			echo "</td></tr>";
		}
		

		echo "<tr class='info'><td><img src='/media/icons/ic_info_outline_black_24dp.png'></td><td>Mains Fail</td><td";
		//$FLAG_05_MAINS 	= 0x0000000020;	//Mains fail	1 Means main Fail,  0 Means  Main healthy
		if (($stats[5] & 0x20) == false) 
			echo "  ><img id='led_white' src='media/img_trans.gif'>";
		else
			echo "  ><img id='led_red' src='media/img_trans.gif'>";
		echo "</td></tr>";
		
		echo "<tr class='warning'><td><img src='/media/icons/ic_report_problem_black_24dp.png'></td><td>Site Battery Low</td><td";
		//$FLAG_06_BATT 	= 0x0000000040;	//Site battery Low  1 Means Site Battery Low ,0 Means  Battery Ok
		if (($stats[5] & 0x40) == false) 
			echo "  ><img id='led_white' src='media/img_trans.gif'>";
		else
			echo "  ><img id='led_red' src='media/img_trans.gif'>";
		echo "</td></tr>";
		
		
		echo "<tr class='info'><td><img src='/media/icons/ic_info_outline_black_24dp.png'></td><td>Room Temperature</td><td";
		//$FLAG_07_TEMP 	= 0x0000000080;	//Room Temperature  1 Means high Temperature, 0 Means Normal temp
		if (($stats[5] & 0x80) == false) 
			echo "  ><img id='led_white' src='media/img_trans.gif'>";
		else
			echo "  ><img id='led_red' src='media/img_trans.gif'>";
		echo "</td></tr>";
		
		
		echo "<tr class='warning'><td><img src='/media/icons/ic_report_problem_black_24dp.png'></td><td>LLOP</td><td";
		//$FLAG_08_LLOP 	= 0x0000000100;	//LLOP
		if (($stats[4] & 0x01) == false) 
			echo "  ><img id='led_white' src='media/img_trans.gif'>";
		else
			echo "  ><img id='led_red' src='media/img_trans.gif'>";
		echo "</td></tr>";
		
		
		echo "<tr class='warning'><td><img src='/media/icons/ic_report_problem_black_24dp.png'></td><td>HCT/HWT</td><td";
		//$FLAG_09_HCT 	= 0x0000000200;	//HCT/HWT
		if (($stats[4] & 0x02) == false) 
			echo "  ><img id='led_white' src='media/img_trans.gif'>";
		else
			echo "  ><img id='led_red' src='media/img_trans.gif'>";
		echo "</td></tr>";
		
		
		echo "<tr class='warning'><td><img src='/media/icons/ic_report_problem_black_24dp.png'></td><td>Alternate Fault</td><td";
		//$FLAG_10_ALT 	= 0x0000000400;	//Alternate Fault
		if (($stats[4] & 0x04) == false) 
			echo "  ><img id='led_white' src='media/img_trans.gif'>";
		else
			echo "  ><img id='led_red' src='media/img_trans.gif'>";
		echo "</td></tr>";
		
		
		echo "<tr class='warning'><td><img src='/media/icons/ic_report_problem_black_24dp.png'></td><td>DG Over Speed</td><td";
		//$FLAG_11_DG 	= 0x0000000800;	//DG Over Speed
		if (($stats[4] & 0x08) == false) 
			echo "  ><img id='led_white' src='media/img_trans.gif'>";
		else
			echo "  ><img id='led_red' src='media/img_trans.gif'>";
		echo "</td></tr>";
		
		
		echo "<tr class='danger'><td><img src='/media/icons/ic_info_black_24dp.png'></td><td>DG Over Load</td><td";
		//$FLAG_12_DG 	= 0x0000001000;	//DG Over Load
		if (($stats[4] & 0x10) == false) 
			echo "  ><img id='led_white' src='media/img_trans.gif'>";
		else
			echo "  ><img id='led_red' src='media/img_trans.gif'>";
		echo "</td></tr>";
		
		
		echo "<tr class='danger'><td><img src='/media/icons/ic_info_black_24dp.png'></td><td>DG Low Fuel</td><td";
		//$FLAG_13_DG 	= 0x0000002000;	//DG Low Fuel
		if (($stats[4] & 0x20) == false) 
			echo "  ><img id='led_white' src='media/img_trans.gif'>";
		else
			echo "  ><img id='led_red' src='media/img_trans.gif'>";
		echo "</td></tr>";
		
		
		echo "<tr class='warning'><td><img src='/media/icons/ic_report_problem_black_24dp.png'></td><td>DG Start Fail</td><td";
		//$FLAG_14_DG 	= 0x0000004000;	//DG Start Fail
		if (($stats[4] & 0x40) == false) 
			echo "  ><img id='led_white' src='media/img_trans.gif'>";
		else
			echo "  ><img id='led_red' src='media/img_trans.gif'>";
		echo "</td></tr>";
		
		
		echo "<tr class='warning'><td><img src='/media/icons/ic_report_problem_black_24dp.png'></td><td>DG Stop Fail</td><td";
		//$FLAG_15_DG 	= 0x0000008000;	//DG Stop Fail
		if (($stats[4] & 0x80) == false) 
			echo "  ><img id='led_white' src='media/img_trans.gif'>";
		else
			echo "  ><img id='led_red' src='media/img_trans.gif'>";
		echo "</td></tr>";
		
		
		echo "<tr class='warning'><td><img src='/media/icons/ic_report_problem_black_24dp.png'></td><td>DG Battery Low</td><td";
		//$FLAG_16_DG 	= 0x0000010000;	//DG battery Low
		if (($stats[3] & 0x01) == false) 
			echo "  ><img id='led_white' src='media/img_trans.gif'>";
		else
			echo "  ><img id='led_red' src='media/img_trans.gif'>";
		echo "</td></tr>";
		
		
		echo "<tr class='danger'><td><img src='/media/icons/ic_info_black_24dp.png'></td><td>LCU Fail</td><td";
		//$FLAG_17_LCU 	= 0x0000020000;	//LCU fail
		if (($stats[3] & 0x02) == false) 
			echo "  ><img id='led_green' src='media/img_trans.gif'>";
		else
			echo "  ><img id='led_red' src='media/img_trans.gif'>";
		echo "</td></tr>";
		
		
		echo "<tr class='warning'><td><img src='/media/icons/ic_report_problem_black_24dp.png'></td><td>Rectifier Fail</td><td";
		//$FLAG_18_RECT 	= 0x0000040000;	//Rectifier Fail
		if (($stats[3] & 0x04) == false) 
			echo "  ><img id='led_green' src='media/img_trans.gif'>";
		else
			echo "  ><img id='led_red' src='media/img_trans.gif'>";
		echo "</td></tr>";
		
		
		echo "<tr class='warning'><td><img src='/media/icons/ic_report_problem_black_24dp.png'></td><td>Multi Rectifier Fail</td><td";
		//$FLAG_19_RECT 	= 0x0000080000;	//Multi Rectifier Fail
		if (($stats[3] & 0x08) == false) 
			echo "  ><img id='led_green' src='media/img_trans.gif'>";
		else
			echo "  ><img id='led_red' src='media/img_trans.gif'>";
		echo "</td></tr>";
		
		
		echo "<tr class='danger'><td><img src='/media/icons/ic_info_black_24dp.png'></td><td>LVD Trip</td><td";
		//$FLAG_20_LVD 	= 0x0000100000;	//LVD TRIP
		if (($stats[3] & 0x10) == false) 
			echo "  ><img id='led_green' src='media/img_trans.gif'>";
		else
			echo "  ><img id='led_red' src='media/img_trans.gif'>";
		echo "</td></tr>";
		
		
		echo "<tr class='info'><td><img src='/media/icons/ic_info_outline_black_24dp.png'></td><td>LVD BY PASS</td><td";
		//$FLAG_21_LVD 	= 0x0000200000;	//LVD BY pass
		if (($stats[3] & 0x20) == false) 
			echo "  ><img id='led_green' src='media/img_trans.gif'>";
		else
			echo "  ><img id='led_red' src='media/img_trans.gif'>";
		echo "</td></tr>";
		
		
		//$FLAG_22_RES 	= 0x0000400000;	//Reserved
		//$FLAG_23_RES 	= 0x0000800000;	//Reserved
		//$FLAG_24_RES 	= 0x0001000000;	//Reserved
		//$FLAG_25_RES 	= 0x0002000000;	//Reserved
		//$FLAG_26_RES 	= 0x0004000000;	//Reserved
		//$FLAG_27_RES 	= 0x0008000000;	//Reserved
		//$FLAG_28_RES 	= 0x0010000000;	//Reserved
		//$FLAG_29_RES 	= 0x0020000000;	//Reserved
		//$FLAG_30_RES 	= 0x0040000000;	//Reserved
		//$FLAG_31_RES 	= 0x0080000000;	//Reserved
		
		
		
		
		echo "<tr class='info'><td><img src='/media/icons/ic_info_outline_black_24dp.png'></td><td>DG STOP  Normally</td><td";
		//$FLAG_37_DG 	= 0x2000000000;	//DG STOP  Normally
		if (($stats[1] & 0x20) == false) 
			echo "  ><img id='led_white' src='media/img_trans.gif'>";
		else
			echo "  ><img id='led_green' src='media/img_trans.gif'>";
		echo "</td></tr>";
		
		
		echo "<tr class='warning'><td><img src='/media/icons/ic_report_problem_black_24dp.png'></td><td>DG STOP due to Fault</td><td";
		//$FLAG_38_DG 	= 0x8000000000;	//DG STOP due to Fault
		if (($stats[1] & 0x40) == false) 
			echo "  ><img id='led_white' src='media/img_trans.gif'>";
		else
			echo "  ><img id='led_green' src='media/img_trans.gif'>";
		echo "</td></tr>";
		
		
		echo "<tr class='info'><td><img src='/media/icons/ic_info_outline_black_24dp.png'></td><td>DG Stop Due to Maximum Run Expiry</td><td";
		//$FLAG_39_DG 	= 0x8000000000;	//DG Stop Due to Maximum Run Exipary 
		if (($stats[1] & 0x80) == false) 
			echo "  ><img id='led_white' src='media/img_trans.gif'>";
		else
			echo "  ><img id='led_green' src='media/img_trans.gif'>";
		echo "</td></tr>";
		
	?>
		</tbody>
	</table>
	

	
	
	
  </div>
</div>


<div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title">Ambient</h3>
  </div>
  <div class="panel-body">
    

  </div>
</div>