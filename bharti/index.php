<?php
session_start();
if (!isset($_SESSION['bhartiinfratel_user']))
{
    header("Location: loginsis.php");
}

?>
<?php include 'header.php'; ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Bharti Infratel</title>

    <!-- Bootstrap -->
    <!-- Latest compiled and minified CSS -->
	  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">

	  <!-- Optional theme -->
	  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css">

	  <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>

	  <!-- Latest compiled and minified JavaScript -->
	  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>

	
</head>
<body>
<!--
<h3>Bharti Infratel</h3>
-->


	<?php
		//$dir    = '/akhilesh/code/bharti/';
		//$dir    = '/akhilesh/code/dump/';
		//$dir    = '/home/admin/akhilesh/bharti/';

        //$dir = '/home/';  //Not working

        $dir    = '/var/www/bharti/home/';



		$files2 = scandir($dir, SCANDIR_SORT_DESCENDING);
		//print_r($files2);
        //var_dump($files2);
		
		$count1 = sizeof($files2)-2;
		echo "Total: {$count1}<br>";
    //echo $files2[0];
		
		// output file list as HTML table
		echo ("<table class='table table-striped table-condensed table-responsive'>");
		echo "<thead>\n";
		echo "<tr><th></th><th>Date</th><th>Site</th></tr>\n";
		echo "</thead>\n";
		echo "<tbody>\n";
		
		$count1 = 0;
		
		foreach($files2 as $file) {
			if($file[0] == ".") continue;

            //var_dump($file);
			
			if ($count1 >= 50) break;
			$count1++;
			
			$fullPath = $dir.$file;
			$protocolData = file_get_contents($fullPath);
			
			$protocolLen = strlen($protocolData);
			if ($protocolLen < 286) break;
			
			
			$site_id = substr($protocolData, 5, 9);
			
			/*$year = substr($file, 0, 4);
			$month = substr($file, 4, 2);
			$day = substr($file, 6, 2);
			$hour = substr($file, 8, 2);
			$min = substr($file, 10, 2);*/
			
			$site_year = substr($protocolData, 24, 2);
			$site_month = substr($protocolData, 22, 2);
			$site_day = substr($protocolData, 14, 2);
			$site_hour = substr($protocolData, 16, 2);
			$site_min = substr($protocolData, 18, 2);
			$site_sec = substr($protocolData, 20, 2);

			
			echo "<tr>\n";
			echo "<td>{$count1}</td>\n";
			echo "<td>{$site_day}/{$site_month}/{$site_year} {$site_hour}:{$site_min}</td>\n";
			//echo "<td></td>\n";
			echo "<td><a href='data.php?file=$fullPath'>{$site_id}</a></td>\n";
			//echo "<td>{$protocolData}</td>\n";
			echo "</tr>\n";
		}
		echo "</tbody>\n";
		echo "</table>\n\n";

	?>
	
	<?php include 'footersis.php'; ?>
	
</body>
</html>