<?php
session_start();
if (!isset($_SESSION['bhartiinfratel_user']))
{
	header("Location: loginsis.php");
}

?>
<?php include 'header.php'; ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Bharti Infratel</title>

    <!-- Bootstrap -->
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css">

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>

    <script>

        function ShowDetail(id, type){
            if (type ==  0)
            {
                $("#myModalLabel").html("Equipment Status");
                $.ajax({url: "alarm-status.php?id="+id, success: function(result){
                    $("#id_device_detail").html(result);
                    $('#myModal').modal('show');

                }});
            }
            else if (type == 1)
            {
                $("#myModalLabel").html("Binary Data");
                $.ajax({url: "binary.php?id="+id, success: function(result){
                    $("#id_device_detail").html(result);
                    $('#myModal').modal('show');

                }});
            }
            else if (type == 2)
            {
                $("#myModalLabel").html("Equipment Data Phase 2");
                $.ajax({url: "detailPhase2.php?id="+id, success: function(result){
                    $("#id_device_detail").html(result);
                    $('#myModal').modal('show');

                }});
            }

        }
    </script>


    <style>
        #led_white {
            width: 28px;
            height: 28px;
            background: url(/media/leds.png) 0 0;
        }

        #led_red {
            width: 28px;
            height: 28px;
            background: url(/media/leds.png) -30px 0;
        }

        #led_orange {
            width: 28px;
            height: 28px;
            background: url(/media/leds.png) -62px 0;
        }

        #led_green {
            width: 28px;
            height: 28px;
            background: url(/media/leds.png) -124px 0;
        }

    </style>
</head>
<body>


<!--<h3>Bharti Infratel</h3>-->


<?php

    $fullPath = $_GET['file'];

    include 'common.php';

        //DisplayEntryType1Header();
        $array = GetDeviceRecord($fullPath);
        DisplayEntryType1($array);
        //DisplayEntryType1HeaderEnd();

        //var_dump(GetDeviceRecord($fullPath));


?>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"></h4>
            </div>
            <div class="modal-body">
                <div id="id_device_detail">
                    Please wait...
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

</body>
</html>
<?php include 'footersis.php'; ?>