<?php


function DisplayEntryType1Header()
{
    echo ("<table border='1' class='table table-striped table-condensed table-responsive'>");

    echo("<tr><thead>
					<th>#</th>
					<th>S/W Ver.</th>
					<th>Site ID</th>
					<th>Date Time</th>
					
					<th>Alarm</th>
					<th>Status</th>
					<th>Comm. Status</th>
					
					<th>Room Temperature DEG C</th>
					<th>Fuel Level %</th>
					<th>Site Battery Voltage VDC</th>
					<th>Output current</th>
					<th>Mains Frequency</th>
					<th>DG frequency</th>
					<th>DG Voltage VAC</th>
					<th>PIU O/P1 Voltage VAC</th>
					<th>PIU O/P2 Voltage VAC</th>
                    <th>O/P3 Voltage VAC</th>
                    <th>I/P Mains Voltage 1 VAC</th>
                    <th>I/P Mains Voltage 2 VAC</th>
                    <th>I/P Mains Voltage 3 VAC</th>
                    <th>Down Counter of Site 1 Hrs</th>
                    <th>Down Counter of Site 2 Hrs</th>
                    <th>Down Counter of Site 3 Hrs</th>
                    <th>Down Counter of Site 4 Hrs</th>
                    <th>DG Run Hours Hrs</th>
                    <th>Mains Run Hours Hrs</th>
                    
                    <th>Batt Run Hours Hrs</th>
                    <th>System Status</th>
                    <th>DG Fault Status</th>
                    
                    <th>DG Energy Kwh</th>
                    <th>I/P Mains Energy Kwh</th>
                    
                    <th>CH1_DCEM1</th>
                    <th>CH2_DCEM1</th>
                    <th>CH3_DCEM1</th>
                    <th>CH4_DCEM1</th>
                    
                    <th>CH1_DCEM2</th>
                    <th>CH2_DCEM2</th>
                    <th>CH3_DCEM2</th>
                    <th>CH4_DCEM2</th>
                    
                    <th>CH1_DCEM3</th>
                    <th>CH2_DCEM3</th>
                    <th>CH3_DCEM3</th>
                    <th>CH4_DCEM3</th>
                    
                    <th>CH1_DCEM4</th>
                    <th>CH2_DCEM4</th>
                    <th>CH3_DCEM4</th>
                    <th>CH4_DCEM4</th>

                    <th>BTS Status</th>
					
				</thead></tr><tbody>");


}

function DisplayEntryType1HeaderEnd()
{

    echo ("</tbody></table>");

}

function DisplayEntryType1($array)
{
    //var_dump($array);
    echo ("<table border='4' class='table-condensed table-responsive text-nowrap'  style='margin-left: 200px; font-size: small; width: 500px;'>");

    echo("<tr><td><b>#</b></td><td>".$array[0]."</td></tr>");	//#S
    echo("<tr><td><b>S/W Ver.</b></td><td>".$array[1]."</td></tr>");	//S/W Ver.
    echo("<tr><td><b>Site ID</b></td><td>".$array[2]."</td></tr>");//Site Id

    echo("<tr><td><b>Date Time</b></td><td>".$array[3]."</td></tr>");//Date Time


    $flag = intval($array[4], 10);
    echo("<tr><td><b>Alarm</b></td><td><button type='button' class='btn btn-primary btn-sm' data-toggle='modal' OnClick='ShowDetail($array[4], 0)'>$flag</button></td></tr>");//Status
    //echo("<td>".$array[4]."</td>");//Alarm

    echo("<tr><td><b>Status</b></td><td>".$array[5]."</td></tr>");//
    echo("<tr><td><b>Comm. Status</b></td><td>".$array[6]."</td></tr>");//
    echo("<tr><td><b>Room Temperature DEG C</b></td><td>".$array[7]."</td></tr>");//
    echo("<tr><td><b>Fuel Level</b></td><td>".$array[8]."</td></tr>");//
    echo("<tr><td><b>Site Battery Voltage VDC</b></td><td>".$array[9]."</td></tr>");//
    echo("<tr><td><b>Output current</b></td><td>".$array[10]."</td></tr>");//
    echo("<tr><td><b>Mains Frequency</b></td><td>".$array[11]."</td></tr>");//
    echo("<tr><td><b>DG frequency</b></td><td>".$array[12]."</td></tr>");//
    echo("<tr><td><b>DG Voltage VAC</b></td><td>".$array[13]."</td></tr>");//
    echo("<tr><td><b>PIU O/P1 Voltage VAC</b></td><td>".$array[14]."</td></tr>");//
    echo("<tr><td><b>PIU O/P2 Voltage VAC</b></td><td>".$array[15]."</td></tr>");//
    echo("<tr><td><b>O/P3 Voltage VAC</b></td><td>".$array[16]."</td></tr>");//
    echo("<tr><td><b>I/P Mains Voltage 1 VAC</b></td><td>".$array[17]."</td></tr>");//
    echo("<tr><td><b>I/P Mains Voltage 2 VAC</b></td><td>".$array[18]."</td></tr>");//
    echo("<tr><td><b>I/P Mains Voltage 3 VAC</b></td><td>".$array[19]."</td></tr>");//
    echo("<tr><td><b>Down Counter of Site 1 Hrs</b></td><td>".$array[20]."</td></tr>");//
    echo("<tr><td><b>Down Counter of Site 2 Hrs</b></td><td>".$array[21]."</td></tr>");//
    echo("<tr><td><b>Down Counter of Site 3 Hrs</b></td><td>".$array[22]."</td></tr>");//
    echo("<tr><td><b>Down Counter of Site 4 Hrs</b></td><td>".$array[23]."</td></tr>");//
    echo("<tr><td><b>DG Run Hours Hrs</b></td><td>".$array[24]."</td></tr>");//
    echo("<tr><td><b>Mains Run Hours Hrs</b></td><td>".$array[25]."</td></tr>");//
    echo("<tr><td><b>Batt. Run Hours Hrs</b></td><td>".$array[26]."</td></tr>");//
    echo("<tr><td><b>System Status</b></td><td>".$array[27]."</td></tr>");//
    echo("<tr><td><b>DG Fault Status</b></td><td>".$array[28]."</td></tr>");//
    echo("<tr><td><b>DG Energy Kwh</b></td><td>".$array[29]."</td></tr>");//
    echo("<tr><td><b>I/P Mains Energy Kwh</b></td><td>".$array[30]."</td></tr>");//
    echo("<tr><td><b>CH1_DCEM1</b></td><td>".$array[31]."</td></tr>");//
    echo("<tr><td><b>CH2_DCEM1</b></td><td>".$array[32]."</td></tr>");//
    echo("<tr><td><b>CH3_DCEM1</b></td><td>".$array[33]."</td></tr>");//
    echo("<tr><td><b>CH4_DCEM1</b></td><td>".$array[34]."</td></tr>");//
    echo("<tr><td><b>CH1_DCEM2</b></td><td>".$array[35]."</td></tr>");//
    echo("<tr><td><b>CH2_DCEM2</b></td><td>".$array[36]."</td></tr>");//
    echo("<tr><td><b>CH3_DCEM2</b></td><td>".$array[37]."</td></tr>");//
    echo("<tr><td><b>CH4_DCEM2</b></td><td>".$array[38]."</td></tr>");//
    echo("<tr><td><b>CH1_DCEM3</b></td><td>".$array[39]."</td></tr>");//
    echo("<tr><td><b>CH2_DCEM3</b></td><td>".$array[40]."</td></tr>");//
    echo("<tr><td><b>CH3_DCEM3</b></td><td>".$array[41]."</td></tr>");//
    echo("<tr><td><b>CH4_DCEM3</b></td><td>".$array[42]."</td></tr>");//
    echo("<tr><td><b>CH1_DCEM4</b></td><td>".$array[43]."</td></tr>");//
    echo("<tr><td><b>CH2_DCEM4</b></td><td>".$array[44]."</td></tr>");//
    echo("<tr><td><b>CH3_DCEM4</b></td><td>".$array[45]."</td></tr>");//
    echo("<tr><td><b>CH4_DCEM4</b></td><td>".$array[46]."</td></tr>");//
    echo("<tr><td><b>BTS Status</b></td><td>".$array[47]."</td></tr>");//
    //echo("</tr>");

    echo ("</table>");

}

function GetDeviceRecord($fullPath)
{
    $array = array();

    $protocolData = file_get_contents($fullPath);

    $protocolLen = strlen($protocolData);
    if ($protocolLen < 286) return;

    $array[] = substr($protocolData, 0, 2);//0  #S
    $array[] = substr($protocolData, 2, 3);         //1-Software Version
    $array[] = substr($protocolData, 5, 9);         //2-Site Id
    $array[] = '20'.substr($protocolData, 24, 2).'-'.substr($protocolData, 22, 2).'-'.substr($protocolData, 14, 2).' '.  //year-month-day hrs:minuts:seconds
               substr($protocolData, 16, 2).':'.substr($protocolData, 18, 2).':'.substr($protocolData, 20, 2);    //3-datetime

    $array[] = substr($protocolData, 26, 6);   //4ALARM STRING
    $array[] =substr($protocolData, 32, 4);   //5STATUS STRING
    $array[] =substr($protocolData, 36, 2);   //6COMM STRING

    $array = array_merge($array, DeviceGetData($protocolData));
    //var_dump()
    return $array;

}


function DeviceGetData($data_raw)
{
    //var_dump($data_raw);
    $array = array();
    $sub_data = substr($data_raw, 38, 39); //13____
    $stats = str_split($sub_data, 3);

    //var_dump($stats);

    //$sub_data = unpack("n",$stats[0]);
    //$sub_data = $stats[0];
    //echo "<tr><td>Room Temperature</td><td>";
    $stats[0] = substr_replace($stats[0], ".", 2, 0);
    $stats[0] = ltrim($stats[0], '0');
    if ($stats[0][0] == '.')
        $stats[0] = "0".$stats[0];

    $array[] = $stats[0];	//7


    //echo "<tr><td>Fuel Level</td><td>";
    $stats[1] = ltrim($stats[1], '0');
    if($stats[1] == '')
    {
        $stats[1] = "0";
    }
    $array[] = $stats[1];	//8

    //echo "<tr><td>Site Batt. Volt.</td><td>";

    $stats[2] = substr_replace($stats[2], ".", 2, 0);
    $stats[2] = ltrim($stats[2], '0');
    if ($stats[2][0] == '.')
        $stats[2] = "0".$stats[2];

    $array[] = $stats[2];	//9

    //echo "<tr><td>Output current</td><td>";
    $stats[3] = ltrim($stats[3], '0');
    if($stats[3] == '')
    {
        $stats[3] = "0";
    }

    $array[] = $stats[3];	//10



    for($i = 4; $i < 6; $i++)
    {
        $stats[$i] = substr_replace($stats[$i], ".", 2, 0);
        $stats[$i] = ltrim($stats[$i], '0');
        if ($stats[$i][0] == '.')
            $stats[$i] = "0".$stats[$i];
    }

    //echo "<tr><td>Mains Freq.</td><td>";
    $array[] = $stats[4];	//11

    //echo "<tr><td>DG Freq.</td><td>";
    $array[] = $stats[5];	//12

    for($i = 6; $i < 12; $i++)
    {
        $stats[$i] = ltrim($stats[$i], '0');
        if($stats[$i] == '')
        {
            $stats[$i] = "0";
        }
    }

    //echo "<tr><td>DG Volt.</td><td>";
    $array[] = $stats[6];	//13

    //echo "<tr><td>PIU O/P 1 volt.</td><td>";
    $array[] = $stats[7];	//14

    //echo "<tr><td>PIU O/P 2 volt.</td><td>";
    $array[] = $stats[8];	//15

    //echo "<tr><td>O/P 3 volt.</td><td>";
    $array[] = $stats[9];	//16

    //echo "<tr><td>I/P Mains volt 1</td><td>";
    $array[] = $stats[10];	//17

    //echo "<tr><td>I/P Mains volt 2</td><td>";
    $array[] = $stats[11];	//18

    //echo "<tr><td>I/P Mains volt 3</td><td>";
    $array[] = $stats[12];	//19

    $sub_data = substr($data_raw, 77, 56); //7
    $stats = str_split($sub_data, 8);

    for($i = 0; $i < 7; $i++)
    {
        $stats[$i] = substr_replace($stats[$i], ".", 7, 0);
        $stats[$i] = ltrim($stats[$i], '0');
        if ($stats[$i][0] == '.')
            $stats[$i] = "0".$stats[$i];
    }

    $array[] = $stats[0];	//20
    $array[] = $stats[1];	//21
    $array[] = $stats[2];	//22
    $array[] = $stats[3];	//23
    $array[] = $stats[4];	//24
    $array[] = $stats[5];	//25
    $array[] = $stats[6];	//26

    $sub_data = substr($data_raw, 133, 4); //2
    $stats = str_split($sub_data, 2);

    $array[] = $stats[0];	//27
    $array[] = $stats[1];	//28

    $sub_data = substr($data_raw, 137, 144); //18
    $stats = str_split($sub_data, 8);

    for($i = 0; $i < 2; $i++)
    {
        $stats[$i] = ltrim($stats[$i], '0');
        if($stats[$i] == '')
        {
            $stats[$i] = "0";
        }
    }


    $array[] = $stats[0];	//29
    $array[] = $stats[1];	//30

    for($i = 2; $i < 18; $i++)
    {
        $stats[$i] = substr_replace($stats[$i], ".", 7, 0);
        $stats[$i] = ltrim($stats[$i], '0');
        if ($stats[$i][0] == '.')
            $stats[$i] = "0".$stats[$i];
    }

    $array[] = $stats[2];	//31
    $array[] = $stats[3];	//32
    $array[] = $stats[4];	//33
    $array[] = $stats[5];	//34
    $array[] = $stats[6];	//35
    $array[] = $stats[7];	//36
    $array[] = $stats[8];	//37
    $array[] = $stats[9];	//38
    $array[] = $stats[10];	//39
    $array[] = $stats[11];	//40
    $array[] = $stats[12];	//41
    $array[] = $stats[13];	//42
    $array[] = $stats[14];	//43
    $array[] = $stats[15];	//44
    $array[] = $stats[16];	//45
    $array[] = $stats[17];	//46

    $sub_data = substr($data_raw, 281, 4);
    $stats = str_split($sub_data, 4);

    $array[] = $stats[0];	//47

























    //var_dump($stats);

    /*-----------------------------



            //18*2	= 36
            //6 *8	= 48
            //1 *2	= 02
            //----------
            //        86
            //----------


            //Updated Data - Updated Protocol

            //18*2	= 36
            //6 *8	= 48
            //1 *2	= 02
            //2 *2	= 04	*
            //2 *1	= 02	*
            //----------
            //        92
            //----------

            //REV2 Total 134 Bytes
           */

    return $array;
}


?>