<?php
/**
 * Created by PhpStorm.
 * User: user1
 * Date: 6/1/2017
 * Time: 3:07 PM
 */
$file = $_GET['file'];
$fullPath = $_GET['fullpath'];

function stringDownload($fullPath,$name)
{
    //$name= $_GET['nama'];

    header('Content-Description: File Transfer');
    header('Content-Type: application/force-download');
    header("Content-Disposition: attachment; filename=\"" . basename($name) . "\";");
    header('Content-Transfer-Encoding: binary');
    header('Expires: 0');
    header('Cache-Control: must-revalidate');
    header('Pragma: public');
    header('Content-Length: ' . filesize($fullPath));
    ob_clean();
    flush();
    readfile($fullPath); //showing the path to the server where the file is to be download
    exit;
}

stringDownload($fullPath,$file);

?>