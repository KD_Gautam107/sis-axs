<?php


function DeviceGetStatusBits($data_status)
{
	$array = array();
	$stats = unpack("C*", $data_status);

	//Smoke Fire Alarm
	$array[] = ($stats[5] & 0x01);

	//Door
	$array[] = ($stats[5] & 0x02);

	//Auto/Man Mode 0 Means  Auto Mode, 1 Means  Man Mode
	$array[] = ($stats[5] & 0x04);

	//00    :Load on EB,01: Load on DG, 10: Load on site Battery   11: Not used
	if (($stats[5] & 0x08) == false) {
		if (($stats[5] & 0x10) == false)
			$array[] =  "Load on EB";
		else
			$array[] =  "Load on site Battery";
	} else {
		if (($stats[5] & 0x10) == false)
			$array[] =  "Load on DG";
		else
			$array[] =  "Not Used";
	}

	//Mains Fail
	$array[] = ($stats[5] & 0x20);

	//Site Battery Low
	$array[] = ($stats[5] & 0x40);


	//Room Temperature
	$array[] = ($stats[5] & 0x80);


	//LLOP
	$array[] = ($stats[4] & 0x01);


	//HCT/HWT
	$array[] = ($stats[4] & 0x02);

	//Alternate Fault
	$array[] = ($stats[4] & 0x04);

	//DG Over Speed
	$array[] = ($stats[4] & 0x08);

	//DG Over Load
	$array[] = ($stats[4] & 0x10);

	//DG Low Fuel
	$array[] = ($stats[4] & 0x20);

	//DG Start Fail
	$array[] = ($stats[4] & 0x40);

	//DG Stop Fail
	$array[] = ($stats[4] & 0x80);

	//DG battery Low
	$array[] = ($stats[3] & 0x01);

	//LCU fail
	$array[] = ($stats[3] & 0x02);

	//Rectifier Fail
	$array[] = ($stats[3] & 0x04);

	//Multi Rectifier Fail
	$array[] = ($stats[3] & 0x08);

	//LVD TRIP
	$array[] = ($stats[3] & 0x10);

	//LVD BY pass
	$array[] = ($stats[3] & 0x20);


	//$FLAG_22_RES 	= 0x0000400000;	//Reserved
	//$FLAG_23_RES 	= 0x0000800000;	//Reserved
	//$FLAG_24_RES 	= 0x0001000000;	//Reserved
	//$FLAG_25_RES 	= 0x0002000000;	//Reserved
	//$FLAG_26_RES 	= 0x0004000000;	//Reserved
	//$FLAG_27_RES 	= 0x0008000000;	//Reserved
	//$FLAG_28_RES 	= 0x0010000000;	//Reserved
	//$FLAG_29_RES 	= 0x0020000000;	//Reserved
	//$FLAG_30_RES 	= 0x0040000000;	//Reserved
	//$FLAG_31_RES 	= 0x0080000000;	//Reserved

	//DG OFF
	$array[] = ($stats[1] & 0x01);

	//DG ON
	$array[] = ($stats[1] & 0x02);

	//DG Cranking
	$array[] = ($stats[1] & 0x04);

	//DG Start in Progress
	$array[] = ($stats[1] & 0x08);

	//DG Cool Down (Idle Running)
	$array[] = ($stats[1] & 0x10);

	//DG STOP  Normally
	$array[] = ($stats[1] & 0x20);

	//DG STOP due to Fault
	$array[] = ($stats[1] & 0x40);

	//DG Stop Due to Maximum Run Exipary 
	$array[] = ($stats[1] & 0x80);

	return $array;
}



function DeviceGetData($data_raw)
{
	$array = array();
	$sub_data = substr($data_raw, 41, 36);
	$stats = str_split($sub_data, 2);
	//var_dump($stats);

	$sub_data = unpack("n", $stats[0]);
	$sub_data = $sub_data[1] / 10;
	//echo "<tr><td>Room Temperature</td><td>";
	$array[] = $sub_data;	//8


	$sub_data = unpack("n", $stats[1]);
	$sub_data = $sub_data[1] / 10;
	//echo "<tr><td>Fuel Level</td><td>";
	$array[] = $sub_data;	//9

	$sub_data = unpack("n", $stats[2]);
	$sub_data = $sub_data[1] / 10;
	//echo "<tr><td>Fuel Data</td><td>";
	$array[] = $sub_data;	//10

	$sub_data = unpack("n", $stats[3]);
	$sub_data = $sub_data[1] / 10;
	//echo "<tr><td>Site Batt. bank Voltage</td><td>";
	$array[] = $sub_data;	//11

	$sub_data = unpack("n", $stats[4]);
	$sub_data = $sub_data[1] / 10;
	//echo "<tr><td>R-Phase current</td><td>";
	$array[] = $sub_data;	//12

	$sub_data = unpack("n", $stats[5]);
	$sub_data = $sub_data[1] / 10;
	//echo "<tr><td>Y-Phase current</td><td>";
	$array[] = $sub_data;	//13

	$sub_data = unpack("n", $stats[6]);
	$sub_data = $sub_data[1] / 10;
	//echo "<tr><td>B-Phase current</td><td>";
	$array[] = $sub_data;	//14

	$sub_data = unpack("n", $stats[7]);
	$sub_data = $sub_data[1] / 10;
	//echo "<tr><td>Mains Frequency</td><td>";
	$array[] = $sub_data;	//15

	$sub_data = unpack("n", $stats[8]);
	$sub_data = $sub_data[1] / 10;
	//echo "<tr><td>DG frequency</td><td>";
	$array[] = $sub_data;	//16

	$sub_data = unpack("n", $stats[9]);
	$sub_data = $sub_data[1] / 10;
	//echo "<tr><td>DG-R phase Voltage</td><td>";
	$array[] = $sub_data;	//17

	$sub_data = unpack("n", $stats[10]);
	$sub_data = $sub_data[1] / 10;
	//echo "<tr><td>DG-Y phase Voltage</td><td>";
	$array[] = $sub_data;	//18

	$sub_data = unpack("n", $stats[11]);
	$sub_data = $sub_data[1] / 10;
	//echo "<tr><td>DG-B phase Voltage</td><td>";
	$array[] = $sub_data;	//19

	$sub_data = unpack("n", $stats[12]);
	$sub_data = $sub_data[1] / 10;
	//echo "<tr><td>LCU1 Output Voltage</td><td>";
	$array[] = $sub_data;	//20

	$sub_data = unpack("n", $stats[13]);
	$sub_data = $sub_data[1] / 10;
	//echo "<tr><td>LCU2 Output Voltage</td><td>";
	$array[] = $sub_data;	//21

	$sub_data = unpack("n", $stats[14]);
	$sub_data = $sub_data[1] / 10;
	//echo "<tr><td>LCU3 Output Voltage</td><td>";
	$array[] = $sub_data;	//22

	$sub_data = unpack("n", $stats[15]);
	$sub_data = $sub_data[1] / 10;
	//echo "<tr><td>Input Mains Voltage - R Phase</td><td>";
	$array[] = $sub_data;	//23

	$sub_data = unpack("n", $stats[16]);
	$sub_data = $sub_data[1] / 10;
	//echo "<tr><td>Input Mains Voltage - Y Phase</td><td>";
	$array[] = $sub_data;	//24

	$sub_data = unpack("n", $stats[17]);
	$sub_data = $sub_data[1] / 10;
	//echo "<tr><td>Input Mains Voltage - B Phase</td><td>";
	$array[] = $sub_data;	//25

	/////


	$sub_data = substr($data_raw, 77, 48);
	$stats = str_split($sub_data, 8);
	$stats = substr_replace($stats, ".", 7, 0);

	for ($i = 0; $i < 6; $i++) {
		$stats[$i] = ltrim($stats[$i], '0');
		if ($stats[$i][0] == '.')
			$stats[$i] = "0" . $stats[$i];
	}

	//echo "<tr><td>DG Running Hours</td><td>";
	$array[] = $stats[0];	//26

	//echo "<tr><td>Mains RUN HOURS</td><td>";
	$array[] = $stats[1];	//27

	//echo "<tr><td>Batt RUN HOURS</td><td>";
	$array[] = $stats[2];	//28

	//echo "<tr><td>O/P Mains Energy</td><td>";
	$array[] = $stats[3];	//29

	//echo "<tr><td>DG Energy</td><td>";
	$array[] = $stats[4];	//30

	//echo "<tr><td>I/P Mains Energy</td><td>";
	$array[] = $stats[5];	//31

	$sub_data = substr($data_raw, 125, 2);

	$sub_data = unpack("n", $sub_data);
	$sub_data = $sub_data[1] / 10;
	//echo "<tr><td>DG Battery Voltage</td><td>";
	$array[] = $sub_data;	//32


	//18*2	= 36
	//6 *8	= 48
	//1 *2	= 02
	//----------
	//        86
	//----------


	//Updated Data - Updated Protocol

	//18*2	= 36
	//6 *8	= 48
	//1 *2	= 02
	//2 *2	= 04	*
	//2 *1	= 02	*
	//----------
	//        92
	//----------

	//REV2 Total 134 Bytes
	if (strlen($data_raw) >= 134) {
		$sub_data = substr($data_raw, 127, 4);
		$stats = str_split($sub_data, 2);

		$sub_data = unpack("n", $stats[0]);
		$sub_data = $sub_data[1] / 10;
		//echo "<tr><td>Battery Charging current</td><td>";
		$array[] = $sub_data;	//33


		$sub_data = unpack("n", $stats[1]);
		$sub_data = $sub_data[1] / 10;
		//echo "<tr><td>Battery Discharging current</td><td>";
		$array[] = $sub_data;	//34

		$sub_data = substr($data_raw, 131, 2);
		$stats = str_split($sub_data, 1);

		$sub_data = unpack("c", $stats[0]);
		$sub_data = $sub_data[1] / 1;
		//echo "<tr><td>Battery status</td><td>";
		$array[] = $sub_data;	//35

		$sub_data = unpack("c", $stats[1]);
		$sub_data = $sub_data[1] / 10;
		//echo "<tr><td>Battery back up time</td><td>";
		$array[] = $sub_data;	//36


		//REV3	Total 150 Bytes
		if (strlen($data_raw) >= 150) {
			$sub_data = substr($data_raw, 133, 16);
			$stats = str_split($sub_data, 8);
			$stats = substr_replace($stats, ".", 7, 0);

			for ($i = 0; $i < 2; $i++) {
				$stats[$i] = ltrim($stats[$i], '0');
				if ($stats[$i][0] == '.')
					$stats[$i] = "0" . $stats[$i];
			}

			//echo "<tr><td>Battery Charging Energy (Kwh)</td><td>";
			$array[] = $stats[0];	//37

			//echo "<tr><td>Battery Discharging Energy (Kwh)</td><td>";
			$array[] = $stats[1];	//38

		} else {
			$array[] = 0;	//37
			$array[] = 0;	//38
		}
	} else {
		$array[] = 0;	//33
		$array[] = 0;	//34
		$array[] = 0;	//35
		$array[] = 0;	//36
		$array[] = 0;	//37
		$array[] = 0;	//38
	}

	return $array;
}



function GetDeviceRecordFromId($id)
{
	$array = array();

	include 'dbinc.php';


	$mysqli = new mysqli("$mysql_hostname", "$mysql_user", "$mysql_password", "$mysql_database");
	if (mysqli_connect_errno()) {
		printf("Connect failed: %s\n", mysqli_connect_error());
		return null;
	}

	//$sql = 'SELECT a.tutorial_id, a.tutorial_author, b.tutorial_count
	//	FROM tutorials_tbl a, tcount_tbl b
	//	WHERE a.tutorial_author = b.tutorial_author';

	//		$order = "SELECT 
	//			a.ID, a.timestamp , a.data_validated , a.type , a.site_id , a.date_time , a.device_id , a.status , a.raw_data,  
	//			b.Make, b.Cluster, b.Circle, b.SiteName,
	//			c.Name, c.Contact1,
	//			b.mobile, b.Id
	//		FROM quanta a, siteinfo b, technicians c
	//		WHERE (a.site_id = b.site_id) AND (b.TechId = c.Id) AND a.id=".$id;

	$order = "SELECT 
			ID, timestamp , data_validated , type , site_id , date_time , device_id , status , raw_data
			FROM quanta 
			WHERE id=" . $id;


	//echo $order;

	//$order = "SELECT * FROM quanta where id=".$id;
	$result = $mysqli->query($order);
	$data = $result->fetch_row();

	$array[] 	= $data[0];		//0Unique Id
	$array[] 	= $data[1];		//1Time Stamp by Server
	$array[]	= $data[2];		//2Validated by Server
	$array[] 	= $data[3];		//3Type 0 - Periodic / 1 - Fault
	$array[]	= $data[4];		//4String Site Id
	$array[]	= $data[5];		//5Date Time by Device
	$array[]	= $data[6];		//6Device Id/ Only 1 known for now
	$array[]	= $data[7];		//7Status Bits - To Expand
	$data_raw	= $data[8];		//Raw Data, for device specific information

	//var_dump($array);
	//$array = array_merge($array, DeviceGetStatusBits($data[7]));
	//var_dump($array);


	if ($data[6] == 1) {
		$array = array_merge($array, DeviceGetData($data_raw));
	}

	$array[]	= ""; //$data[9];	//39 Make	
	$array[]	= ""; //$data[10];	//40 Cluster
	$array[]	= ""; //$data[11];	//41 Circle
	$array[]	= ""; //$data[12];	//42 SiteName
	$array[]	= ""; //$data[13];	//43 Technician Name
	$array[]	= ""; //$data[14];	//44 Technician Mobile
	$array[]	= ""; //$data[15];	//45 Site Mobile
	$array[]	= ""; //$data[16];	//46 Site Id


	$result->close();
	mysqli_close($mysqli);

	//var_dump($array);

	return $array;
}

function AddUp($array, $sum)
{
	if ($sum == null) {
		$array1[]	= abs($array[10]);
		$array1[]	= abs($array[11]);
		$array1[]	= abs($array[12]);
		$array1[]	= abs($array[13]);
	} else {
		$array1[]	= abs($array[10]) + $sum[0];
		$array1[]	= abs($array[11]) + $sum[1];
		$array1[]	= abs($array[12]) + $sum[2];
		$array1[]	= abs($array[13]) + $sum[3];
	}

	return $array1;
}


function DisplayEntryType1Header()
{
	echo ("<table class='table table-striped table-condensed table-responsive'>");

	echo ("<tr><thead>
					<th>SITE NAME</th>
					<th>SITE ID</th>
					<th>Circle</th>
					<th>Cluster</th>
					<th>Make</th>
					<th>AM NAME</th>
					<th>Mob no.</th>
					<th>Type</th>
					<th>Date Time</th>
					<th>Status</th>
					<th>Device Id</th>
					
					<th>Room Temperature</th>
					<th>Fuel Level</th>
					<th>Fuel Data</th>
					<th>Site Battery bank Voltage</th>
					<th>R-Phase current</th>
					<th>Y-Phase current</th>
					<th>B-Phase current</th>
					<th>Mains Frequency</th>
					<th>DG frequency</th>
					<th>DG-R phase Voltage</th>
					<th>DG-Y phase Voltage</th>
					<th>DG-B phase Voltage</th>
					<th>LCU1 Output Voltage</th>
					<th>LCU2 Output Voltage</th>
					<th>LCU3 Output Voltage</th>
					<th>Input Mains Voltage - R Phase</th>
					<th>Input Mains Voltage - Y Phase</th>
					<th>Input Mains Voltage - B Phase</th>
					<th>DG Running Hours</th>
					<th>Mains RUN HOURS</th>
					<th>Battery RUN HOURS</th>
					<th>O/P Mains Energy</th>
					<th>DG Energy</th>
					<th>I/P Mains Energy</th>
					<th>DG Battery Voltage</th>
					<th>Battery Charging Current</th>
					<th>Battery Discharging Current</th>
					<th>Battery Status</th>
					<th>Battery Back Up Time</th>
					<th>Battery Charging Energy</th>
					<th>Battery Discharging Energy</th>
					<th>Generator Start</th>
					<th>Generator Stop</th>
				</thead></tr><tbody>");
}

function DisplayEntryType2Header()
{
	echo ("<table class='table table-hover table-condensed table-responsive'>");

	echo ("<tr><thead>
					<th>SITE NAME</th>
					<th>SITE ID</th>
					<th>Circle</th>
					<th>Cluster</th>
					
					<th>Make</th>
					
					<th>AM NAME</th>
					<th>Mob no.</th>
					<th>Date Time</th>
					<th>Interval</th>
					

					<th>DG Running Hours</th>
					<th>Mains RUN HOURS</th>
					<th>Battery RUN HOURS</th>

					<th>Generator Start</th>
					<th>Generator Stop</td>
				</thead></tr><tbody>");
}


function DisplayEntryType1HeaderEnd()
{

	echo ("</tbody></table>");
}

function DisplayEntryType1($array)
{
	$color = "";
	if ($array[3] == 1) {
		$color = " class='danger' ";
	}
	echo ("<tr $color><td>" . $array[40] . "</td>");
	echo ("<td>" . $array[4] . "</td>");	//Site Id


	echo ("<td>" . $array[41] . "</td>"); //Circle
	echo ("<td>" . $array[40] . "</td>"); //Culster
	echo ("<td>" . $array[39] . "</td>"); //Make
	echo ("<td>" . $array[43] . "</td>"); //AM NAME
	echo ("<td>" . $array[44] . "</td>"); //Mob no.

	$data_type_str = "";
	if ($array[3] == 0) $data_type_str = "P";
	if ($array[3] == 1) $data_type_str = "F";
	echo ("<td>" . $data_type_str . "</td>"); //Type

	echo ("<td>" . $array[5] . "</td>"); //Date Time



	$data_status_hex = bin2hex($array[7]);
	echo ("<td><button type='button' class='btn btn-primary btn-sm' data-toggle='modal' OnClick='ShowDetail($array[0])'>$data_status_hex</button></td>"); //Status
	echo ("<td>" . $array[6] . "</td>"); //Device Id

	echo ("<td>" . $array[8] . "</td>"); //Room Temperature
	echo ("<td>" . $array[9] . "</td>"); //Fuel Level
	echo ("<td>" . $array[10] . "</td>"); //Fuel Data
	echo ("<td>" . $array[11] . "</td>"); //Site Batt. bank Voltage
	echo ("<td>" . $array[12] . "</td>"); //R-Phase current
	echo ("<td>" . $array[13] . "</td>"); //Y-Phase current
	echo ("<td>" . $array[14] . "</td>"); //B-Phase current
	echo ("<td>" . $array[15] . "</td>"); //Mains Frequency
	echo ("<td>" . $array[16] . "</td>"); //DG frequency
	echo ("<td>" . $array[17] . "</td>"); //DG-R phase Voltage
	echo ("<td>" . $array[18] . "</td>"); //DG-Y phase Voltage
	echo ("<td>" . $array[19] . "</td>"); //DG-B phase Voltage
	echo ("<td>" . $array[20] . "</td>"); //LCU1 Output Voltage
	echo ("<td>" . $array[21] . "</td>"); //LCU2 Output Voltage
	echo ("<td>" . $array[22] . "</td>"); //LCU3 Output Voltage
	echo ("<td>" . $array[23] . "</td>"); //Input Mains Voltage - R Phase
	echo ("<td>" . $array[24] . "</td>"); //Input Mains Voltage - Y Phase
	echo ("<td>" . $array[25] . "</td>"); //Input Mains Voltage - B Phase
	echo ("<td>" . $array[26] . "</td>"); //DG Running Hours
	echo ("<td>" . $array[27] . "</td>"); //Mains RUN HOURS
	echo ("<td>" . $array[28] . "</td>"); //Batt RUN HOURS
	echo ("<td>" . $array[29] . "</td>"); //O/P Mains Energy
	echo ("<td>" . $array[30] . "</td>"); //DG Energy
	echo ("<td>" . $array[31] . "</td>"); //I/P Mains Energy
	echo ("<td>" . $array[32] . "</td>"); //DG Battery Voltage
	echo ("<td>" . $array[33] . "</td>"); //Battery Charging current
	echo ("<td>" . $array[34] . "</td>"); //Battery Discharging current
	echo ("<td>" . $array[35] . "</td>"); //Battery status
	echo ("<td>" . $array[36] . "</td>"); //Battery back up time
	echo ("<td>" . $array[37] . "</td>"); //Battery Charging Energy
	echo ("<td>" . $array[38] . "</td>"); //Battery Disharging Energy

	if (ctype_space($array[43]) || $array[43] == '') //Mobile
	{
		echo ("<td></td>");
		echo ("<td></td>");
	} else {
		echo ("<td><button type='button' class='btn-success btn-primary btn-sm' data-toggle='modal' OnClick='GenStartStop(1, $array[40])'>START</button></td>"); //START
		echo ("<td><button type='button' class='btn-danger btn-primary btn-sm' data-toggle='modal' OnClick='GenStartStop(0, $array[40])'>STOP</button></td>"); //STOP
	}
}

function DisplayEntryType2($array)
{
	echo "<tr>";
	echo ("<td>" . $array[14] . "</td>"); //Site Name
	echo ("<td>" . $array[0] . "</td>");	//Site Id


	echo ("<td>" . $array[1] . "</td>"); //Circle
	echo ("<td>" . $array[2] . "</td>"); //Culster
	echo ("<td>" . $array[3] . "</td>"); //Make
	echo ("<td>" . $array[4] . "</td>"); //AM NAME
	echo ("<td>" . $array[5] . "</td>"); //Mob no.

	echo ("<td>" . $array[7]->format('Y-m-d H:i:s') . "</td>"); //Date Time
	echo ("<td>" . round($array[10], 2) . "</td>"); //Interval

	echo ("<td>" . abs(round($array[11], 2)) . "</td>"); //DG Running Hours
	echo ("<td>" . abs(round($array[12], 2)) . "</td>"); //Mains RUN HOURS
	echo ("<td>" . abs(round($array[13], 2)) . "</td>"); //Batt RUN HOURS


	if (ctype_space($array[6]) || $array[6] == '') //Mobile
	{
		echo ("<td></td>");
		echo ("<td></td>");
	} else {
		echo ("<td><button type='button' class='btn-success btn-primary btn-sm' data-toggle='modal' OnClick='GenStartStop(1, $array[40])'>START</button></td>"); //START
		echo ("<td><button type='button' class='btn-danger btn-primary btn-sm' data-toggle='modal' OnClick='GenStartStop(0, $array[40])'>STOP</button></td>"); //STOP
	}
	echo "</tr>";
}

function DisplayEntryType2Sum($array)
{
	echo "<tr class='info'>";
	echo ("<td><strong>Total</strong></td>"); //Site Name
	echo ("<td></td>");	//Site Id


	echo ("<td></td>"); //Circle
	echo ("<td></td>"); //Culster
	echo ("<td></td>"); //Make
	echo ("<td></td>"); //AM NAME
	echo ("<td></td>"); //Mob no.

	echo ("<td></td>"); //Date Time
	echo ("<td>" . round($array[0], 2) . "</td>"); //Interval

	echo ("<td>" . abs(round($array[1], 2)) . "</td>"); //DG Running Hours
	echo ("<td>" . abs(round($array[2], 2)) . "</td>"); //Mains RUN HOURS
	echo ("<td>" . abs(round($array[3], 2)) . "</td>"); //Batt RUN HOURS


	echo ("<td></td>");
	echo ("<td></td>");

	echo "</tr>";
}



function GetComparisonFromIds($id1, $id2)
{
	$array = array();

	$arr1 = GetDeviceRecordFromId($id1);	//New
	$arr2 = GetDeviceRecordFromId($id2);	//Old
	//var_dump($arr1);
	//var_dump($arr2);

	$array[] = $arr1[4];	//0 Site Id
	$array[] = $arr1[39];	//1 Circle
	$array[] = $arr1[38];	//2 Culster
	$array[] = $arr1[37];	//3 Make
	$array[] = $arr1[41];	//4 AM NAME
	$array[] = $arr1[42];	//5 Mob no.
	$array[] = $arr1[43]; 	//6 DG Mobile


	//What Diffs to Retrieve
	//$array[] = new DateTime(arr2[5]);	//7 From Time
	$array[] = date_create_from_format('Y-m-d H:i:s', $arr2[5]);
	//$array[] = new DateTime(arr1[5]);	//8 To Time
	$array[] = date_create_from_format('Y-m-d H:i:s', $arr1[5]);

	//echo "F: $arr2[5]". " > ".$array[0]->format('Y-m-d H:i:s')."<br>";
	//echo "T: $arr1[5]". " > ".$array[1]->format('Y-m-d H:i:s')."<br>";

	$array[] = $array[7]->diff($array[8]);	//9 Interval
	//echo "Interval: ".$array[2]->format("%d %h %i")."<br>";
	//var_dump($array[2]);

	$delta_h = ($array[8]->getTimestamp() - $array[7]->getTimestamp()) / 3600;
	$array[] = $delta_h;	//10 Hours
	//echo "Hours: $array[3]<br>";

	$array[] = $arr1[26] - $arr2[26]; //11 System On DG
	$array[] = $arr1[27] - $arr2[27]; //12 System On Mains
	$array[] = $arr1[28] - $arr2[28]; //13 System On Batt

	//echo "DG: ".$arr1[26]." - ".$arr2[26]." = ".$array[4]."<br>";
	//echo "Mains: ".$arr1[27]." - ".$arr2[27]." = ".$array[5]."<br>";
	//echo "Batt: ".$arr1[28]." - ".$arr2[28]." = ".$array[6]."<br>";

	$array[] = $arr1[40];	//14 Site Name

	return $array;
}


function GetSiteDetailFromId($id)
{
	$array = array();

	include 'dbinc.php';


	$mysqli = new mysqli("$mysql_hostname", "$mysql_user", "$mysql_password", "$mysql_database");
	if (mysqli_connect_errno()) {
		printf("Connect failed: %s\n", mysqli_connect_error());
		return null;
	}
	$order = "SELECT 
			b.site_id,
			b.Make,
			b.Cluster,
			b.Circle,
			b.SiteName,
			c.Name,
			c.Contact1 
			FROM
			siteinfo b,
			technicians c
		WHERE (b.TechId = c.Id) AND b.id=" . $id;

	//print($order);
	//print("<br>");

	$result = $mysqli->query($order);

	$data = $result->fetch_row();

	$array[]	= $data[0];	//0 Site ID String	
	$array[]	= $data[1];	//1 Make
	$array[]	= $data[2];	//2 Cluster
	$array[]	= $data[3];	//3 Circle
	$array[]	= $data[4];	//4 SiteName
	$array[]	= $data[5];	//5 Technician Name
	$array[]	= $data[6];	//6 Technician Mobile


	$result->close();
	mysqli_close($mysqli);

	//var_dump($array);

	return $array;
}

function GetSiteStatsFromIdStartEnd($site_id, $start_date, $end_date)
{
}

function GetSiteStatsFromId($site_id)
{
	//return GetSiteStatsFromIdStartInterval($site_id, null, 24);
	//return GetSiteStatsFromIdStartInterval($site_id, "2015-05-16 16:02:04", 24);
	return GetSiteStatsFromIdStartInterval($site_id, "2015-05-02 15:16:26", 24);
}

function GetSiteStatsFromIdStartInterval($site_id, $start_date_time, $hours)
{
	include 'dbinc.php';

	$sql_dt = "";
	if ($start_date_time == null) {
		$sql_dt = "NOW()";
	} else {
		//$sql_dt = "\"".date_create_from_format('Y-m-d H:i:s', $start_date_time)."\"";
		$sql_dt = "\"" . $start_date_time . "\"";
	}


	$mysqli = new mysqli("$mysql_hostname", "$mysql_user", "$mysql_password", "$mysql_database");
	if (mysqli_connect_errno()) {
		printf("Connect failed: %s\n", mysqli_connect_error());
		return null;
	}

	//Get first Id
	$sql = "SELECT
					id,
					date_time
				FROM
				  quanta
				WHERE
					site_id=\"$site_id\" 
				ORDER BY
				  (ABS(TIMESTAMPDIFF(SECOND, date_time, ($sql_dt)))) ASC
				LIMIT 1";

	//echo "$sql <br>";

	$result = $mysqli->query($sql);
	//var_dump($result);
	if ($result !== false) {
		$data = $result->fetch_row();
		$id1		= $data[0];	//First Id	
		//echo "<br>ID1: $id1";

		$new_dt = date_create_from_format('Y-m-d H:i:s', $data[1]);
		$new_dt->sub(new DateInterval('PT' . $hours . 'H'));
		$sql_dt = "\"" . $new_dt->format('Y-m-d H:i:s') . "\"";

		$result->close();


		//Get 2nd Id
		$sql = "SELECT
						id
					FROM
					  quanta
					WHERE
						site_id=\"$site_id\"  					  
					ORDER BY
					  (ABS(TIMESTAMPDIFF(SECOND, date_time, ($sql_dt)))) ASC
					LIMIT 2";

		//echo "$sql <br>";

		$result = $mysqli->query($sql);
		if ($result !== false) {
			$data1 = $result->fetch_row();
			$id2		= $data1[0];	//Second Id	
			//echo "<br>ID2: $id2";

			if ($id1 == $id2) {
				//echo "<br>Same Id $id1 and $id2, We have results more than given time interval apart<br>";
				//Find the next record

				$data1 = $result->fetch_row();
				if ($data1 !== false) {
					$id2 = $data1[0];	//Second Id
				} else {
					//We have not enough records
					$result->close();
					mysqli_close($mysqli);
					echo "Not Enough Records";
					return null;
				}
			}

			$result->close();

			//echo "<br>Comparison between $id1 and $id2<br>";
			$array = GetComparisonFromIds($id1, $id2);

			mysqli_close($mysqli);
			return $array;
		}
	}

	mysqli_close($mysqli);
	echo "NULL";

	return null;
}
