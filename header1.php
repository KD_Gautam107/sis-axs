<nav class="navbar navbar-default">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
	  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="\base.php">
        <img alt="Brand" src="media\logo25.png">
      </a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li class="active"><a href="#">REAL TIME DATA <span class="sr-only">(current)</span></a></li>
        <li><a href="#">REPORTS</a></li>
		<li><a href="#">ALARMS</a></li>
		<li><a href="#">MAINTENANCE</a></li>
 
      </ul>

	  <?php if (isset($_SESSION['login_user'])){ ?>
      <ul class="nav navbar-nav navbar-right">
        <li><p class="navbar-text">Signed in as <?php echo $_SESSION['login_user']; ?></p></li>
        <li><p class="navbar-btn"><a href="logout.php" class="btn btn-danger">Sign out</a></p></li>
      </ul>
	  <?php } ?>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>  

