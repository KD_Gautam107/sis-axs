<?php

	session_start();

	$error=0;
	if($_SERVER["REQUEST_METHOD"] == "POST")
	{
		$myusername=addslashes($_POST['username']);
		$mypassword=addslashes($_POST['password']);

		 
		// If result matched $myusername and $mypassword -- hardcoded
		if (($myusername=="admin") && ($mypassword=="sis@123"))
		{
			$_SESSION['sysinfra_user']=$myusername;
			
			header("location: index.php");
			
		}
		else
		{
			$error=1;
		}		
	}
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>SIS-AXS Administration Console</title>

    <!-- Bootstrap -->
    <!-- Latest compiled and minified CSS -->
	  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">

	  <!-- Optional theme -->
	  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css">

	  <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>

	  <!-- Latest compiled and minified JavaScript -->
	  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
	
	<style>
		.borderless tbody tr td, .borderless tbody tr th, .borderless thead tr th {
			border: none;
		}
	</style>
	
  </head>
  <body>
	
	<p>Administrative Console Login<p>
	
	<div style="margin:10px; width: 300px;">

	
		<?php if ($error == 1)
			{
				echo "<p class='bg-danger'>Your Login Name or Password is invalid</p>";
			}
		?>

		
		<form action="" method="post">
		  <div class="form-group">
			<label for="exampleInputEmail1">Console User Name</label>
			<input type="text" class="form-control" name="username" placeholder="Enter User Name">
		  </div>
		  <div class="form-group">
			<label for="exampleInputPassword1">Password</label>
			<input type="password" class="form-control" name="password" placeholder="Password">
		  </div>

		  <button type="submit" class="btn btn-default" value=" Submit ">Submit</button>
		</form>
		
		
	</div>
	
</body>
</html>