<?php

/**
 * View any string as a hexdump.
 *
 * This is most commonly used to view binary data from streams
 * or sockets while debugging, but can be used to view any string
 * with non-viewable characters.
 *
 * @version     1.3.2
 * @author      Aidan Lister <aidan@php.net>
 * @author      Peter Waller <iridum@php.net>
 * @link        http://aidanlister.com/2004/04/viewing-binary-data-as-a-hexdump-in-php/
 * @param       string  $data        The string to be dumped
 * @param       bool    $htmloutput  Set to false for non-HTML output
 * @param       bool    $uppercase   Set to true for uppercase hex
 * @param       bool    $return      Set to true to return the dump
 */
function hexdump ($data, $htmloutput = true, $uppercase = false, $return = false)
{
    // Init
    $hexi   = '';
    $ascii  = '';
    $dump   = ($htmloutput === true) ? '<pre>' : '';
    $offset = 0;
    $len    = strlen($data);

    // Upper or lower case hexadecimal
    $x = ($uppercase === false) ? 'x' : 'X';

    // Iterate string
    for ($i = $j = 0; $i < $len; $i++)
    {
        // Convert to hexidecimal
        $hexi .= sprintf("%02$x ", ord($data[$i]));

        // Replace non-viewable bytes with '.'
        if (ord($data[$i]) >= 32) {
            $ascii .= ($htmloutput === true) ?
                htmlentities($data[$i]) :
                $data[$i];
        } else {
            $ascii .= '.';
        }

        // Add extra column spacing
        if ($j === 7) {
            $hexi  .= ' ';
            $ascii .= ' ';
        }

        // Add row
        if (++$j === 16 || $i === $len - 1) {
            // Join the hexi / ascii output
            $dump .= sprintf("%04$x  %-49s  %s", $offset, $hexi, $ascii);

            // Reset vars
            $hexi   = $ascii = '';
            $offset += 16;
            $j      = 0;

            // Add newline
            if ($i !== $len - 1) {
                $dump .= "\n";
            }
        }
    }

    // Finish dump
    $dump .= $htmloutput === true ?
        '</pre>' :
        '';
    $dump .= "\n";

    // Output method
    if ($return === false) {
        echo $dump;
    } else {
        return $dump;
    }
}

function IsNullOrEmptyString($question){
    return (!isset($question) || trim($question)==='');
}


function getlogpath()
{
    //return "/akhilesh/code/collector.log";
    return "/data/";
}

function getdumppath()
{
    //return "/akhilesh/code/dump/";
    return "/home/data/";
}


function dumphexfile($filepath)
{
    $path = getdumppath();


    $bin = file_get_contents($path.$filepath);
    //var_dump( $bin);
    if ($bin === false)
    {
        echo "File not found";
    }
    else
    {
        hexdump($bin);
    }
}


function processerrorcode($code)
{
    $result = "";
    switch ($code)
    {
        case 0:
            $result =  "Success";
            break;

        case 1:
            $result =  "Unknown Device, just add data to DB";
            break;

        case -2:
            $result =  "Data format incorrect - #S not present";
            break;

        case -3:
            $result =  "Data format incorrect - P/F not present";
            break;

        case -4:
            $result =  "Data format incorrect - , not present";
            break;

        case -5:
            $result =  "Data format incorrect - Length less than expected";
            break;

        case -6:
            $result =  "Data format incorrect - End of Data not present";
            break;

        default:
            $result =  "Unknown Error";
    }

    return $result;


}


function processdumpline($line)
{
    $data = "";
    $result = substr($line, 1, 5);

    if (strcmp($result, "INFO ") == 0)
    {
        $result = substr($line, 37, 20);
        if (strcmp($result, "Collector Response: ") == 0)
        {
            $result = substr($line, 57, 6);
            $pos = strpos($result, "(");

            if ($pos === false) {
            } else {
                $result = substr($result, 0, $pos-1);
                $data = $data . " ".$result." [";
                $data = $data . processerrorcode($result);
                $data = $data . "]<br>";

                $pos = strpos($line, ") (dump ");
                if ($pos === false) {
                } else {
                    $result = substr($line, $pos+8);

                    $pos = strpos($result, ")");
                    if ($pos === false) {
                    } else {
                        $result = substr($result, 0, $pos);
                        //dumphexfile($result);
                        $data = $data . "Binary Data: <a href='dumphexfile.php?fn=".$result."'>".$result."</a>";
                    }
                }



            }


        }
    }

    return $data;
}

function logdump($filepath)
{
    $myfile = fopen($filepath, "r") or die("Unable to open file: $filepath");

    echo "<table class='table table-striped'>";
    echo "<tr>
				<thead>
					<th>#</th>
					<th>Detail</th>
					<th>Log Text</th>
					
				</thead>
			 </tr><tbody>";

    $i = 1;
    while(!feof($myfile))
    {
        $line = fgets($myfile);
        $info = processdumpline($line);
        echo "<tr><td>".$i."</td>";
        echo "<td>".$info."</td><td>";
        echo $line;
        echo "</td></tr>";

        $i++;
    }
    echo "</tbody></table>";
    fclose($myfile);
}


function GetSiteTypeForQuanta($q_site_id, $site_type_list)
{

    if ($site_type_list == null) return -1;
    if ($q_site_id == null) return -1;
    for ($i=0; $i<count($site_type_list); $i++)
    {
        if ($site_type_list[$i][0] == $q_site_id)
        {
            return $site_type_list[$i][2];
        }
    }

    return -1;
}

function GetDeviceRecordFromId($id, $isExport)
{
    $array = array();

    include 'dbinc.php';


    $mysqli = new mysqli("$mysql_hostname", "$mysql_user", "$mysql_password", "$mysql_database");
    if (mysqli_connect_errno())
    {
        printf("Connect failed: %s\n", mysqli_connect_error());
        return null;
    }

    $order = "SELECT 
			ID, timestamp , data_validated , type , site_id , date_time , device_id , status , raw_data
			FROM quanta 
			WHERE id=".$id;


    //echo $order;

    //$order = "SELECT * FROM quanta where id=".$id;
    $result = $mysqli->query($order);
    $data = $result->fetch_row();

    $array[] 	= $data[0];		//0Unique Id
    if (!$isExport) $array[] 	= $data[1];		//1Time Stamp by Server
    if (!$isExport) $array[]	= $data[2];		//2Validated by Server
    $array[] 	= $data[3];		//3Type 0 - Periodic / 1 - Fault
    $array[]	= $data[4];		//4String Site Id
    $array[]	= $data[5];		//5Date Time by Device
    $array[]	= $data[6];		//6Device Id/ Only 1 & 2 known for now
    if (!$isExport)
        $array[]	= $data[7];		//7Status Bits - To Expand
    else
        $array[]	= bin2hex($data[7]);		//7Status Bits in Hex for Export
    $data_raw	= $data[8];		//Raw Data, for device specific information

    //var_dump($array);
    //$array = array_merge($array, DeviceGetStatusBits($data[7]));
    //var_dump($array);


    if ($data[6] == 1)
    {
        $array = array_merge($array, DeviceGetData($data_raw));
    }
    else if ($data[6] == 2)
    {
        $array = array_merge($array, DeviceGetDataType2($data_raw));
    }

    $result->close();
    mysqli_close($mysqli);

    //var_dump($array);

    return $array;

}

