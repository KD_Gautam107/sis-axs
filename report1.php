<?php
session_start();
if (!isset($_SESSION['login_user']))
{
	header("Location: login.php");
}

if($_SERVER["REQUEST_METHOD"] == "POST")
{
	$_SESSION['range_data_type'] = $_POST['range_data_type'];
	

}

	$hours_interval = 24;
	$range_data_type = 0;
	if ($_SESSION['range_data_type'] == 1) {$range_data_type = 1; $hours_interval=24*7;}
	if ($_SESSION['range_data_type'] == 2) {$range_data_type = 2; $hours_interval=24*30;}
	
	


include 'common.php';

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Equipment Data</title>

    <!-- Bootstrap -->
    <!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">

	<!-- Optional theme -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css">

	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
	
	<!-- Latest compiled and minified JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
	
	
	<link class="include" rel="stylesheet" type="text/css" href="charts/jquery.jqplot.min.css" />
	<!--[if lt IE 9]><script language="javascript" type="text/javascript" src="charts/excanvas.js"></script><![endif]-->
	
	<script class="include" type="text/javascript" src="charts/jquery.jqplot.min.js"></script>
	<script class="include" type="text/javascript" src="charts/plugins/jqplot.pieRenderer.min.js"></script>
	<script type="text/javascript" src="charts/plugins/jqplot.highlighter.js"></script>
	
	<style>
		.jqplot-data-label{
			color: #ffffff !important ;
		 }
		 
		.jqplot-highlighter-tooltip{
			background: #ffffff;
		}
	</style>

	

	
	<script>
	
		function ShowDetail(id){
			$("#myModalLabel").html("Equipment Status");
			$.ajax({url: "detail.php?id="+id, success: function(result){
				$("#id_device_detail").html(result);
				$('#myModal').modal('show');
				
			}});
		}
		
		function GenStartStop(i, id){
			$("#myModalLabel").html("Generator Control");
			$.ajax({url: "genstartstop.php?operation="+i+"&id="+id, success: function(result){
				$("#id_device_detail").html(result);
				$('#myModal').modal('show');
			}});
		}
		
		$('#myModal').on('hidden.bs.modal', function (e) {
			$("#id_device_detail").html('');
		})
	
	</script>
	
	<style>
		#led_white {
			width: 28px;
			height: 28px;
			background: url(media/leds.png) 0 0;
		}

		#led_red {
			width: 28px;
			height: 28px;
			background: url(media/leds.png) -30px 0;
		}
		
		#led_green {
			width: 28px;
			height: 28px;
			background: url(media/leds.png) -124px 0;
		}
	</style>
	
  </head>
  <body>
  
	<?php $page_title="Equipment Data"; include 'header.php'; ?>
    
	  
		<div style="margin-left:20px; margin-bottom: 20px;">
			<h4>Data Options</h4>
		
			<form method="post" class="form-inline" role="form">
				<select class="form-control" name="range_data_type">
				  <option value="0" <?php  if ($range_data_type==0) echo "selected";?>>24 Hrs</option>
				  <option value="1" <?php  if ($range_data_type==1) echo "selected";?>>One Week</option>
				  <option value="2" <?php  if ($range_data_type==2) echo "selected";?>>One Month</option>
				</select>
				<button type="submit" class="btn btn-default" value=" Submit ">Apply</button>
			</form>
			
		</div>
	  

			 
<?php 

	include 'dbinc.php';

	$mysqli = new mysqli("$mysql_hostname", "$mysql_user", "$mysql_password", "$mysql_database");
	if (mysqli_connect_errno()) {
		printf("Connect failed: %s\n", mysqli_connect_error());
		exit();
	}
	

	
	$order = "SELECT site_id FROM siteinfo WHERE CustomerId=".$_SESSION['login_cust_id'];

	//echo "order: $order<br>";	
	$result = $mysqli->query($order);

	DisplayEntryType2Header();
	//echo $order;
	//var_dump($result);

	$count = 0;
	$sum = null;
	
	while($data = $result->fetch_row()){
		
		$array = GetSiteStatsFromIdStartInterval($data[0], date('Y-m-d H:i:s'), $hours_interval);
		$sum = AddUp($array, $sum);
		DisplayEntryType2($array);
		
		//var_dump($sum);
		
		$count++;
	}
	
	if ($sum != null)
		DisplayEntryType2Sum($sum);
	
	//echo $count;
	
	DisplayEntryType1HeaderEnd();
	
	$result->close();
	
	mysqli_close($mysqli);

?>



	
	
    <div style="margin-top:20px; margin-left:20px; width:300px;"><strong><center>Power Usage</center></strong></div>
	<div id="pie1" style="margin-top:20px; margin-left:20px; width:300px; height:300px;"></div>
	<script class="code" type="text/javascript">$(document).ready(function(){
		var plot1 = $.jqplot('pie1', [[['Mains Running',<?php echo $sum[2]; ?>],['DG Running',<?php echo $sum[1]; ?>],['Battery Running',<?php echo $sum[3]; ?>]]], {
			gridPadding: {top:0, bottom:38, left:0, right:0},
			seriesDefaults:{
				renderer:$.jqplot.PieRenderer, 
				trendline:{ show:false }, 
				rendererOptions: { padding: 8, showDataLabels: true }
			},
			legend:{
				show:true, 
				placement: 'outside', 
				rendererOptions: {
					numberRows: 3
				}, 
				location:'s',
				marginTop: '15px'
			},
			seriesColors: [ "#990099", "#3366cc", "#dc3912", "#ff9900", "#109618"],
			grid: {
				drawBorder: false, 
				drawGridlines: false,
				background: '#ffffff',
				shadow:false
			},
			highlighter: {
				//tooltipContentEditor: function (str, seriesIndex, pointIndex) {
				//	return str + "<br/> additional data";
				//},

				// other options just for completeness
				show: true,
				showTooltip: true,
				tooltipFade: true,
				sizeAdjust: 10,
				formatString: '%s',
				tooltipLocation: 'n',
				useAxesFormatters: false,
			},
		});
	});</script>


	
	<!-- Modal -->
	<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	  <div class="modal-dialog">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel"></h4>
		  </div>
		  <div class="modal-body">
			<div id="id_device_detail">
				Please wait...
			</div>
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		  </div>
		</div>
	  </div>
	</div>

 

<?php include 'footer.php'; ?>
  

</body>
</html>
