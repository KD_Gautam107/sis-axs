<?php
session_start();
if (!isset($_SESSION['login_user']))
{
	header("Location: login.php");
}

if($_SERVER["REQUEST_METHOD"] == "GET")
{
	$site_id = $_GET['site_id'];
	$_SESSION['range_data_type'] = $_GET['range_data_type'];
	
	$hours_interval = 24;
	$hours_interval_txt = "24 Hours";
	$range_data_type = 0;
	if ($_SESSION['range_data_type'] == 1) {$range_data_type = 1; $hours_interval=24*7; $hours_interval_txt = "One Week";}
	if ($_SESSION['range_data_type'] == 2) {$range_data_type = 2; $hours_interval=24*30; $hours_interval_txt = "One Month";}
}
	
	include 'common.php';

	$sum = null;	
	$array = GetSiteStatsFromIdStartInterval($site_id, date('Y-m-d H:i:s'), $hours_interval);
	$sum = AddUp($array, $sum);		
		

?>
	
	<div class="btn-toolbar" role="toolbar" aria-label="time-frame-buttons">
	  <a role="button" class="btn <?php  if ($range_data_type==0) echo "btn-primary"; else echo "btn-default"; ?>" href='javascript:AsyncLoad("node-power-supply.php?site_id=<?php echo $site_id; ?>&range_data_type=0", "#id_power_supply")'>24 Hours</a>
	  <a role="button" class="btn <?php  if ($range_data_type==1) echo "btn-primary"; else echo "btn-default"; ?>" href='javascript:AsyncLoad("node-power-supply.php?site_id=<?php echo $site_id; ?>&range_data_type=1", "#id_power_supply")'>One Week</a>
	  <a role="button" class="btn <?php  if ($range_data_type==2) echo "btn-primary"; else echo "btn-default"; ?>" href='javascript:AsyncLoad("node-power-supply.php?site_id=<?php echo $site_id; ?>&range_data_type=2", "#id_power_supply")'>One Month</a>
	</div>
	
    <div style="margin-top:20px; margin-left:20px; width:300px;"><strong><center>Power in last <?php echo $hours_interval_txt; ?></center></strong></div>
	<div id="pie1" style="margin-top:20px; margin-left:20px; width:300px; height:300px;"></div>
	<script class="code" type="text/javascript">$(document).ready(function(){
		var plot1 = $.jqplot('pie1', [[['Mains Running',<?php echo $sum[2]; ?>],['DG Running',<?php echo $sum[1]; ?>],['Battery Running',<?php echo $sum[3]; ?>]]], {
			gridPadding: {top:0, bottom:38, left:0, right:0},
			seriesDefaults:{
				renderer:$.jqplot.PieRenderer, 
				trendline:{ show:false }, 
				rendererOptions: { padding: 8, showDataLabels: true }
			},
			legend:{
				show:true, 
				placement: 'outside', 
				rendererOptions: {
					numberRows: 3
				}, 
				location:'e',
				marginTop: '15px'
			},
			seriesColors: [ "#990099", "#3366cc", "#dc3912", "#ff9900", "#109618"],
			grid: {
				drawBorder: false, 
				drawGridlines: false,
				background: '#ffffff',
				shadow:false
			},
			highlighter: {
				//tooltipContentEditor: function (str, seriesIndex, pointIndex) {
				//	return str + "<br/> additional data";
				//},

				// other options just for completeness
				show: true,
				showTooltip: true,
				tooltipFade: true,
				sizeAdjust: 10,
				formatString: '%s',
				tooltipLocation: 'n',
				useAxesFormatters: false,
			},
		});
	});</script>


