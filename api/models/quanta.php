<?php

include_once $_SERVER['DOCUMENT_ROOT'] . 'api/common/paths.php';
include_once $pathDbinc;

function selectQuanta($siteNo)
{
    $mysqli = dbContext();
    $sql = "SELECT      ID,
                        device_id,
                        site_id,
                        data_validated,
                        type,
                        timestamp,
                        date_time,
                        status,
                        raw_data,
                        (SELECT versionCode FROM version ORDER BY versionCode DESC limit 1) as versionCode

            FROM        quanta
            WHERE       site_id = '$siteNo'
            ORDER BY    timestamp DESC
            LIMIT       1";

    //echo $sql;

    $result = $mysqli->query($sql);
    $dataTable = array(); // c#: var dt = new DataTable();

    if ($result) {
        while ($row = $result->fetch_row()) {
            $dataRow = array(
                'quantaId' => $row[0],
                'deviceId' => $row[1],
                'siteNo' => $row[2],
                'dataValidated' => $row[3],
                'type' => $row[4],
                'timestamp' => (string) strtotime($row[5]),
                'datetime' => (string) strtotime($row[6]),
                'status' => base64_encode($row[7]),
                'quantum' => base64_encode($row[8]),
                'versionCode' => $row[9],
            );

            array_push($dataTable, $dataRow); // C#: dt.Rows.Add(dr);
        }
    }

    //var_dump($result);
    $mysqli->close();
    //print_r($dataTable[0]);
    return $dataTable;
}

function getQuanta()
{
    $mysqli = dbContext();
    $sql = "SELECT      ID,
                        site_id,
                        timestamp,
                        date_time

            FROM        quanta
            ORDER BY    ID DESC
            LIMIT       25";

    //echo $sql;

    $result = $mysqli->query($sql);
    $dataTable = array(); // c#: var dt = new DataTable();

    if ($result) {
        while ($row = $result->fetch_row()) {
            $dataRow = array(
                'quantaId' => $row[0],
                'siteNo' => $row[1],
                'timestamp' => (string) strtotime($row[2]),
                'datetime' => (string) strtotime($row[3]),
            );

            array_push($dataTable, $dataRow); // C#: dt.Rows.Add(dr);
        }
    }

    //var_dump($result);
    $mysqli->close();
    //print_r($dataTable[0]);
    return $dataTable;
}

function selectQuantaById($quantaId)
{
    $mysqli = dbContext();
    $sql = "SELECT      ID,
                        device_id,
                        site_id,
                        data_validated,
                        type,
                        timestamp,
                        date_time,
                        status,
                        raw_data

            FROM        quanta
            WHERE       ID = $quantaId
            ORDER BY    timestamp DESC
            LIMIT       1";

    //echo $sql;

    $result = $mysqli->query($sql);
    $dataTable = array(); // c#: var dt = new DataTable();

    if ($result) {
        while ($row = $result->fetch_row()) {
            $dataRow = array(
                'quantaId' => $row[0],
                'deviceId' => $row[1],
                'siteNo' => $row[2],
                'dataValidated' => $row[3],
                'type' => $row[4],
                'timestamp' => (string) strtotime($row[5]),
                'datetime' => (string) strtotime($row[6]),
                'status' => base64_encode($row[7]),
                'quantum' => base64_encode($row[8]),
            );

            array_push($dataTable, $dataRow); // C#: dt.Rows.Add(dr);
        }
    }

    //var_dump($result);
    $mysqli->close();
    //print_r($dataTable[0]);
    return $dataTable;
}

function getSiteDateRange($site_id)
{
    //include 'dbinc.php';

    $array = array();

    $mysqli = dbContext();
    // if (mysqli_connect_errno()) {
    //     printf("Connect failed: %s\n", mysqli_connect_error());
    //     exit();
    // }
    $order = "SELECT ID, date_time " .
        "FROM quanta WHERE site_id='$site_id' " .
        "ORDER BY date_time ASC LIMIT 1";

    //aecho "order: $order<br>";
    $result = $mysqli->query($order);
    //var_dump($result);

    $data = $result->fetch_row();
    $array[] = $data[1];

    $result->close();

    $order = "SELECT ID, date_time " .
        "FROM quanta WHERE site_id='$site_id' " .
        "ORDER BY date_time DESC LIMIT 1";

    //echo "order: $order<br>";
    $result = $mysqli->query($order);

    $data = $result->fetch_row();
    $array[] = $data[1];

    $result->close();

    mysqli_close($mysqli);

    //var_dump($site_id);
    //var_dump($array);

    return $array;
}

function getPowerSupply($siteNo, $endDateTime, $startDateTime)
{
    $array = GetSiteStatsFromIdStartEnd($siteNo, $endDateTime, $startDateTime);
    $sum = AddUp($array, null);

    $adj_time_total = $sum[2] + $sum[1] + $sum[3] + $sum[4];
    $adj_time_diff = $array[10] - $adj_time_total;

    $adj_four_percent = $array[10] * 0.04;

    $adj_offset = 0;
    if (($adj_time_diff > 0) && ($adj_time_diff < $adj_four_percent)) {
        $adj_offset = $adj_time_diff;
    }

    return array(
        'startDate' => (string) strtotime($array[8]->format('Y-m-d H:i:s')),
        'endDate' => (string) strtotime($array[7]->format('Y-m-d H:i:s')),
        'mains' => (string) round($sum[2], 1),
        'dg' => (string) round($sum[1] + $sum[4], 1),
        'battery' => (string) round($sum[3] + $adj_offset, 1),
        'dgStopFailTime' => (string) round($array[15], 1),
    );
}

function GetSiteStatsFromIdStartEnd($siteNo, $start_date_time, $end_date_time)
{
    //include 'dbinc.php';

    if ($start_date_time == null) {
        return null;
    }
    if ($end_date_time == null) {
        return null;
    }

    $sql_sdt = "\"" . $start_date_time . "\"";
    $sql_edt = "\"" . $end_date_time . "\"";

    $mysqli = dbContext();
    // $mysqli = new mysqli("$mysql_hostname", "$mysql_user", "$mysql_password", "$mysql_database");
    // if (mysqli_connect_errno()) {
    //     printf("Connect failed: %s\n", mysqli_connect_error());
    //     return null;
    // }

    //Get first Id
    $sql = "SELECT
					id,
					date_time
				FROM
				  quanta
				WHERE
					site_id=\"$siteNo\"
				ORDER BY
				  (ABS(TIMESTAMPDIFF(SECOND, date_time, ($sql_edt)))) ASC
				LIMIT 1";

    //echo "$sql <br>";

    $result = $mysqli->query($sql);
    //var_dump($result);
    if ($result !== false) {
        $data = $result->fetch_row();
        $id1 = $data[0]; //First Id
        //echo "<br>ID1: $id1";

        $result->close();

        //Get 2nd Id
        $sql = "SELECT
						id
					FROM
					  quanta
					WHERE
						site_id=\"$siteNo\"
					ORDER BY
					  (ABS(TIMESTAMPDIFF(SECOND, date_time, ($sql_sdt)))) ASC
					LIMIT 2";

        //echo "$sql <br>";

        $result = $mysqli->query($sql);
        if ($result !== false) {
            $data1 = $result->fetch_row();
            $id2 = $data1[0]; //Second Id
            //echo "<br>ID2: $id2";

            if ($id1 == $id2) {
                //echo "<br>Same Id $id1 and $id2, We have results more than given time interval apart<br>";
                //Find the next record

                $data1 = $result->fetch_row();
                if ($data1 !== false) {
                    $id2 = $data1[0]; //Second Id
                } else {
                    //We have not enough records
                    $result->close();
                    mysqli_close($mysqli);
                    echo "Not Enough Records";
                    return null;
                }
            }

            $result->close();

            //echo "<br>Comparison between $id1 and $id2<br>";
            $array = GetComparisonFromIds($id1, $id2);

            mysqli_close($mysqli);
            return $array;
        }
    }

    mysqli_close($mysqli);
    echo "NULL";

    return null;
}

function GetComparisonFromIds($id1, $id2)
{
    $array = array();

    $arr1 = GetDeviceRecordFromId($id1); //New
    $arr2 = GetDeviceRecordFromId($id2); //Old
    //var_dump($arr1);
    //var_dump($arr2);

    $array[] = $arr1[4]; //0 Site Id
    $array[] = $arr1[39]; //1 Circle
    $array[] = $arr1[38]; //2 Culster
    $array[] = $arr1[37]; //3 Make
    $array[] = $arr1[41]; //4 AM NAME
    $array[] = $arr1[42]; //5 Mob no.
    $array[] = $arr1[43]; //6 DG Mobile

    //What Diffs to Retrieve
    //$array[] = new DateTime(arr2[5]);    //7 From Time
    $array[] = date_create_from_format('Y-m-d H:i:s', $arr2[5]);
    //$array[] = new DateTime(arr1[5]);    //8 To Time
    $array[] = date_create_from_format('Y-m-d H:i:s', $arr1[5]);

    //echo "F: $arr2[5]". " > ".$array[0]->format('Y-m-d H:i:s')."<br>";
    //echo "T: $arr1[5]". " > ".$array[1]->format('Y-m-d H:i:s')."<br>";

    $array[] = $array[7]->diff($array[8]); //9 Interval
    //echo "Interval: ".$array[2]->format("%d %h %i")."<br>";
    //var_dump($array[2]);

    $delta_h = ($array[8]->getTimestamp() - $array[7]->getTimestamp()) / 3600;
    $array[] = $delta_h; //10 Hours
    //echo "Hours: $array[3]<br>";

    $array[] = $arr1[26] - $arr2[26]; //11 System On DG
    $array[] = $arr1[27] - $arr2[27]; //12 System On Mains
    $array[] = $arr1[28] - $arr2[28]; //13 System On Batt

    //echo "DG: ".$arr1[26]." - ".$arr2[26]." = ".$array[4]."<br>";
    //echo "Mains: ".$arr1[27]." - ".$arr2[27]." = ".$array[5]."<br>";
    //echo "Batt: ".$arr1[28]." - ".$arr2[28]." = ".$array[6]."<br>";

    $array[] = $arr1[43]; //14 Site Name

    if ($arr1[5] < $arr2[5]) {
        $array[] = CalculateGeneratorFailedToStopTime($arr1[4], $arr1[5], $arr2[5]);
    } else {
        $array[] = CalculateGeneratorFailedToStopTime($arr1[4], $arr2[5], $arr1[5]);
    }

    if (($arr1[6] == 1) && ($arr1[2] == 2) && ($arr2[6] == 1) && ($arr2[2] == 2)) { //Double DG
        $array[] = 1; //16 Double DG Case
        $array[] = $arr1[38][6] - $arr2[38][6]; //17 System On DG2
    } else {
        $array[] = 0; //16 Double DG Case
        $array[] = 0; //17 System On DG2 - 0
    }

    return $array;
}

function GetDeviceRecordFromId($id)
{
    $array = array();

    //include 'dbinc.php';

    $mysqli = dbContext();
    // $mysqli = new mysqli("$mysql_hostname", "$mysql_user", "$mysql_password", "$mysql_database");
    // if (mysqli_connect_errno()) {
    //     printf("Connect failed: %s\n", mysqli_connect_error());
    //     return null;
    // }

    //$sql = 'SELECT a.tutorial_id, a.tutorial_author, b.tutorial_count
    //    FROM tutorials_tbl a, tcount_tbl b
    //    WHERE a.tutorial_author = b.tutorial_author';

    $order = "SELECT
			a.ID, a.timestamp , a.data_validated , a.type , a.site_id , a.date_time , a.device_id , a.status , a.raw_data,
			b.ClusterId, b.DeviceName, b.DeviceMake, b.Circle, b.SiteName, b.District, b.mobile, b.Longitude, b.Latitude, b.SiteIndoorType,  b.DG_KVA, b.SAM_Name, b.SAM_Number, b.AM_Name, b.AM_Number, b.Tech_Name,  b.Tech_Number, b.Comments,
			c.name,
			b.date_of_installation, b.DG_make, b.EB_capacity, b.EB_phase, b.battery_AH, b.battery_qty, b.battery_make,
			b.DG2_KVA, b.DG2_MAKE, b.site_type

		FROM quanta a, siteinfo b, clusters c
		WHERE (a.site_id = b.site_id) AND (b.ClusterId = c.Id) AND a.id=" . $id;

    //echo $order;

    //$order = "SELECT * FROM quanta where id=".$id;
    $result = $mysqli->query($order);
    $data = $result->fetch_row();

    //var_dump($data);

    $array[] = $data[0]; //0Unique Id
    $array[] = $data[1]; //1Time Stamp by Server
    $array[] = $data[2]; //2Validated by Server
    $array[] = $data[3]; //3Type 0 - Periodic / 1 - Fault
    $array[] = $data[4]; //4String Site Id
    $array[] = $data[5]; //5Date Time by Device
    $array[] = $data[6]; //6Device Id/ Only 1, 2 & 32 known for now
    $array[] = $data[7]; //7Status Bits - To Expand
    $data_raw = $data[8]; //Raw Data, for device specific information

    //var_dump($array);
    //$array = array_merge($array, DeviceGetStatusBits($data[7]));
    //var_dump($array);

    $array = array_merge($array, ConvertFromRawData($data_raw));
    // if ($data[6] == 1) {
    //     //$data_validated
    //         //    0 - Not Validated
    //         //    1 - Single DG
    //         //    2 - Double DG
    //         if ($data[2] == 2) {    //Double DG
    //             $array = array_merge($array, DeviceGetDataDouble($data_raw));
    //         } else {
    //             $array = array_merge($array, DeviceGetData($data_raw));
    //         }
    // } elseif ($data[6] == 2) {    //2 = Solar
    //     if ($data[2] == 1) {
    //         //if($data[37] == 3)
    //         //{
    //         $array = array_merge($array, DeviceGetDataDevice3($data_raw));
    //         for ($i = 26; $i <39;$i++) {
    //             $array[$i] = "";
    //         }
    //         //}
    //     } else {
    //         $array = array_merge($array, DeviceGetDataDevice2($data_raw));
    //     }
    // } elseif ($data[6] == 16) {    //Phase 2
    //         $array = array_merge($array, DeviceGetData($data_raw));//DeviceGetDataDevicePhase2($data_raw));
    // } elseif ($data[6] == 32) {
    //     $array = array_merge($array, DeviceGetDataDoubleBattery($data_raw));
    //     for ($i = 21; $i <39;$i++) {
    //         $array[$i] = "";
    //     }
    // }

    $array[] = $data[9]; //39 ClusterId
    $array[] = $data[10]; //40 DeviceName
    $array[] = $data[11]; //41 DeviceMake
    $array[] = $data[12]; //42 Circle
    $array[] = $data[13]; //43 SiteName
    $array[] = $data[14]; //44 District
    $array[] = $data[15]; //45 Site Mobile
    $array[] = $data[16]; //46 Longitude

    $array[] = $data[17]; //47 Latitude
    $array[] = $data[18]; //48 SiteIndoorType
    $array[] = $data[19]; //49 DG_KVA
    $array[] = $data[20]; //50 SAM_Name
    $array[] = $data[21]; //51 SAM_Number
    $array[] = $data[22]; //52 AM_Name
    $array[] = $data[23]; //53 AM_Number
    $array[] = $data[24]; //54 Tech_Name
    $array[] = $data[25]; //55 Tech_Number

    $array[] = $data[28]; //56 date_of_installation
    $array[] = $data[29]; //57 DG_make
    $array[] = $data[30]; //58 EB_capacity
    $array[] = $data[31]; //59 EB_phase
    $array[] = $data[32]; //60 battery_AH
    $array[] = $data[33]; //61 battery_qty
    $array[] = $data[34]; //62 battery_make

    $array[] = $data[35]; //63 DG2_KVA
    $array[] = $data[36]; //64 DG2_MAKE
    $array[] = $data[37]; //65 site_type

    $result->close();
    mysqli_close($mysqli);

    //var_dump($array);

    return $array;
}

function AddUp($array, $sum)
{
    $array1 = array();

    if ($sum == null) {
        $array1[] = abs($array[10]); //0 => 10 ...
        $array1[] = abs($array[11]); //1 => 11 System On DG
        $array1[] = abs($array[12]); //2 => 12 System On Mains
        $array1[] = abs($array[13]); //3 => 13 System On Batt
        $array1[] = abs($array[17]);

        //var_dump($array);
    } else {
        $array1[] = abs($array[10]) + $sum[0];
        $array1[] = abs($array[11]) + $sum[1];
        $array1[] = abs($array[12]) + $sum[2];
        $array1[] = abs($array[13]) + $sum[3];
        $array1[] = abs($array[17]) + $sum[4];

        //var_dump($array);
        //var_dump($sum);
    }

    return $array1;
}

function ConvertFromRawData($data_raw)
{
    $array = array();
    $sub_data = substr($data_raw, 41, 36);
    $stats = str_split($sub_data, 2);
    //var_dump($stats);

    $sub_data = unpack("n", $stats[0]);
    $sub_data = $sub_data[1] / 10;
    //echo "<tr><td>Room Temperature</td><td>";
    $array[] = $sub_data; //8

    $sub_data = unpack("n", $stats[1]);
    $sub_data = $sub_data[1] / 10;
    //echo "<tr><td>Fuel Level</td><td>";
    $array[] = $sub_data; //9

    $sub_data = unpack("n", $stats[2]);
    $sub_data = $sub_data[1] / 10;
    //echo "<tr><td>Fuel Data</td><td>";
    $array[] = $sub_data; //10

    $sub_data = unpack("n", $stats[3]);
    $sub_data = $sub_data[1] / 10;
    //echo "<tr><td>Site Batt. bank Voltage</td><td>";
    $array[] = $sub_data; //11

    $sub_data = unpack("n", $stats[4]);
    $sub_data = $sub_data[1] / 10;
    //echo "<tr><td>R-Phase current</td><td>";
    $array[] = $sub_data; //12

    $sub_data = unpack("n", $stats[5]);
    $sub_data = $sub_data[1] / 10;
    //echo "<tr><td>Y-Phase current</td><td>";
    $array[] = $sub_data; //13

    $sub_data = unpack("n", $stats[6]);
    $sub_data = $sub_data[1] / 10;
    //echo "<tr><td>B-Phase current</td><td>";
    $array[] = $sub_data; //14

    $sub_data = unpack("n", $stats[7]);
    $sub_data = $sub_data[1] / 10;
    //echo "<tr><td>Mains Frequency</td><td>";
    $array[] = $sub_data; //15

    $sub_data = unpack("n", $stats[8]);
    $sub_data = $sub_data[1] / 10;
    //echo "<tr><td>DG frequency</td><td>";
    $array[] = $sub_data; //16

    $sub_data = unpack("n", $stats[9]);
    $sub_data = $sub_data[1] / 10;
    //echo "<tr><td>DG-R phase Voltage</td><td>";
    $array[] = $sub_data; //17

    $sub_data = unpack("n", $stats[10]);
    $sub_data = $sub_data[1] / 10;
    //echo "<tr><td>DG-Y phase Voltage</td><td>";
    $array[] = $sub_data; //18

    $sub_data = unpack("n", $stats[11]);
    $sub_data = $sub_data[1] / 10;
    //echo "<tr><td>DG-B phase Voltage</td><td>";
    $array[] = $sub_data; //19

    $sub_data = unpack("n", $stats[12]);
    $sub_data = $sub_data[1] / 10;
    //echo "<tr><td>LCU1 Output Voltage</td><td>";
    $array[] = $sub_data; //20

    $sub_data = unpack("n", $stats[13]);
    $sub_data = $sub_data[1] / 10;
    //echo "<tr><td>LCU2 Output Voltage</td><td>";
    $array[] = $sub_data; //21

    $sub_data = unpack("n", $stats[14]);
    $sub_data = $sub_data[1] / 10;
    //echo "<tr><td>LCU3 Output Voltage</td><td>";
    $array[] = $sub_data; //22

    $sub_data = unpack("n", $stats[15]);
    $sub_data = $sub_data[1] / 10;
    //echo "<tr><td>Input Mains Voltage - R Phase</td><td>";
    $array[] = $sub_data; //23

    $sub_data = unpack("n", $stats[16]);
    $sub_data = $sub_data[1] / 10;
    //echo "<tr><td>Input Mains Voltage - Y Phase</td><td>";
    $array[] = $sub_data; //24

    $sub_data = unpack("n", $stats[17]);
    $sub_data = $sub_data[1] / 10;
    //echo "<tr><td>Input Mains Voltage - B Phase</td><td>";
    $array[] = $sub_data; //25

    /////

    $sub_data = substr($data_raw, 77, 48);
    $stats = str_split($sub_data, 8);
    $stats = substr_replace($stats, ".", 7, 0);

    for ($i = 0; $i < 6; $i++) {
        $stats[$i] = ltrim($stats[$i], '0');
        if ($stats[$i][0] == '.') {
            $stats[$i] = "0" . $stats[$i];
        }
    }

    //echo "<tr><td>DG Running Hours</td><td>";
    $array[] = $stats[0]; //26

    //echo "<tr><td>Mains RUN HOURS</td><td>";
    $array[] = $stats[1]; //27

    //echo "<tr><td>Batt RUN HOURS</td><td>";
    $array[] = $stats[2]; //28

    //echo "<tr><td>O/P Mains Energy</td><td>";
    $array[] = $stats[3]; //29

    //echo "<tr><td>DG Energy</td><td>";
    $array[] = $stats[4]; //30

    //echo "<tr><td>I/P Mains Energy</td><td>";
    $array[] = $stats[5]; //31

    $sub_data = substr($data_raw, 125, 2);

    $sub_data = unpack("n", $sub_data);
    $sub_data = $sub_data[1] / 10;
    //echo "<tr><td>DG Battery Voltage</td><td>";
    $array[] = $sub_data; //32

    //18*2    = 36
    //6 *8    = 48
    //1 *2    = 02
    //----------
    //        86
    //----------

    //Updated Data - Updated Protocol

    //18*2    = 36
    //6 *8    = 48
    //1 *2    = 02
    //2 *2    = 04    *
    //2 *1    = 02    *
    //----------
    //        92
    //----------

    //REV2 Total 134 Bytes
    if (strlen($data_raw) >= 134) {
        $sub_data = substr($data_raw, 127, 4);
        $stats = str_split($sub_data, 2);

        $sub_data = unpack("n", $stats[0]);
        $sub_data = $sub_data[1] / 10;
        //echo "<tr><td>Battery Charging current</td><td>";
        $array[] = $sub_data; //33

        $sub_data = unpack("n", $stats[1]);
        $sub_data = $sub_data[1] / 10;
        //echo "<tr><td>Battery Discharging current</td><td>";
        $array[] = $sub_data; //34

        $sub_data = substr($data_raw, 131, 2);
        $stats = str_split($sub_data, 1);

        $sub_data = unpack("c", $stats[0]);
        $sub_data = (hexdec(bin2hex($stats[0]))); //$sub_data[1]/1;
        //echo "<tr><td>Battery status</td><td>";
        $array[] = $sub_data; //35

        $sub_data = unpack("c", $stats[1]);
        $sub_data = (hexdec(bin2hex($stats[1])) / 10); //$sub_data[1]/10;
        //echo "<tr><td>Battery back up time</td><td>";
        $array[] = $sub_data; //36

        //REV3    Total 150 Bytes
        if (strlen($data_raw) >= 150) {
            $sub_data = substr($data_raw, 133, 16);
            $stats = str_split($sub_data, 8);
            $stats = substr_replace($stats, ".", 7, 0);

            for ($i = 0; $i < 2; $i++) {
                $stats[$i] = ltrim($stats[$i], '0');
                if ($stats[$i][0] == '.') {
                    $stats[$i] = "0" . $stats[$i];
                }
            }

            //echo "<tr><td>Battery Charging Energy (Kwh)</td><td>";
            $array[] = $stats[0]; //37

            //echo "<tr><td>Battery Discharging Energy (Kwh)</td><td>";
            $array[] = $stats[1]; //38
        } else {
            $array[] = 0; //37
            $array[] = 0; //38
        }
    } else {
        $array[] = 0; //33
        $array[] = 0; //34
        $array[] = 0; //35
        $array[] = 0; //36
        $array[] = 0; //37
        $array[] = 0; //38
    }

    return $array;
}

function CalculateGeneratorFailedToStopTime($site_id, $dateStart, $dateEnd)
{
    //include 'dbinc.php';
    $array = array();
    $mysqli = dbContext();
    // $mysqli = new mysqli("$mysql_hostname", "$mysql_user", "$mysql_password", "$mysql_database");
    // if (mysqli_connect_errno()) {
    //     printf("Connect failed: %s\n", mysqli_connect_error());
    //     exit();
    // }

    $sqlquery = "SELECT date_time, MID(HEX(`status`),7,1) & 8 " .
        "FROM quanta WHERE `site_id`='$site_id' AND `date_time` >= '$dateStart' AND `date_time` <= '$dateEnd' " .
        "ORDER BY date_time ASC ";

    //echo ": $sqlquery<br>";
    $result = $mysqli->query($sqlquery);
    //var_dump($result);

    $intervals = null;

    if ($result !== false) {
        $genError = false;
        $alarmFlag = false;

        while ($data = $result->fetch_row()) {
            //var_dump($data);

            //If first data is in error then assume ??????
            //if (firstTime && !genError && ($data[1] == '8'))
            //    genError = true;

            $alarmFlag = ($data[1] == '8');
            if ($genError) {
                if (!$alarmFlag) {
                    //Yes Alarm --> No Alarm
                    $timeE = $data[0];
                    $genError = false;

                    $tmpInterval = date_create_from_format('Y-m-d H:i:s', $timeE)->getTimestamp() - date_create_from_format('Y-m-d H:i:s', $timeS)->getTimestamp();

                    if ($intervals == null) {
                        $intervals = $tmpInterval;
                    } else {
                        $intervals += $tmpInterval;
                    }
                }
            } else {
                if ($alarmFlag) {
                    //No Alarm --> Yes Alarm
                    $timeS = $data[0];
                    $genError = true;
                }
            }

            $lastTime = $data[0];
            //var_dump($intervals);
        }

        //Error flag true even at end. Diff it from the last time.
        if ($genError) {
            $timeE = $lastTime;
            $genError = false;

            $tmpInterval = date_create_from_format('Y-m-d H:i:s', $timeE)->getTimestamp() - date_create_from_format('Y-m-d H:i:s', $timeS)->getTimestamp();

            if ($intervals == null) {
                $intervals = $tmpInterval;
            } else {
                $intervals += $tmpInterval;
            }
        }

        $result->close();
    }

    mysqli_close($mysqli);

    if ($intervals == null) {
        $intervals = 0;
    } else {
        $intervals = $intervals / 3600;
    }

    return $intervals;
}
