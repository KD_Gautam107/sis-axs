<?php

include_once $_SERVER['DOCUMENT_ROOT'] . 'api/common/paths.php';
include_once $pathDbinc;

function validateUser($username, $password, $token)
{
    $mysqli = dbContext();
    $sql = "SELECT          authentication.id,
                            authentication.Customer,
                            username,
                            customers.Name,
                            type,
                            typeref
            FROM            authentication
            JOIN            customers ON authentication.Customer = customers.Id
            WHERE           username = '$username'
            AND             passcode = '$password'";

    //echo $sql;

    $result = $mysqli->query($sql);
    //var_dump($result);

    $dataTable = array();       // c#: var dt = new DataTable();

    if ($result) {
        while ($row = $result->fetch_row()) {
            # type != 0 user should not an admin or super user.
            if ($row[4] != 0) {
                $dataRow = array(
                    "userId" => $row[0],
                    "customerId" => $row[1],
                    "username" => $row[2],
                    "customerName" => $row[3],
                    "type" => $row[4],
                    "typeref" => $row[5],
                );
            }
            
            //$dataRow['tokenFlag'] = false;
            $tokenFlag = hasTokenforUser($row[0]);
            if ($token == null) {
                if (!$tokenFlag) {
                    $dataRow['tokenFlag'] = false;
                }
            }
            else
            {
                if (!insertUpdateTokens($row[0], $token)) 
                    $dataRow['tokenFlag'] = false;
                else
                    $dataRow['tokenFlag'] = true;
            }
           // print_r($dataRow);
            array_push($dataTable, $dataRow);   // C#: dt.Rows.Add(dr);
        }
    }

    //var_dump($result);
    $mysqli->close();
    //print_r($dataTable[0]);
    return $dataTable;
}

function insertUpdateTokens($userId, $token)
{
    //echo " $userId ---- $token ";
    $mysqli = dbContext();
    //Check if token already exists.
    
    //var_dump($result);
    if (!hasToken($userId, $token)) {
        //echo "RESULT WAS TRUE";
        //query to insert record
        if($token == null || $token == 'null')
        return false;
        $sql = "INSERT INTO         `pushNotifications`
                     SET           `userId` =  $userId,
                                   `token` = '$token'";
           
        //echo $sql;
        $result = $mysqli->query($sql);
        if (!$result) {
            return false;
        }
    }
       
    //$result->close();
    $mysqli->close();
    return true;
}

function hasTokenforUser($userId)
{
    $mysqli = dbContext();
    //Check if token already exists.
    $sql = "  SELECT        token
                  FROM          pushNotifications
                  WHERE         userId = $userId";

    //echo $sql;
    // prepare query statement

        
    $result = $mysqli->query($sql);
    if (!$result) {
        return false;
    }
    
    $mysqli->close();
    return mysqli_num_rows($result) > 0;
}

function hasToken($userId, $token)
{
    $mysqli = dbContext();
    //Check if token already exists.
    $sql = "  SELECT        token
                  FROM          pushNotifications
                  WHERE         token = '$token'
                  AND           userId = $userId";

    //echo $sql;
    // prepare query statement
        
    $result = $mysqli->query($sql);
    if (!$result) {
        return false;
    }

    $mysqli->close();
    return mysqli_num_rows($result) > 0;
}

function selectUsers($username, $password)
{
    $mysqli = new mysqli("$mysql_hostname", "$mysql_user", "$mysql_password", "$mysql_database");
    if (mysqli_connect_errno()) {
        printf("Connect failed: %s\n", mysqli_connect_error());
        exit();
    }

    $sql = "SELECT          authentication.id,
                            authentication.Customer,
                            username,
                            customers.Name,
                            type,
                            typeref
            FROM            authentication
            JOIN            customers ON authentication.Customer = customers.Id
            WHERE           username = '$username'
            AND             passcode = '$password'";

    //echo $sql;

    $result = $mysqli->query($sql);
    $dataTable = array();       // c#: var dt = new DataTable();

    if ($result) {
        while ($row = $result->fetch_row()) {
            # type != 0 user should not an admin or super user.
            if ($row[4] != 0) {
                $dataRow = array(
                    "userId" => $row[0],
                    "customerId" => $row[1],
                    "username" => $row[2],
                    "customerName" => $row[3],
                    "type" => $row[4],
                    "typeref" => $row[5]
                );
                array_push($dataTable, $dataRow);   // C#: dt.Rows.Add(dr);
            }
        }
    }
    //var_dump($result);
    $mysqli->close();
    //print_r($dataTable[0]);
    return $dataTable;
}
