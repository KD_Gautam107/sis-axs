<?php

include_once $_SERVER['DOCUMENT_ROOT'] . 'api/common/paths.php';
include_once $pathDbinc;
include_once $pathGeneralMethods;


function selectSites($login_type, $customerId, $typeref)
{
    $mysqli = dbContext();
    $sql = null;
    if ($login_type == 1) {	//Head, show all clusters and sites for the customer
        $sql = "SELECT      siteinfo.Id,
                            siteinfo.ClusterId,
                            siteinfo.site_id,
                            siteinfo.SiteName,
                            clusters.name,
                            siteinfo.SiteIndoorType,
                            siteinfo. AM_Name,
                            siteinfo.AM_Number,
                            siteinfo.Tech_Name,
                            siteinfo.Tech_Number

			    FROM        siteinfo
				INNER JOIN  clusters ON siteinfo.ClusterId = clusters.id
			    WHERE       ((siteinfo.CustomerId = $customerId) AND (siteinfo.site_enabled = 1))
			    ORDER BY    ClusterId";
    } elseif ($login_type == 2) {	//Cluster Head, show only the cluster of the head
        $clusters = $typeref[0];	//Only a single cluster
        $sql = "SELECT      siteinfo.Id,
                            siteinfo.ClusterId,
                            siteinfo.site_id,
                            siteinfo.SiteName,
                            clusters.name,
                            siteinfo.SiteIndoorType

                FROM        siteinfo
                INNER JOIN  clusters ON siteinfo.ClusterId = clusters.id
			    WHERE       ((siteinfo.CustomerId = $customerId) 
                AND         (siteinfo.ClusterId = $clusters) 
                AND         (siteinfo.site_enabled = 1))
			    ORDER BY    ClusterId";
    } elseif ($login_type == 4) {	//Zone Head, show all the clusters under the zone
        $clusters = join(',', $typeref);
        //echo "$clustersid <br>";
        $sql = "SELECT      siteinfo.Id,
                            siteinfo.ClusterId,
                            siteinfo.site_id,
                            siteinfo.SiteName,
                            clusters.name,
                            siteinfo.SiteIndoorType

                FROM        siteinfo
                INNER JOIN  clusters ON siteinfo.ClusterId = clusters.id
			    WHERE       ((siteinfo.CustomerId = $customerId) 
                AND         (siteinfo.ClusterId IN ($clusters)) 
                AND         (siteinfo.site_enabled = 1))
			    ORDER BY    ClusterId";
    }
    
    //echo $sql;
    $dataTable = array();
    if (!isNullOrWhiteSpace($sql)) {
        $result = $mysqli->query($sql);
        if ($result) {
            while ($row = $result->fetch_row()) {
                $dataRow = array(
                        /*  siteinfo. AM_Name,
                        siteinfo.AM_Number,
                        siteinfo.Tech_Name,
                        siteinfo.Tech_Number,*/
                        "siteId" => $row[0],
                        "clusterId" => $row[1],
                        "siteNo" => $row[2],
                        "siteName" => $row[3],
                        "clusterName" => $row[4],
                        "isOutdoor" => ($row[5] == 1),
                        "assetMangerName" => $row[6],
                        "assetMangerNo" => $row[7],
                        "technicianName" => $row[8],
                        "technicianNo" => $row[9],
                    );
                array_push($dataTable, $dataRow);
            }
        }
    }

    $mysqli->close();
    //print_r($dataTable);
    return $dataTable;
}