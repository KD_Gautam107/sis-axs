
<?php

function responseHandler($responseStatus, $action, $array)
{
    $responseDescription = "none";
    switch ($responseStatus) {
        case 0:
            $responseDescription = "success";
            break;
        case 1:
            $responseDescription = "credentials do not match";
            break;
        case 2:
            $responseDescription = "no data";
            break;
        case 3:
            $responseDescription = "invalid parameter";
            break;
            case 4:
            $responseDescription = "database error";
            break;

        default:
            break;
    }
    
    $response = array(
        "response" => array(
            "responseStatus" => (string)$responseStatus,
            "responseDescription" => $responseDescription,
            "action" => $action,
            "data" => $array
        )
    );

    // var_dump($response);

    return $response;
}
?>