<?php

function dbContext()
{
    $mysql_hostname = "localhost";
    $mysql_user = "deviceuser1";
    $mysql_password = "deviceuser1";
    $mysql_database = "devices";

    //$mysqli = new mysqli('localhost', 'deviceuser1', 'deviceuser1', 'devices');
    //echo $mysql_database;
    $mysqli = new mysqli("$mysql_hostname", "$mysql_user", "$mysql_password", "$mysql_database");
    if (mysqli_connect_errno()) {
        printf("Connect failed: %s\n", mysqli_connect_error());
        exit();
    }

    return $mysqli;
}
