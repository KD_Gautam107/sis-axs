<?php

include_once $_SERVER['DOCUMENT_ROOT'] . 'api/common/paths.php';
include_once $pathUsers;
include_once $pathSites;
include_once $pathQuanta;
include_once $pathResponse;
include_once $pathProperties;

function sites($username, $password, $token)
{
    if (!$token) {
        $token = null;
    }

    $users = validateUser($username, $password, $token);
    if (count($users) > 0) {

        // echo "br\n";
        // echo $users[0]['tokenFlag'];
        // echo "br\n";

        //select sites
        $sites = selectSites($users[0]['type'], $users[0]['customerId'], $users[0]['typeref']);
        if (count($sites) > 0) {
            foreach ($sites as $index => $site) {
                $quanta = selectQuanta($site['siteNo']);

                if (count($quanta) > 0) {
                    $sites[$index]['quantaId'] = $quanta[0]['quantaId'];
                    $sites[$index]['deviceId'] = $quanta[0]['deviceId'];
                    $sites[$index]['type'] = $quanta[0]['type'];
                    $sites[$index]['dataValidated'] = $quanta[0]['dataValidated'];
                    $sites[$index]['timestamp'] = $quanta[0]['timestamp'];
                    $sites[$index]['status'] = $quanta[0]['status'];
                    $sites[$index]['versionCode'] = $quanta[0]['versionCode'];
                    if (($users[0]['tokenFlag']) != null) {
                        $sites[$index]['tokenFlag'] = $users[0]['tokenFlag'];
                    }

                }
            }

            return responseHandler(0, 'sites', $sites);
        } else {
            return responseHandler(2, 'sites', array());
        }
    } else {
        return responseHandler(1, 'sites', array());
    }
}

function hybrid($username, $password, $quantaId)
{
    $users = validateUser($username, $password);
    if (count($users) > 0) {
        //select quanta
        $quanta = selectQuantaById($quantaId);
        if (count($quanta) > 0) {
            $dateRange = getSiteDateRange($quanta[0]['siteNo']);
            $powerSupply = getPowerSupply($quanta[0]['siteNo'], $dateRange[1], $dateRange[0]);

            //echo "SiteId =  $quanta[0]['siteNo']";
            $quantum = array(
                'status' => $quanta[0]['status'],
                'quantum' => deviceGetData(base64_decode($quanta[0]['quantum'])),
                'powerSupply' => $powerSupply,
            );
            return responseHandler(0, 'hybrid', $quantum);
        } else {
            return responseHandler(2, 'hybrid', array());
        }
    } else {
        return responseHandler(1, 'hybrid', array());
    }
}

function quanta()
{
    $quanta = getQuanta();

    if (!$quanta) {
        return responseHandler(2, 'quanta', array());
    }

    return responseHandler(0, 'quanta', $quanta);
}

function powerSupply($username, $password, $siteNo, $startDate, $endDate)
{
    $users = validateUser($username, $password);
    if (count($users) > 0) {
        // convert UNIX timestamp to PHP DateTime
        $startDateTime = (new DateTime("@$startDate"))->format('Y-m-d H:i:s');
        $endDateTime = (new DateTime("@$endDate"))->format('Y-m-d H:i:s');

        $powerSupply = getPowerSupply($siteNo, $endDateTime, $startDateTime);

        return responseHandler(0, 'powerSupply', $powerSupply);
    } else {
        return responseHandler(1, 'powerSupply', array());
    }
}
