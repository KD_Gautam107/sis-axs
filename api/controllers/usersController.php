<?php

include_once $_SERVER['DOCUMENT_ROOT'] . 'api/common/paths.php';
include_once $pathUsers;
include_once $pathResponse;

function verifyCredentials($username, $password, $token)
{
    /*
        {
            "response": {
                "responseStatus": "0",
                "responseDescription": "success",
                "action": "verifyCredentials",
                "data": {
                    "userId": "21",
                    "username": "BSNLU01",
                    "type": "1",
                    "customerId": "17",
                    "typeref": ""
                }
            }
        }
       */

    //echo $token;
    if(!$token){
        $token = null;
        //echo "\n Token : $token"; 
    }

    $users = validateUser($username, $password ,$token);
    if (count($users) > 0) {
        return responseHandler(0, "verifyCredentials", $users[0]);
    } else {
        return responseHandler(1, "verifyCredentials", array());
    }
}
