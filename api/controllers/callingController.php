<?php

// function calling($mobileNo)
// {
//     $apiEndpoint = "http://182.77.56.18/api/calling?mobileNo=$mobileNo";
//     $apiEndpointPort = 8080;

//     $ch = curl_init();

//     curl_setopt($ch, CURLOPT_URL, $apiEndpoint);
//     curl_setopt($ch, CURLOPT_PORT, $apiEndpointPort);
//     curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//     //curl_setopt($ch, CURLOPT_HEADER, false);
//     curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
//     curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

//     $output = curl_exec($ch);

//     curl_close($ch);
//     return json_decode($output);
// }

function calling($requestUri)
{
    // $requestUri = "/api/calling?username=admin&password=system&mobiles=9625191665&siteId=SITEIDTEST&siteName=Noida%20Sector%205&alarms=smoke";
    // apiEndpoint = "http://192.168.0.236:20000/function001?username=admin&password=system&mobiles=9625191665&siteId=SITEIDTEST&siteName=Noida%20Sector%205&alarms=smoke"

    $apiEndpoint = 'http://192.168.0.236/function001?';
    $parseUrl = parse_url($requestUri);
    $apiEndpoint .= $parseUrl['query'];
    $apiEndpointPort = 20000;

    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL, $apiEndpoint);
    curl_setopt($ch, CURLOPT_PORT, $apiEndpointPort);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    //curl_setopt($ch, CURLOPT_HEADER, false);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

    $output = curl_exec($ch);

    curl_close($ch);
    return json_decode($output);
}
