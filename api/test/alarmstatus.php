<?php
session_start();
if (!isset($_SESSION['login_user']))
{
    header("Location: login.php");
}

header('Content-Type: application/json');

$array = array();
$aResult = array();
if( !isset($_POST['functionname']) ) { $aResult['error'] = 'No function name!'; }
if( !isset($_POST['arguments']) ) { $aResult['error'] = 'No function arguments!'; }
if( !isset($aResult['error']) ) {
    switch($_POST['functionname']) {
        case 'CallQuantaForDatatableById':
            if( !is_array($_POST['arguments']) || (count($_POST['arguments']) < 2) ) {
                $aResult['error'] = 'Error in arguments!';
            }
            else {
                //$aResult = GetClustersByCustomerId(floatval($_POST['arguments'][0]));
                $array =  CallQuantaForDatatableById($_POST['arguments'][0],$_POST['arguments'][1]);
            }
            break;
        default:
            $aResult['error'] = 'Not found function '.$_POST['functionname'].'!';
            break;
    }
}
        echo json_encode($array);


function CallQuantaForDatatableById($site_id, $indoor_type)
{
    $array = array();
include 'dbinc.php';

//$rec_limit = 10;

$mysqli = new mysqli("$mysql_hostname", "$mysql_user", "$mysql_password", "$mysql_database");
if (mysqli_connect_errno()) {
    printf("Connect failed: %s\n", mysqli_connect_error());
    exit();
}
    $where_clause = "WHERE (site_id = '$site_id')";  // AND (ID > '$id')  ";

$sql = "SELECT 
        ID, timestamp, data_validated, type, site_id, date_time, device_id, status, raw_data 
        FROM quanta 
        ".$where_clause." 
        ORDER BY timestamp DESC LIMIT 1 ";
//echo "Final: $sql<br>";
$retval = $mysqli->query($sql);
if(! $retval )
{
    die('No Data' . $mysqli->error);
}

$row = $retval->fetch_array(MYSQLI_NUM);

                $array['id']                 = $row[0];                      //Id
                $array['timestamp']          = $row[1];                      //timestamp
                $array['data_validated']     = $row[2];                      //data_validated
                $array['type']               = $row[3];                     //type
                $array['site_id']            = $row[4];                      //site_id
                $array['date_time']          = $row[5];                        //date_time
                $array['device_id']          = $row[6];                    //device_id
                $array['status']             = base64_encode($row[7]);       //status
                $array['raw_data']           = base64_encode($row[8]);       //raw_data
                //$array['indoor_type']        = $indoor_type;


//var_dump($base64Array);
//var_dump($jsonArray);
    return $array;
}
?>



