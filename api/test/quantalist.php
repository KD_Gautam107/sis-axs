<?php
session_start();
if (!isset($_SESSION['login_user'])) {
	header("Location: login.php");
}
if ($_SERVER["REQUEST_METHOD"] == "GET") {
	$site_id = $_GET['site_id'];
	$indoor_type = $_GET['indoor_type'];
	$site_name = $_GET['site_name'];
}
include 'common.php';

?>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
	<title>Equipment Data</title>

	<!-- Bootstrap -->
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">

	<!-- Optional theme -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css">

	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>

	<!-- Latest compiled and minified JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>

	<script src="/libs/bootstrap-paginator.min.js"></script>
	<script>
		function ShowDetail(id) {
			$("#myModalQuantaLabel").html("Equipment Status");
			$.ajax({
				url: "quantadetail.php?id=" + id + "&indoor_type=" + <?php echo $indoor_type; ?>,
				success: function(result) {
					$("#id_quanta_detail").html(result);
					$('#myModalQuanta').modal('show');
				}
			});
		}


		$('#myModalQuanta').on('hidden.bs.modal', function(e) {
			$("#id_device_detail").html('');
		});
	</script>

	<style>
		#led_white {
			width: 28px;
			height: 28px;
			background: url(/media/leds.png) 0 0;
		}

		#led_red {
			width: 28px;
			height: 28px;
			background: url(/media/leds.png) -30px 0;
		}

		#led_green {
			width: 28px;
			height: 28px;
			background: url(/media/leds.png) -124px 0;
		}
	</style>

</head>

<body>

	<h3><img src="/media/icons/ic_settings_input_antenna_black_36dp.png" title="Site Name"> <?php echo $site_name; ?></h3>

	<?php

	include 'dbinc.php';

	$rec_limit = 10;

	$mysqli = new mysqli("$mysql_hostname", "$mysql_user", "$mysql_password", "$mysql_database");
	if (mysqli_connect_errno()) {
		printf("Connect failed: %s\n", mysqli_connect_error());
		exit();
	}

	$where_clause = "WHERE (site_id = '$site_id') ";
	$filter_data_type = 0;
	if ($_GET['filter_data_type'] == 1) {
		$where_clause = $where_clause . " AND type=1  ";
		$filter_data_type = 1;
	}
	if ($_GET['filter_data_type'] == 2) {
		$where_clause = $where_clause . " AND type=0  ";
		$filter_data_type = 2;
	}

	/* Get total number of records */
	$sql = "SELECT count(ID) FROM quanta " . $where_clause;
	//echo "Final: $sql<br>";
	$retval = $mysqli->query($sql);
	if (!$retval) {
		die('No Data' . $mysqli->error);
	}

	$row = $retval->fetch_array(MYSQLI_NUM);
	$rec_count = $row[0];

	//echo "ROWS: $rec_count<br>";

	if (isset($_GET{
	'page'})) {
		$page = $_GET{
		'page'} + 1;
		$offset = $rec_limit * $page;
	} else {
		$page = 0;
		$offset = 0;
	}
	$left_rec = $rec_count - ($page * $rec_limit);
	//echo "<br>left_rec: $left_rec<br>";
	////

	//SELECT id, date_time FROM `quanta` WHERE date_time IN (SELECT MAX(date_time) from `quanta`)

	$order = "SELECT ID " .
		"FROM quanta " . $where_clause .
		"ORDER BY date_time DESC " .
		"LIMIT $offset, $rec_limit ";

	//echo "order: $order<br>";	
	$result = $mysqli->query($order);


	$first_time = true;

	$count = 0;
	while ($data = $result->fetch_row()) {
		$array = GetDeviceRecordFromId($data[0]);
		//var_dump($data[0]);
		//if ($first_time) var_dump($array);
		//echo sizeof($array)."<br>";
		//echo $array[6];
		//var_dump($array);

		if ($first_time) {
			if ($array[6] == 1) {
				if ($array[2] == 2)	//Double DG
				{
					DisplayEntryType1DoubleDGHeader();
				} else {
					DisplayEntryType1Header();
				}
			} else if ($array[6] == 2) {
				if ($array[2] == 1) {
					//if($array[37] == 3)
					//{
					DisplayEntryType3Header();
					//}
				} else {
					DisplayEntryTypeSolarHeader();
				}
			} else if ($array[6] == 16) {
				DisplayEntryType1Header();
			} else if ($array[6] == 32) {
				DisplayEntryTypeDoubleBatteryHeader();
			}
			$first_time = false;
		}

		if ($array[6] == 1) {
			if ($array[2] == 2)	//Double DG
			{
				DisplayEntryType1DoubleDG($array);
			} else {
				DisplayEntryType1($array);
			}
		} else if ($array[6] == 2) {
			if ($array[2] == 1) {
				//if($array[37] == 3)
				//{
				DisplayEntryType3($array);
				//}
			} else {
				DisplayEntryTypeSolar($array);
			}
		} else if ($array[6] == 16) {
			DisplayEntryType1($array);
		} else if ($array[6] == 32) {
			DisplayEntryTypeDoubleBattery($array);
		}
		//var_dump($array);
		$count++;
	}

	DisplayEntryType1HeaderEnd();

	$result->close();

	echo "</table>\n<br><br>";
	$disp = $page + 1;
	$total = ceil($rec_count / $rec_limit);
	$totalChunks = ceil($rec_count / 5000);
	?>

	<div>
		<ul id='id_pages'></ul>
	</div>
	<script type='text/javascript'>
		var options = {
			bootstrapMajorVersion: 3,
			numberOfPages: 10,
			currentPage: <?php echo $disp; ?>,
			totalPages: <?php echo $total; ?>,
			onPageClicked: function(e, originalEvent, type, page) {
				var newpage = page - 2;

				window.location.replace("<?php echo $_PHP_SELF . '?page='; ?>" + newpage + "<?php echo '&site_id=' . $site_id . '&indoor_type=' . $indoor_type . '&site_name=' . $site_name . ''; ?>");
			}
		}

		$('#id_pages').bootstrapPaginator(options);

		function isInt(value) {
			if (isNaN(value)) {
				return false;
			}
			var x = parseFloat(value);
			return (x | 0) === x;
		}

		function Data_Export(total) {

			//var url = "exportcsv.php?filter_data_type="+filter_data_type+"&site_list_data_type="+site_list_data_type+"&export_page_no="+export_page_no+"&excel_format="+excel_format;
			var site_id = "<?php echo $site_id; ?>";
			var export_page_no = $("#ExportPage").val();
			var excel_format = $('#ExcelFormat').is(":checked") ? 1 : 0;

			if (total < 1) {
				alert("No Data Available!");
			} else if ((!isInt(export_page_no) || (export_page_no < 1) || (export_page_no > total))) {
				alert("Invalid Page Number");
			} else {
				var url = "data-export.php?siteid=" + site_id + "&export_page_no=" + export_page_no + "&excel_format=" + excel_format;
				//alert(url);
				//var request = $.ajax({url: url});
				window.open(url);
			}
		}
	</script>

	<!--<form method="post" role="form" class="form-inline">
    <table class='table-condensed table-responsive'>
        <tr >
            <td>Start Date:</td>
            <td>
                <input class="form-control" type="date"  id="StartDateTime" name="StartDateTime">
            </td>
        </tr>
        <tr>
            <td>End Date:</td><td><input type="datetime" class="form-dontrol" id="EndDate" name="EndDate"></td>
        </tr>
    </table>

</form>-->

	<form method="post" role="form" class="form-inline">
		Export Page <input class="form-control" type="text" name="ExportPage" id="ExportPage" value="1" size="2"> of total <b><?php echo $totalChunks; ?></b> pages.&nbsp;
		&nbsp;
		<a role="button" href="#" onclick="event.preventDefault(); Data_Export('<?php echo $totalChunks; ?>');" class="btn btn-default">Export as CSV</a>

		<input class="form-control" type="checkbox" name="ExcelFormat" id="ExcelFormat" checked="true">Excel Format</input>
	</form>

	<?php
	/*if ($total == 1)
	{
		echo "<a class='btn btn-info disabled' role='button' href=\"#\">Last 10 Records</a>";
		echo " $disp / $total ";
	    echo "<a class='btn btn-info disabled' role='button' href=\"#\">Next 10 Records</a>";
	}
	else if( $page > 0 )
	{
	   $last = $page - 2;


	   if (($offset+$rec_limit) < $rec_count)
	   {
		   echo "<a class='btn btn-info' role='button' href=\"$_PHP_SELF?page=$last&site_id=$site_id&indoor_type=$indoor_type&site_name=$site_name\">Last 10 Records</a>";
		   echo " $disp / $total ";
		   echo "<a class='btn btn-info' role='button' href=\"$_PHP_SELF?page=$page&site_id=$site_id&indoor_type=$indoor_type&site_name=$site_name\">Next 10 Records</a>";
	   }
	   else
	   {
			echo "<a class='btn btn-info' role='button' href=\"$_PHP_SELF?page=$last&site_id=$site_id&indoor_type=$indoor_type&site_name=$site_name\">Last 10 Records</a>";
			echo " $disp / $total ";
	   }
	}
	else if( $page == 0 )
	{
		echo "<a class='btn btn-info disabled' role='button' href=\"#\">Last 10 Records</a>";
		echo " $disp / $total ";
	    echo "<a class='btn btn-info' role='button' href=\"$_PHP_SELF?page=$page&site_id=$site_id&indoor_type=$indoor_type&site_name=$site_name\">Next 10 Records</a>";
	}*/

	mysqli_close($mysqli);

	?>


	<!-- Modal -->
	<div class="modal fade" id="myModalQuanta" tabindex="-1" role="dialog" aria-labelledby="myModalQuantaLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="myModalQuantaLabel"></h4>
				</div>
				<div class="modal-body">
					<div id="id_quanta_detail">
						Please wait...
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>


</body>