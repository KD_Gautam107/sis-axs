
<?php
$host = $_SERVER['DOCUMENT_ROOT'] . 'api';

//Common Methods
$pathGeneralMethods = $host . '/common/generalMethods.php';
$pathPushNotifications = $host . '/common/pushNotifications.php';
$pathProperties = $host . '/common/properties.php';

//Configurations
$pathDbinc = $host . '/config/dbinc.php';
$pathResponse = $host . '/config/response.php';

//Models
$pathUsers = $host . '/models/users.php';
$pathSites = $host . '/models/sites.php';
$pathQuanta = $host . '/models/quanta.php';

//Controllers
$pathUsersController = $host . '/controllers/usersController.php';
$pathSitesController = $host . '/controllers/sitesController.php';
$pathCallingController = $host . '/controllers/callingController.php';
?>