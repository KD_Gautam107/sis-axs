
<?php

include_once $_SERVER['DOCUMENT_ROOT'] . 'api/common/paths.php';
include_once $pathDbinc;
include_once $pathRanegData;

	
/********************************************************************************/


			/* ranges array returns */
	
	function GetDeviceRecordFromId($id)
	{
		$array = array();
		$mysqli = dbContext();
		if (mysqli_connect_errno())
		{
			printf("Connect failed: %s\n", mysqli_connect_error());
			return null;
		}
		
		//$sql = 'SELECT a.tutorial_id, a.tutorial_author, b.tutorial_count
		//	FROM tutorials_tbl a, tcount_tbl b
		//	WHERE a.tutorial_author = b.tutorial_author';
			
		$order = "SELECT 
			a.ID, a.timestamp , a.data_validated , a.type , a.site_id , a.date_time , a.device_id , a.status , a.raw_data,  
			b.ClusterId, b.DeviceName, b.DeviceMake, b.Circle, b.SiteName, b.District, b.mobile, b.Longitude, b.Latitude, b.SiteIndoorType,  b.DG_KVA, b.SAM_Name, b.SAM_Number, b.AM_Name, b.AM_Number, b.Tech_Name,  b.Tech_Number, b.Comments,
			c.name,
			b.date_of_installation, b.DG_make, b.EB_capacity, b.EB_phase, b.battery_AH, b.battery_qty, b.battery_make, 
			b.DG2_KVA, b.DG2_MAKE, b.site_type
			
		FROM quanta a, siteinfo b, clusters c
		WHERE (a.site_id = b.site_id) AND (b.ClusterId = c.Id) AND a.id=".$id;
		
		//echo $order;
						
		//$order = "SELECT * FROM quanta where id=".$id;
		$result = $mysqli->query($order);	
		$data = $result->fetch_row();
		
		//var_dump($data);
	
		$array[] 	= $data[0];		//0Unique Id
		$array[] 	= $data[1];		//1Time Stamp by Server
		$array[]	= $data[2];		//2Validated by Server
		$array[] 	= $data[3];		//3Type 0 - Periodic / 1 - Fault
		$array[]	= $data[4];		//4String Site Id
		$array[]	= $data[5];		//5Date Time by Device
		$array[]	= $data[6];		//6Device Id/ Only 1, 2 & 32 known for now
		$array[]	= $data[7];		//7Status Bits - To Expand
		$data_raw	= $data[8];		//Raw Data, for device specific information
		
		//var_dump($array);
		//$array = array_merge($array, DeviceGetStatusBits($data[7]));
		//var_dump($array);
		
		
		if ($data[6] == 1)
		{
			//$data_validated
			//	0 - Not Validated
			//	1 - Single DG
			//	2 - Double DG
			if ($data[2] == 2)	//Double DG
				$array = array_merge($array, DeviceGetDataDouble($data_raw));

			else
				$array = array_merge($array, DeviceGetData($data_raw));
		}
		else if ($data[6] == 2)	//2 = Solar
		{
			if($data[2] == 1)
			{
				//if($data[37] == 3)
				//{
					$array = array_merge($array, DeviceGetDataDevice3($data_raw));
                for($i = 26; $i <39;$i++)
                {
                    $array[$i] = "";
                }
				//}
			}
			else
			{
				$array = array_merge($array, DeviceGetDataDevice2($data_raw));
			}

		}
		else if ($data[6] == 16)	//Phase 2
		{
			$array = array_merge($array, DeviceGetData($data_raw));//DeviceGetDataDevicePhase2($data_raw));
		}
		else if ($data[6] == 32)
        {
            $array = array_merge($array, DeviceGetDataDoubleBattery($data_raw));
			for($i = 21; $i <39;$i++)
			{
				$array[$i] = "";
			}
		}
		
		$array[]	= $data[9];		//39 ClusterId	
		$array[]	= $data[10];	//40 DeviceName
		$array[]	= $data[11];	//41 DeviceMake
		$array[]	= $data[12];	//42 Circle
		$array[]	= $data[13];	//43 SiteName
		$array[]	= $data[14];	//44 District
		$array[]	= $data[15];	//45 Site Mobile
		$array[]	= $data[16];	//46 Longitude
		
		$array[]	= $data[17];	//47 Latitude
		$array[]	= $data[18];	//48 SiteIndoorType
		$array[]	= $data[19];	//49 DG_KVA
		$array[]	= $data[20];	//50 SAM_Name
		$array[]	= $data[21];	//51 SAM_Number
		$array[]	= $data[22];	//52 AM_Name
		$array[]	= $data[23];	//53 AM_Number
		$array[]	= $data[24];	//54 Tech_Name
		$array[]	= $data[25];	//55 Tech_Number
		
		$array[]	= $data[28];	//56 date_of_installation
		$array[]	= $data[29];	//57 DG_make
		$array[]	= $data[30];	//58 EB_capacity
		$array[]	= $data[31];	//59 EB_phase
		$array[]	= $data[32];	//60 battery_AH
		$array[]	= $data[33];	//61 battery_qty
		$array[]	= $data[34];	//62 battery_make
		
		$array[]	= $data[35];	//63 DG2_KVA
		$array[]	= $data[36];	//64 DG2_MAKE
		$array[]	= $data[37];	//65 site_type
		
		
		$result->close();
		mysqli_close($mysqli);
		
		//var_dump($array);
		
		return $array;
	
	}


	/*********OTHER DETAILS************ */

	function GetComparisonbetweenIds($id1, $id2)
	{
		//echo"LALA BAHI";
	$array = array();
		
	$arr1 = GetDeviceRecordFromId($id1);	//New
	$arr2 = GetDeviceRecordFromId($id2);	//Old
	
	
	$array[] = $arr1[4];	//0 Site Id
	$array[] = $arr1[39];	//1 Circle
	$array[] = $arr1[38];	//2 Culster
	$array[] = $arr1[37];	//3 Make
	$array[] = $arr1[41];	//4 AM NAME
	$array[] = $arr1[42];	//5 Mob no.
	$array[] = $arr1[43]; 	//6 DG Mobile
	
	//What Diffs to Retrieve
	//$array[] = new DateTime(arr2[5]);	//7 From Time
	$array[] = date_create_from_format('Y-m-d H:i:s', $arr2[5]);
	//$array[] = new DateTime(arr1[5]);	//8 To Time
	$array[] = date_create_from_format('Y-m-d H:i:s', $arr1[5]);
	
	//echo "F: $arr2[5]". " > ".$array[0]->format('Y-m-d H:i:s')."<br>";
	//echo "T: $arr1[5]". " > ".$array[1]->format('Y-m-d H:i:s')."<br>";
	
	$array[] = $array[7]->diff($array[8]);	//9 Interval
	//echo "Interval: ".$array[2]->format("%d %h %i")."<br>";
	//var_dump($array[2]);
	
	$delta_h = ($array[8]->getTimestamp() - $array[7]->getTimestamp()) / 3600;
	$array[] = $delta_h;	//10 Hours
	//echo "Hours: $array[3]<br>";
	
	$array[] = $arr1[26]-$arr2[26]; //11 System On DG
	$array[] = $arr1[27]-$arr2[27]; //12 System On Mains
	$array[] = $arr1[28]-$arr2[28]; //13 System On Batt
	
	//echo "DG: ".$arr1[26]." - ".$arr2[26]." = ".$array[4]."<br>";
	//echo "Mains: ".$arr1[27]." - ".$arr2[27]." = ".$array[5]."<br>";
	//echo "Batt: ".$arr1[28]." - ".$arr2[28]." = ".$array[6]."<br>";
	
	$array[] = $arr1[43];	//14 Site Name
	
	if ($arr1[5] < $arr2[5])
		$array[] = ApiCalculateGeneratorFailedToStopTime($arr1[4], $arr1[5], $arr2[5]);
	else
		$array[] = ApiCalculateGeneratorFailedToStopTime($arr1[4], $arr2[5], $arr1[5]);
	
	if (($arr1[6] == 1)&&($arr1[2] == 2)&&($arr2[6] == 1)&&($arr2[2] == 2))	//Double DG
	{
		$array[] = 1;	//16 Double DG Case
		$array[] = $arr1[38][6]-$arr2[38][6]; //17 System On DG2
	}
	else
	{
		$array[] = 0;	//16 Double DG Case
		$array[] = 0; 	//17 System On DG2 - 0
	}
	
	return $array;
}

	/////////// CalculateGeneratorFailedToStopTime//////////
	function ApiCalculateGeneratorFailedToStopTime($site_id, $dateStart, $dateEnd)
	{
		$array = array();
		//echo "----------hey I was here-------";
		$mysqli = dbContext();
		if (mysqli_connect_errno()) {
			printf("Connect failed: %s\n", mysqli_connect_error());
			exit();
		}
		
		$sqlquery = "SELECT date_time, MID(HEX(`status`),7,1) & 8 ".
					"FROM quanta WHERE `site_id`='$site_id' AND `date_time` >= '$dateStart' AND `date_time` <= '$dateEnd' ".
					"ORDER BY date_time ASC ";

		//echo "\n\n\n\n: $sqlquery<br>";	
		$result = $mysqli->query($sqlquery);
		//var_dump($result);
		
		$intervals = null;
		
		if ($result !== false)
		{
			$genError = false;
			$alarmFlag = false;
			
			
			while($data = $result->fetch_row())
			{
				//var_dump($data);
				
				//If first data is in error then assume ??????
				//if (firstTime && !genError && ($data[1] == '8')) 
				//	genError = true;
				
				$alarmFlag = ($data[1] == '8');				
				if ($genError)
				{
					if (!$alarmFlag)
					{
						//Yes Alarm --> No Alarm
						$timeE = $data[0];
						$genError = false;

						$tmpInterval = date_create_from_format('Y-m-d H:i:s', $timeE)->getTimestamp() - date_create_from_format('Y-m-d H:i:s', $timeS)->getTimestamp();

						if ($intervals == null)
							$intervals = $tmpInterval;
						else
							$intervals += $tmpInterval;

					}
				}
				else
				{
					if ($alarmFlag)
					{
						//No Alarm --> Yes Alarm
						$timeS = $data[0];
						$genError = true;
					}
				}
				
				$lastTime = $data[0];
				//var_dump($intervals);
			}
			
			//Error flag true even at end. Diff it from the last time.
			if ($genError)
			{
				$timeE = $lastTime;
				$genError = false;

				$tmpInterval = date_create_from_format('Y-m-d H:i:s', $timeE)->getTimestamp() - date_create_from_format('Y-m-d H:i:s', $timeS)->getTimestamp();

				if ($intervals == null)
					$intervals = $tmpInterval;
				else
					$intervals += $tmpInterval;
			}

		
			$result->close();
		}
			
		mysqli_close($mysqli);
		
		if ($intervals == null)
		{
			$intervals = 0;
		}
		else
		{
			$intervals = $intervals/3600;
		}
		
		return $intervals;
	}

    ///////get device Data

    function DeviceGetData($data_raw) 
	{
		$array = array();
		$sub_data = substr($data_raw, 41, 36);
		$stats = str_split($sub_data, 2);
		//var_dump($stats);
		
		$sub_data = unpack("n",$stats[0]);
		$sub_data = $sub_data[1]/10;
		//echo "<tr><td>Room Temperature</td><td>";
		$array[] = $sub_data;	//8
		
		
		$sub_data = unpack("n",$stats[1]);
		$sub_data = $sub_data[1]/10;
		//echo "<tr><td>Fuel Level</td><td>";
		$array[] = $sub_data;	//9
		
		$sub_data = unpack("n",$stats[2]);
		$sub_data = $sub_data[1]/10;
		//echo "<tr><td>Fuel Data</td><td>";
		$array[] = $sub_data;	//10
		
		$sub_data = unpack("n",$stats[3]);
		$sub_data = $sub_data[1]/10;
		//echo "<tr><td>Site Batt. bank Voltage</td><td>";
		$array[] = $sub_data;	//11
		
		$sub_data = unpack("n",$stats[4]);
		$sub_data = $sub_data[1]/10;
		//echo "<tr><td>R-Phase current</td><td>";
		$array[] = $sub_data;	//12
		
		$sub_data = unpack("n",$stats[5]);
		$sub_data = $sub_data[1]/10;
		//echo "<tr><td>Y-Phase current</td><td>";
		$array[] = $sub_data;	//13
		
		$sub_data = unpack("n",$stats[6]);
		$sub_data = $sub_data[1]/10;
		//echo "<tr><td>B-Phase current</td><td>";
		$array[] = $sub_data;	//14
		
		$sub_data = unpack("n",$stats[7]);
		$sub_data = $sub_data[1]/10;
		//echo "<tr><td>Mains Frequency</td><td>";
		$array[] = $sub_data;	//15
		
		$sub_data = unpack("n",$stats[8]);
		$sub_data = $sub_data[1]/10;
		//echo "<tr><td>DG frequency</td><td>";
		$array[] = $sub_data;	//16
		
		$sub_data = unpack("n",$stats[9]);
		$sub_data = $sub_data[1]/10;
		//echo "<tr><td>DG-R phase Voltage</td><td>";
		$array[] = $sub_data;	//17
		
		$sub_data = unpack("n",$stats[10]);
		$sub_data = $sub_data[1]/10;
		//echo "<tr><td>DG-Y phase Voltage</td><td>";
		$array[] = $sub_data;	//18
		
		$sub_data = unpack("n",$stats[11]);
		$sub_data = $sub_data[1]/10;
		//echo "<tr><td>DG-B phase Voltage</td><td>";
		$array[] = $sub_data;	//19
		
		$sub_data = unpack("n",$stats[12]);
		$sub_data = $sub_data[1]/10;
		//echo "<tr><td>LCU1 Output Voltage</td><td>";
		$array[] = $sub_data;	//20
		
		$sub_data = unpack("n",$stats[13]);
		$sub_data = $sub_data[1]/10;
		//echo "<tr><td>LCU2 Output Voltage</td><td>";
		$array[] = $sub_data;	//21
		
		$sub_data = unpack("n",$stats[14]);
		$sub_data = $sub_data[1]/10;
		//echo "<tr><td>LCU3 Output Voltage</td><td>";
		$array[] = $sub_data;	//22
		
		$sub_data = unpack("n",$stats[15]);
		$sub_data = $sub_data[1]/10;
		//echo "<tr><td>Input Mains Voltage - R Phase</td><td>";
		$array[] = $sub_data;	//23
		
		$sub_data = unpack("n",$stats[16]);
		$sub_data = $sub_data[1]/10;
		//echo "<tr><td>Input Mains Voltage - Y Phase</td><td>";
		$array[] = $sub_data;	//24
		
		$sub_data = unpack("n",$stats[17]);
		$sub_data = $sub_data[1]/10;
		//echo "<tr><td>Input Mains Voltage - B Phase</td><td>";
		$array[] = $sub_data;	//25
		
		/////
		
		
		$sub_data = substr($data_raw, 77, 48);
		$stats = str_split($sub_data, 8);
		$stats = substr_replace($stats, ".", 7, 0);
		
		for ($i=0; $i<6; $i++)
		{
			$stats[$i] = ltrim($stats[$i], '0');
			if ($stats[$i][0] == '.')
				$stats[$i] = "0".$stats[$i];
		}
		
		//echo "<tr><td>DG Running Hours</td><td>";
		$array[] = $stats[0];	//26
		
		//echo "<tr><td>Mains RUN HOURS</td><td>";
		$array[] = $stats[1];	//27
		
		//echo "<tr><td>Batt RUN HOURS</td><td>";
		$array[] = $stats[2];	//28
		
		//echo "<tr><td>O/P Mains Energy</td><td>";
		$array[] = $stats[3];	//29
		
		//echo "<tr><td>DG Energy</td><td>";
		$array[] = $stats[4];	//30
		
		//echo "<tr><td>I/P Mains Energy</td><td>";
		$array[] = $stats[5];	//31
		
		$sub_data = substr($data_raw, 125, 2);
		
		$sub_data = unpack("n",$sub_data);
		$sub_data = $sub_data[1]/10;
		//echo "<tr><td>DG Battery Voltage</td><td>";
		$array[] = $sub_data;	//32


		//18*2	= 36
		//6 *8	= 48
		//1 *2	= 02
		//----------
		//        86
		//----------
		
		
		//Updated Data - Updated Protocol
		
		//18*2	= 36
		//6 *8	= 48
		//1 *2	= 02
		//2 *2	= 04	*
		//2 *1	= 02	*
		//----------
		//        92
		//----------
		
		//REV2 Total 134 Bytes
		if (strlen($data_raw) >= 134)
		{
			$sub_data = substr($data_raw, 127, 4);
			$stats = str_split($sub_data, 2);
			
			$sub_data = unpack("n",$stats[0]);
			$sub_data = $sub_data[1]/10;
			//echo "<tr><td>Battery Charging current</td><td>";
			$array[] = $sub_data;	//33
			
			
			$sub_data = unpack("n",$stats[1]);
			$sub_data = $sub_data[1]/10;
			//echo "<tr><td>Battery Discharging current</td><td>";
			$array[] = $sub_data;	//34
			
			$sub_data = substr($data_raw, 131, 2);
			$stats = str_split($sub_data, 1);
			
			$sub_data = unpack("c",$stats[0]);
			$sub_data = (hexdec(bin2hex($stats[0])));	//$sub_data[1]/1;
			//echo "<tr><td>Battery status</td><td>";
			$array[] = $sub_data;	//35
			
			$sub_data = unpack("c",$stats[1]);
			$sub_data = (hexdec(bin2hex($stats[1]))/10);	//$sub_data[1]/10;
			//echo "<tr><td>Battery back up time</td><td>";
			$array[] = $sub_data;	//36
			
			
			//REV3	Total 150 Bytes
			if (strlen($data_raw) >= 150)
			{
				$sub_data = substr($data_raw, 133, 16);
				$stats = str_split($sub_data, 8);
				$stats = substr_replace($stats, ".", 7, 0);
		
				for ($i=0; $i<2; $i++)
				{
					$stats[$i] = ltrim($stats[$i], '0');
					if ($stats[$i][0] == '.')
						$stats[$i] = "0".$stats[$i];
				}
				
				//echo "<tr><td>Battery Charging Energy (Kwh)</td><td>";
				$array[] = $stats[0];	//37
				
				//echo "<tr><td>Battery Discharging Energy (Kwh)</td><td>";
				$array[] = $stats[1];	//38
			
			}
			else
			{
				$array[] = 0;	//37
				$array[] = 0;	//38
			}
		
		}
		else
		{
			$array[] = 0;	//33
			$array[] = 0;	//34
			$array[] = 0;	//35
			$array[] = 0;	//36
			$array[] = 0;	//37
			$array[] = 0;	//38
		}
	
		return $array;
	}
	
	function DeviceGetDataDevice2($data_raw) 
	{
		$array = array();
		
		//14 * 2 Bytes
		$sub_data = substr($data_raw, 41, 28);
		$stats = str_split($sub_data, 2);
		//var_dump($stats);
		
		$sub_data = unpack("n",$stats[0]);
		$sub_data = $sub_data[1]/10;
		//echo "<tr><td>Room Temperature</td><td>";
		$array[] = $sub_data;	//8
		
		
		$sub_data = unpack("n",$stats[1]);
		$sub_data = $sub_data[1]/10;
		//echo "<tr><td>Solar array 1 Voltage</td><td>";
		$array[] = $sub_data;	//9
		
		$sub_data = unpack("n",$stats[2]);
		$sub_data = $sub_data[1]/10;
		//echo "<tr><td>Solar array 2 Voltage</td><td>";
		$array[] = $sub_data;	//10
		
		$sub_data = unpack("n",$stats[3]);
		$sub_data = $sub_data[1]/10;
		//echo "<tr><td>Solar array 3 Voltage</td><td>";
		$array[] = $sub_data;	//11
		
		$sub_data = unpack("n",$stats[4]);
		$sub_data = $sub_data[1]/10;
		//echo "<tr><td>Array1 current</td><td>";
		$array[] = $sub_data;	//12
		
		$sub_data = unpack("n",$stats[5]);
		$sub_data = $sub_data[1]/10;
		//echo "<tr><td>Array2 current</td><td>";
		$array[] = $sub_data;	//13
		
		$sub_data = unpack("n",$stats[6]);
		$sub_data = $sub_data[1]/10;
		//echo "<tr><td>Array3 current</td><td>";
		$array[] = $sub_data;	//14
		
		$sub_data = unpack("n",$stats[7]);
		$sub_data = $sub_data[1]/10;
		//echo "<tr><td>R-Phase current</td><td>";
		$array[] = $sub_data;	//15
		
		$sub_data = unpack("n",$stats[8]);
		$sub_data = $sub_data[1]/10;
		//echo "<tr><td>Y-Phase current</td><td>";
		$array[] = $sub_data;	//16
		
		$sub_data = unpack("n",$stats[9]);
		$sub_data = $sub_data[1]/10;
		//echo "<tr><td>B-Phase current</td><td>";
		$array[] = $sub_data;	//17
		
		$sub_data = unpack("n",$stats[10]);
		$sub_data = $sub_data[1]/10;
		//echo "<tr><td>Mains Frequency</td><td>";
		$array[] = $sub_data;	//18
		
		$sub_data = unpack("n",$stats[11]);
		$sub_data = $sub_data[1]/10;
		//echo "<tr><td>Input Mains Voltage – R Phase</td><td>";
		$array[] = $sub_data;	//19
		
		$sub_data = unpack("n",$stats[12]);
		$sub_data = $sub_data[1]/10;
		//echo "<tr><td>Input Mains Voltage – Y Phase</td><td>";
		$array[] = $sub_data;	//20
		
		$sub_data = unpack("n",$stats[13]);
		$sub_data = $sub_data[1]/10;
		//echo "<tr><td>Input Mains Voltage – B Phase</td><td>";
		$array[] = $sub_data;	//21
		
		
		//5 * 8 Bytes
		
		$sub_data = substr($data_raw, 69, 40);
		$stats = str_split($sub_data, 8);
		$stats = substr_replace($stats, ".", 7, 0);
		
		for ($i=0; $i<5; $i++)
		{
			$stats[$i] = ltrim($stats[$i], '0');
			if ($stats[$i][0] == '.')
				$stats[$i] = "0".$stats[$i];
		}
		
		//echo "<tr><td>Solar Run Hours</td><td>";
		$array[] = $stats[0];	//22
		
		//echo "<tr><td>Mains RUN HOURS</td><td>";
		$array[] = $stats[1];	//23
		
		//echo "<tr><td>Solar + mains  RUN HOURS</td><td>";
		$array[] = $stats[2];	//24
		
		//echo "<tr><td>Mains Energy</td><td>";
		$array[] = $stats[3];	//25
		
		//echo "<tr><td>Solar Energy</td><td>";
		$array[] = $stats[4];	//26
		
		
		
		//3 * 2 Bytes
		
		$sub_data = substr($data_raw, 109, 6);
		$stats = str_split($sub_data, 2);
		
		$sub_data = unpack("n",$stats[0]);
		$sub_data = $sub_data[1]/10;
		//echo "<tr><td>Battery bank Voltage</td><td>";
		$array[] = $sub_data;	//27
		
		$sub_data = unpack("n",$stats[1]);
		$sub_data = $sub_data[1]/10;
		//echo "<tr><td>Battery Charging current</td><td>";
		$array[] = $sub_data;	//28
		
		$sub_data = unpack("n",$stats[2]);
		$sub_data = $sub_data[1]/10;
		//echo "<tr><td>Battery Discharging current</td><td>";
		$array[] = $sub_data;	//29

		
		//1 * 1 Byte
		
		$sub_data = substr($data_raw, 115, 1);
		$sub_data = (hexdec(bin2hex($sub_data)));
		//echo "<tr><td>Battery status</td><td>";
		$array[] = $sub_data;	//30		
		
		
		//2 * 8 Bytes
		
		$sub_data = substr($data_raw, 116, 16);
		$stats = str_split($sub_data, 8);
		$stats = substr_replace($stats, ".", 7, 0);
		
		for ($i=0; $i<2; $i++)
		{
			$stats[$i] = ltrim($stats[$i], '0');
			if ($stats[$i][0] == '.')
				$stats[$i] = "0".$stats[$i];
		}
		
		//echo "<tr><td>Batt CHG energy</td><td>";
		$array[] = $stats[0];	//31
		
		//echo "<tr><td>BAT Dischg Energy</td><td>";
		$array[] = $stats[1];	//32
		
		
		//Padding to make same size as for Device Type 1
		$array[] = null;	//33
		$array[] = null;	//34
		$array[] = null;	//35
		$array[] = null;	//36
		$array[] = null;	//37
		$array[] = null;	//38
		
	
		return $array;
	}
	
	//Data for Double DG
	function DeviceGetDataDouble($data_raw) 
	{
		$array = array();
		$sub_data = substr($data_raw, 42, 2);
		$stats = str_split($sub_data, 2);
		//var_dump($stats);
		
		$sub_data = unpack("n",$stats[0]);
		$sub_data = $sub_data[1]/10;
		//echo "<tr><td>Room Temperature</td><td>";
		$array[] = $sub_data;	//8
		
		
		$sub_data = substr($data_raw, 44, 1);
		$stats = str_split($sub_data, 1);
		$sub_data = (hexdec(bin2hex($stats[0])));	//$sub_data[1]/1;
		//echo "<tr><td>DG1 Fuel Level (0-100 %)</td><td>";
		$array[] = $sub_data;	//9
		
		
		
		$sub_data = substr($data_raw, 45, 32);
		$stats = str_split($sub_data, 2);
		
		$sub_data = unpack("n",$stats[0]);
		$sub_data = $sub_data[1]/10;
		//echo "<tr><td>DG1 Fuel Data</td><td>";
		$array[] = $sub_data;	//10
		
		
		$sub_data = unpack("n",$stats[1]);
		$sub_data = $sub_data[1]/10;
		//echo "<tr><td>DG1 Site Batt. bank Voltage</td><td>";
		$array[] = $sub_data;	//11
		
		$sub_data = unpack("n",$stats[2]);
		$sub_data = $sub_data[1]/10;
		//echo "<tr><td>DG1 R-Phase current</td><td>";
		$array[] = $sub_data;	//12
		
		$sub_data = unpack("n",$stats[3]);
		$sub_data = $sub_data[1]/10;
		//echo "<tr><td>DG1 Y-Phase current</td><td>";
		$array[] = $sub_data;	//13
		
		$sub_data = unpack("n",$stats[4]);
		$sub_data = $sub_data[1]/10;
		//echo "<tr><td>DG1 B-Phase current</td><td>";
		$array[] = $sub_data;	//14
		
		$sub_data = unpack("n",$stats[5]);
		$sub_data = $sub_data[1]/10;
		//echo "<tr><td>Mains Frequency</td><td>";
		$array[] = $sub_data;	//15
		
		$sub_data = unpack("n",$stats[6]);
		$sub_data = $sub_data[1]/10;
		//echo "<tr><td>DG1 frequency</td><td>";
		$array[] = $sub_data;	//16
		
		$sub_data = unpack("n",$stats[7]);
		$sub_data = $sub_data[1]/10;
		//echo "<tr><td>DG1-R phase Voltage</td><td>";
		$array[] = $sub_data;	//17
		
		$sub_data = unpack("n",$stats[8]);
		$sub_data = $sub_data[1]/10;
		//echo "<tr><td>DG1-Y phase Voltage</td><td>";
		$array[] = $sub_data;	//18
		
		$sub_data = unpack("n",$stats[9]);
		$sub_data = $sub_data[1]/10;
		//echo "<tr><td>DG1-B phase Voltage</td><td>";
		$array[] = $sub_data;	//19
		
		$sub_data = unpack("n",$stats[10]);
		$sub_data = $sub_data[1]/10;
		//echo "<tr><td>LCU1 Output Voltage</td><td>";
		$array[] = $sub_data;	//20
		
		$sub_data = unpack("n",$stats[11]);
		$sub_data = $sub_data[1]/10;
		//echo "<tr><td>LCU2 Output Voltage</td><td>";
		$array[] = $sub_data;	//21
		
		$sub_data = unpack("n",$stats[12]);
		$sub_data = $sub_data[1]/10;
		//echo "<tr><td>LCU3 Output Voltage</td><td>";
		$array[] = $sub_data;	//22
		
		$sub_data = unpack("n",$stats[13]);
		$sub_data = $sub_data[1]/10;
		//echo "<tr><td>Input Mains Voltage - R Phase</td><td>";
		$array[] = $sub_data;	//23
		
		$sub_data = unpack("n",$stats[14]);
		$sub_data = $sub_data[1]/10;
		//echo "<tr><td>Input Mains Voltage - Y Phase</td><td>";
		$array[] = $sub_data;	//24
		
		$sub_data = unpack("n",$stats[15]);
		$sub_data = $sub_data[1]/10;
		//echo "<tr><td>Input Mains Voltage - B Phase</td><td>";
		$array[] = $sub_data;	//25
		
		/////
			
		
		$sub_data = substr($data_raw, 77, 48);
		$stats = str_split($sub_data, 8);
		$stats = substr_replace($stats, ".", 7, 0);
		
		for ($i=0; $i<6; $i++)
		{
			$stats[$i] = ltrim($stats[$i], '0');
			if ($stats[$i][0] == '.')
				$stats[$i] = "0".$stats[$i];
		}
		
		//echo "<tr><td>DG1 Running Hours</td><td>";
		$array[] = $stats[0];	//26
		
		//echo "<tr><td>Mains RUN HOURS</td><td>";
		$array[] = $stats[1];	//27
		
		//echo "<tr><td>Batt RUN HOURS</td><td>";
		$array[] = $stats[2];	//28
		
		//echo "<tr><td>O/P Mains Energy</td><td>";
		$array[] = $stats[3];	//29
		
		//echo "<tr><td>DG1 Energy</td><td>";
		$array[] = $stats[4];	//30
		
		//echo "<tr><td>I/P Mains Energy</td><td>";
		$array[] = $stats[5];	//31
		
		$sub_data = substr($data_raw, 125, 6);
		$stats = str_split($sub_data, 2);
		
		$sub_data = unpack("n",$stats[0]);
		$sub_data = $sub_data[1]/10;
		//echo "<tr><td>DG1 Battery Voltage</td><td>";
		$array[] = $sub_data;	//32

		$sub_data = unpack("n",$stats[1]);
		$sub_data = $sub_data[1]/10;
		//echo "<tr><td>Battery Charging current</td><td>";
		$array[] = $sub_data;	//33		
		
		$sub_data = unpack("n",$stats[2]);
		$sub_data = $sub_data[1]/10;
		//echo "<tr><td>Battery Discharging current</td><td>";
		$array[] = $sub_data;	//34
		
		
		$sub_data = substr($data_raw, 131, 2);
		$stats = str_split($sub_data, 1);
		
		$sub_data = (hexdec(bin2hex($stats[0])));	//$sub_data[1]/1;
		//echo "<tr><td>Battery status</td><td>";
		$array[] = $sub_data;	//35
		
		$sub_data = (hexdec(bin2hex($stats[1]))/10);	//$sub_data[1]/10;
		//echo "<tr><td>Battery back up time</td><td>";
		$array[] = $sub_data;	//36
		
		

		$sub_data = substr($data_raw, 133, 16);
		$stats = str_split($sub_data, 8);
		$stats = substr_replace($stats, ".", 7, 0);

		for ($i=0; $i<2; $i++)
		{
			$stats[$i] = ltrim($stats[$i], '0');
			if ($stats[$i][0] == '.')
				$stats[$i] = "0".$stats[$i];
		}
		
		//echo "<tr><td>Battery Charging Energy (Kwh)</td><td>";
		$array[] = $stats[0];	//37
		
		$embedded_array = array();
		
		//echo "<tr><td>Battery Discharging Energy (Kwh)</td><td>";
		$embedded_array[] = $stats[1];	//38 [0]

		
		
		$sub_data = substr($data_raw, 149, 10);
		$stats = str_split($sub_data, 2);

		$sub_data = unpack("n",$stats[0]);
		$sub_data = $sub_data[1]/10;
		//echo "<tr><td>DG2 frequency</td><td>";
		$embedded_array[] = $sub_data;	//38 [1]
		
		$sub_data = unpack("n",$stats[1]);
		$sub_data = $sub_data[1]/10;
		//echo "<tr><td>DG2-R phase Voltage</td><td>";
		$embedded_array[] = $sub_data;	//38 [2]
		
		$sub_data = unpack("n",$stats[2]);
		$sub_data = $sub_data[1]/10;
		//echo "<tr><td>DG2-Y phase Voltage</td><td>";
		$embedded_array[] = $sub_data;	//38 [3]
		
		$sub_data = unpack("n",$stats[3]);
		$sub_data = $sub_data[1]/10;
		//echo "<tr><td>DG2-B phase Voltage</td><td>";
		$embedded_array[] = $sub_data;	//38 [4]
		
		
		$sub_data = unpack("n",$stats[4]);
		$sub_data = $sub_data[1]/10;
		//echo "<tr><td>DG2 Battery Voltage</td><td>";
		$embedded_array[] = $sub_data;	//38 [5]
		
		
		$sub_data = substr($data_raw, 159, 16);
		$stats = str_split($sub_data, 8);
		$stats = substr_replace($stats, ".", 7, 0);

		for ($i=0; $i<2; $i++)
		{
			$stats[$i] = ltrim($stats[$i], '0');
			if ($stats[$i][0] == '.')
				$stats[$i] = "0".$stats[$i];
		}
		
		//echo "<tr><td>DG2 Running Hours (Hrs)</td><td>";
		$embedded_array[] = $stats[0];	//38 [6]
		
		//echo "<tr><td>DG2 Energy (Kwh)</td><td>";
		$embedded_array[] = $stats[1];	//38 [7] 
				
		
		$sub_data = substr($data_raw, 175, 1);
		$sub_data = (hexdec(bin2hex($sub_data)));	
		//echo "<tr><td>DG2 Fuel Level %</td><td>";
		$embedded_array[] = $sub_data;	//38 [8] 
		
		$sub_data = substr($data_raw, 176, 2);
		$sub_data = unpack("n",$sub_data);
		$sub_data = $sub_data[1]/10;
		//echo "<tr><td>DG2 Fuel Data (Litres)</td><td>";
		$embedded_array[] = $sub_data;	//38 [9] 
		
		
		$array[] = $embedded_array;	//38 -- Array of Extra elements beyond 37
		
		//var_dump($array);
	
		return $array;
	}
	
	
	function DeviceGetDataDevicePhase2($data_raw)
	{
		$array = array();

		$sub_data = substr($data_raw, 44, 36);
		$stats = str_split($sub_data, 2);
		//var_dump($stats);

		$sub_data = unpack("n", $stats[0]);
		$sub_data = $sub_data[1] / 10;

		//echo "<tr><td>Room Temperature</td><td>";
		//echo("$sub_data Deg C");
		//echo "</td></tr>";
		$array[] = $sub_data;	//8


		$sub_data = unpack("n", $stats[1]);
		$sub_data = $sub_data[1] / 10;
		//echo "<tr><td>Fuel Level</td><td>";
		//echo("$sub_data %");
		//echo "</td></tr>";
		$array[] = $sub_data;	//9

		$sub_data = unpack("n", $stats[2]);
		$sub_data = $sub_data[1] / 10;
		//echo "<tr><td>Fuel Data</td><td>";
		//echo("$sub_data Liters");
		//echo "</td></tr>";
		$array[] = $sub_data;	//10

		$sub_data = unpack("n", $stats[3]);
		$sub_data = $sub_data[1] / 10;
		//echo "<tr><td>Site Batt. bank Voltage</td><td>";
		//echo("$sub_data VDC");
		//echo "</td></tr>";
		$array[] = $sub_data;	//11

		$sub_data = unpack("n", $stats[4]);
		$sub_data = $sub_data[1] / 10;
		//echo "<tr><td>R-Phase current</td><td>";
		//echo("$sub_data");
		//echo "</td></tr>";
		$array[] = $sub_data;	//12

		$sub_data = unpack("n", $stats[5]);
		$sub_data = $sub_data[1] / 10;
		//echo "<tr><td>Y-Phase current</td><td>";
		//echo("$sub_data");
		//echo "</td></tr>";
		$array[] = $sub_data;	//13


		$sub_data = unpack("n", $stats[6]);
		$sub_data = $sub_data[1] / 10;
		//echo "<tr><td>B-Phase current</td><td>";
		//echo("$sub_data");
		//echo "</td></tr>";
		$array[] = $sub_data;	//14

		$sub_data = unpack("n", $stats[7]);
		$sub_data = $sub_data[1] / 10;
		//echo "<tr><td>Mains Frequency</td><td>";
		//echo("$sub_data");
		//echo "</td></tr>";
		$array[] = $sub_data;	//15

		$sub_data = unpack("n", $stats[8]);
		$sub_data = $sub_data[1] / 10;
		//echo "<tr><td>DG frequency</td><td>";
		//echo("$sub_data");
		//echo "</td></tr>";
		$array[] = $sub_data;	//16

		$sub_data = unpack("n", $stats[9]);
		$sub_data = $sub_data[1] / 10;
		//echo "<tr><td>DG-R phase Voltage</td><td>";
		//echo("$sub_data VAC");
		//echo "</td></tr>";
		$array[] = $sub_data;	//17

		$sub_data = unpack("n", $stats[10]);
		$sub_data = $sub_data[1] / 10;
		//echo "<tr><td>DG-Y phase Voltage</td><td>";
		//echo("$sub_data VAC");
		//echo "</td></tr>";
		$array[] = $sub_data;	//18

		$sub_data = unpack("n", $stats[11]);
		$sub_data = $sub_data[1] / 10;
		//echo "<tr><td>DG-B phase Voltage</td><td>";
		//echo("$sub_data VAC");
		//echo "</td></tr>";
		$array[] = $sub_data;	//19

		$sub_data = unpack("n", $stats[12]);
		$sub_data = $sub_data[1] / 10;
		//echo "<tr><td>LCU1 Output Voltage</td><td>";
		//echo("$sub_data VAC");
		//echo "</td></tr>";
		$array[] = $sub_data;	//20

		$sub_data = unpack("n", $stats[13]);
		$sub_data = $sub_data[1] / 10;
		//echo "<tr><td>LCU2 Output Voltage</td><td>";
		//echo("$sub_data VAC");
		//echo "</td></tr>";
		$array[] = $sub_data;	//21

		$sub_data = unpack("n", $stats[14]);
		$sub_data = $sub_data[1] / 10;
		//echo "<tr><td>LCU3 Output Voltage</td><td>";
		//echo("$sub_data VAC");
		//echo "</td></tr>";
		$array[] = $sub_data;	//22

		$sub_data = unpack("n", $stats[15]);
		$sub_data = $sub_data[1] / 10;
		//echo "<tr><td>Input Mains Voltage - R Phase</td><td>";
		//echo("$sub_data VAC");
		//echo "</td></tr>";
		$array[] = $sub_data;	//23

		$sub_data = unpack("n", $stats[16]);
		$sub_data = $sub_data[1] / 10;
		//echo "<tr><td>Input Mains Voltage - Y Phase</td><td>";
		//echo("$sub_data VAC");
		//echo "</td></tr>";
		$array[] = $sub_data;	//24

		$sub_data = unpack("n", $stats[17]);
		$sub_data = $sub_data[1] / 10;
		//echo "<tr><td>Input Mains Voltage - B Phase</td><td>";
		//echo("$sub_data VAC");
		//echo "</td></tr>";
		$array[] = $sub_data;	//25

		/////


		$sub_data = substr($data_raw, 80, 48);
		$stats = str_split($sub_data, 8);
		$stats = substr_replace($stats, ".", 7, 0);

		for ($i = 0; $i<6; $i++)
		{
			$stats[$i] = ltrim($stats[$i], '0');
			if ($stats[$i][0] == '.')
				$stats[$i] = "0".$stats[$i];
		}
		//var_dump($stats);
		//echo "<tr><td>DG Running Hours</td><td>";
		//echo("$stats[0] Hrs");
		//echo "</td></tr>";
		$array[] = $stats[0];	//26

		//echo "<tr><td>Mains RUN HOURS</td><td>";
		//echo("$stats[1] Hrs");
		//echo "</td></tr>";
		$array[] = $stats[1];	//27

		//echo "<tr><td>Batt RUN HOURS</td><td>";
		//echo("$stats[2] Hrs");
		//echo "</td></tr>";
		$array[] = $stats[2];	//28

		//echo "<tr><td>O/P Mains Energy</td><td>";
		//echo("$stats[3] Kwh");
		//echo "</td></tr>";
		$array[] = $stats[3];	//29

		//echo "<tr><td>DG Energy</td><td>";
		//echo("$stats[4] Kwh");
		//echo "</td></tr>";
		$array[] = $stats[4];	//30	

		//echo "<tr><td>I/P Mains Energy</td><td>";
		//echo("$stats[5] Kwh");
		//echo "</td></tr>";
		$array[] = $stats[5];	//31

		$sub_data = substr($data_raw, 128, 6);
		$stats = str_split($sub_data, 2);

		$sub_data = unpack("n", $stats[0]);
		$sub_data = $sub_data[1] / 10;
		//echo "<tr><td>DG Battery Voltage</td><td>";
		//echo("$sub_data VDC");
		//echo "</td></tr>";
		$array[] = $sub_data;	//32

		$sub_data = unpack("n", $stats[1]);
		$sub_data = $sub_data[1] / 10;
		//echo "<tr><td>Battery Charging current</td><td>";
		//echo("$sub_data Amp");
		//echo "</td></tr>";
		$array[] = $sub_data;	//33

		$sub_data = unpack("n", $stats[2]);
		$sub_data = $sub_data[1] / 10;
		//echo "<tr><td>Battery Discharging current</td><td>";
		//echo("$sub_data Amp");
		//echo "</td></tr>";
		$array[] = $sub_data;	//34

		$sub_data = substr($data_raw, 134, 2);
		$stats = str_split($sub_data, 1);

		$sub_data = (hexdec(bin2hex($stats[0])));
		//echo "<tr><td>Battery status</td><td>";
		//echo("$sub_data %");
		//echo "</td></tr>";
		$array[] = $sub_data;	//35

		$sub_data = (hexdec(bin2hex($stats[1])) / 10);
		//echo "<tr><td>Battery back up time</td><td>";
		//echo("$sub_data hours");
		//echo "</td></tr>";
		$array[] = $sub_data;	//36


		$sub_data = substr($data_raw, 136, 16);
		$stats = str_split($sub_data, 8);
		$stats = substr_replace($stats, ".", 7, 0);

		for ($i = 0; $i<2; $i++)
		{
			$stats[$i] = ltrim($stats[$i], '0');
			if ($stats[$i][0] == '.')
				$stats[$i] = "0".$stats[$i];
		}

		//echo "<tr><td>Battery Charging Energy</td><td>";
		//echo("$stats[0] Kwh");
		//echo "</td></tr>";
		$array[] = $stats[0];	//37
		
		
		$embedded_array = array();

		//echo "<tr><td>Battery Discharging Energy</td><td>";
		//echo("$stats[1] Kwh");
		//echo "</td></tr>";
		$embedded_array[] = $stats[1];	//38[0]

		//echo "<tr><td>CRC LOWER BYTE</td><td>";
		//echo(hexdec(bin2hex($data_raw[152])));
		//echo "</td></tr>";
		//$array[] = $stats[2];

		//echo "<tr><td>CRC HIGHER BYTE</td><td>";
		//echo(hexdec(bin2hex($data_raw[153])));
		//echo "</td></tr>";
		//$array[] = $stats[3];


		

		//echo "<tr><td></td><td>";
		//echo "DC Energy Meter Data";
		//echo "</td></tr>";

		


		//echo "<tr><td>Device 2 Id</td><td>";
		$embedded_array[] = (hexdec(bin2hex($data_raw[154])));	//38[1]
		//echo "</td></tr>";


		//echo "<tr><td>Function Code</td><td>";
		$embedded_array[] = (hexdec(bin2hex($data_raw[155])));	//38[2]
		//echo "</td></tr>";

		//echo "<tr><td># bytes</td><td>";
		$embedded_array[] = (hexdec(bin2hex($data_raw[156])));	//38[3]
		//echo "</td></tr>";



		//9 * 4 IEEE-754 32-bit float value
		$sub_data = substr($data_raw, 157, 36);
		$stats = str_split($sub_data, 4);

		for ($i = 0; $i<9; $i++)
		{
			$bin = "\x4A\x5B\x1B\x05";
			$tmp = unpack('f', strrev($stats[$i]));
			//$tmp = unpack('f', $stats[$i]);
			//echo $tmp[1];  // 3589825.25
			$stats[$i] = $tmp[1];
		}

		//echo "<tr><td>KWH of Channel 1</td><td>";
		$embedded_array[] = $stats[0];					//38[4]
		//echo "</td></tr>";

		//echo "<tr><td>Current of Channel 1</td><td>";
		$embedded_array[] = $stats[1];					//38[5]
		//echo "</td></tr>";

		//echo "<tr><td>KWH of Channel 2</td><td>";
		$embedded_array[] = $stats[2];					//38[6]
		//echo "</td></tr>";

		//echo "<tr><td>Current of Channel 2</td><td>";
		$embedded_array[] = $stats[3];					//38[7]
		//echo "</td></tr>";

		//echo "<tr><td>KWH of Channel 3</td><td>";
		$embedded_array[] = $stats[4];					//38[8]
		//echo "</td></tr>";

		//echo "<tr><td>Current of Channel 3</td><td>";
		$embedded_array[] = $stats[5];					//38[9]
		//echo "</td></tr>";

		//echo "<tr><td>KWH of Channel 4</td><td>";
		$embedded_array[] = $stats[6];					//38[10]
		//echo "</td></tr>";

		//echo "<tr><td>Current of Channel 4</td><td>";
		$embedded_array[] = $stats[7];					//38[11]
		//echo "</td></tr>";

		//echo "<tr><td>Voltage</td><td>";
		$embedded_array[] = $stats[8];					//38[12]
		//echo "</td></tr>";


		//echo "<tr><td>CRC LOWER BYTE</td><td>";
		//echo(hexdec(bin2hex($data_raw[193])));
		//echo "</td></tr>";

		//echo "<tr><td>CRC HIGHER BYTE</td><td>";
		//echo(hexdec(bin2hex($data_raw[194])));
		//echo "</td></tr>";
		
		$array[] = $embedded_array;	//38 -- Array of Extra elements beyond 37

		//var_dump($array);

		return $array;
	}


function DeviceGetDataDoubleBattery($data_raw)
{
    $array = array();
    $sub_data = substr($data_raw, 41, 22);
    $stats = str_split($sub_data, 2);
    //var_dump($stats);

    $sub_data = unpack("n",$stats[0]);
    $sub_data = $sub_data[1]/10;
    //echo "<tr><td>Room Temperature</td><td>";
    $array[] = $sub_data;	//8


    $sub_data = unpack("n",$stats[1]);
    $sub_data = $sub_data[1]/10;
    //echo "<tr><td>Site Battery Bank Voltage</td><td>";
    $array[] = $sub_data;	//9

    $sub_data = unpack("n",$stats[2]);
    $sub_data = $sub_data[1]/10;
    //echo "<tr><td>Site Batt. bank Voltage</td><td>";
    $array[] = $sub_data;	//10

    $sub_data = unpack("n",$stats[3]);
    $sub_data = $sub_data[1]/10;
    //echo "<tr><td>Load Current current</td><td>";
    $array[] = $sub_data;	//11

    $sub_data = unpack("n",$stats[4]);
    $sub_data = $sub_data[1]/10;
    //echo "<tr><td>Mains Voltage</td><td>";
    $array[] = $sub_data;	//12

    $sub_data = unpack("n",$stats[5]);
    $sub_data = $sub_data[1]/10;
    //echo "<tr><td>DG Voltage</td><td>";
    $array[] = $sub_data;	//13

    $sub_data = unpack("n",$stats[6]);
    $sub_data = $sub_data[1]/10;
    //echo "<tr><td>Input Mains Voltage B Phase</td><td>";
    $array[] = $sub_data;	//14

    $sub_data = unpack("n",$stats[7]);
    $sub_data = $sub_data[1]/10;
    //echo "<tr><td>Battery1 Charging Current</td><td>";
    $array[] = $sub_data;	//15

    $sub_data = unpack("n",$stats[8]);
    $sub_data = $sub_data[1]/10;
    //echo "<tr><td>Battery1 Discharging Current</td><td>";
    $array[] = $sub_data;	//16

    $sub_data = unpack("n",$stats[9]);
    $sub_data = $sub_data[1]/10;
    //echo "<tr><td>Battery2 Charging Current</td><td>";
    $array[] = $sub_data;	//17

    $sub_data = unpack("n",$stats[10]);
    $sub_data = $sub_data[1]/10;
    //echo "<tr><td>Battery2 Discharging Current</td><td>";
    $array[] = $sub_data;	//18


    $sub_data = substr($data_raw, 63, 2);
    $stats = str_split($sub_data, 1);

    //$sub_data = unpack("C",$stats[0]);
    $sub_data = hexdec(bin2hex($stats[0]));
    //echo "<tr><td>Battery1 Status</td><td>";
    $array[] = $sub_data;	//19

    //$sub_data = unpack("C",$stats[1]);
    $sub_data = hexdec(bin2hex($stats[1]));
   // echo "<tr><td>Battery2 Status</td><td>";
    $array[] = $sub_data;	//20


    /////

    //11*2	= 22
    //2 *1	= 02
    //----------
    //        66
    //----------

    //REV2 Total 66 Bytes


    return $array;
}

function DeviceGetDataDevice3($data_raw)
{
	$array = array();
	$sub_data = substr($data_raw, 41, 22);
	$stats = str_split($sub_data, 2);
	//var_dump($stats);

	$sub_data = unpack("n",$stats[0]);
	$sub_data = $sub_data[1]/10;
	//echo "<tr><td>Room Temperature</td><td>";
	$array[] = $sub_data;	//8


	$sub_data = unpack("n",$stats[1]);
	$sub_data = $sub_data[1]/10;
	//echo "<tr><td>Site Battery Bank Voltage</td><td>";
	$array[] = $sub_data;	//9

	$sub_data = unpack("n",$stats[2]);
	$sub_data = $sub_data[1]/10;
	//echo "<tr><td>Site Batt. bank Voltage</td><td>";
	$array[] = $sub_data;	//10

	$sub_data = unpack("n",$stats[3]);
	$sub_data = $sub_data[1]/10;
	//echo "<tr><td>Total Load Current current</td><td>";
	$array[] = $sub_data;	//11

	$sub_data = unpack("n",$stats[4]);
	$sub_data = $sub_data[1]/10;
	//echo "<tr><td>Mains Voltage-R Phase</td><td>";
	$array[] = $sub_data;	//12

	$sub_data = unpack("n",$stats[5]);
	$sub_data = $sub_data[1]/10;
	//echo "<tr><td>DG Voltage-Y Phase</td><td>";
	$array[] = $sub_data;	//13

	$sub_data = unpack("n",$stats[6]);
	$sub_data = $sub_data[1]/10;
	//echo "<tr><td>Input Mains Voltage-B Phase Phase</td><td>";
	$array[] = $sub_data;	//14

	$sub_data = unpack("n",$stats[7]);
	$sub_data = $sub_data[1]/10;
	//echo "<tr><td>Battery1 Charging Current</td><td>";
	$array[] = $sub_data;	//15

	$sub_data = unpack("n",$stats[8]);
	$sub_data = $sub_data[1]/10;
	//echo "<tr><td>Battery1 Discharging Current</td><td>";
	$array[] = $sub_data;	//16

	$sub_data = unpack("n",$stats[9]);
	$sub_data = $sub_data[1]/10;
	//echo "<tr><td>Battery2 Charging Current</td><td>";
	$array[] = $sub_data;	//17

	$sub_data = unpack("n",$stats[10]);
	$sub_data = $sub_data[1]/10;
	//echo "<tr><td>Battery2 Discharging Current</td><td>";
	$array[] = $sub_data;	//18


	$sub_data = substr($data_raw, 63, 4);
	$stats = str_split($sub_data, 2);

	//$sub_data = unpack("C",$stats[0]);
	$sub_data = hexdec(bin2hex($stats[0]));
	//echo "<tr><td>Battery1 Status</td><td>";
	$array[] = $sub_data;	//19

	//$sub_data = unpack("C",$stats[1]);
	$sub_data = hexdec(bin2hex($stats[1]));
	// echo "<tr><td>Battery2 Status</td><td>";
	$array[] = $sub_data;	//20

    $sub_data = substr($data_raw, 67, 40);
    $stats = str_split($sub_data, 8);
    $stats = substr_replace($stats, ".", 7, 0);

    for ($i=0; $i<6; $i++)
    {
        $stats[$i] = ltrim($stats[$i], '0');
        if ($stats[$i][0] == '.')
            $stats[$i] = "0".$stats[$i];
    }

    //echo "<tr><td>Mains Energy</td><td>";
    $array[] = $stats[0];	//21

    //echo "<tr><td>Battery#1 CHG Energy</td><td>";
    $array[] = $stats[1];	//22

    //echo "<tr><td>Battery#1 DISCHG Energy </td><td>";
    $array[] = $stats[2];	//23

    //echo "<tr><td>Battery#2 CHG Energy </td><td>";
    $array[] = $stats[3];	//24

    //echo "<tr><td>Battery#2 DISCHG Energy </td><td>";
    $array[] = $stats[4];	//25


	/////
            //41
	//11*2	= 22
	//2 *2	= 04
    //5*8   = 40
	//----------
	//       107
	//----------

	//REV2 Total 66 Bytes


	return $array;
}

	