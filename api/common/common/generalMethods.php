<?php

function isNullOrWhiteSpace($question){
    return (!isset($question) || trim($question)==='');
}

//32-bit binary bits from text 10101010 flag
function packBinaryFlags($flags)
{
    //var_dump($flags);
    $flags_binary = "0000";
    
    $stats = unpack("C*", $flags_binary);
    $stats[1] = 0;
    $stats[2] = 0;
    $stats[3] = 0;
    $stats[4] = 0;
    
    //Byte 1
    if (substr($flags, 0, 1) == "1")
        $stats[1] = $stats[1] | 0x80;
        
    if (substr($flags, 1, 1) == "1")
        $stats[1] = $stats[1] | 0x40;
    
    if (substr($flags, 2, 1) == "1")
        $stats[1] = $stats[1] | 0x20;

    if (substr($flags, 3, 1) == "1")
        $stats[1] = $stats[1] | 0x10;			
        
    if (substr($flags, 4, 1) == "1")
        $stats[1] = $stats[1] | 0x08;
        
    if (substr($flags, 5, 1) == "1")
        $stats[1] = $stats[1] | 0x04;
    
    if (substr($flags, 6, 1) == "1")
        $stats[1] = $stats[1] | 0x02;

    if (substr($flags, 7, 1) == "1")
        $stats[1] = $stats[1] | 0x01;
        
    //Byte 2
    if (substr($flags, 8, 1) == "1")
        $stats[2] = $stats[2] | 0x80;
        
    if (substr($flags, 9, 1) == "1")
        $stats[2] = $stats[2] | 0x40;
    
    if (substr($flags, 10, 1) == "1")
        $stats[2] = $stats[2] | 0x20;

    if (substr($flags, 11, 1) == "1")
        $stats[2] = $stats[2] | 0x10;			
        
    if (substr($flags, 12, 1) == "1")
        $stats[2] = $stats[2] | 0x08;
        
    if (substr($flags, 13, 1) == "1")
        $stats[2] = $stats[2] | 0x04;
    
    if (substr($flags, 14, 1) == "1")
        $stats[2] = $stats[2] | 0x02;

    if (substr($flags, 15, 1) == "1")
        $stats[2] = $stats[2] | 0x01;
        
    //Byte 3
    if (substr($flags, 16, 1) == "1")
        $stats[3] = $stats[3] | 0x80;
        
    if (substr($flags, 17, 1) == "1")
        $stats[3] = $stats[3] | 0x40;
    
    if (substr($flags, 18, 1) == "1")
        $stats[3] = $stats[3] | 0x20;

    if (substr($flags, 19, 1) == "1")
        $stats[3] = $stats[3] | 0x10;			
        
    if (substr($flags, 20, 1) == "1")
        $stats[3] = $stats[3] | 0x08;
        
    if (substr($flags, 21, 1) == "1")
        $stats[3] = $stats[3] | 0x04;
    
    if (substr($flags, 22, 1) == "1")
        $stats[3] = $stats[3] | 0x02;

    if (substr($flags, 23, 1) == "1")
        $stats[3] = $stats[3] | 0x01;
        
    //Byte 4
    if (substr($flags, 24, 1) == "1")
        $stats[4] = $stats[4] | 0x80;
        
    if (substr($flags, 25, 1) == "1")
        $stats[4] = $stats[4] | 0x40;
    
    if (substr($flags, 26, 1) == "1")
        $stats[4] = $stats[4] | 0x20;

    if (substr($flags, 27, 1) == "1")
        $stats[4] = $stats[4] | 0x10;			
        
    if (substr($flags, 28, 1) == "1")
        $stats[4] = $stats[4] | 0x08;
        
    if (substr($flags, 29, 1) == "1")
        $stats[4] = $stats[4] | 0x04;
    
    if (substr($flags, 30, 1) == "1")
        $stats[4] = $stats[4] | 0x02;

    if (substr($flags, 31, 1) == "1")
        $stats[4] = $stats[4] | 0x01;
        
    $output = pack("CCCC", $stats[1], $stats[2],$stats[3],$stats[4]);

    return $output;
    
}

function unpackBinaryFlag($flags, $flag_constant)
{
    if (is_string($flags))
    {
        $tmp = unpack("N", $flags);
        $flags = $tmp[1];
        //echo(dechex($flags));
    }
    //var_dump($flags);
    $tmp = $flags & $flag_constant;
    return ($tmp != 0);
}

// IProtect Flags
$FLAG_1_IPROTECT_DEVICE     = 0x80000000;
$FLAG_2_IPROTECT_SIREN      = 0x40000000;
$FLAG_3_IPROTECT_PIR 	    = 0x20000000;
$FLAG_4_IPROTECT_DOOR       = 0x10000000;    
$FLAG_5_IPROTECT_SMOKE      = 0x08000000;

// Sensor Flags
$FLAG_1_SENSOR_DEVICE       = 0x80000000;
$FLAG_2_SENSOR_PIR          = 0x40000000;
$FLAG_3_SENSOR_DOOR         = 0x20000000;
$FLAG_4_SENSOR_FIRE         = 0x10000000;    
$FLAG_5_SENSOR_SIREN_ON     = 0x08000000;
$FLAG_6_SENSOR_SIREN_OFF    = 0x04000000;

?>