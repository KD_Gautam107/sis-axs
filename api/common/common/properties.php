<?php

	include_once $_SERVER['DOCUMENT_ROOT'] . 'api/common/paths.php';
	include_once $pathDbinc;
	
	function checkAlarms($status) {
        $alarms = array();
        $stats = unpack("C*", $status);

        //		var_dump($stats);
        //		var_dump($stats[5] & 0x01);
        //		var_dump($stats[5] & 0x02);
        //		var_dump($stats[5] & 0x04);
        //		var_dump($stats[5] & 0x08);
        //		var_dump($stats[5] & 0x10);
        //		var_dump($stats[5] & 0x20);
        //		var_dump($stats[5] & 0x40);
        //		var_dump($stats[5] & 0x80);
        
        //0x0000000000 Smoke fire    0 means No alarm,  1 means Alarm
        $alarms['smoke'] = ($stats[5] & 0x01) ? 1 : 0;

        //Door Alarm Only Applicable if Indoor
        if ($indoor_type == 0) {
            //$FLAG_01_DOOR 	= 0x0000000002;	//Door Open     0  means  Door Close , 1 Means  Door open
            $alarms['door'] = ($stats[5] & 0x02) ? 1 : 0;
        }
            
        //$FLAG_02_AUTO 	= 0x0000000004;	//Auto/Man Mode 0 Means  Auto Mode, 1 Means  Man Mode
        $alarms['mode'] = ($stats[5] & 0x04) ? 1 : 0;
            
        //$FLAG_034_LOAD 	= 0x0000000018;	//00    :Load on EB,01: Load on DG, 10: Load on site Battery   11: Not used
        if (($stats[5] & 0x08) == false) {
            if (($stats[5] & 0x10) == false) {
                echo "[- Load on EB -]<br>";
            }				//00
            else {
                echo "[- Load on site Battery -]<br>";
            }	//10
        } else {
            if (($stats[5] & 0x10) == false) {
                echo "[- Load on DG -]<br>";
            }				//01
            else {
                echo "[- Not Used -]<br>";
            }	//11
        }
        echo "</td></tr>";
            
        echo "<tr><td>Mains Fail</td><td";
        //$FLAG_05_MAINS 	= 0x0000000020;	//Mains fail	1 Means main Fail,  0 Means  Main healthy
        if (($stats[5] & 0x20) == false) {
            echo "  ><img id='led_white' src='/media/img_trans.gif'>";
        } else {
            echo "  ><img id='led_red' src='/media/img_trans.gif'>";
        }
        echo "</td></tr>";
            
        echo "<tr><td>Site Battery Low</td><td";
        //$FLAG_06_BATT 	= 0x0000000040;	//Site battery Low  1 Means Site Battery Low ,0 Means  Battery Ok
        if (($stats[5] & 0x40) == false) {
            echo "  ><img id='led_white' src='/media/img_trans.gif'>";
        } else {
            echo "  ><img id='led_red' src='/media/img_trans.gif'>";
        }
        echo "</td></tr>";
            
            
        echo "<tr><td>Room Temperature</td><td";
        //$FLAG_07_TEMP 	= 0x0000000080;	//Room Temperature  1 Means high Temperature, 0 Means Normal temp
        if (($stats[5] & 0x80) == false) {
            echo "  ><img id='led_white' src='/media/img_trans.gif'>";
        } else {
            echo "  ><img id='led_red' src='/media/img_trans.gif'>";
        }
        echo "</td></tr>";
            
            
        echo "<tr><td>LLOP</td><td";
        //$FLAG_08_LLOP 	= 0x0000000100;	//LLOP
        if (($stats[4] & 0x01) == false) {
            echo "  ><img id='led_white' src='/media/img_trans.gif'>";
        } else {
            echo "  ><img id='led_red' src='/media/img_trans.gif'>";
        }
        echo "</td></tr>";
            
            
        echo "<tr><td>HCT/HWT</td><td";
        //$FLAG_09_HCT 	= 0x0000000200;	//HCT/HWT
        if (($stats[4] & 0x02) == false) {
            echo "  ><img id='led_white' src='/media/img_trans.gif'>";
        } else {
            echo "  ><img id='led_red' src='/media/img_trans.gif'>";
        }
        echo "</td></tr>";
            
            
        echo "<tr><td>Alternator Fault</td><td";
        //$FLAG_10_ALT 	= 0x0000000400;	//Alternator Fault
        if (($stats[4] & 0x04) == false) {
            echo "  ><img id='led_white' src='/media/img_trans.gif'>";
        } else {
            echo "  ><img id='led_red' src='/media/img_trans.gif'>";
        }
        echo "</td></tr>";
            
            
        echo "<tr><td>DG Over Speed</td><td";
        //$FLAG_11_DG 	= 0x0000000800;	//DG Over Speed
        if (($stats[4] & 0x08) == false) {
            echo "  ><img id='led_white' src='/media/img_trans.gif'>";
        } else {
            echo "  ><img id='led_red' src='/media/img_trans.gif'>";
        }
        echo "</td></tr>";
            
            
        echo "<tr><td>DG Over Load</td><td";
        //$FLAG_12_DG 	= 0x0000001000;	//DG Over Load
        if (($stats[4] & 0x10) == false) {
            echo "  ><img id='led_white' src='/media/img_trans.gif'>";
        } else {
            echo "  ><img id='led_red' src='/media/img_trans.gif'>";
        }
        echo "</td></tr>";
            
            
        echo "<tr><td>DG Low Fuel</td><td";
        //$FLAG_13_DG 	= 0x0000002000;	//DG Low Fuel
        if (($stats[4] & 0x20) == false) {
            echo "  ><img id='led_white' src='/media/img_trans.gif'>";
        } else {
            echo "  ><img id='led_red' src='/media/img_trans.gif'>";
        }
        echo "</td></tr>";
            
            
        echo "<tr><td>DG Start Fail</td><td";
        //$FLAG_14_DG 	= 0x0000004000;	//DG Start Fail
        if (($stats[4] & 0x40) == false) {
            echo "  ><img id='led_white' src='/media/img_trans.gif'>";
        } else {
            echo "  ><img id='led_red' src='/media/img_trans.gif'>";
        }
        echo "</td></tr>";
            
            
        echo "<tr><td>DG Stop Fail</td><td";
        //$FLAG_15_DG 	= 0x0000008000;	//DG Stop Fail
        if (($stats[4] & 0x80) == false) {
            echo "  ><img id='led_white' src='/media/img_trans.gif'>";
        } else {
            echo "  ><img id='led_red' src='/media/img_trans.gif'>";
        }
        echo "</td></tr>";
            
            
        echo "<tr><td>DG Battery</td><td";
        //$FLAG_16_DG 	= 0x0000010000;	//DG battery Low
        if (($stats[3] & 0x01) == false) {
            echo "  ><img id='led_white' src='/media/img_trans.gif'>";
        } else {
            echo "  ><img id='led_red' src='/media/img_trans.gif'>";
        }
        echo "</td></tr>";
            
            
        echo "<tr><td>LCU Fail</td><td";
        //$FLAG_17_LCU 	= 0x0000020000;	//LCU fail
        if (($stats[3] & 0x02) == false) {
            echo "  ><img id='led_green' src='/media/img_trans.gif'>";
        } else {
            echo "  ><img id='led_red' src='/media/img_trans.gif'>";
        }
        echo "</td></tr>";
            
            
        echo "<tr><td>Rectifier Fail</td><td";
        //$FLAG_18_RECT 	= 0x0000040000;	//Rectifier Fail
        if (($stats[3] & 0x04) == false) {
            echo "  ><img id='led_green' src='/media/img_trans.gif'>";
        } else {
            echo "  ><img id='led_red' src='/media/img_trans.gif'>";
        }
        echo "</td></tr>";
            
            
        echo "<tr><td>Multi Rectifier Fail</td><td";
        //$FLAG_19_RECT 	= 0x0000080000;	//Multi Rectifier Fail
        if (($stats[3] & 0x08) == false) {
            echo "  ><img id='led_green' src='/media/img_trans.gif'>";
        } else {
            echo "  ><img id='led_red' src='/media/img_trans.gif'>";
        }
        echo "</td></tr>";
            
            
        echo "<tr><td>LVD Trip</td><td";
        //$FLAG_20_LVD 	= 0x0000100000;	//LVD TRIP
        if (($stats[3] & 0x10) == false) {
            echo "  ><img id='led_green' src='/media/img_trans.gif'>";
        } else {
            echo "  ><img id='led_red' src='/media/img_trans.gif'>";
        }
        echo "</td></tr>";
            
            
        echo "<tr><td>LVD BY PASS</td><td";
        //$FLAG_21_LVD 	= 0x0000200000;	//LVD BY pass
        if (($stats[3] & 0x20) == false) {
            echo "  ><img id='led_green' src='/media/img_trans.gif'>";
        } else {
            echo "  ><img id='led_red' src='/media/img_trans.gif'>";
        }
        echo "</td></tr>";
            
            
        //$FLAG_22_RES 	= 0x0000400000;	//Reserved
        //$FLAG_23_RES 	= 0x0000800000;	//Reserved
        //$FLAG_24_RES 	= 0x0001000000;	//Reserved
        //$FLAG_25_RES 	= 0x0002000000;	//Reserved
        //$FLAG_26_RES 	= 0x0004000000;	//Reserved
        //$FLAG_27_RES 	= 0x0008000000;	//Reserved
        //$FLAG_28_RES 	= 0x0010000000;	//Reserved
        //$FLAG_29_RES 	= 0x0020000000;	//Reserved
        //$FLAG_30_RES 	= 0x0040000000;	//Reserved
        //$FLAG_31_RES 	= 0x0080000000;	//Reserved
            
        echo "<tr><td>DG OFF</td><td";
        //$FLAG_32_DG 	= 0x0100000000;	//DG OFF
        if (($stats[1] & 0x01) == false) {
            echo "  ><img id='led_white' src='/media/img_trans.gif'>";
        } else {
            echo "  ><img id='led_green' src='/media/img_trans.gif'>";
        }
        echo "</td></tr>";
            
            
        echo "<tr><td>DG ON</td><td";
        //$FLAG_33_DG 	= 0x0200000000;	//DG ON
        if (($stats[1] & 0x02) == false) {
            echo "  ><img id='led_white' src='/media/img_trans.gif'>";
        } else {
            echo "  ><img id='led_green' src='/media/img_trans.gif'>";
        }
        echo "</td></tr>";
            
            
        echo "<tr><td>DG Cranking</td><td";
        //$FLAG_34_DG 	= 0x0400000000;	//DG Cranking
        if (($stats[1] & 0x04) == false) {
            echo "  ><img id='led_white' src='/media/img_trans.gif'>";
        } else {
            echo "  ><img id='led_green' src='/media/img_trans.gif'>";
        }
        echo "</td></tr>";
            
            
        echo "<tr><td>DG Start in Progress</td><td";
        //$FLAG_35_DG 	= 0x0800000000;	//DG Start in Progress
        if (($stats[1] & 0x08) == false) {
            echo "  ><img id='led_white' src='/media/img_trans.gif'>";
        } else {
            echo "  ><img id='led_green' src='/media/img_trans.gif'>";
        }
        echo "</td></tr>";
            
            
        echo "<tr><td>DG Cool Down (Idle Running)</td><td";
        //$FLAG_36_DG 	= 0x1000000000;	//DG Cool Down (Idle Running)
        if (($stats[1] & 0x10) == false) {
            echo "  ><img id='led_white' src='/media/img_trans.gif'>";
        } else {
            echo "  ><img id='led_green' src='/media/img_trans.gif'>";
        }
        echo "</td></tr>";
            
            
        echo "<tr><td>DG STOP  Normally</td><td";
        //$FLAG_37_DG 	= 0x2000000000;	//DG STOP  Normally
        if (($stats[1] & 0x20) == false) {
            echo "  ><img id='led_white' src='/media/img_trans.gif'>";
        } else {
            echo "  ><img id='led_green' src='/media/img_trans.gif'>";
        }
        echo "</td></tr>";
            
            
        echo "<tr><td>DG STOP due to Fault</td><td";
        //$FLAG_38_DG 	= 0x8000000000;	//DG STOP due to Fault
        if (($stats[1] & 0x40) == false) {
            echo "  ><img id='led_white' src='/media/img_trans.gif'>";
        } else {
            echo "  ><img id='led_green' src='/media/img_trans.gif'>";
        }
        echo "</td></tr>";
            
            
        echo "<tr><td>DG Stop Due to Maximum Run Expiry</td><td";
        //$FLAG_39_DG 	= 0x8000000000;	//DG Stop Due to Maximum Run Exipary
        if (($stats[1] & 0x80) == false) {
            echo "  ><img id='led_white' src='/media/img_trans.gif'>";
        } else {
            echo "  ><img id='led_green' src='/media/img_trans.gif'>";
        }
        echo "</td></tr>";
	}
	
	function AddUp($array, $sum)
	{
		$array1 = array();
		
		if ($sum == null)
		{
			$array1[]	= abs($array[10]);
			$array1[]	= abs($array[11]);
			$array1[]	= abs($array[12]);
			$array1[]	= abs($array[13]);
			$array1[]	= abs($array[17]);
		
			//var_dump($array);
		}
		else
		{
			$array1[]	= abs($array[10])+$sum[0];
			$array1[]	= abs($array[11])+$sum[1];
			$array1[]	= abs($array[12])+$sum[2];
			$array1[]	= abs($array[13])+$sum[3];
			$array1[]	= abs($array[17])+$sum[4];
	
			//var_dump($array);
			//var_dump($sum);
		}
		return $array1;
    }
    

    function deviceGetHybridData($data_raw)
    {
		$array = array();
		$sub_data = substr($data_raw, 41, 36);
		$stats = str_split($sub_data, 2);
		//var_dump($stats);
		
		$sub_data = unpack("n",$stats[0]);
		$sub_data = $sub_data[1]/10;
		//echo "<tr><td>Room Temperature</td><td>";
		$array['temperature'] = (string)$sub_data;	//8
		
		
		$sub_data = unpack("n",$stats[1]);
		$sub_data = $sub_data[1]/10;
		//echo "<tr><td>Fuel Level</td><td>";
		$array['fuelLevel'] = (string)$sub_data;	//9
		
		$sub_data = unpack("n",$stats[2]);
		$sub_data = $sub_data[1]/10;
		//echo "<tr><td>Fuel Data</td><td>";
		$array['fuelData'] = (string)$sub_data;	//10
		
		$sub_data = unpack("n",$stats[3]);
		$sub_data = $sub_data[1]/10;
		//echo "<tr><td>Site Batt. bank Voltage</td><td>";
		$array['siteBatteryBankVoltage'] = (string)$sub_data;	//11
		
		$sub_data = unpack("n",$stats[4]);
		$sub_data = $sub_data[1]/10;
		//echo "<tr><td>R-Phase current</td><td>";
		$array['rPhaseCurrent'] = (string)$sub_data;	//12
		
		$sub_data = unpack("n",$stats[5]);
		$sub_data = $sub_data[1]/10;
		//echo "<tr><td>Y-Phase current</td><td>";
		$array['yPhaseCurrent'] = (string)$sub_data;	//13
		
		$sub_data = unpack("n",$stats[6]);
		$sub_data = $sub_data[1]/10;
		//echo "<tr><td>B-Phase current</td><td>";
		$array['bPhaseCurrent'] = (string)$sub_data;	//14
		
		$sub_data = unpack("n",$stats[7]);
		$sub_data = $sub_data[1]/10;
		//echo "<tr><td>Mains Frequency</td><td>";
		$array['mainsFrequency'] = (string)$sub_data;	//15
		
		$sub_data = unpack("n",$stats[8]);
		$sub_data = $sub_data[1]/10;
		//echo "<tr><td>DG frequency</td><td>";
		$array['dgFrequency'] = (string)$sub_data;	//16
		
		$sub_data = unpack("n",$stats[9]);
		$sub_data = $sub_data[1]/10;
		//echo "<tr><td>DG-R phase Voltage</td><td>";
		$array['dgRPhaseVoltage'] = (string)$sub_data;	//17
		
		$sub_data = unpack("n",$stats[10]);
		$sub_data = $sub_data[1]/10;
		//echo "<tr><td>DG-Y phase Voltage</td><td>";
		$array['dgYPhaseVoltage'] = (string)$sub_data;	//18
		
		$sub_data = unpack("n",$stats[11]);
		$sub_data = $sub_data[1]/10;
		//echo "<tr><td>DG-B phase Voltage</td><td>";
		$array['dgBPhaseVoltage'] = (string)$sub_data;	//19
		
		$sub_data = unpack("n",$stats[12]);
		$sub_data = $sub_data[1]/10;
		//echo "<tr><td>LCU1 Output Voltage</td><td>";
		$array['lcu1OutputVoltage'] = (string)$sub_data;	//20
		
		$sub_data = unpack("n",$stats[13]);
		$sub_data = $sub_data[1]/10;
		//echo "<tr><td>LCU2 Output Voltage</td><td>";
		$array['lcu2OutputVoltage'] = (string)$sub_data;	//21
		
		$sub_data = unpack("n",$stats[14]);
		$sub_data = $sub_data[1]/10;
		//echo "<tr><td>LCU3 Output Voltage</td><td>";
		$array['lcu3OutputVoltage'] = (string)$sub_data;	//22
		
		$sub_data = unpack("n",$stats[15]);
		$sub_data = $sub_data[1]/10;
		//echo "<tr><td>Input Mains Voltage - R Phase</td><td>";
		$array['inputMainsVoltageRPhase'] = (string)$sub_data;	//23
		
		$sub_data = unpack("n",$stats[16]);
		$sub_data = $sub_data[1]/10;
		//echo "<tr><td>Input Mains Voltage - Y Phase</td><td>";
		$array['inputMainsVoltageYPhase'] = (string)$sub_data;	//24
		
		$sub_data = unpack("n",$stats[17]);
		$sub_data = $sub_data[1]/10;
		//echo "<tr><td>Input Mains Voltage - B Phase</td><td>";
		$array['inputMainsVoltageBPhase'] = (string)$sub_data;	//25
		
		/////
		
		
		$sub_data = substr($data_raw, 77, 48);
		$stats = str_split($sub_data, 8);
		$stats = substr_replace($stats, ".", 7, 0);
		
		for ($i=0; $i<6; $i++)
		{
			$stats[$i] = ltrim($stats[$i], '0');
			if ($stats[$i][0] == '.')
				$stats[$i] = "0".$stats[$i];
		}
		
		//echo "<tr><td>DG Running Hours</td><td>";
		$array['dgRunningHours'] = $stats[0];	//26
		
		//echo "<tr><td>Mains RUN HOURS</td><td>";
		$array['mainsRunHours'] = $stats[1];	//27
		
		//echo "<tr><td>Batt RUN HOURS</td><td>";
		$array['batteryRunHours'] = $stats[2];	//28
		
		//echo "<tr><td>O/P Mains Energy</td><td>";
		$array['outputMainsEnergy'] = $stats[3];	//29
		
		//echo "<tr><td>DG Energy</td><td>";
		$array['dgEnergy'] = $stats[4];	//30
		
		//echo "<tr><td>I/P Mains Energy</td><td>";
		$array['inputMainsEnergy'] = $stats[5];	//31
		
		$sub_data = substr($data_raw, 125, 2);
		
		$sub_data = unpack("n",$sub_data);
		$sub_data = $sub_data[1]/10;
		//echo "<tr><td>DG Battery Voltage</td><td>";
		$array['dgBatteryVoltage'] = (string)$sub_data;	//32


		//18*2	= 36
		//6 *8	= 48
		//1 *2	= 02
		//----------
		//        86
		//----------
		
		
		//Updated Data - Updated Protocol
		
		//18*2	= 36
		//6 *8	= 48
		//1 *2	= 02
		//2 *2	= 04	*
		//2 *1	= 02	*
		//----------
		//        92
		//----------
		
		//REV2 Total 134 Bytes
		if (strlen($data_raw) >= 134)
		{
			$sub_data = substr($data_raw, 127, 4);
			$stats = str_split($sub_data, 2);
			
			$sub_data = unpack("n",$stats[0]);
			$sub_data = $sub_data[1]/10;
			//echo "<tr><td>Battery Charging current</td><td>";
			$array['batteryChargingCurrent'] = (string)$sub_data;	//33
			
			
			$sub_data = unpack("n",$stats[1]);
			$sub_data = $sub_data[1]/10;
			//echo "<tr><td>Battery Discharging current</td><td>";
			$array['batteryDischargingCurrent'] = (string)$sub_data;	//34
			
			$sub_data = substr($data_raw, 131, 2);
			$stats = str_split($sub_data, 1);
			
			$sub_data = unpack("c",$stats[0]);
			$sub_data = (hexdec(bin2hex($stats[0])));	//$sub_data[1]/1;
			//echo "<tr><td>Battery status</td><td>";
			$array['batteryStatus'] = (string)$sub_data;	//35
			
			$sub_data = unpack("c",$stats[1]);
			$sub_data = (hexdec(bin2hex($stats[1]))/10);	//$sub_data[1]/10;
			//echo "<tr><td>Battery back up time</td><td>";
			$array['batteryBackupTime'] = (string)$sub_data;	//36
			
			
			//REV3	Total 150 Bytes
			if (strlen($data_raw) >= 150)
			{
				$sub_data = substr($data_raw, 133, 16);
				$stats = str_split($sub_data, 8);
				$stats = substr_replace($stats, ".", 7, 0);
		
				for ($i=0; $i<2; $i++)
				{
					$stats[$i] = ltrim($stats[$i], '0');
					if ($stats[$i][0] == '.')
						$stats[$i] = "0".$stats[$i];
				}
				
				//echo "<tr><td>Battery Charging Energy (Kwh)</td><td>";
				$array['batteryChargingEnergyKwh'] = $stats[0];	//37
				
				//echo "<tr><td>Battery Discharging Energy (Kwh)</td><td>";
				$array['batteryDischargingEnergyKwh'] = $stats[1];	//38
			}
			else
			{
				$array[] = 0;	//37
				$array[] = 0;	//38
			}
		}
		else
		{
			$array[] = 0;	//33
			$array[] = 0;	//34
			$array[] = 0;	//35
			$array[] = 0;	//36
			$array[] = 0;	//37
			$array[] = 0;	//38
		}
	
		return $array;
    }
