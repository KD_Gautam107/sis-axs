<?php
// response headers
header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json; charset=UTF-8');
header('Access-Control-Allow-Methods: POST');
header('Access-Control-Max-Age: 3600');
header('Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With');

// include statements
include_once $_SERVER['DOCUMENT_ROOT'] . 'api/common/paths.php';
include_once $pathUsersController;
include_once $pathSitesController;
include_once $pathCallingController;

// frombody post request data
$postObject = file_get_contents('php://input');

// attribute routing enabled
switch (ltrim(split('\?', $_SERVER['REQUEST_URI'])[0], '/')) {

    //usersController
    //GET api/verifyCredentials?username=username&password=password
    case 'api/verifyCredentials':
        echo json_encode(verifyCredentials($_GET['username'], $_GET['password'], $_GET['token']));
        break;

    //sitesController
    //GET api/sites?username=username&password=password
    case 'api/sites':
        echo json_encode(sites($_GET['username'], $_GET['password'], $_GET['token']));
        break;

    //sitesController
    //GET api/hybrids?username=username&password=password&siteId=1
    case 'api/hybrid':
        echo json_encode(hybrid($_GET['username'], $_GET['password'], $_GET['quantaId']));
        break;

    case 'api/quanta':
        echo json_encode(quanta());
        break;

    case 'api/powerSupply':
        echo json_encode(powerSupply($_GET['username'], $_GET['password'], $_GET['siteNo'], $_GET['startDate'], $_GET['endDate']));
        break;

    case 'api/calling':
        echo json_encode(calling($_SERVER['REQUEST_URI']));
        break;

    default:
        echo json_encode('Error 404: Not Found!');
        die();
        break;
}
