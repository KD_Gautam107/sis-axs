<?php
session_start();
if (!isset($_SESSION['login_user']))
{
	header("Location: login.php");
}

if($_SERVER["REQUEST_METHOD"] == "POST")
{
	$_SESSION['filter_data_type'] = $_POST['filter_data_type'];
	

}

include 'common.php';

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Equipment Data</title>

    <!-- Bootstrap -->
    <!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">

	<!-- Optional theme -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css">

	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
	
	<!-- Latest compiled and minified JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
	

	
	<script>
	
		function ShowDetail(id){
			$("#myModalLabel").html("Equipment Status");
			$.ajax({url: "detail.php?id="+id, success: function(result){
				$("#id_device_detail").html(result);
				$('#myModal').modal('show');
				
			}});
		}
		
		function GenStartStop(i, id){
			$("#myModalLabel").html("Generator Control");
			$.ajax({url: "genstartstop.php?operation="+i+"&id="+id, success: function(result){
				$("#id_device_detail").html(result);
				$('#myModal').modal('show');
			}});
		}
		
		$('#myModal').on('hidden.bs.modal', function (e) {
			$("#id_device_detail").html('');
		})
	
	</script>
	
	<style>
		#led_white {
			width: 28px;
			height: 28px;
			background: url(media/leds.png) 0 0;
		}

		#led_red {
			width: 28px;
			height: 28px;
			background: url(media/leds.png) -30px 0;
		}
		
		#led_green {
			width: 28px;
			height: 28px;
			background: url(media/leds.png) -124px 0;
		}
		
	</style>
	
  </head>
  <body>
    
	<?php $page_title="Equipment Data"; include 'header.php'; ?>
	
			 
<?php 

	include 'dbinc.php';

	$rec_limit = 10;

	$mysqli = new mysqli("$mysql_hostname", "$mysql_user", "$mysql_password", "$mysql_database");
	if (mysqli_connect_errno()) {
		printf("Connect failed: %s\n", mysqli_connect_error());
		exit();
	}
	
	
	//Get the Customer Id
	//echo "Cus Id: ";
	//echo $_SESSION['login_cust_id'];
	//echo "<br>";
	
	$sql = "SELECT site_id FROM siteinfo WHERE CustomerId=".$_SESSION['login_cust_id'];
	//$result = $mysqli->query($sql);
	//while($data = $result->fetch_row()){
	//	echo $data[0]." , ";
	//}
	//$result->close();
	//echo "<br>";
	
	$cust_restrict_where = "WHERE (site_id IN ( $sql )) 
			AND ((site_id, date_time) IN (SELECT site_id, MAX(date_time) from `quanta` GROUP BY site_id)) ";
	//echo $cust_restrict_where;
	//echo "<br>";
	

	$where_clause=$cust_restrict_where;
	$filter_data_type = 0;
	if ($_SESSION['filter_data_type'] == 1) {$where_clause = $where_clause." AND type=1  "; $filter_data_type = 1;}
	if ($_SESSION['filter_data_type'] == 2) {$where_clause = $where_clause." AND type=0  "; $filter_data_type = 2;}

	/* Get total number of records */
	$sql = "SELECT count(ID) FROM quanta ".$where_clause;
	//echo "Final: $sql<br>";
	$retval = $mysqli->query($sql);
	if(! $retval )
	{
	  die('No Data' . $mysqli->error);
	}

	$row = $retval->fetch_array(MYSQLI_NUM);
	$rec_count = $row[0];

	//echo "ROWS: $rec_count<br>";

	if( isset($_GET{'page'} ) )
	{
	   $page = $_GET{'page'} + 1;
	   $offset = $rec_limit * $page ;
	}
	else
	{
	   $page = 0;
	   $offset = 0;
	}
	$left_rec = $rec_count - ($page * $rec_limit);
	//echo "<br>left_rec: $left_rec<br>";
	////

	//SELECT id, date_time FROM `quanta` WHERE date_time IN (SELECT MAX(date_time) from `quanta`)
					
	$order = "SELECT ID ".
				"FROM quanta ".$where_clause.
				"ORDER BY date_time DESC ".
				"LIMIT $offset, $rec_limit ";

	//echo "order: $order<br>";	
	$result = $mysqli->query($order);

	 DisplayEntryType1Header();

	$count = 0;
	while($data = $result->fetch_row()){
		//$disp = "";
		//$color = "";
		$array = GetDeviceRecordFromId($data[0]);
		DisplayEntryType1($array);
		//var_dump($array);
		/*if ($data[3]==0)
			$disp = "Periodic";
		if ($data[3]==1)
		{
			$color= " class='danger' ";
			$disp = "Fault";	
		}
		
	  echo("<tr $color><td>$data[5]</td>
		<td>$disp</td>
		<td><button type='button' class='btn btn-primary btn-sm' data-toggle='modal' OnClick='ShowDetail($data[0])'>$data[4]</button></td>
		</tr>");*/
	  $count++;
	}
	
	DisplayEntryType1HeaderEnd();
	
	$result->close();
	
	echo "</table>\n<br><br>";
	$disp = $page+1;
	$total = ceil($rec_count/$rec_limit);
	
	if ($total == 1)
	{
		echo "<a class='btn btn-info disabled' role='button' href=\"#\">Last 10 Records</a>";
		echo " $disp / $total ";
	    echo "<a class='btn btn-info disabled' role='button' href=\"#\">Next 10 Records</a>";
	}
	else if( $page > 0 )
	{
	   $last = $page - 2;
	   
	   if (($offset+$rec_limit) < $rec_count)
	   {
		   echo "<a class='btn btn-info' role='button' href=\"$_PHP_SELF?page=$last\">Last 10 Records</a>";
		   echo " $disp / $total ";
		   echo "<a class='btn btn-info' role='button' href=\"$_PHP_SELF?page=$page\">Next 10 Records</a>";
	   }
	   else
	   {
			echo "<a class='btn btn-info' role='button' href=\"$_PHP_SELF?page=$last\">Last 10 Records</a>";
			echo " $disp / $total ";
	   }
	}
	else if( $page == 0 )
	{
		echo "<a class='btn btn-info disabled' role='button' href=\"#\">Last 10 Records</a>";
		echo " $disp / $total ";
	    echo "<a class='btn btn-info' role='button' href=\"$_PHP_SELF?page=$page\">Next 10 Records</a>";
	}
	
	mysqli_close($mysqli);

?>

	
	<!-- Modal -->
	<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	  <div class="modal-dialog">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel"></h4>
		  </div>
		  <div class="modal-body">
			<div id="id_device_detail">
				Please wait...
			</div>
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		  </div>
		</div>
	  </div>
	</div>

 
<?php include 'footer.php'; ?>  
  
  

</body>
</html>
