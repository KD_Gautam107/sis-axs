<?php
session_start();
if (!isset($_SESSION['sysinfra_user']))
{
    header("Location: loginsis.php");
}

include 'common.php';
//include 'test.php';

if (isset( $_GET['authid']))
{
    $custid = $_GET['authid'];
    $custname = $_GET['username'];

    $_SESSION['current_auth_id']=$authid;
    $_SESSION['current_user_name']=$username;
}

if (isset( $_GET['add_name']))
{
    /*var_dump($_GET);
    //Add new record
    AddNewCustomerRecord($_GET['add_name'], $_GET['add_address'], "");
    $maint = file_get_contents('maintenance.php');
    echo $maint;
    return;*/
}

$tab = $_GET['tab'];
$authentications = GetAllAuths();
$customers = GetAllCustomers();

?>

<script>
    var checkbox_array_length = 0;
    var typeref = "";
    /*function SelectCustomer(custid, custname)
    {
        SetRefresh();
        url = "maint-cust.php?custid="+custid+"&custname="+custname;
        AsyncLoad(url, "#context");
    }*/

    function AddAuth(tab)
    {
        var param = "add_username="+encodeURIComponent($( "#Username" ).val().trim());
        param += "&add_passcode="+encodeURIComponent($( "#Passcode" ).val().trim());
        param += "&add_type="+encodeURIComponent($( "#Type" ).val().trim());
        param += "&add_customer="+encodeURIComponent($( "#Customer" ).val().trim());

//alert(checkbox_array_length);
        if(document.getElementById("Type").value == 4)
        {
            for(i = 0; i < checkbox_array_length; i++)
            {
                if(document.getElementById(i+"cluster").checked)
                    typeref += document.getElementById(i+"cluster").value+",";
            }
            //alert(typeref.substring(0,typeref.length-1));
            //
            param += "&add_typeref="+encodeURIComponent(typeref.substring(0,typeref.length-1).trim());
        }
        else
        {
            param += "&add_typeref="+encodeURIComponent("");
        }


        //alert(param);

        //window.open("maint-auth-addedit.php?"+param,"_top");
        //ShowModalUrl("Add New Customer", url);

        if(document.getElementById("Username").value=="")
        {alert("Username is Mandatory!");}
        else if(document.getElementById("Passcode").value=="")
        {alert("Passcode is Mandatory!");}
        else
        {
            if (window.XMLHttpRequest) {
                // code for IE7+, Firefox, Chrome, Opera, Safari
                xmlhttp = new XMLHttpRequest();
            } else {
                // code for IE6, IE5
                xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
            }
            xmlhttp.onreadystatechange = function () {
                if (this.readyState == 4 && this.status == 200) {

                }
            };
            xmlhttp.open("GET", "maint-auth-addedit.php?"+param, true);
            xmlhttp.send();
            RrefreshTab(tab);

            /*var request = $.ajax({url: "maint-auth-addedit.php?"+param});
             request.done(function () {
             RrefreshTab(tab);
             });
             request.fail(function () {
             alert("There is a problem!");
             });}*/
        }
    }

    function EditAuth(id,tab)
    {
        url = "maint-auth-addedit.php?id="+id+"&tab="+tab;
        ShowModalUrl("Edit Authentication "+id, url);
    }


</script>


<table class='table table-striped table-condensed table-responsive'>
    <tr><thead>
        <th>#</th>
        <th>Username</th>
        <th>Passcode</th>
        <th>Customer</th>
        <th>Type</th>
        <th>Typeref</th>
        </thead></tr>
    <tbody>

    <?php
    foreach ($authentications as $authentication)
    {
        echo "<tr>";
        echo "<td><a class='btn btn-default' href='#' onclick='event.preventDefault(); EditAuth(".$authentication[0].','.$tab.")'>".$authentication[0]."</a></td>";
        echo "<td>".$authentication[1]."</td>";
        echo "<td>".$authentication[2]."</td>";

            $ctype = "";
        if($authentication[3]==0)
            $ctype = "Admin";
        else if($authentication[3]==1)
            $ctype = "Head";
        else if($authentication[3]==2)
            $ctype = "Cluster Head";
        else if($authentication[3]==3)
            $ctype = "Tech";
        else if($authentication[3]==4)
            $ctype = "Zone Head";

        $customer_name = GetCustomerWithId($authentication[4]);
        echo "<td>".$authentication[4].'-'.$customer_name[1]."</td>";
        echo "<td>".$authentication[3].'-'.$ctype."</td>";
        echo "<td>".$authentication[5]."</td>";
        /*echo "<td><a class='btn btn-default' href='#' onclick='event.preventDefault(); SelectCustomer(".$customer[0].",\"".$customer[1]."\")'>Select</a></td>";*/
        echo "</tr>";
    }



    ?>
    <tr>
        <td>+</td>
        <td><input type="text" id="Username" name="Username" placeholder="Enter Username"></td>
        <td><input type="text" id="Passcode" name="Passcode" placeholder="Enter Passcode"></td>

        <td><select id="Customer" name="Customer" onchange="Customer_change()">
                <option value="">--Select Customer--</option>
                <?php     foreach ($customers as $customer)
                {
                    echo "<option value='$customer[0]'>$customer[0] -   $customer[1]</option>";
                }
                ?></select></td>


        <td><select id="Type" name="Type" onchange="Type_change()">
                <option value="">--Select Type--</option>
                <option value="0">0-Admin</option>
                <option value="1">1-Head</option>
                <option value="2">2-Cluster Head</option>
                <option value="3">3-Tech</option>
                <option value="4">4-Zone Head</option>
            </select></td>
        <td><p id="Typeref"></p></td>

<script type="text/javascript">

    function Customer_change()
    {
        if(document.getElementById("Type").value != 4)
        document.getElementById("Typeref").innerHTML = "";
        else
        {
            ClusterAjax(document.getElementById("Customer").value);
        }
    }

    function Type_change()
    {
        if(document.getElementById("Type").value == 4)
        {
            ClusterAjax(document.getElementById("Customer").value);
        }
        else
        {
            document.getElementById("Typeref").innerHTML = "";
        }
    }

            //ClusterAjax(document.getElementById("Customer").value);

        //var clustid = [];
        //var clustname = [];
        //var custid = document.getElementById("Customer").value;
        //console.log(custid);

        function ClusterAjax(cust_id)
        {
            jQuery.ajax({
                type: "POST",
                url: 'getclusters.php',
                dataType: 'json',
                data: {functionname: 'GetClustersByCustomerId', arguments: [cust_id]},

                success: function (obj, textstatus) {
                    if( !('error' in obj) ) {
                        document.getElementById("Typeref").innerHTML = "";
                        for(i=0;i<obj.length;i++)
                        {
                            //var clustid[i] = obj[i].id;
                            //var clustname[i] = obj[i].name;
                            if(document.getElementById("Customer").value != "")
                            {
                                document.getElementById("Typeref").innerHTML +=
                                    "<td><input type='checkbox' id='"+i+"cluster' value='"+obj[i].id+"'></td>"+obj[i].id+" - "+obj[i].name+"<br>";
                            }

                        }
                        checkbox_array_length = obj.length;
                    }
                    else {
                        console.log(obj.error);
                    }
                    //console.log(obj.result);
                }

            });
        }


</script>

       <!-- <td><input type="text" id="Typeref" name="Typeref" placeholder="Enter Type"></td>-->
        </tr>
    <tr>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
       <?php echo "<td><a class='btn btn-default' href='#' onclick='event.preventDefault(); AddAuth($tab)'>Add Customer</a></td>";
?>

    </tr>
    </tbody>
</table>

<br>

