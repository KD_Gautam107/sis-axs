<?php
	$id = $_GET['id'];
?>

<h5>Record ##<?php echo $id;?></h5>

<?php

	include 'dbinc.php';

	$mysqli = new mysqli("$mysql_hostname", "$mysql_user", "$mysql_password", "$mysql_database");
	if (mysqli_connect_errno())
	{
		printf("Connect failed: %s\n", mysqli_connect_error());
		return null;
	}
				
	$order = "SELECT * FROM quanta where id=".$id;
	$result = $mysqli->query($order);
	$data = $result->fetch_row();
	
	
	$data_id 		= $data[0];		//Unique Id
	$data_timestamp = $data[1];		//Time Stamp by Server
	$data_validated	= $data[2];		//Validated by Server
	
	$data_type 		= $data[3];		//Type 0 - Periodic / 1 - Fault
	$data_type_str = "Unknown";
	if ($data[3] == 0) $data_type_str = "Periodic";
	if ($data[3] == 1) $data_type_str = "Fault";
	
	$data_site_id	= $data[4];		//String Site Id
	$data_datetime	= $data[5];		//Date Time by Device
	
	
	
	$data_raw		= $data[8];		//Raw Data, for device specific information
	
	$raw_data_size = strlen($data_raw);
	if (strlen($data_raw) < 196)
	{
		echo "<br><b>Invalid Data for this Site: size = $raw_data_size</b><br>";
		die();
	}
	else
	{
		
		if (hexdec(bin2hex($data_raw[195])) != 0x2e)
		{
			echo "<br><b>Invalid Data for this Site: 197th Packet not 0x2E</b><br>";
			die();
		}	
	}
	//$data_device_id	= $data[6];		//Device Id/ Only 1 known for now
	
	$data_device_id	= (hexdec(bin2hex($data_raw[35])));
	$data_device_func	= (hexdec(bin2hex($data_raw[36])));
	$data_device_bytes	= (hexdec(bin2hex($data_raw[37])));
	
	
	//$data_status	= $data[7];		//Status Bits - To Expand
	$data_status	= substr($data_raw, 38, 6);
	$data_status_hex= bin2hex($data_status);
	
	
	echo ("<table class='table table-striped'>");
	
	echo("<tr><td>Type</td><td>$data_type_str</td></tr>
			<tr><td>Site Id</td><td>$data_site_id</td></tr>
			<tr><td>Date Time</td><td>$data_datetime</td></tr>
			<tr><td>Device Id</td><td>$data_device_id</td></tr>
			<tr><td>Device Func</td><td>$data_device_func</td></tr>
			<tr><td>Device Bytes</td><td>$data_device_bytes</td></tr>");
	
	//echo ("Status: <b>$data_status</b><br>");
	//echo ("Status: <b>$data_status_hex</b></tr>");
	
	//$tmp = gettype($data_status);
	//echo "Var: $tmp<br>";
	
	$stats = unpack ( "C*" , $data_status );
	//var_dump($stats);
	
	
	//$tmp = $stats[5] & 0x02;
	//echo "NUM: $stats[5]<br>";
	//echo "AND: $tmp<br>";
	
	echo "<tr><td>Smoke Fire Alarm</td><td";
	//0x0000000000 Smoke fire    0 means No alarm,  1 means Alarm
	if (($stats[5] & 0x01) == false) 
		echo " ><img id='led_white' src='/media/img_trans.gif'>";
	else
		echo " ><img id='led_red' src='/media/img_trans.gif'>";
	
	echo "</td></tr>";
	
	echo "<tr><td>Door</td><td";
	//$FLAG_01_DOOR 	= 0x0000000002;	//Door Open     0  means  Door Close , 1 Means  Door open  
	if (($stats[5] & 0x02) == false) 
		echo "  ><img id='led_white' src='/media/img_trans.gif'><br>";
	else
		echo "  ><img id='led_red' src='/media/img_trans.gif'>";

	echo "</td></tr>";
	
	
	echo "<tr><td>Mode</td><td";
	//$FLAG_02_AUTO 	= 0x0000000004;	//Auto/Man Mode 0 Means  Auto Mode, 1 Means  Man Mode
	if (($stats[5] & 0x04) == false) 
		echo "  >Auto";
	else
		echo "  >Manual";
	echo "</td></tr>";
	
	echo "<tr><td>Load</td><td>";
	//$FLAG_034_LOAD 	= 0x0000000018;	//00    :Load on EB,01: Load on DG, 10: Load on site Battery   11: Not used
	if (($stats[5] & 0x08) == false) 
	{
		if (($stats[5] & 0x10) == false) 
			echo "[- Load on EB -]<br>";				//00
		else
			echo "[- Load on site Battery -]<br>";	//10
	}
	else
	{
		if (($stats[5] & 0x10) == false) 
			echo "[- Load on DG -]<br>";				//01
		else
			echo "[- Not Used -]<br>";	//11
	}
	echo "</td></tr>";
	
	echo "<tr><td>Mains Fail</td><td";
	//$FLAG_05_MAINS 	= 0x0000000020;	//Mains fail	1 Means main Fail,  0 Means  Main healthy
	if (($stats[5] & 0x20) == false) 
		echo "  ><img id='led_white' src='/media/img_trans.gif'>";
	else
		echo "  ><img id='led_red' src='/media/img_trans.gif'>";
	echo "</td></tr>";
	
	echo "<tr><td>Site Battery Low</td><td";
	//$FLAG_06_BATT 	= 0x0000000040;	//Site battery Low  1 Means Site Battery Low ,0 Means  Battery Ok
	if (($stats[5] & 0x40) == false) 
		echo "  ><img id='led_white' src='/media/img_trans.gif'>";
	else
		echo "  ><img id='led_red' src='/media/img_trans.gif'>";
	echo "</td></tr>";
	
	
	echo "<tr><td>Room Temperature</td><td";
	//$FLAG_07_TEMP 	= 0x0000000080;	//Room Temperature  1 Means high Temperature, 0 Means Normal temp
	if (($stats[5] & 0x80) == false) 
		echo "  ><img id='led_white' src='/media/img_trans.gif'>";
	else
		echo "  ><img id='led_red' src='/media/img_trans.gif'>";
	echo "</td></tr>";
	
	
	echo "<tr><td>LLOP</td><td";
	//$FLAG_08_LLOP 	= 0x0000000100;	//LLOP
	if (($stats[4] & 0x01) == false) 
		echo "  ><img id='led_white' src='/media/img_trans.gif'>";
	else
		echo "  ><img id='led_red' src='/media/img_trans.gif'>";
	echo "</td></tr>";
	
	
	echo "<tr><td>HCT/HWT</td><td";
	//$FLAG_09_HCT 	= 0x0000000200;	//HCT/HWT
	if (($stats[4] & 0x02) == false) 
		echo "  ><img id='led_white' src='/media/img_trans.gif'>";
	else
		echo "  ><img id='led_red' src='/media/img_trans.gif'>";
	echo "</td></tr>";
	
	
	echo "<tr><td>Alternate Fault</td><td";
	//$FLAG_10_ALT 	= 0x0000000400;	//Alternate Fault
	if (($stats[4] & 0x04) == false) 
		echo "  ><img id='led_white' src='/media/img_trans.gif'>";
	else
		echo "  ><img id='led_red' src='/media/img_trans.gif'>";
	echo "</td></tr>";
	
	
	echo "<tr><td>DG Over Speed</td><td";
	//$FLAG_11_DG 	= 0x0000000800;	//DG Over Speed
	if (($stats[4] & 0x08) == false) 
		echo "  ><img id='led_white' src='/media/img_trans.gif'>";
	else
		echo "  ><img id='led_red' src='/media/img_trans.gif'>";
	echo "</td></tr>";
	
	
	echo "<tr><td>DG Over Load</td><td";
	//$FLAG_12_DG 	= 0x0000001000;	//DG Over Load
	if (($stats[4] & 0x10) == false) 
		echo "  ><img id='led_white' src='/media/img_trans.gif'>";
	else
		echo "  ><img id='led_red' src='/media/img_trans.gif'>";
	echo "</td></tr>";
	
	
	echo "<tr><td>DG Low Fuel</td><td";
	//$FLAG_13_DG 	= 0x0000002000;	//DG Low Fuel
	if (($stats[4] & 0x20) == false) 
		echo "  ><img id='led_white' src='/media/img_trans.gif'>";
	else
		echo "  ><img id='led_red' src='/media/img_trans.gif'>";
	echo "</td></tr>";
	
	
	echo "<tr><td>DG Start Fail</td><td";
	//$FLAG_14_DG 	= 0x0000004000;	//DG Start Fail
	if (($stats[4] & 0x40) == false) 
		echo "  ><img id='led_white' src='/media/img_trans.gif'>";
	else
		echo "  ><img id='led_red' src='/media/img_trans.gif'>";
	echo "</td></tr>";
	
	
	echo "<tr><td>DG Stop Fail</td><td";
	//$FLAG_15_DG 	= 0x0000008000;	//DG Stop Fail
	if (($stats[4] & 0x80) == false) 
		echo "  ><img id='led_white' src='/media/img_trans.gif'>";
	else
		echo "  ><img id='led_red' src='/media/img_trans.gif'>";
	echo "</td></tr>";
	
	
	echo "<tr><td>DG Battery</td><td";
	//$FLAG_16_DG 	= 0x0000010000;	//DG battery Low
	if (($stats[3] & 0x01) == false) 
		echo "  ><img id='led_white' src='/media/img_trans.gif'>";
	else
		echo "  ><img id='led_red' src='/media/img_trans.gif'>";
	echo "</td></tr>";
	
	
	echo "<tr><td>LCU Fail</td><td";
	//$FLAG_17_LCU 	= 0x0000020000;	//LCU fail
	if (($stats[3] & 0x02) == false) 
		echo "  ><img id='led_green' src='/media/img_trans.gif'>";
	else
		echo "  ><img id='led_red' src='/media/img_trans.gif'>";
	echo "</td></tr>";
	
	
	echo "<tr><td>Rectifier Fail</td><td";
	//$FLAG_18_RECT 	= 0x0000040000;	//Rectifier Fail
	if (($stats[3] & 0x04) == false) 
		echo "  ><img id='led_green' src='/media/img_trans.gif'>";
	else
		echo "  ><img id='led_red' src='/media/img_trans.gif'>";
	echo "</td></tr>";
	
	
	echo "<tr><td>Multi Rectifier Fail</td><td";
	//$FLAG_19_RECT 	= 0x0000080000;	//Multi Rectifier Fail
	if (($stats[3] & 0x08) == false) 
		echo "  ><img id='led_green' src='/media/img_trans.gif'>";
	else
		echo "  ><img id='led_red' src='/media/img_trans.gif'>";
	echo "</td></tr>";
	
	
	echo "<tr><td>LVD Trip</td><td";
	//$FLAG_20_LVD 	= 0x0000100000;	//LVD TRIP
	if (($stats[3] & 0x10) == false) 
		echo "  ><img id='led_green' src='/media/img_trans.gif'>";
	else
		echo "  ><img id='led_red' src='/media/img_trans.gif'>";
	echo "</td></tr>";
	
	
	echo "<tr><td>LVD BY PASS</td><td";
	//$FLAG_21_LVD 	= 0x0000200000;	//LVD BY pass
	if (($stats[3] & 0x20) == false) 
		echo "  ><img id='led_green' src='/media/img_trans.gif'>";
	else
		echo "  ><img id='led_red' src='/media/img_trans.gif'>";
	echo "</td></tr>";
	
	
	//$FLAG_26_RES 	= 0x0004000000;	//Reserved
	//$FLAG_27_RES 	= 0x0008000000;	//Reserved
	//$FLAG_28_RES 	= 0x0010000000;	//Reserved
	//$FLAG_29_RES 	= 0x0020000000;	//Reserved
	//$FLAG_30_RES 	= 0x0040000000;	//Reserved
	//$FLAG_31_RES 	= 0x0080000000;	//Reserved
	
	echo "<tr><td>DG OFF</td><td";
	//$FLAG_32_DG 	= 0x0100000000;	//DG OFF
	if (($stats[1] & 0x01) == false) 
		echo "  ><img id='led_white' src='/media/img_trans.gif'>";
	else
		echo "  ><img id='led_green' src='/media/img_trans.gif'>";
	echo "</td></tr>";
	
	
	echo "<tr><td>DG ON</td><td";
	//$FLAG_33_DG 	= 0x0200000000;	//DG ON
	if (($stats[1] & 0x02) == false) 
		echo "  ><img id='led_white' src='/media/img_trans.gif'>";
	else
		echo "  ><img id='led_green' src='/media/img_trans.gif'>";
	echo "</td></tr>";
	
	
	echo "<tr><td>DG Cranking</td><td";
	//$FLAG_34_DG 	= 0x0400000000;	//DG Cranking
	if (($stats[1] & 0x04) == false) 
		echo "  ><img id='led_white' src='/media/img_trans.gif'>";
	else
		echo "  ><img id='led_green' src='/media/img_trans.gif'>";
	echo "</td></tr>";
	
	
	echo "<tr><td>DG Start in Progress</td><td";
	//$FLAG_35_DG 	= 0x0800000000;	//DG Start in Progress
	if (($stats[1] & 0x08) == false) 
		echo "  ><img id='led_white' src='/media/img_trans.gif'>";
	else
		echo "  ><img id='led_green' src='/media/img_trans.gif'>";
	echo "</td></tr>";
	
	
	echo "<tr><td>DG Cool Down (Idle Running)</td><td";
	//$FLAG_36_DG 	= 0x1000000000;	//DG Cool Down (Idle Running)
	if (($stats[1] & 0x10) == false) 
		echo "  ><img id='led_white' src='/media/img_trans.gif'>";
	else
		echo "  ><img id='led_green' src='/media/img_trans.gif'>";
	echo "</td></tr>";
	
	
	echo "<tr><td>DG STOP  Normally</td><td";
	//$FLAG_37_DG 	= 0x2000000000;	//DG STOP  Normally
	if (($stats[1] & 0x20) == false) 
		echo "  ><img id='led_white' src='/media/img_trans.gif'>";
	else
		echo "  ><img id='led_green' src='/media/img_trans.gif'>";
	echo "</td></tr>";
	
	
	echo "<tr><td>DG STOP due to Fault</td><td";
	//$FLAG_38_DG 	= 0x8000000000;	//DG STOP due to Fault
	if (($stats[1] & 0x40) == false) 
		echo "  ><img id='led_white' src='/media/img_trans.gif'>";
	else
		echo "  ><img id='led_green' src='/media/img_trans.gif'>";
	echo "</td></tr>";
	
	
	echo "<tr><td>DG Stop Due to Maximum Run Expiry</td><td";
	//$FLAG_39_DG 	= 0x8000000000;	//DG Stop Due to Maximum Run Exipary 
	if (($stats[1] & 0x80) == false) 
		echo "  ><img id='led_white' src='/media/img_trans.gif'>";
	else
		echo "  ><img id='led_green' src='/media/img_trans.gif'>";
	echo "</td></tr>";
	
	echo ("</table>");
	
	
	
	echo "<h3>Data for Hybrid Controller Phase 2</h3>";
	
	echo ("<table class='table table-striped'>");

	
	
	$sub_data = substr($data_raw, 44, 36);
	$stats = str_split($sub_data, 2);
	//var_dump($stats);
	
	$sub_data = unpack("n",$stats[0]);
	$sub_data = $sub_data[1]/10;
	
	echo "<tr><td>Room Temperature</td><td>";
	echo("$sub_data Deg C");
	echo "</td></tr>";
	
	
	$sub_data = unpack("n",$stats[1]);
	$sub_data = $sub_data[1]/10;
	echo "<tr><td>Fuel Level</td><td>";
	echo("$sub_data %");
	echo "</td></tr>";
	
	$sub_data = unpack("n",$stats[2]);
	$sub_data = $sub_data[1]/10;
	echo "<tr><td>Fuel Data</td><td>";
	echo("$sub_data Liters");
	echo "</td></tr>";
	
	$sub_data = unpack("n",$stats[3]);
	$sub_data = $sub_data[1]/10;
	echo "<tr><td>Site Batt. bank Voltage</td><td>";
	echo("$sub_data VDC");
	echo "</td></tr>";
	
	$sub_data = unpack("n",$stats[4]);
	$sub_data = $sub_data[1]/10;
	echo "<tr><td>R-Phase current</td><td>";
	echo("$sub_data");
	echo "</td></tr>";
	
	$sub_data = unpack("n",$stats[5]);
	$sub_data = $sub_data[1]/10;
	echo "<tr><td>Y-Phase current</td><td>";
	echo("$sub_data");
	echo "</td></tr>";
	
	$sub_data = unpack("n",$stats[6]);
	$sub_data = $sub_data[1]/10;
	echo "<tr><td>B-Phase current</td><td>";
	echo("$sub_data");
	echo "</td></tr>";
	
	$sub_data = unpack("n",$stats[7]);
	$sub_data = $sub_data[1]/10;
	echo "<tr><td>Mains Frequency</td><td>";
	echo("$sub_data");
	echo "</td></tr>";
	
	$sub_data = unpack("n",$stats[8]);
	$sub_data = $sub_data[1]/10;
	echo "<tr><td>DG frequency</td><td>";
	echo("$sub_data");
	echo "</td></tr>";
	
	$sub_data = unpack("n",$stats[9]);
	$sub_data = $sub_data[1]/10;
	echo "<tr><td>DG-R phase Voltage</td><td>";
	echo("$sub_data VAC");
	echo "</td></tr>";
	
	$sub_data = unpack("n",$stats[10]);
	$sub_data = $sub_data[1]/10;
	echo "<tr><td>DG-Y phase Voltage</td><td>";
	echo("$sub_data VAC");
	echo "</td></tr>";
	
	$sub_data = unpack("n",$stats[11]);
	$sub_data = $sub_data[1]/10;
	echo "<tr><td>DG-B phase Voltage</td><td>";
	echo("$sub_data VAC");
	echo "</td></tr>";
	
	$sub_data = unpack("n",$stats[12]);
	$sub_data = $sub_data[1]/10;
	echo "<tr><td>LCU1 Output Voltage</td><td>";
	echo("$sub_data VAC");
	echo "</td></tr>";
	
	$sub_data = unpack("n",$stats[13]);
	$sub_data = $sub_data[1]/10;
	echo "<tr><td>LCU2 Output Voltage</td><td>";
	echo("$sub_data VAC");
	echo "</td></tr>";
	
	$sub_data = unpack("n",$stats[14]);
	$sub_data = $sub_data[1]/10;
	echo "<tr><td>LCU3 Output Voltage</td><td>";
	echo("$sub_data VAC");
	echo "</td></tr>";
	
	$sub_data = unpack("n",$stats[15]);
	$sub_data = $sub_data[1]/10;
	echo "<tr><td>Input Mains Voltage - R Phase</td><td>";
	echo("$sub_data VAC");
	echo "</td></tr>";
	
	$sub_data = unpack("n",$stats[16]);
	$sub_data = $sub_data[1]/10;
	echo "<tr><td>Input Mains Voltage - Y Phase</td><td>";
	echo("$sub_data VAC");
	echo "</td></tr>";
	
	$sub_data = unpack("n",$stats[17]);
	$sub_data = $sub_data[1]/10;
	echo "<tr><td>Input Mains Voltage - B Phase</td><td>";
	echo("$sub_data VAC");
	echo "</td></tr>";
	
	/////
	
	
	$sub_data = substr($data_raw, 80, 48);
	$stats = str_split($sub_data, 8);
	$stats = substr_replace($stats, ".", 7, 0);
	
	for ($i=0; $i<6; $i++)
	{
		$stats[$i] = ltrim($stats[$i], '0');
		if ($stats[$i][0] == '.')
			$stats[$i] = "0".$stats[$i];
	}
	//var_dump($stats);
	echo "<tr><td>DG Running Hours</td><td>";
	echo("$stats[0] Hrs");
	echo "</td></tr>";
	
	echo "<tr><td>Mains RUN HOURS</td><td>";
	echo("$stats[1] Hrs");
	echo "</td></tr>";
	
	echo "<tr><td>Batt RUN HOURS</td><td>";
	echo("$stats[2] Hrs");
	echo "</td></tr>";
	
	echo "<tr><td>O/P Mains Energy</td><td>";
	echo("$stats[3] Kwh");
	echo "</td></tr>";
	
	echo "<tr><td>DG Energy</td><td>";
	echo("$stats[4] Kwh");
	echo "</td></tr>";
	
	echo "<tr><td>I/P Mains Energy</td><td>";
	echo("$stats[5] Kwh");
	echo "</td></tr>";
	
	$sub_data = substr($data_raw, 128, 6);
	$stats = str_split($sub_data, 2);
	
	$sub_data = unpack("n",$stats[0]);
	$sub_data = $sub_data[1]/10;
	echo "<tr><td>DG Battery Voltage</td><td>";
	echo("$sub_data VDC");
	echo "</td></tr>";

	$sub_data = unpack("n",$stats[1]);
	$sub_data = $sub_data[1]/10;
	echo "<tr><td>Battery Charging current</td><td>";
	echo("$sub_data Amp");
	echo "</td></tr>";
		
	$sub_data = unpack("n",$stats[2]);
	$sub_data = $sub_data[1]/10;
	echo "<tr><td>Battery Discharging current</td><td>";
	echo("$sub_data Amp");
	echo "</td></tr>";
	
	$sub_data = substr($data_raw, 134, 2);
	$stats = str_split($sub_data, 1);
	
	$sub_data = (hexdec(bin2hex($stats[0])));
	echo "<tr><td>Battery status</td><td>";
	echo("$sub_data %");
	echo "</td></tr>";
	
	$sub_data = (hexdec(bin2hex($stats[1]))/10);
	echo "<tr><td>Battery back up time</td><td>";
	echo("$sub_data hours");
	echo "</td></tr>";
	
	
	$sub_data = substr($data_raw, 136, 16);
	$stats = str_split($sub_data, 8);
	$stats = substr_replace($stats, ".", 7, 0);
	
	for ($i=0; $i<2; $i++)
	{
		$stats[$i] = ltrim($stats[$i], '0');
		if ($stats[$i][0] == '.')
			$stats[$i] = "0".$stats[$i];
	}

	echo "<tr><td>Battery Charging Energy</td><td>";				
	echo("$stats[0] Kwh");
	echo "</td></tr>";
	
	echo "<tr><td>Battery Discharging Energy</td><td>";				
	echo("$stats[1] Kwh");
	echo "</td></tr>";

	echo "<tr><td>CRC LOWER BYTE</td><td>";				
	echo(hexdec(bin2hex($data_raw[152])));
	echo "</td></tr>";
	
	echo "<tr><td>CRC HIGHER BYTE</td><td>";				
	echo(hexdec(bin2hex($data_raw[153])));
	echo "</td></tr>";

	
	echo "<tr><td>-------------</td><td>";				
	echo "--------------------";
	echo "</td></tr>";

	echo "<tr><td></td><td>";				
	echo "DC Energy Meter Data";
	echo "</td></tr>";

	
	
	
	echo "<tr><td>Device 2 Id</td><td>";				
	echo(hexdec(bin2hex($data_raw[154])));
	echo "</td></tr>";
	
	
	echo "<tr><td>Function Code</td><td>";				
	echo(hexdec(bin2hex($data_raw[155])));
	echo "</td></tr>";
	
	echo "<tr><td># bytes</td><td>";				
	echo(hexdec(bin2hex($data_raw[156])));
	echo "</td></tr>";
	
	
	
	//9 * 4 IEEE-754 32-bit float value
	$sub_data = substr($data_raw, 157, 36);
	$stats = str_split($sub_data, 4);
	
	for ($i=0; $i<9; $i++)
	{
		$bin = "\x4A\x5B\x1B\x05";
		$tmp = unpack('f', strrev($stats[$i]));
		//$tmp = unpack('f', $stats[$i]);
		//echo $tmp[1];  // 3589825.25
		$stats[$i] = $tmp[1];
	}
	
	echo "<tr><td>KWH of Channel 1</td><td>";				
	echo($stats[0]);
	echo "</td></tr>";
	
	echo "<tr><td>Current of Channel 1</td><td>";				
	echo($stats[1]);
	echo "</td></tr>";
	
	echo "<tr><td>KWH of Channel 2</td><td>";				
	echo($stats[2]);
	echo "</td></tr>";
	
	echo "<tr><td>Current of Channel 2</td><td>";				
	echo($stats[3]);
	echo "</td></tr>";
	
	echo "<tr><td>KWH of Channel 3</td><td>";				
	echo($stats[4]);
	echo "</td></tr>";
	
	echo "<tr><td>Current of Channel 3</td><td>";				
	echo($stats[5]);
	echo "</td></tr>";
	
	echo "<tr><td>KWH of Channel 4</td><td>";				
	echo($stats[6]);
	echo "</td></tr>";
	
	echo "<tr><td>Current of Channel 4</td><td>";				
	echo($stats[7]);
	echo "</td></tr>";
	
	echo "<tr><td>Voltage</td><td>";				
	echo($stats[8]);
	echo "</td></tr>";
	
	
	echo "<tr><td>CRC LOWER BYTE</td><td>";				
	echo(hexdec(bin2hex($data_raw[193])));
	echo "</td></tr>";
	
	echo "<tr><td>CRC HIGHER BYTE</td><td>";				
	echo(hexdec(bin2hex($data_raw[194])));
	echo "</td></tr>";


	echo ("</table>");
	
	
	

	
	$result->close();
	mysqli_close($mysqli);
	

?>
