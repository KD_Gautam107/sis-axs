<?php
session_start();
if (!isset($_SESSION['sysinfra_user'])) {
	header("Location: loginsis.php");
}

if ($_SERVER["REQUEST_METHOD"] == "POST") {
	$reset_page = false;
	if (isset($_POST['filter_data_type'])) {
		$_SESSION['filter_data_type'] = $_POST['filter_data_type'];
		$reset_page = true;
	}

	if (isset($_POST['site_list_data_type'])) {
		$_SESSION['site_list_data_type'] = $_POST['site_list_data_type'];
		$reset_page = true;
	}

	if ($reset_page == true)
		unset($_GET{
		'page'});
}


include 'common.php';

$site_list = GetSiteList();

?>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
	<title>Equipment Data</title>

	<!-- Bootstrap -->
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">

	<!-- Optional theme -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css">

	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>

	<!-- Latest compiled and minified JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>

	<script src="/libs/bootstrap-paginator.min.js"></script>




	<script>
		function ShowDetail(id, type) {
			if (type == 0) {
				$("#myModalLabel").html("Equipment Status");
				$.ajax({
					url: "detail.php?id=" + id,
					success: function(result) {
						$("#id_device_detail").html(result);
						$('#myModal').modal('show');

					}
				});
			} else if (type == 1) {
				$("#myModalLabel").html("Binary Data");
				$.ajax({
					url: "binary.php?id=" + id,
					success: function(result) {
						$("#id_device_detail").html(result);
						$('#myModal').modal('show');

					}
				});
			} else if (type == 2) {
				$("#myModalLabel").html("Equipment Data Phase 2");
				$.ajax({
					url: "detailPhase2.php?id=" + id,
					success: function(result) {
						$("#id_device_detail").html(result);
						$('#myModal').modal('show');

					}
				});
			}

		}

		$('#myModal').on('hidden.bs.modal', function(e) {
			$("#id_device_detail").html('');
		})

		function isInt(value) {
			if (isNaN(value)) {
				return false;
			}
			var x = parseFloat(value);
			return (x | 0) === x;
		}

		function ExportCSV(total) {
			var filter_data_type = $("#filter_data_type option:selected").val();
			var site_list_data_type = $("#site_list_data_type option:selected").val();
			var export_page_no = $("#ExportPage").val();
			var excel_format = $('#ExcelFormat').is(":checked") ? 1 : 0;

			if ((!isInt(export_page_no) || (export_page_no < 1) || (export_page_no > total))) {
				alert("Invalid Page Number");
			} else {
				var url = "exportcsv.php?filter_data_type=" + filter_data_type + "&site_list_data_type=" + site_list_data_type + "&export_page_no=" + export_page_no + "&excel_format=" + excel_format;

				//alert(url);
				window.open(url);
			}
		}
	</script>

	<style>
		#led_white {
			width: 28px;
			height: 28px;
			background: url(/media/leds.png) 0 0;
		}

		#led_red {
			width: 28px;
			height: 28px;
			background: url(/media/leds.png) -30px 0;
		}

		#led_green {
			width: 28px;
			height: 28px;
			background: url(/media/leds.png) -124px 0;
		}
	</style>

</head>

<body>



	<?php

	include 'dbinc.php';

	$rec_limit = 50;

	$mysqli = new mysqli("$mysql_hostname", "$mysql_user", "$mysql_password", "$mysql_database");
	if (mysqli_connect_errno()) {
		printf("Connect failed: %s\n", mysqli_connect_error());
		exit();
	}


	$where_clause = "";
	$filter_data_type = 0;
	if ($_SESSION['filter_data_type'] == 1) {
		$where_clause = " WHERE (type=1)  ";
		$filter_data_type = 1;
	}
	if ($_SESSION['filter_data_type'] == 2) {
		$where_clause = " WHERE (type=0)  ";
		$filter_data_type = 2;
	}


	if (isset($_SESSION['site_list_data_type']))
		$site_list_data_type = $_SESSION['site_list_data_type'];
	else
		$site_list_data_type = "0";

	if ($site_list_data_type != "0") {
		if ($filter_data_type == 0)
			$where_clause = " WHERE ";
		else
			$where_clause .= "  AND ";
		$where_clause .= " (site_id=\"" . $site_list_data_type . "\")  ";
	}

	//echo "site_list_data_type: $site_list_data_type<br>";

	//echo $where_clause;
	/* Get total number of records */
	$sql = "SELECT count(ID) FROM quanta " . $where_clause;
	//echo "Final: $sql<br>";
	$retval = $mysqli->query($sql);
	if (!$retval) {
		die('No Data' . $mysqli->error);
	}

	$row = $retval->fetch_array(MYSQLI_NUM);
	$rec_count = $row[0];
	//echo $rec_count;
	//echo "ROWS: $rec_count<br>";

	if (isset($_GET{
	'page'})) {
		$page = $_GET{
		'page'} + 1;
		$offset = $rec_limit * $page;
	} else {
		$page = 0;
		$offset = 0;
	}
	$left_rec = $rec_count - ($page * $rec_limit);

	//	echo $rec_count."<br>";
	//	echo $page."<br>";
	//	echo "<br>left_rec: $left_rec<br>";
	////



	$order = "SELECT ID, site_id, date_time " .
		"FROM quanta " . $where_clause .
		"ORDER BY date_time DESC " .
		"LIMIT $offset, $rec_limit ";

	//echo "order: $order<br>";	
	$result = $mysqli->query($order);

	DisplayEntryType1Header();

	$count = 0;
	while ($data = $result->fetch_row()) {
		$site_type = GetSiteTypeForQuanta($data[1], $site_list);

		if ($site_type == 2) {
			//This is the new Phase 2 site
			DisplayEntryTypePhase2($data[0], $data[1], $data[2]);
		} else {
			$array = GetDeviceRecordFromId($data[0], false);

			if ($array[6] == 1)
				DisplayEntryType1($array);
			else if ($array[6] == 2)
				DisplayEntryType2($array);
			else if ($array[6] == 16)
				DisplayEntryType1($array);
		}
		//var_dump($array);
		$count++;
	}

	DisplayEntryType1HeaderEnd();

	$result->close();
	mysqli_close($mysqli);

	echo "</table>\n<br><br>";
	$disp = $page + 1;
	$total = ceil($rec_count / $rec_limit);

	echo "($disp / $total)";
	?>



	<div>
		<ul id='id_pages'></ul>
	</div>
	<script type='text/javascript'>
		var options = {
			bootstrapMajorVersion: 3,
			numberOfPages: 10,
			currentPage: <?php echo $disp; ?>,
			totalPages: <?php echo $total; ?>,
			onPageClicked: function(e, originalEvent, type, page) {
				var newpage = page - 2;
				window.location.replace("<?php echo $_PHP_SELF . '?page='; ?>" + newpage);
			}
		}

		$('#id_pages').bootstrapPaginator(options);
	</script>



	<form method="post" role="form" class="form-inline">
		<select class="form-control" name="filter_data_type" id="filter_data_type">
			<option value="0" <?php if ($filter_data_type == 0) echo "selected"; ?>>All Data</option>
			<option value="1" <?php if ($filter_data_type == 1) echo "selected"; ?>>Faults Only</option>
			<option value="2" <?php if ($filter_data_type == 2) echo "selected"; ?>>Periodic Data Only</option>
		</select>

		<select class="form-control" name="site_list_data_type" id="site_list_data_type">
			<option value="0" <?php if ($ccc == 0) echo "selected"; ?>>All Sites</option>
			<?php
			for ($i = 0; $i < count($site_list); $i++) {
				$site_id = $site_list[$i][0];
				$site_name = $site_list[$i][1];

				if ($site_id == $site_list_data_type)
					print("<option value='$site_id' selected >$site_id - $site_name</option>");
				else
					print("<option value='$site_id' >$site_id - $site_name</option>");
			}
			?>

		</select>
		<?php $totalChunks = ceil($rec_count / 5000); ?>

		<!--<br><?php /*echo $totalChunks; */ ?><br>-->

		<button type="submit" class="btn btn-default" value=" Submit ">Submit</button>

	</form>
	<br>
	<form method="post" role="form" class="form-inline">
		Export Page <input class="form-control" type="text" name="ExportPage" id="ExportPage" value="1" size="2"> of total <b><?php echo $totalChunks; ?></b> pages.&nbsp;&nbsp;
		<a role="button" href="#" onclick="event.preventDefault(); ExportCSV(<?php echo $totalChunks; ?>);" class="btn btn-default">Export as CSV</a>
		<input class="form-control" type="checkbox" name="ExcelFormat" id="ExcelFormat" checked="true">Excel Format</input>
	</form>


	<!-- Modal -->
	<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="myModalLabel"></h4>
				</div>
				<div class="modal-body">
					<div id="id_device_detail">
						Please wait...
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>


	<?php include 'footersis.php'; ?>


</body>

</html>