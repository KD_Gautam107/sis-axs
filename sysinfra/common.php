<?php 

	/**
	 * View any string as a hexdump.
	 *
	 * This is most commonly used to view binary data from streams
	 * or sockets while debugging, but can be used to view any string
	 * with non-viewable characters.
	 *
	 * @version     1.3.2
	 * @author      Aidan Lister <aidan@php.net>
	 * @author      Peter Waller <iridum@php.net>
	 * @link        http://aidanlister.com/2004/04/viewing-binary-data-as-a-hexdump-in-php/
	 * @param       string  $data        The string to be dumped
	 * @param       bool    $htmloutput  Set to false for non-HTML output
	 * @param       bool    $uppercase   Set to true for uppercase hex
	 * @param       bool    $return      Set to true to return the dump
	 */
	function hexdump ($data, $htmloutput = true, $uppercase = false, $return = false)
	{
		// Init
		$hexi   = '';
		$ascii  = '';
		$dump   = ($htmloutput === true) ? '<pre>' : '';
		$offset = 0;
		$len    = strlen($data);
	 
		// Upper or lower case hexadecimal
		$x = ($uppercase === false) ? 'x' : 'X';
	 
		// Iterate string
		for ($i = $j = 0; $i < $len; $i++)
		{
			// Convert to hexidecimal
			$hexi .= sprintf("%02$x ", ord($data[$i]));
	 
			// Replace non-viewable bytes with '.'
			if (ord($data[$i]) >= 32) {
				$ascii .= ($htmloutput === true) ?
								htmlentities($data[$i]) :
								$data[$i];
			} else {
				$ascii .= '.';
			}
	 
			// Add extra column spacing
			if ($j === 7) {
				$hexi  .= ' ';
				$ascii .= ' ';
			}
	 
			// Add row
			if (++$j === 16 || $i === $len - 1) {
				// Join the hexi / ascii output
				$dump .= sprintf("%04$x  %-49s  %s", $offset, $hexi, $ascii);
				
				// Reset vars
				$hexi   = $ascii = '';
				$offset += 16;
				$j      = 0;
				
				// Add newline            
				if ($i !== $len - 1) {
					$dump .= "\n";
				}
			}
		}
	 
		// Finish dump
		$dump .= $htmloutput === true ?
					'</pre>' :
					'';
		$dump .= "\n";
	 
		// Output method
		if ($return === false) {
			echo $dump;
		} else {
			return $dump;
		}
	}
	
	function IsNullOrEmptyString($question){
		return (!isset($question) || trim($question)==='');
	}

	
	function getlogpath()
	{
		return "/akhilesh/code/collector.log";
		//return "/home/admin/akhilesh/collector.log";
		//return "/var/www/sysinfra/home/";
	}

	function getdumppath()
	{
		return "/akhilesh/code/dump/";
		//return "/home/admin/akhilesh/dump/";
	}
	
	
	function dumphexfile($filepath)
	{
		$path = getdumppath();
		
		
		$bin = file_get_contents($path.$filepath);
		//var_dump( $bin);
		if ($bin === false)
		{
			echo "File not found";
		}
		else
		{
			hexdump($bin);
		}
	}


	function processerrorcode($code)
	{
		$result = "";
		switch ($code)
		{
			case 0:
				$result =  "Success";
				break;
			
			case 1:
				$result =  "Unknown Device, just add data to DB";
				break;
			
			case -2:
				$result =  "Data format incorrect - #S not present";
				break;
			
			case -3:
				$result =  "Data format incorrect - P/F not present";
				break;
				
			case -4:
				$result =  "Data format incorrect - , not present";
				break;
			
			case -5:
				$result =  "Data format incorrect - Length less than expected";
				break;
			
			case -6:
				$result =  "Data format incorrect - End of Data not present";
				break;
				
			default:
				$result =  "Unknown Error";
		}
		
		return $result;

	}


	function processdumpline($line)
	{
		$data = "";
		$result = substr($line, 1, 5);
		
		if (strcmp($result, "INFO ") == 0)
		{
			$result = substr($line, 37, 20);
			if (strcmp($result, "Collector Response: ") == 0)
			{
				$result = substr($line, 57, 6);			
				$pos = strpos($result, "(");

				if ($pos === false) {
				} else {
					$result = substr($result, 0, $pos-1);
					$data = $data . " ".$result." [";
					$data = $data . processerrorcode($result);
					$data = $data . "]<br>";
					
					$pos = strpos($line, ") (dump ");
					if ($pos === false) {
					} else {
						$result = substr($line, $pos+8);
												
						$pos = strpos($result, ")");
						if ($pos === false) {
						} else {
							$result = substr($result, 0, $pos);
							//dumphexfile($result);
							$data = $data . "Binary Data: <a href='dumphexfile.php?fn=".$result."'>".$result."</a>";
						}
					}
					
					
					
				}
				
		
			}
		}
		
		return $data;
	}

	function logdump($filepath)
	{
		$myfile = fopen($filepath, "r") or die("Unable to open file: $filepath");
		 
		echo "<table class='table table-striped'>";
		echo "<tr>
				<thead>
					<th>#</th>
					<th>Detail</th>
					<th>Log Text</th>
					
				</thead>
			 </tr><tbody>";
		
		$i = 1;
		while(!feof($myfile)) 
		{
			$line = fgets($myfile);
			$info = processdumpline($line);
			echo "<tr><td>".$i."</td>";
			echo "<td>".$info."</td><td>";
			echo $line;
			echo "</td></tr>";
			
			$i++;
		}
		echo "</tbody></table>";
		fclose($myfile);
	}


	function GetSiteTypeForQuanta($q_site_id, $site_type_list)
	{
		
		if ($site_type_list == null) return -1;
		if ($q_site_id == null) return -1;
		for ($i=0; $i<count($site_type_list); $i++)
		{
			if ($site_type_list[$i][0] == $q_site_id)
			{
				return $site_type_list[$i][2];
			}
		}
				
		return -1;
	}

	function GetDeviceRecordFromId($id, $isExport)
	{
		$array = array();
		
		include 'dbinc.php'; 
		

		$mysqli = new mysqli("$mysql_hostname", "$mysql_user", "$mysql_password", "$mysql_database");
		if (mysqli_connect_errno())
		{
			printf("Connect failed: %s\n", mysqli_connect_error());
			return null;
		}
		
		$order = "SELECT 
			ID, timestamp , data_validated , type , site_id , date_time , device_id , status , raw_data
			FROM quanta 
			WHERE id=".$id;

		
		//echo $order;
						
		//$order = "SELECT * FROM quanta where id=".$id;
		$result = $mysqli->query($order);	
		$data = $result->fetch_row();
	
		$array[] 	= $data[0];		//0Unique Id
		if (!$isExport) $array[] 	= $data[1];		//1Time Stamp by Server
		if (!$isExport) $array[]	= $data[2];		//2Validated by Server
		$array[] 	= $data[3];		//3Type 0 - Periodic / 1 - Fault
		$array[]	= $data[4];		//4String Site Id
		$array[]	= $data[5];		//5Date Time by Device
		$array[]	= $data[6];		//6Device Id/ Only 1 & 2 known for now
		if (!$isExport) 
			$array[]	= $data[7];		//7Status Bits - To Expand
		else
			$array[]	= bin2hex($data[7]);		//7Status Bits in Hex for Export
		$data_raw	= $data[8];		//Raw Data, for device specific information
		
		//var_dump($array);
		//$array = array_merge($array, DeviceGetStatusBits($data[7]));
		//var_dump($array);
		
		
		if ($data[6] == 1 || $data[6] == 16 )
		{
			$array = array_merge($array, DeviceGetData($data_raw));
		}
		else if ($data[6] == 2)
		{
			$array = array_merge($array, DeviceGetDataType2($data_raw));
		}
		
		$result->close();
		mysqli_close($mysqli);
		
		//var_dump($array);
		
		return $array;
	
	}
	

	function DeviceGetData($data_raw) 
	{
		$array = array();
		$sub_data = substr($data_raw, 41, 36);
		$stats = str_split($sub_data, 2);
		//var_dump($stats);
		
		$sub_data = unpack("n",$stats[0]);
		$sub_data = $sub_data[1]/10;
		//echo "<tr><td>Room Temperature</td><td>";
		$array[] = $sub_data;	//8
		
		
		$sub_data = unpack("n",$stats[1]);
		$sub_data = $sub_data[1]/10;
		//echo "<tr><td>Fuel Level</td><td>";
		$array[] = $sub_data;	//9
		
		$sub_data = unpack("n",$stats[2]);
		$sub_data = $sub_data[1]/10;
		//echo "<tr><td>Fuel Data</td><td>";
		$array[] = $sub_data;	//10
		
		$sub_data = unpack("n",$stats[3]);
		$sub_data = $sub_data[1]/10;
		//echo "<tr><td>Site Batt. bank Voltage</td><td>";
		$array[] = $sub_data;	//11
		
		$sub_data = unpack("n",$stats[4]);
		$sub_data = $sub_data[1]/10;
		//echo "<tr><td>R-Phase current</td><td>";
		$array[] = $sub_data;	//12
		
		$sub_data = unpack("n",$stats[5]);
		$sub_data = $sub_data[1]/10;
		//echo "<tr><td>Y-Phase current</td><td>";
		$array[] = $sub_data;	//13
		
		$sub_data = unpack("n",$stats[6]);
		$sub_data = $sub_data[1]/10;
		//echo "<tr><td>B-Phase current</td><td>";
		$array[] = $sub_data;	//14
		
		$sub_data = unpack("n",$stats[7]);
		$sub_data = $sub_data[1]/10;
		//echo "<tr><td>Mains Frequency</td><td>";
		$array[] = $sub_data;	//15
		
		$sub_data = unpack("n",$stats[8]);
		$sub_data = $sub_data[1]/10;
		//echo "<tr><td>DG frequency</td><td>";
		$array[] = $sub_data;	//16
		
		$sub_data = unpack("n",$stats[9]);
		$sub_data = $sub_data[1]/10;
		//echo "<tr><td>DG-R phase Voltage</td><td>";
		$array[] = $sub_data;	//17
		
		$sub_data = unpack("n",$stats[10]);
		$sub_data = $sub_data[1]/10;
		//echo "<tr><td>DG-Y phase Voltage</td><td>";
		$array[] = $sub_data;	//18
		
		$sub_data = unpack("n",$stats[11]);
		$sub_data = $sub_data[1]/10;
		//echo "<tr><td>DG-B phase Voltage</td><td>";
		$array[] = $sub_data;	//19
		
		$sub_data = unpack("n",$stats[12]);
		$sub_data = $sub_data[1]/10;
		//echo "<tr><td>LCU1 Output Voltage</td><td>";
		$array[] = $sub_data;	//20
		
		$sub_data = unpack("n",$stats[13]);
		$sub_data = $sub_data[1]/10;
		//echo "<tr><td>LCU2 Output Voltage</td><td>";
		$array[] = $sub_data;	//21
		
		$sub_data = unpack("n",$stats[14]);
		$sub_data = $sub_data[1]/10;
		//echo "<tr><td>LCU3 Output Voltage</td><td>";
		$array[] = $sub_data;	//22
		
		$sub_data = unpack("n",$stats[15]);
		$sub_data = $sub_data[1]/10;
		//echo "<tr><td>Input Mains Voltage - R Phase</td><td>";
		$array[] = $sub_data;	//23
		
		$sub_data = unpack("n",$stats[16]);
		$sub_data = $sub_data[1]/10;
		//echo "<tr><td>Input Mains Voltage - Y Phase</td><td>";
		$array[] = $sub_data;	//24
		
		$sub_data = unpack("n",$stats[17]);
		$sub_data = $sub_data[1]/10;
		//echo "<tr><td>Input Mains Voltage - B Phase</td><td>";
		$array[] = $sub_data;	//25
		
		/////
		
		
		$sub_data = substr($data_raw, 77, 48);
		$stats = str_split($sub_data, 8);
		$stats = substr_replace($stats, ".", 7, 0);
		
		for ($i=0; $i<6; $i++)
		{
			$stats[$i] = ltrim($stats[$i], '0');
			if ($stats[$i][0] == '.')
				$stats[$i] = "0".$stats[$i];
		}
		
		//echo "<tr><td>DG Running Hours</td><td>";
		$array[] = $stats[0];	//26
		
		//echo "<tr><td>Mains RUN HOURS</td><td>";
		$array[] = $stats[1];	//27
		
		//echo "<tr><td>Batt RUN HOURS</td><td>";
		$array[] = $stats[2];	//28
		
		//echo "<tr><td>O/P Mains Energy</td><td>";
		$array[] = $stats[3];	//29
		
		//echo "<tr><td>DG Energy</td><td>";
		$array[] = $stats[4];	//30
		
		//echo "<tr><td>I/P Mains Energy</td><td>";
		$array[] = $stats[5];	//31
		
		$sub_data = substr($data_raw, 125, 2);
		
		$sub_data = unpack("n",$sub_data);
		$sub_data = $sub_data[1]/10;
		//echo "<tr><td>DG Battery Voltage</td><td>";
		$array[] = $sub_data;	//32


		//18*2	= 36
		//6 *8	= 48
		//1 *2	= 02
		//----------
		//        86
		//----------
		
		
		//Updated Data - Updated Protocol
		
		//18*2	= 36
		//6 *8	= 48
		//1 *2	= 02
		//2 *2	= 04	*
		//2 *1	= 02	*
		//----------
		//        92
		//----------
		
		//REV2 Total 134 Bytes
		if (strlen($data_raw) >= 134)
		{
			$sub_data = substr($data_raw, 127, 4);
			$stats = str_split($sub_data, 2);
			
			$sub_data = unpack("n",$stats[0]);
			$sub_data = $sub_data[1]/10;
			//echo "<tr><td>Battery Charging current</td><td>";
			$array[] = $sub_data;	//33
			
			
			$sub_data = unpack("n",$stats[1]);
			$sub_data = $sub_data[1]/10;
			//echo "<tr><td>Battery Discharging current</td><td>";
			$array[] = $sub_data;	//34
			
			$sub_data = substr($data_raw, 131, 2);
			$stats = str_split($sub_data, 1);
			
			$sub_data = unpack("c",$stats[0]);
			$sub_data = (hexdec(bin2hex($stats[0])));	//$sub_data[1]/1;
			//echo "<tr><td>Battery status</td><td>";
			$array[] = $sub_data;	//35
			
			$sub_data = unpack("c",$stats[1]);
			$sub_data = (hexdec(bin2hex($stats[1]))/10);	//$sub_data[1]/10;
			//echo "<tr><td>Battery back up time</td><td>";
			$array[] = $sub_data;	//36
			
			
			//REV3	Total 150 Bytes
			if (strlen($data_raw) >= 150)
			{
				$sub_data = substr($data_raw, 133, 16);
				$stats = str_split($sub_data, 8);
				$stats = substr_replace($stats, ".", 7, 0);
		
				for ($i=0; $i<2; $i++)
				{
					$stats[$i] = ltrim($stats[$i], '0');
					if ($stats[$i][0] == '.')
						$stats[$i] = "0".$stats[$i];
				}
				
				//echo "<tr><td>Battery Charging Energy (Kwh)</td><td>";
				$array[] = $stats[0];	//37
				
				//echo "<tr><td>Battery Discharging Energy (Kwh)</td><td>";
				$array[] = $stats[1];	//38
			
			}
			else
			{
				$array[] = 0;	//37
				$array[] = 0;	//38
			}
		
		}
		else
		{
			$array[] = 0;	//33
			$array[] = 0;	//34
			$array[] = 0;	//35
			$array[] = 0;	//36
			$array[] = 0;	//37
			$array[] = 0;	//38
		}
	
		return $array;
	}
		
	
	function DeviceGetDataType2($data_raw) 
	{
		$array = array();
		
		//14 * 2 Bytes
		$sub_data = substr($data_raw, 41, 28);
		$stats = str_split($sub_data, 2);
		//var_dump($stats);
		
		$sub_data = unpack("n",$stats[0]);
		$sub_data = $sub_data[1]/10;
		//echo "<tr><td>Room Temperature</td><td>";
		$array[] = $sub_data;	//8
		
		
		$sub_data = unpack("n",$stats[1]);
		$sub_data = $sub_data[1]/10;
		//echo "<tr><td>Solar array 1 Voltage</td><td>";
		$array[] = $sub_data;	//9
		
		$sub_data = unpack("n",$stats[2]);
		$sub_data = $sub_data[1]/10;
		//echo "<tr><td>Solar array 2 Voltage</td><td>";
		$array[] = $sub_data;	//10
		
		$sub_data = unpack("n",$stats[3]);
		$sub_data = $sub_data[1]/10;
		//echo "<tr><td>Solar array 3 Voltage</td><td>";
		$array[] = $sub_data;	//11
		
		$sub_data = unpack("n",$stats[4]);
		$sub_data = $sub_data[1]/10;
		//echo "<tr><td>Array1 current</td><td>";
		$array[] = $sub_data;	//12
		
		$sub_data = unpack("n",$stats[5]);
		$sub_data = $sub_data[1]/10;
		//echo "<tr><td>Array2 current</td><td>";
		$array[] = $sub_data;	//13
		
		$sub_data = unpack("n",$stats[6]);
		$sub_data = $sub_data[1]/10;
		//echo "<tr><td>Array3 current</td><td>";
		$array[] = $sub_data;	//14
		
		$sub_data = unpack("n",$stats[7]);
		$sub_data = $sub_data[1]/10;
		//echo "<tr><td>R-Phase current</td><td>";
		$array[] = $sub_data;	//15
		
		$sub_data = unpack("n",$stats[8]);
		$sub_data = $sub_data[1]/10;
		//echo "<tr><td>Y-Phase current</td><td>";
		$array[] = $sub_data;	//16
		
		$sub_data = unpack("n",$stats[9]);
		$sub_data = $sub_data[1]/10;
		//echo "<tr><td>B-Phase current</td><td>";
		$array[] = $sub_data;	//17
		
		$sub_data = unpack("n",$stats[10]);
		$sub_data = $sub_data[1]/10;
		//echo "<tr><td>Mains Frequency</td><td>";
		$array[] = $sub_data;	//18
		
		$sub_data = unpack("n",$stats[11]);
		$sub_data = $sub_data[1]/10;
		//echo "<tr><td>Input Mains Voltage – R Phase</td><td>";
		$array[] = $sub_data;	//19
		
		$sub_data = unpack("n",$stats[12]);
		$sub_data = $sub_data[1]/10;
		//echo "<tr><td>Input Mains Voltage – Y Phase</td><td>";
		$array[] = $sub_data;	//20
		
		$sub_data = unpack("n",$stats[13]);
		$sub_data = $sub_data[1]/10;
		//echo "<tr><td>Input Mains Voltage – B Phase</td><td>";
		$array[] = $sub_data;	//21
		
		
		//5 * 8 Bytes
		
		$sub_data = substr($data_raw, 69, 40);
		$stats = str_split($sub_data, 8);
		$stats = substr_replace($stats, ".", 7, 0);
		
		for ($i=0; $i<5; $i++)
		{
			$stats[$i] = ltrim($stats[$i], '0');
			if ($stats[$i][0] == '.')
				$stats[$i] = "0".$stats[$i];
		}
		
		//echo "<tr><td>Solar Run Hours</td><td>";
		$array[] = $stats[0];	//22
		
		//echo "<tr><td>Mains RUN HOURS</td><td>";
		$array[] = $stats[1];	//23
		
		//echo "<tr><td>Solar + mains  RUN HOURS</td><td>";
		$array[] = $stats[2];	//24
		
		//echo "<tr><td>Mains Energy</td><td>";
		$array[] = $stats[3];	//25
		
		//echo "<tr><td>Solar Energy</td><td>";
		$array[] = $stats[4];	//26
		
		
		
		//3 * 2 Bytes
		
		$sub_data = substr($data_raw, 109, 6);
		$stats = str_split($sub_data, 2);
		
		$sub_data = unpack("n",$stats[0]);
		$sub_data = $sub_data[1]/10;
		//echo "<tr><td>Battery bank Voltage</td><td>";
		$array[] = $sub_data;	//27
		
		$sub_data = unpack("n",$stats[1]);
		$sub_data = $sub_data[1]/10;
		//echo "<tr><td>Battery Charging current</td><td>";
		$array[] = $sub_data;	//28
		
		$sub_data = unpack("n",$stats[2]);
		$sub_data = $sub_data[1]/10;
		//echo "<tr><td>Battery Discharging current</td><td>";
		$array[] = $sub_data;	//29

		
		//1 * 1 Byte
		
		//Battery status
		$array[] = substr($data_raw, 115, 1);	//30
		
		
		//2 * 8 Bytes
		
		$sub_data = substr($data_raw, 116, 16);
		$stats = str_split($sub_data, 8);
		$stats = substr_replace($stats, ".", 7, 0);
		
		for ($i=0; $i<2; $i++)
		{
			$stats[$i] = ltrim($stats[$i], '0');
			if ($stats[$i][0] == '.')
				$stats[$i] = "0".$stats[$i];
		}
		
		//echo "<tr><td>Batt CHG energy</td><td>";
		$array[] = $stats[0];	//31
		
		//echo "<tr><td>BAT Dischg Energy</td><td>";
		$array[] = $stats[1];	//32
	
		return $array;
	}
	
	function DisplayEntryType1Header()
	{
		echo ("<table class='table table-striped table-condensed table-responsive'>");
	
		echo("<tr><thead>
					<th>#</th>
					<th>Site Id</th>

					<th>Type</th>
					<th>Date Time</th>
					<th>Status</th>
					<th>Device Id</th>
					
					<th>Room Temperature</th>
					<th>Fuel Level</th>
					<th>Fuel Data</th>
					<th>Site Battery bank Voltage</th>
					<th>R-Phase current</th>
					<th>Y-Phase current</th>
					<th>B-Phase current</th>
					<th>Mains Frequency</th>
					<th>DG frequency</th>
					<th>DG-R phase Voltage</th>
					<th>DG-Y phase Voltage</th>
					<th>DG-B phase Voltage</th>
					<th>LCU1 Output Voltage</th>
					<th>LCU2 Output Voltage</th>
					<th>LCU3 Output Voltage</th>
					<th>Input Mains Voltage - R Phase</th>
					<th>Input Mains Voltage - Y Phase</th>
					<th>Input Mains Voltage - B Phase</th>
					<th>DG Running Hours</th>
					<th>Mains RUN HOURS</th>
					<th>Battery RUN HOURS</th>
					<th>O/P Mains Energy</th>
					<th>DG Energy</th>
					<th>I/P Mains Energy</th>
					<th>DG Battery Voltage</th>
					<th>Battery Charging Current</th>
					<th>Battery Discharging Current</th>
					<th>Battery Status</th>
					<th>Battery Back Up Time</th>
					<th>Battery Charging Energy</th>
					<th>Battery Discharging Energy</th>
					<th>Raw Data</th>
				</thead></tr><tbody>");
	
	
	}
	
	function DisplayEntryType1HeaderEnd()
	{
	
		echo ("</tbody></table>");
	
	}
	
	function DisplayEntryType1($array)
	{
		$color = "";
		if ($array[3]==1)
		{
			$color= " class='danger' ";	
		}
		echo("<tr $color>");
		echo("<td>".$array[0]."</td>");	//#
		echo("<td>".$array[4]."</td>");	//Site Id
				
		$data_type_str = "";
		if ($array[3] == 0) $data_type_str = "P";
		if ($array[3] == 1) $data_type_str = "F";
		echo("<td>".$data_type_str."</td>");//Type
		
		echo("<td>".$array[5]."</td>");//Date Time
		
		
		
		$data_status_hex= bin2hex($array[7]);
		echo("<td><button type='button' class='btn btn-primary btn-sm' data-toggle='modal' OnClick='ShowDetail($array[0], 0)'>$data_status_hex</button></td>");//Status
		echo("<td>".$array[6]."</td>");//Device Id
		
		echo("<td>".$array[8]."</td>");//Room Temperature
		echo("<td>".$array[9]."</td>");//Fuel Level
		echo("<td>".$array[10]."</td>");//Fuel Data
		echo("<td>".$array[11]."</td>");//Site Batt. bank Voltage
		echo("<td>".$array[12]."</td>");//R-Phase current
		echo("<td>".$array[13]."</td>");//Y-Phase current
		echo("<td>".$array[14]."</td>");//B-Phase current
		echo("<td>".$array[15]."</td>");//Mains Frequency
		echo("<td>".$array[16]."</td>");//DG frequency
		echo("<td>".$array[17]."</td>");//DG-R phase Voltage
		echo("<td>".$array[18]."</td>");//DG-Y phase Voltage
		echo("<td>".$array[19]."</td>");//DG-B phase Voltage
		echo("<td>".$array[20]."</td>");//LCU1 Output Voltage
		echo("<td>".$array[21]."</td>");//LCU2 Output Voltage
		echo("<td>".$array[22]."</td>");//LCU3 Output Voltage
		echo("<td>".$array[23]."</td>");//Input Mains Voltage - R Phase
		echo("<td>".$array[24]."</td>");//Input Mains Voltage - Y Phase
		echo("<td>".$array[25]."</td>");//Input Mains Voltage - B Phase
		echo("<td>".$array[26]."</td>");//DG Running Hours
		echo("<td>".$array[27]."</td>");//Mains RUN HOURS
		echo("<td>".$array[28]."</td>");//Batt RUN HOURS
		echo("<td>".$array[29]."</td>");//O/P Mains Energy
		echo("<td>".$array[30]."</td>");//DG Energy
		echo("<td>".$array[31]."</td>");//I/P Mains Energy
		echo("<td>".$array[32]."</td>");//DG Battery Voltage
		echo("<td>".$array[33]."</td>");//Battery Charging current
		echo("<td>".$array[34]."</td>");//Battery Discharging current
		echo("<td>".$array[35]."</td>");//Battery status
		echo("<td>".$array[36]."</td>");//Battery back up time
		echo("<td>".$array[37]."</td>");//Battery Charging Energy
		echo("<td>".$array[38]."</td>");//Battery Disharging Energy
		
		echo("<td><button type='button' class='btn btn-primary btn-sm' data-toggle='modal' OnClick='ShowDetail($array[0], 1)'>Binary Data</button></td>");//Status

	}
	
	function DisplayEntryType2($array)
	{
		$color = "";
		if ($array[3]==1)
		{
			$color= " class='danger' ";	
		}
		echo("<tr $color>");
		echo("<td>".$array[0]."</td>");	//#
		echo("<td>".$array[4]."</td>");	//Site Id
				
		$data_type_str = "";
		if ($array[3] == 0) $data_type_str = "P";
		if ($array[3] == 1) $data_type_str = "F";
		echo("<td>".$data_type_str."</td>");//Type
		
		echo("<td>".$array[5]."</td>");//Date Time
		
		
		
		$data_status_hex= bin2hex($array[7]);
		echo("<td><button type='button' class='btn btn-primary btn-sm' data-toggle='modal' OnClick='ShowDetail($array[0], 0)'>$data_status_hex</button></td>");//Status
		echo("<td>".$array[6]."</td>");//Device Id
		
		echo("<td>".$array[8]."</td>");//Room Temperature
		echo("<td>".$array[9]."</td>");//Solar array 1 Voltage 
		echo("<td>".$array[10]."</td>");//Solar array 2 Voltage 
		echo("<td>".$array[11]."</td>");//Solar array 3 Voltage 
		echo("<td>".$array[12]."</td>");//Array1 current
		echo("<td>".$array[13]."</td>");//Array2 current
		echo("<td>".$array[14]."</td>");//Array3 current
		echo("<td>".$array[15]."</td>");//R-Phase current
		echo("<td>".$array[16]."</td>");//Y-Phase current
		echo("<td>".$array[17]."</td>");//B-Phase current
		echo("<td>".$array[18]."</td>");//Mains Frequency
		echo("<td>".$array[19]."</td>");//Input Mains Voltage – R Phase  
		echo("<td>".$array[20]."</td>");//Input Mains Voltage – Y Phase 
		echo("<td>".$array[21]."</td>");//Input Mains Voltage – B Phase 
		echo("<td></td>");//
		echo("<td></td>");//
		echo("<td></td>");//
		echo("<td></td>");//
		echo("<td>".$array[22]."</td>");//Solar Run Hours 
		echo("<td>".$array[23]."</td>");//Mains RUN HOURS
		echo("<td>".$array[24]."</td>");//Solar + mains  RUN HOURS
		echo("<td>".$array[25]."</td>");//Mains Energy
		echo("<td>".$array[26]."</td>");//Solar Energy
		
		echo("<td></td>");//
		echo("<td>".$array[27]."</td>");// Battery bank Voltage  Voltage 
		echo("<td>".$array[28]."</td>");//Battery Charging current
		echo("<td>".$array[29]."</td>");//Battery Discharging current
		
		echo("<td>".$array[30]."</td>");//Battery status
		echo("<td></td>");//
		echo("<td>".$array[31]."</td>");//Battery Charging Energy
		echo("<td>".$array[32]."</td>");//Battery Disharging Energy
		
		echo("<td><button type='button' class='btn btn-primary btn-sm' data-toggle='modal' OnClick='ShowDetail($array[0], 1)'>Binary Data</button></td>");//Status

	}
	
	
	function DisplayEntryTypePhase2($id, $site_id, $date_time)
	{
		echo("<tr>");
		echo("<td>".$id."</td>");	//#
		echo("<td>".$site_id."</td>");	//Site Id
		
		echo("<td>".$date_time."</td>");//Date Time
		
		
		echo("<td><button type='button' class='btn btn-primary btn-sm' data-toggle='modal' OnClick='ShowDetail($id, 2)'>Phase 2 Site</button></td>");//Status
				
		echo("<td><button type='button' class='btn btn-primary btn-sm' data-toggle='modal' OnClick='ShowDetail($id, 1)'>Binary Data</button></td>");//Status

	}
	
	
	function GetSiteList()
	{
		include 'dbinc.php'; 
		

		$array = array();
		
		

		$mysqli = new mysqli("$mysql_hostname", "$mysql_user", "$mysql_password", "$mysql_database");
		if (mysqli_connect_errno())
		{
			printf("Connect failed: %s\n", mysqli_connect_error());
			return null;
		}
		
		$sql = "SELECT site_id, SiteName, site_type 
					FROM siteinfo";

		//echo "$sql <br>";				
		$result = $mysqli->query($sql);

		if ($result !== false)
		{
			while($data = $result->fetch_row())
			{
				
				$arr2 = array();
				$arr2[] = $data[0]; 
				$arr2[] = $data[1];
				$arr2[] = $data[2];
				
				$array[] = $arr2;
			}
			
		}
		
		//var_dump($array);
		
		$result->close();
		
		mysqli_close($mysqli);
		//echo "NULL";
		
		return $array;
	
	}

function GetReports()
{
	include 'dbinc.php';

	$array = array();

	$mysqli = new mysqli("$mysql_hostname", "$mysql_user", "$mysql_password", "$mysql_database");
	if (mysqli_connect_errno()) {
		printf("Connect failed: %s\n", mysqli_connect_error());
		return null;
	}

	$sql = "SELECT DISTINCT
          site_id
          FROM quanta";
//echo $sql;


	$result = $mysqli->query($sql);
	if (!$result)
		die("problem" . mysqli_error());


	$one_date = date("Y-m-d 24:59:59");
	$two_date = date("Y-m-d 00:00:00");

	$y = date("d") - 1;

	$three_date = date("Y-m-$y 24:59:59");
	$four_date = date("Y-m-$y 00:00:00");

	$count_total = 0;
	$c = 0;
	while ($data = $result->fetch_array()) {

		//$sql1 = "SELECT site_id FROM quanta WHERE site_id = '$data[0]'";
		//$result1 = $mysqli->query($sql1);


		$sql_site_id = "SELECT 
                site_id
                FROM quanta
                WHERE site_id = '$data[0]'";

		$sql_last_record = "SELECT
                    timestamp,date_time
                    FROM quanta
                    WHERE site_id = '$data[0]'
                    ORDER BY ID DESC";

		$sql_today_timestamp = "SELECT
                        timestamp
                        FROM quanta
                        WHERE site_id = '$data[0]' AND timestamp >= '$two_date' AND timestamp <= '$one_date' ";

		$sql_today_date_time = "SELECT
                        date_time
                        FROM quanta
                        WHERE site_id = '$data[0]' AND date_time >= '$two_date' AND date_time <= '$one_date'";

		$sql_yesterday_timestamp = "SELECT
                        timestamp
                        FROM quanta
                        WHERE site_id = '$data[0]' AND timestamp >= '$four_date' AND timestamp <= '$three_date' ";

		$sql_yesterday_date_time = "SELECT
                        date_time
                        FROM quanta
                        WHERE site_id = '$data[0]' AND date_time >= '$four_date' AND date_time <= '$three_date'";


		$result_site_id = $mysqli->query($sql_site_id);
		if (!$result_site_id)
			die("problem1w" . mysqli_error());
		$num_records = $result_site_id->num_rows;


		$result_last_record = $mysqli->query($sql_last_record);
		if (!$result_last_record)
			die("problem2" . mysqli_error());
		$last = $result_last_record->fetch_row();
		$last_timestamp = $last[0];
		$last_date_time = $last[1];


		$result_today_timestamp = $mysqli->query($sql_today_timestamp);
		if (!$result_today_timestamp)
			die("problem3" . mysqli_error());
		$total_today_timestamp = $result_today_timestamp->num_rows;

		$result_today_date_time = $mysqli->query($sql_today_date_time);
		if (!$result_today_date_time)
			die("problem4" . mysqli_error());
		$total_today_date_time = $result_today_date_time->num_rows;


		$result_yesterday_timestamp = $mysqli->query($sql_yesterday_timestamp);
		if (!$result_yesterday_timestamp)
			die("problem3" . mysqli_error());
		$total_yesterday_timestamp = $result_yesterday_timestamp->num_rows;

		$result_yesterday_date_time = $mysqli->query($sql_yesterday_date_time);
		if (!$result_yesterday_date_time)
			die("problem4" . mysqli_error());
		$total_yesterday_date_time = $result_yesterday_date_time->num_rows;

//    for($i = 0; $i<count($result->fetch_row()); $i++)
//    {
		$array[$c][0] = $data[0];                   //Site Id
		$array[$c][1] = $num_records;               //Record Count
		$array[$c][2] = $last_timestamp;            //Last Timestamp
		$array[$c][3] = $last_date_time;            //Last Date time
		$array[$c][4] = $total_today_timestamp;     //Total today timestamp
		$array[$c][5] = $total_today_date_time;     //Total today datetime
		$array[$c][6] = $total_yesterday_timestamp;  //Total yesterday timestamp
		$array[$c][7] = $total_yesterday_date_time;  //Total yesterday datetime

		$count_total = $count_total + $num_records;
		$c++;
	}
	mysqli_close($mysqli);
	$size = sizeof($array)+1;
	$array[$size][0] = "";
	$array[$size][1] = $count_total;
	//array_push($array,'',$count_total);
	return $array;
}
function ExportReports()
{
	$getReport = GetReports();

	header('Content-Type: text/csv; charset=utf-8');
	header('Content-Disposition: attachment; filename=exported_reports.csv');

	$output = fopen('php://output', 'w');
	$date = new DateTime("now", new DateTimeZone('Asia/Kolkata') );
	$localtime_str = $date->format('Y-m-d H:i:s');

	fputcsv($output, array('Site Data Exported on '.$localtime_str));
    fputcsv($output, array(''));

	fputcsv($output, array('Site Id','Record Count','Last Record Timestamp','Last Record Date Time','Total Timestamp Records Received Today'
	,'Total Date Time Records Received Today','Total Timestamp Records Received Yesterday','Total Date Time Records Received Yesterday'));
	//fputcsv($output, $getReport[0][0]);

	//$reports = $getReport;
	$xlsreport = array();
	for($i = 0; $i < sizeof($getReport)-1; $i++)
	{
		for($j=0; $j < 8; $j++)
		{
			$xlsreport[$j] = $getReport[$i][$j];
		}
		fputcsv($output,$xlsreport);
	}
	$size = sizeof($getReport);
	fputcsv($output, array('Total',$getReport[$size][1]));
	fclose($output);
}


	
	function ExportCSV($filter_data_type, $site_list_data_type, $export_page_no, $excel_format)
	{
		//print("filter_data_type = $filter_data_type -- site_list_data_type = $site_list_data_type");
		include 'dbinc.php';

		$rec_limit = 5000;
		$rec_start = ($export_page_no - 1)*$rec_limit;

		$mysqli = new mysqli("$mysql_hostname", "$mysql_user", "$mysql_password", "$mysql_database");
		if (mysqli_connect_errno()) {
			printf("Connect failed: %s\n", mysqli_connect_error());
			exit();
		}
		

		$where_clause="";
		$filter_data_type_str = "All Data";
		if ($filter_data_type == 1) {$where_clause = " WHERE (type=1)  "; $filter_data_type_str = "Fault Data";}
		if ($filter_data_type == 2) {$where_clause = " WHERE (type=0)  "; $filter_data_type_str = "Periodic Data";}
		
		$site_list_data_type_str = "All Sites";
		if ($site_list_data_type == "0")
		{
			
		}
		else
		{
			if ($filter_data_type == 0) 
				$where_clause = " WHERE ";
			else
				$where_clause .= "  AND ";
				
			$where_clause .= " (site_id=\"".$site_list_data_type."\")  "; 
			$site_list_data_type_str = $site_list_data_type;
			
		}
		
		//echo "site_list_data_type: $site_list_data_type<br>";
		
		$order = "SELECT ID ".
					"FROM quanta ".$where_clause.
					"ORDER BY date_time DESC ".
					"LIMIT $rec_limit OFFSET $rec_start";

		//echo "order: $order<br>";	
		$result = $mysqli->query($order);

		
		// output headers so that the file is downloaded rather than displayed
		header('Content-Type: text/csv; charset=utf-8');
		header('Content-Disposition: attachment; filename=exported_data.csv');

		// create a file pointer connected to the output stream
		$output = fopen('php://output', 'w');
		
		// Heading and other information
		
		$date = new DateTime("now", new DateTimeZone('Asia/Kolkata') );
		$localtime_str = $date->format('Y-m-d H:i:s');
		
		fputcsv($output, array('Site Data Exported on '.$localtime_str));
		fputcsv($output, array('Filter Details -- Data Type: '.$filter_data_type_str.', Site: '.$site_list_data_type_str));
		fputcsv($output, array(''));
		

		// output the column headings
		fputcsv($output, array('Id', 'Periodic / Fault', 'Site Id', 'Date Time', 'Device Code', 'Status Bits', 'Temperature',
			'Fuel Level', 'Fuel Data', 'Batt Voltage', 'R-Phase Current', 'Y-Phase Current', 'B-Phase Current', 'Mains Frequency', 'DG Frequency',
			'DG-R Phase Voltage', 'DG-Y Phase Voltage', 'DG-B Phase Voltage', 'LCU1 Output Voltage', 'LCU2 Output Voltage', 'LCU3 Output Voltage', 'Mains Voltage - R Phase', 'Mains Voltage - Y Phase', 'Mains Voltage - B Phase', 'DG Running Hours', 'Mains Run Hours', 'Batt Run Hours', 'O/P Mains Energy', 'DG Energy', 'I/P Mains Energy', 'DG Battery Voltage', 'Battery Charging Current', 'Battery Discharging Current', 
			'Battery Status', 'Battery Backup Time', 'Battery Charging Energy', 'Battery Discharging Energy'));



		$count = 0;
		while($data = $result->fetch_row()){
			$array = GetDeviceRecordFromId($data[0], true);
			//var_dump($array);
			
			//For excel put status bits like ="xxxxxxxxxx"
			if ($excel_format == 1)
				$array[5] = '="'.$array[5].'"';
			
			fputcsv($output, $array);	
			
			$count++;
		}
		
		
		$result->close();
		mysqli_close($mysqli);
		
		fclose($output);
	
	
	}
	
	
	
	
	//Customer List
	function GetAllCustomers()
	{
		$array = array();
		
		include 'dbinc.php'; 
		

		$mysqli = new mysqli("$mysql_hostname", "$mysql_user", "$mysql_password", "$mysql_database");
		if (mysqli_connect_errno())
		{
			printf("Connect failed: %s\n", mysqli_connect_error());
			return null;
		}
		
		$sql = "SELECT 
			Id, Name , Address , CRMRef , logo
			FROM customers ";
		//echo $sql;

		$result = $mysqli->query($sql);	
		
		while($data = $result->fetch_row()){
			$array[] = $data;
		}
		
		
		$result->close();
		mysqli_close($mysqli);
		
		//var_dump($array);
		
		return $array;
	
	}
	
	function GetCustomerWithId($custid)
	{
		$array = null;
		
		include 'dbinc.php'; 
		

		$mysqli = new mysqli("$mysql_hostname", "$mysql_user", "$mysql_password", "$mysql_database");
		if (mysqli_connect_errno())
		{
			printf("Connect failed: %s\n", mysqli_connect_error());
			return null;
		}
		
		$sql = "SELECT 
			Id, Name , Address , CRMRef , logo
			FROM customers 
			WHERE Id=" . $custid;
		//echo $sql;

		$result = $mysqli->query($sql);	
		
		if($data = $result->fetch_row()){
			$array = $data;
		}
		
		
		$result->close();
		mysqli_close($mysqli);
		
		//var_dump($array);
		
		return $array;
	
	}
	
	function EditCustomerWithId($custid, $name, $address)
	{
		$resp = null;
		
		include 'dbinc.php';
		

		$mysqli = new mysqli("$mysql_hostname", "$mysql_user", "$mysql_password", "$mysql_database");
		if (mysqli_connect_errno())
		{
			printf("Connect failed: %s\n", mysqli_connect_error());
			return null;
		}
		
		$sql = "UPDATE customers 
			SET Name = '$name' , Address = '$address' 
			WHERE Id=" . $custid;
		//echo $sql;

		if ($mysqli->query($sql) === TRUE) {
			echo "Customer record edited successfully";
			$resp = null;
		} else {
			$resp = "Error: ". $mysqli->error;
			echo $resp;
		}
		
		/*if($data = $result->fetch_row()){
			$array = $data;
		}*/
		
		

		mysqli_close($mysqli);
		
		//var_dump($array);
		
		return $resp;
	
	}
	
	function AddNewCustomerRecord($add_name, $add_address, $add_logo)
	{
		//var_dump($add_name);
		include 'dbinc.php';

		$resp = null;
		
		
		if (IsNullOrEmptyString($add_name))
			return "Error: Empty Name not allowed";
		

		$mysqli = new mysqli("$mysql_hostname", "$mysql_user", "$mysql_password", "$mysql_database");
		if (mysqli_connect_errno())
		{
			printf("Connect failed: %s\n", mysqli_connect_error());
			return null;
		}
		
		$sql = "INSERT INTO customers (Name, Address, logo, CRMRef)
			VALUES ('$add_name', '$add_address', '$add_logo','')";

		var_dump($sql); 
			
		if ($mysqli->query($sql) === TRUE) {
			echo "New record created successfully";
			$resp = null;
		} else {
			$resp = "Error: ". $mysqli->error;
			echo $resp;
		}

		$mysqli->close();
		
		return $resp;
	}

function DeleteCustomerRecord($customer_id)
{
	//var_dump($add_name);
	include 'dbinc.php';

	$resp = null;





	$mysqli = new mysqli("$mysql_hostname", "$mysql_user", "$mysql_password", "$mysql_database");
	if (mysqli_connect_errno())
	{
		printf("Connect failed: %s\n", mysqli_connect_error());
		return null;
	}

	$sql = "DELETE from customers WHERE Id = " . $customer_id;

	//var_dump($sql);

	if ($mysqli->query($sql) === TRUE) {
		echo "Customer record deleted successfully";
		$resp = null;
	} else {
		$resp = "Error: ". $mysqli->error;
		echo $resp;
	}

	$mysqli->close();

	return $resp;

}




function GetAllClusters()
{
	$array = array();

	include 'dbinc.php';


	$mysqli = new mysqli("$mysql_hostname", "$mysql_user", "$mysql_password", "$mysql_database");
	if (mysqli_connect_errno())
	{
		printf("Connect failed: %s\n", mysqli_connect_error());
		return null;
	}

	$sql = "SELECT 
			id,customerId, name, location 
			FROM clusters ";
	//echo $sql;

	$result = $mysqli->query($sql);
  if(!$result)
  	die("problem".  mysqli_error());
	while($data = $result->fetch_row()){
		$array[] = $data;
	}


	$result->close();
	mysqli_close($mysqli);

	//var_dump($array);

	return $array;

}

function GetClustersWithId($clusterid)
{
	$array = null;

	include 'dbinc.php';

	$mysqli = new mysqli("$mysql_hostname", "$mysql_user", "$mysql_password", "$mysql_database");
	if (mysqli_connect_errno())
	{
		printf("Connect failed: %s\n", mysqli_connect_error());
		return null;
	}

	$sql = "SELECT 
			id,customerId, name , location 
			FROM clusters 
			WHERE id=" . $clusterid;
	//echo $sql;

	$result = $mysqli->query($sql);

	if($data = $result->fetch_row()){
		$array = $data;
	}


	$result->close();
	mysqli_close($mysqli);

	//var_dump($array);

	return $array;

}

function EditClustersWithId($clusterid, $customerid, $name, $location)
{
	$resp = null;

	include 'dbinc.php';


	$mysqli = new mysqli("$mysql_hostname", "$mysql_user", "$mysql_password", "$mysql_database");
	if (mysqli_connect_errno())
	{
		printf("Connect failed: %s\n", mysqli_connect_error());
		return null;
	}

	$sql = "UPDATE clusters
			SET customerId = '$customerid', name = '$name', location = '$location' 
			WHERE id=" . $clusterid;
	//echo $sql;

	if ($mysqli->query($sql) === TRUE) {
		//echo "Cluster Edited successfully";
		$resp = null;
	} else {
		$resp = "Error: ". $mysqli->error;
	}

	$mysqli->close();

	return $resp;

}

function AddNewClusterRecord($add_customerId, $add_name, $add_location)
{
	//var_dump($add_name);
	include 'dbinc.php';

	$resp = null;


	if (IsNullOrEmptyString($add_name))
		return "Error: Empty Name not allowed";


	$mysqli = new mysqli("$mysql_hostname", "$mysql_user", "$mysql_password", "$mysql_database");
	if (mysqli_connect_errno())
	{
		printf("Connect failed: %s\n", mysqli_connect_error());
		return null;
	}

	$sql = "INSERT INTO clusters (customerId, name, location)
			VALUES ('$add_customerId', '$add_name', '$add_location')";

	//var_dump($sql);

	if ($mysqli->query($sql) === TRUE) {
		//echo "New record created successfully";
		$resp = null;
	} else {
		$resp = "Error: ". $mysqli->error;
	}

	$mysqli->close();

	return $resp;

}
function DeleteClusterRecord($cluster_id)
{
	//var_dump($add_name);
	include 'dbinc.php';

	$resp = null;





	$mysqli = new mysqli("$mysql_hostname", "$mysql_user", "$mysql_password", "$mysql_database");
	if (mysqli_connect_errno())
	{
		printf("Connect failed: %s\n", mysqli_connect_error());
		return null;
	}

	$sql = "DELETE from clusters WHERE id = " . $cluster_id;

	//var_dump($sql);

	if ($mysqli->query($sql) === TRUE) {
		//echo "New record created successfully";
		$resp = null;
	} else {
		$resp = "Error: ". $mysqli->error;
	}

	$mysqli->close();

	return $resp;

}



function GetCustomerNameWithId($customer_id)
{
    $array = null;

    include 'dbinc.php';

    $mysqli = new mysqli("$mysql_hostname", "$mysql_user", "$mysql_password", "$mysql_database");
    if (mysqli_connect_errno())
    {
        printf("Connect failed: %s\n", mysqli_connect_error());
        return null;
    }

    $sql = "SELECT 
			Name
			FROM customers
			WHERE Id=" . $customer_id;
    //echo $sql;

    $result = $mysqli->query($sql);

    if($data = $result->fetch_row()){
        $array = $data;
    }


    $result->close();
    mysqli_close($mysqli);

    //var_dump($array);

    return $array;

}










function GetAllAuths()
{
	$array = array();

	include 'dbinc.php';

	$mysqli = new mysqli("$mysql_hostname", "$mysql_user", "$mysql_password", "$mysql_database");
	if (mysqli_connect_errno())
	{
		printf("Connect failed: %s\n", mysqli_connect_error());
		return null;
	}

	$sql = "SELECT 
			id, username, passcode, type, Customer, typeref 
			FROM authentication ";
	//echo $sql;

	$result = $mysqli->query($sql);

	while($data = $result->fetch_row()){
		$array[] = $data;
	}


	$result->close();
	mysqli_close($mysqli);

	//var_dump($array);
//echo "the size is = ". sizeof($data);
	return $array;

}



function GetAuthWithId($authid)
{
	$array = null;

	include 'dbinc.php';

	$mysqli = new mysqli("$mysql_hostname", "$mysql_user", "$mysql_password", "$mysql_database");
	if (mysqli_connect_errno())
	{
		printf("Connect failed: %s\n", mysqli_connect_error());
		return null;
	}

	$sql = "SELECT
			username, passcode, type, Customer, typeref
			FROM authentication
			WHERE id=" . $authid;
	//echo $sql;

	$result = $mysqli->query($sql);

	if($data = $result->fetch_row()){
		$array = $data;
	}


	$result->close();
	mysqli_close($mysqli);

	//var_dump($array);

	return $array;

}

function EditAuthWithId($authid, $username, $passcode, $type, $customer, $typeref)
{
	$resp = null;

	include 'dbinc.php';


	$mysqli = new mysqli("$mysql_hostname", "$mysql_user", "$mysql_password", "$mysql_database");
	if (mysqli_connect_errno())
	{
		printf("Connect failed: %s\n", mysqli_connect_error());
		return null;
	}

	$sql = "UPDATE authentication
			SET 
			username 				= '$username', 
			passcode 				= '$passcode', 
			type 					= '$type', 
			Customer 				= '$customer', 
			typeref      			= '$typeref'
		
			 
			WHERE id=" . $authid;
	//echo $sql;

	if ($mysqli->query($sql) === TRUE) {
		echo "Cluster Edited successfully";
		$resp = null;
	} else {
		$resp = "Error: ". $mysqli->error;
        echo $resp;
	}

	$mysqli->close();

	return $resp;

}

function AddNewAuthRecord($username, $passcode, $type, $customer, $typeref)

{
	//var_dump($add_name);
	include 'dbinc.php';

	$resp = null;

	if (IsNullOrEmptyString($username))
		return "Error: Empty Name not allowed";

	if (IsNullOrEmptyString($passcode))
		return "Error: Empty Name not allowed";


	$mysqli = new mysqli("$mysql_hostname", "$mysql_user", "$mysql_password", "$mysql_database");
	if (mysqli_connect_errno())
	{
		printf("Connect failed: %s\n", mysqli_connect_error());
		return null;
	}

	$sql = "INSERT INTO authentication (username, passcode, type, Customer, typeref)
							
			VALUES ('$username', '$passcode', '$type', '$customer', '$typeref')";

	//var_dump($sql);




	if ($mysqli->query($sql) === TRUE) {
		echo "New record created successfully";
		$resp = null;
	} else {
		$resp = "Error: ". $mysqli->error;
        echo $resp;
	}

	$mysqli->close();

	return $resp;

}
function DeleteAuthRecord($authid)
{
	//var_dump($add_name);
	include 'dbinc.php';

	$resp = null;


	$mysqli = new mysqli("$mysql_hostname", "$mysql_user", "$mysql_password", "$mysql_database");
	if (mysqli_connect_errno())
	{
		printf("Connect failed: %s\n", mysqli_connect_error());
		return null;
	}

	$sql = "DELETE from authentication WHERE id = " . $authid;

	//var_dump($sql);

	if ($mysqli->query($sql) === TRUE) {
		//echo "New record created successfully";
		$resp = null;
	} else {
		$resp = "Error: ". $mysqli->error;
	}

	$mysqli->close();

	return $resp;

}






	$REPORT_FLAG_1_SITE_REPORT_EMAIL 	= 0x80000000;
	$REPORT_FLAG_2_MODE_CHANGE_EMAIL 	= 0x40000000;
	$REPORT_FLAG_3_ALARM_SMS 			= 0x20000000;

	//32-bit binary bits from text 10101010 flag
	function PackBinaryFlag($report_flags)
	{
		var_dump($report_flags);
		$report_flags_binary = "0000";
		
		$stats = unpack ( "C*" , $report_flags_binary );
		$stats[1] = 0;
		$stats[2] = 0;
		$stats[3] = 0;
		$stats[4] = 0;
		
		//Byte 1
		if (substr($report_flags, 0, 1) == "1")
			$stats[1] = $stats[1] | 0x80;
			
		if (substr($report_flags, 1, 1) == "1")
			$stats[1] = $stats[1] | 0x40;
		
		if (substr($report_flags, 2, 1) == "1")
			$stats[1] = $stats[1] | 0x20;

		if (substr($report_flags, 3, 1) == "1")
			$stats[1] = $stats[1] | 0x10;			
			
		if (substr($report_flags, 4, 1) == "1")
			$stats[1] = $stats[1] | 0x08;
			
		if (substr($report_flags, 5, 1) == "1")
			$stats[1] = $stats[1] | 0x04;
		
		if (substr($report_flags, 6, 1) == "1")
			$stats[1] = $stats[1] | 0x02;

		if (substr($report_flags, 7, 1) == "1")
			$stats[1] = $stats[1] | 0x01;
			
		//Byte 2
		if (substr($report_flags, 8, 1) == "1")
			$stats[2] = $stats[2] | 0x80;
			
		if (substr($report_flags, 9, 1) == "1")
			$stats[2] = $stats[2] | 0x40;
		
		if (substr($report_flags, 10, 1) == "1")
			$stats[2] = $stats[2] | 0x20;

		if (substr($report_flags, 11, 1) == "1")
			$stats[2] = $stats[2] | 0x10;			
			
		if (substr($report_flags, 12, 1) == "1")
			$stats[2] = $stats[2] | 0x08;
			
		if (substr($report_flags, 13, 1) == "1")
			$stats[2] = $stats[2] | 0x04;
		
		if (substr($report_flags, 14, 1) == "1")
			$stats[2] = $stats[2] | 0x02;

		if (substr($report_flags, 15, 1) == "1")
			$stats[2] = $stats[2] | 0x01;
			
		//Byte 3
		if (substr($report_flags, 16, 1) == "1")
			$stats[3] = $stats[3] | 0x80;
			
		if (substr($report_flags, 17, 1) == "1")
			$stats[3] = $stats[3] | 0x40;
		
		if (substr($report_flags, 18, 1) == "1")
			$stats[3] = $stats[3] | 0x20;

		if (substr($report_flags, 19, 1) == "1")
			$stats[3] = $stats[3] | 0x10;			
			
		if (substr($report_flags, 20, 1) == "1")
			$stats[3] = $stats[3] | 0x08;
			
		if (substr($report_flags, 21, 1) == "1")
			$stats[3] = $stats[3] | 0x04;
		
		if (substr($report_flags, 22, 1) == "1")
			$stats[3] = $stats[3] | 0x02;

		if (substr($report_flags, 23, 1) == "1")
			$stats[3] = $stats[3] | 0x01;
			
		//Byte 4
		if (substr($report_flags, 24, 1) == "1")
			$stats[4] = $stats[4] | 0x80;
			
		if (substr($report_flags, 25, 1) == "1")
			$stats[4] = $stats[4] | 0x40;
		
		if (substr($report_flags, 26, 1) == "1")
			$stats[4] = $stats[4] | 0x20;

		if (substr($report_flags, 27, 1) == "1")
			$stats[4] = $stats[4] | 0x10;			
			
		if (substr($report_flags, 28, 1) == "1")
			$stats[4] = $stats[4] | 0x08;
			
		if (substr($report_flags, 29, 1) == "1")
			$stats[4] = $stats[4] | 0x04;
		
		if (substr($report_flags, 30, 1) == "1")
			$stats[4] = $stats[4] | 0x02;

		if (substr($report_flags, 31, 1) == "1")
			$stats[4] = $stats[4] | 0x01;
			
		$output = pack("CCCC", $stats[1], $stats[2],$stats[3],$stats[4]);

		return $output;
		
	}
	
	function GetReportFlag($report_flags, $flag_constant)
	{
		if (is_string($report_flags))
		{
			$tmp = unpack("N", $report_flags);
			$report_flags = $tmp[1];
			//echo(dechex($report_flags));
		}
		//var_dump($report_flags);
		$tmp = $report_flags & $flag_constant;
		return ($tmp != 0);
	}





function GetAllSiteList()
{
    $array = array();

    include 'dbinc.php';


    $mysqli = new mysqli("$mysql_hostname", "$mysql_user", "$mysql_password", "$mysql_database");
    if (mysqli_connect_errno())
    {
        printf("Connect failed: %s\n", mysqli_connect_error());
        return null;
    }

    $sql = "SELECT 
			Id, site_id, CustomerId, ClusterId, DeviceName, DeviceMake, Circle, SiteName,	District, mobile, Longitude, Latitude, SiteIndoorType, DG_KVA, SAM_Name, SAM_Number, AM_Name, AM_Number, 

			Tech_Name, Tech_Number,	Comments, date_of_installation,	DG_make, EB_capacity, EB_phase,	battery_AH,	battery_qty, battery_make, DG2_KVA, DG2_MAKE, site_type,sam_email, am_email, tech_email, 
							
			report_flags, site_enabled
			FROM siteinfo ";
    //echo $sql;

    $result = $mysqli->query($sql);

    while($data = $result->fetch_row()){
        $array[] = $data;
    }


    $result->close();
    mysqli_close($mysqli);

    //var_dump($array);
//echo "the size is = ". sizeof($data);
    return $array;

}



function GetSiteWithId($siteid)
{
    $array = null;

    include 'dbinc.php';

    $mysqli = new mysqli("$mysql_hostname", "$mysql_user", "$mysql_password", "$mysql_database");
    if (mysqli_connect_errno())
    {
        printf("Connect failed: %s\n", mysqli_connect_error());
        return null;
    }

    $sql = "SELECT
			site_id, CustomerId, ClusterId, DeviceName, DeviceMake, Circle, SiteName,	District, mobile, Longitude, Latitude, SiteIndoorType, DG_KVA, SAM_Name, SAM_Number, AM_Name, AM_Number, 

			Tech_Name, Tech_Number,	Comments, date_of_installation,	DG_make, EB_capacity, EB_phase,	battery_AH,	battery_qty, battery_make, DG2_KVA, DG2_MAKE, site_type,sam_email, am_email, tech_email, 
							
			report_flags, site_enabled
			FROM siteinfo
			WHERE Id=" . $siteid;
    //echo $sql;

    $result = $mysqli->query($sql);

    if($data = $result->fetch_row()){
        $array = $data;
    }


    $result->close();
    mysqli_close($mysqli);

    //var_dump($array);

    return $array;

}

function EditSiteWithId($sid,$customerid,$clusterid,$devicename,$devicemake,$circle,$sitename,$district,$mobile,$longitude,$latitude,$siteindoor_type,

                        $dg_kva,$sam_name, $sam_number, $am_name, $am_number, $tech_name, $tech_number,$comments, $date_of_installation, $dg_make,

                        $eb_capacity,$eb_phase,$battery_ah,$battery_qty,$battery_make, $dg2_kva, $dg2_make, $site_type,$sam_email, $am_email, $tech_email,$report_flags,
                        $site_enable)
{
    $resp = null;
	$report_flags = PackBinaryFlag($report_flags);

    include 'dbinc.php';


    $mysqli = new mysqli("$mysql_hostname", "$mysql_user", "$mysql_password", "$mysql_database");
    if (mysqli_connect_errno())
    {
        printf("Connect failed: %s\n", mysqli_connect_error());
        return null;
    }

    $sql = "UPDATE siteinfo
			SET 
			CustomerId 				=  $customerid, 
			ClusterId 				= '$clusterid', 
			DeviceName 				= '$devicename', 
			DeviceMake 				= '$devicemake', 
			Circle      			= '$circle',
			SiteName   				= '$sitename',	
			District				= '$district', 
			mobile					= '$mobile', 
			Longitude				= '$longitude', 
			Latitude				= '$latitude', 
			SiteIndoorType			= '$siteindoor_type', 
			DG_KVA					=  $dg_kva, 
			SAM_Name				= '$sam_name', 
			SAM_Number				= '$sam_number', 
			AM_Name					= '$am_name', 
			AM_Number				= '$am_number',
			Tech_Name				= '$tech_name', 
			Tech_Number				= '$tech_number',	
			Comments				= '$comments', 
			date_of_installation	=  $date_of_installation,	
			DG_make					= '$dg_make', 
			EB_capacity				=  $eb_capacity, 
			EB_phase				=  $eb_phase,	
			battery_AH				=  $battery_ah,	
			battery_qty				=  $battery_qty, 
			battery_make			= '$battery_make', 
			DG2_KVA 				=  $dg2_kva, 
			DG2_MAKE				= '$dg2_make', 
			site_type				=  $site_type,
			sam_email				= '$sam_email', 
			am_email				= '$am_email', 
			tech_email				= '$tech_email', 
			report_flags            = '$report_flags', 
			site_enabled			= '$site_enable'
			 
			WHERE Id=" . $sid;
    //echo $sql;

    if ($mysqli->query($sql) === TRUE) {
        echo "Site Edited successfully";
        $resp = null;
    } else {
        $resp = "Error: ". $mysqli->error;
		echo $resp;
    }

    $mysqli->close();

    return $resp;

}


function AddNewSiteRecord($siteid,$customerid,$clusterid,$devicename,$devicemake,$circle,$sitename,$district,$mobile,$longitude,$latitude,$siteindoor_type,
	  $dg_kva,$sam_name, $sam_number, $am_name, $am_number, $tech_name, $tech_number,$comments, $date_of_installation, $dg_make,
	  $eb_capacity,$eb_phase,$battery_ah,$battery_qty,$battery_make, $dg2_kva, $dg2_make, $site_type,$sam_email, $am_email, $tech_email,$report_flags,
	  $site_enable)

{
    // var_dump($report_flags);
	// $report_flags_binary = 0;
	// if (substr($report_flags, 0, 1) == "1")
		// $report_flags_binary = $report_flags_binary | 0x80000000;
		
	// if (substr($report_flags, 1, 1) == "1")
		// $report_flags_binary = $report_flags_binary | 0x40000000;
	
	// if (substr($report_flags, 2, 1) == "1")
		// $report_flags_binary = $report_flags_binary | 0x20000000;
	
	// echo "***** Output:    ";
	// echo(bin2hex($report_flags_binary));
	
	
	
    include 'dbinc.php';

    $resp = null;
	$report_flags = PackBinaryFlag($report_flags);

    if (IsNullOrEmptyString($siteid))
        return "Error: Empty Name not allowed";

    if (IsNullOrEmptyString($sitename))
        return "Error: Empty Name not allowed";


    $mysqli = new mysqli("$mysql_hostname", "$mysql_user", "$mysql_password", "$mysql_database");
    if (mysqli_connect_errno())
    {
        printf("Connect failed: %s\n", mysqli_connect_error());
        return null;
    }

    $sql = "INSERT INTO siteinfo (site_id, CustomerId, ClusterId, DeviceName, DeviceMake, Circle, SiteName,	District, mobile, Longitude, Latitude, SiteIndoorType, DG_KVA, SAM_Name, SAM_Number, AM_Name, AM_Number, 

							Tech_Name, Tech_Number,	Comments, date_of_installation,	DG_make, EB_capacity, EB_phase,	battery_AH,	battery_qty, battery_make, DG2_KVA, DG2_MAKE, site_type,sam_email, am_email, tech_email, 
							
							report_flags, site_enabled)
							
			VALUES ('$siteid',$customerid,'$clusterid','$devicename','$devicemake','$circle','$sitename','$district','$mobile','$longitude','$latitude','$siteindoor_type',

						  $dg_kva,'$sam_name', '$sam_number', '$am_name', '$am_number', '$tech_name', '$tech_number','$comments', $date_of_installation, '$dg_make',

						 $eb_capacity,$eb_phase,$battery_ah,$battery_qty,'$battery_make', $dg2_kva, '$dg2_make', $site_type,'$sam_email', '$am_email', '$tech_email','$report_flags', '$site_enable')";

    //var_dump($sql);




    if ($mysqli->query($sql) === TRUE) {
        echo "New record created successfully";
        $resp = null;
    } else {
        $resp = "Error: ". $mysqli->error;
		echo $resp;
    }

    $mysqli->close();

    return $resp;

}
function DeleteSiteRecord($site_id)
{
    //var_dump($add_name);
    include 'dbinc.php';

    $resp = null;


    $mysqli = new mysqli("$mysql_hostname", "$mysql_user", "$mysql_password", "$mysql_database");
    if (mysqli_connect_errno())
    {
        printf("Connect failed: %s\n", mysqli_connect_error());
        return null;
    }

    $sql = "DELETE from siteinfo WHERE id = " . $site_id;

    //var_dump($sql);

    if ($mysqli->query($sql) === TRUE) {
        echo "Site Deleted successfully";
        $resp = null;
    } else {
        $resp = "Error: ". $mysqli->error;
		echo $resp;
    }

    $mysqli->close();

    return $resp;

}
?>
