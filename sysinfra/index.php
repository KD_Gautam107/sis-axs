<?php
session_start();
if (!isset($_SESSION['sysinfra_user']))
{
	header("Location: loginsis.php");
}

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>SIS-AXS Administration Console</title>

    <!-- Bootstrap -->
    <!-- Latest compiled and minified CSS -->
	  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">

	  <!-- Optional theme -->
	  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css">

	  <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>

	  <!-- Latest compiled and minified JavaScript -->
	  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>

	
</head>
<body>



	<h1>System Infra Administrative Login</h1>
	<h2>Options</h2>
	<p><a href="data.php">See All Data</a></p>
	<p><a href="collector.php">See Collector Log</a></p>
	<br>
	<p><a href="maintenance.php">Maintain SIS-AXS</a></p>
	<p><a href="all-quanta-reports.php">All Quanta Records SIS-AXS</a></p>
	
	<?php include 'footersis.php'; ?>  
	
</body>
</html>