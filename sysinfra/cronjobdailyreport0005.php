 <?php 
 
	// Cron Job to be Run every 10 Min
	//	35 18 * * * /usr/bin/php-cgi -f /var/www/html/sysinfra/cronjobdailyreport0005.php >/dev/null 2>&1
	  
	include '../v1/common-alarms.php';
	
	function ReportCurrentAlarmSites()
	{
		include 'dbinc.php';
		
		$mysqli = new mysqli("$mysql_hostname", "$mysql_user", "$mysql_password", "$mysql_database");
		if (mysqli_connect_errno()) {
			printf("Connect failed: %s\n", mysqli_connect_error());
			exit();
		}

		$sql = "SELECT `id`, `site`, `quanta_id`, `alarm_flag`, `alarm_flag_version`, `start_time`, `last_message_level`, `last_message_time` 
					FROM `siteinalarm` WHERE 1";

		//echo "SQL: $sql<br>";	
		$result = $mysqli->query($sql);
		//var_dump($result);
		
		$site_in_alarm = 0;
		$site_alarms_text = "";
		
		//printf("\nSQL ERROR: %s\n", $mysqli->error);
		
		if ($result !== false)
		{
			$row_count = $result->num_rows;
			
			if ($row_count == 0)
			{
				
			}
			else
			{
				while($data = $result->fetch_row())
				{
					$site_in_alarm++;
					$alarms = GetAlarmBits($data[3]);
					
					//($alarms, $site_id, $site_name, $alarm_time)
					$text = GetAlarmText($alarms, $data[1], "", $data[5]);
					
					$site_alarms_text .= $text."\n";
				}
			}
		}

				
		$result->close();	
		$mysqli->close();
		
		$retval = "";
		
		if ($site_in_alarm == 0)
		{
			$retval = "No site in Alarm\n\n";
		}
		else
		{
			$retval = $site_in_alarm." Sites in Alarm as given below:\n\n".$site_alarms_text;
		}
	
		return $retval;
	}
	

	
	//SMS Usage Report
	//http://api.mvaayoo.com/mvaayooapi/APIUtil?user=dushyant@sysinfra.in:sisaxs&type=0
	
	$ch = curl_init();
	curl_setopt($ch,CURLOPT_URL,  "http://api.mvaayoo.com/mvaayooapi/APIUtil");
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_POSTFIELDS, "user=dushyant@sysinfra.in:sisaxs&type=0");
	$buffer = curl_exec($ch);
	if(empty ($buffer))
	{ 
		$email_message = "SMS Report: mVayoo API Return is Empty "; 
	}
	else
	{ 
		$email_message = "SMS Usage Report: ".$buffer; 
	}
	curl_close($ch);
	
	
	$alarm_site_txt = ReportCurrentAlarmSites();
	$email_message .= "\n\n\nSite Report for sites in Alarm right now\n\n".$alarm_site_txt;
	
	
	//($email_to, $email_from, $email_reply_to, $email_subject, $email_message)
	$result = CommSendMailEx("toakhilesh@gmail.com,Ram.sharma@sysinfra.in,dushyant@sysinfra.in", "admin@sysinfra.in", "support@sysinfra.in", "SIS-AXS Daily Report", $email_message);
	
	echo "Mail Send Result: $result";

 
 ?>