<?php
session_start();
if (!isset($_SESSION['sysinfra_user']))
{
    header("Location: loginsis.php");
}

include 'common.php';

$customers = GetAllCustomers();

$tab = $_GET['tab'];
if (isset( $_GET['add_customerid']))
{
    //Add new record
    AddNewClusterRecord($_GET['add_customerid'], $_GET['add_name'], $_GET['add_location']);
    /*echo "<script>
				window.open('maintenance.php','_top');
				</script>";*/
}
if(isset($_GET['edit_customerid']))
{//$apply_name = "Edit Cluster";
   EditClustersWithId($_GET['edit_cid'],$_GET['edit_customerid'],$_GET['edit_name'],$_GET['edit_location']);
   /* echo "<script>
				window.open('maintenance.php','_top');
				</script>";*/
    /*echo $_GET['edit_cid']."<br>";
    echo $_GET['edit_customerid']."<br>";
    echo $_GET['edit_name']."<br>";
    echo $_GET['edit_location']."<br>";*/
}



if(isset($_GET['delete_cid']))
{
    //$apply_name = "Edit Cluster";
   DeleteClusterRecord($_GET['delete_cid']);
    /*echo "<script>
				window.open('maintenance.php','_top');
				</script>";*/
}

$customerid = "";
$name = "";
$location = "";

//$apply_name = "Add Cluster";

if (isset( $_GET['id']))
{
    //Edit record
    $data = GetClustersWithId($_GET['id']);


    if ($data == null)
    {
        echo "Error: Invalid Cluster Id!";
        die;
    }
    $cluster_edit_id = $_GET['id'];
    $customerid = $data[1];
    $name = $data[2];
    $location = $data[3];
    $apply_name = "Save Changes";

}

$customer_name = GetCustomerWithId($customerid);




//var_dump( $_GET);
?>

<script>

    var tab = <?php echo $tab ?>;

    function EditClusters()
    {
        var param = "edit_cid="+encodeURIComponent($( "#ecid" ).val().trim());
        param += "&edit_customerid="+encodeURIComponent($( "#eCustomerid" ).val().trim());
        param += "&edit_name="+encodeURIComponent($( "#eName" ).val().trim());
        param += "&edit_location="+encodeURIComponent($( "#eLocation" ).val().trim());

        //url = "maint-clusters-addedit.php?"+param;
        //window.open("maint-clusters-addedit.php?"+param,'_top');
        //ShowModalUrl("Edit Customer", url);
        /*var request = $.ajax({url: "maint-clusters-addedit.php?"+param});
        //request.done(function () {
            RrefreshTab(tab);
        //});*/


        if (window.XMLHttpRequest) {
            // code for IE7+, Firefox, Chrome, Opera, Safari
            xmlhttp = new XMLHttpRequest();
        } else {
            // code for IE6, IE5
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {

            }
        };
        xmlhttp.open("GET", "maint-clusters-addedit.php?"+param, true);
        xmlhttp.send();
        RrefreshTab(tab);
    }
    function DeleteClusters() {

        var param = "delete_cid="+encodeURIComponent($( "#ecid" ).val().trim());
        if(confirm("Are you sure"))
        {
            if (window.XMLHttpRequest) {
                // code for IE7+, Firefox, Chrome, Opera, Safari
                xmlhttp = new XMLHttpRequest();
            } else {
                // code for IE6, IE5
                xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
            }
            xmlhttp.onreadystatechange = function () {
                if (this.readyState == 4 && this.status == 200) {

                }
            };
            xmlhttp.open("GET", "maint-clusters-addedit.php?"+param, true);
            xmlhttp.send();
            RrefreshTab(tab);
            /*var request = $.ajax({url: "maint-clusters-addedit.php?"+param});
            request.done(function () {
                RrefreshTab(tab);
            });*/
        }
                //window.open("maint-clusters-addedit.php?"+param,"_top");}
        else
            HideModal();
    }
    $("#eCustomerid").val(<?php echo $customerid?>);
</script>

            <input type="hidden" id="ecid" name="ecid" value="<?php echo $cluster_edit_id; ?>">
Customer: <select id="eCustomerid" name="eCustomerid">
    <?php
         foreach ($customers as $customer)
    {
        echo "<option value='$customer[0]'>$customer[0] -   $customer[1]</option>";
    }
    ?></select><br>

Name: <input type="text" id="eName" name="eName" value="<?php echo $name; ?>"><br>
Location: <textarea id="eLocation" name="eLocation" row=5><?php echo $location; ?></textarea><br>
<a class='btn btn-default' href='#' onclick='event.preventDefault(); EditClusters()'><?php echo $apply_name; ?></a><br>
<a class='btn btn-default' href='#' onclick='event.preventDefault(); DeleteClusters()'>Delete Cluster</a><br>




