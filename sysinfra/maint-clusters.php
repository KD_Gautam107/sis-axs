<html>
<head>


</head>




<?php
session_start();
if (!isset($_SESSION['sysinfra_user']))
{
    header("Location: loginsis.php");
}

include 'common.php';

if (isset( $_GET['clusterid']))
{
    $custid = $_GET['clusterid'];
    $custname = $_GET['clustername'];

    $_SESSION['current_cluster_id']=$custid;
    $_SESSION['current_cluster_name']=$custname;
}

/*if (isset( $_GET['add_name']))
{
    var_dump($_GET);
    //Add new record
    AddNewClustersRecord($_GET['add_name'], $_GET['add_address'], "");
}*/



$tab = $_GET['tab'];
//echo $tab."==========the tab number";
$clusters = GetAllClusters();
$customers = GetAllCustomers();
?>
<script>
    function SelectCluster(clusterid, clustername)
    {
        SetRefresh();

        $url = "maint-clusters.php?clusterid="+clusterid+"&clustername="+clustername;
        AsyncLoad($url, "#context");

    }

    function AddCluster(tab)
    {
        var param = "add_customerid="+encodeURIComponent($( "#ccCustomerid" ).val().trim());
        param += "&add_name="+encodeURIComponent($( "#cName" ).val().trim());
        param += "&add_location="+encodeURIComponent($( "#Location" ).val().trim());

        if(document.getElementById("cName").value=="")
        {alert("Cluster Name is Mandatory!");}
        else
        {

            if (window.XMLHttpRequest) {
                // code for IE7+, Firefox, Chrome, Opera, Safari
                xmlhttp = new XMLHttpRequest();
            } else {
                // code for IE6, IE5
                xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
            }
            xmlhttp.onreadystatechange = function () {
                if (this.readyState == 4 && this.status == 200) {

                }
            };
            xmlhttp.open("GET", "maint-clusters-addedit.php?"+param, true);
            xmlhttp.send();


           RrefreshTab(tab);







            /*var response = $.ajax({url: "maint-clusters-addedit.php?"+param});
            response.done(function () {
                RrefreshTab(tab);
            });

          response.fail(function () {
                alert("There is a problem!");
            });*/
        }



        //ShowModalUrl("Add New Customer", url);

    }


    function EditCluster(id,tab)
    {
        url = "maint-clusters-addedit.php?id="+id+"&tab="+tab;
        ShowModalUrl("Edit Cluster "+id, url);

    }
</script>


<table class='table table-striped table-condensed table-responsive'>
    <tr><thead>
        <th>#</th>
        <th>Customer</th>

        <th>Name</th>
        <th>Location</th>


        </thead></tr>
    <tbody>

    <?php
    foreach ($clusters as $cluster)
    {
        echo "<tr>";
        echo "<td><a class='btn btn-default' href='#' onclick='event.preventDefault(); EditCluster(".$cluster[0].','.$tab.")'>".$cluster[0]."</a></td>";

       $customer_name = GetCustomerWithId($cluster[1]);


        echo "<td>".$cluster[1].'-'.$customer_name[1]."</td>";
        echo "<td>".nl2br($cluster[2])."</td>";
        echo "<td>".$cluster[3]."</td>";
        //echo "<td><a class='btn btn-default' href='#' onclick='event.preventDefault(); SelectCluster(".$cluster[0].",\"".$cluster[1]."\")'>Select</a></td>";
        echo "</tr>";
    }





    ?>

    <tr>
        <td>+</td>
        <td><select id="ccCustomerid" name="ccCustomerid">
                    <option value="">--Select Customer--</option>
            <?php     foreach ($customers as $customer)
            {
                echo "<option value='$customer[0]'>$customer[0] -   $customer[1]</option>";
            }
            ?></select></td>
        <td><input type="text" id="cName" name="cName" placeholder="Enter Name"></td>

        <td><input type="text" id="Location" name="Location" placeholder="Enter Location"></td>
        </tr><tr>
        <td></td><td></td><td></td>
<?php echo "<td><a class='btn btn-default' href='#' onclick='event.preventDefault(); AddCluster($tab)'>Add Cluster</a></td>";
?>
    </tr>

    </tbody>
</table>

<br>
</html>
