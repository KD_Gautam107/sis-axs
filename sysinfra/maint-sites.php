
<script>
    function set_clusters_cust(str) {
        if (str == '') {
            document.getElementById("sCustomerId").value = "";
        } else {
            if (window.XMLHttpRequest) {
                // code for IE7+, Firefox, Chrome, Opera, Safari
                xmlhttp = new XMLHttpRequest();
            } else {
                // code for IE6, IE5
                xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
            }
            xmlhttp.onreadystatechange = function () {
                if (this.readyState == 4 && this.status == 200) {
                    document.getElementById("sCustomerId").value = this.responseText;
                }
            };
            xmlhttp.open("GET", "getclusters.php?q=" + str, true);
            xmlhttp.send();


            if (window.XMLHttpRequest) {
                // code for IE7+, Firefox, Chrome, Opera, Safari
                xmlhttp1 = new XMLHttpRequest();
            } else {
                // code for IE6, IE5
                xmlhttp1 = new ActiveXObject("Microsoft.XMLHTTP");
            }
            xmlhttp1.onreadystatechange = function () {
                if (this.readyState == 4 && this.status == 200) {
                    document.getElementById("hdnCustomerId").value = this.responseText;
                }
            };
            xmlhttp1.open("GET", "getcustomer.php?q=" + str, true);
            xmlhttp1.send();
        }
    }

</script>



<?php
session_start();
if (!isset($_SESSION['sysinfra_user']))
{
    header("Location: loginsis.php");
}

include 'common.php';

if (isset( $_GET['siteid']))
{
    $siteid = $_GET['custid'];
    $sitename = $_GET['sitetname'];

    $_SESSION['current_site_id']=$siteid;
    $_SESSION['current_site_name']=$sitename;
}

if (isset( $_GET['add_sitename']))
{
    /*var_dump($_GET);
    //Add new record
    AddNewCustomerRecord($_GET['add_name'], $_GET['add_address'], "");
    $maint = file_get_contents('maintenance.php');
    echo $maint;
    return;*/
}

$tab = $_GET['tab'];
$sites = GetAllSiteList();
$clusters = GetAllClusters();

//echo sizeof($sites);
?>

<script>
    function SelectSite(siteid, sitename)
    {
        SetRefresh();
        url = "maint-sites.php?siteid="+siteid+"&sitetname="+sitetname;
        AsyncLoad(url, "#context");
    }

    function AddSite(tab)
    {
        var param = "add_siteid="+encodeURIComponent($( "#SiteId" ).val().trim());
        param += "&add_customerid="+encodeURIComponent($( "#hdnCustomerId" ).val().trim());
        param += "&add_clusterid="+encodeURIComponent($( "#ClusterId" ).val().trim());
        param += "&add_devicename="+encodeURIComponent($( "#DeviceName" ).val().trim());
        param += "&add_devicemake="+encodeURIComponent($( "#DeviceMake" ).val().trim());
        param += "&add_circle="+encodeURIComponent($( "#Circle" ).val().trim());
        param += "&add_sitename="+encodeURIComponent($( "#SiteName" ).val().trim());
        param += "&add_district="+encodeURIComponent($( "#District" ).val().trim());
        param += "&add_mobile="+encodeURIComponent($( "#Mobile" ).val().trim());
        param += "&add_longitude="+encodeURIComponent($( "#Longitude" ).val().trim());
        param += "&add_latitude="+encodeURIComponent($( "#Latitude" ).val().trim());
        param += "&add_siteindoortype="+encodeURIComponent($( "#SiteIndoorType" ).val().trim());
        param += "&add_dgkva="+encodeURIComponent($( "#DGKVA" ).val().trim());
        param += "&add_samname="+encodeURIComponent($( "#SAMName" ).val().trim());
        param += "&add_samnumber="+encodeURIComponent($( "#SAMNumber" ).val().trim());
        param += "&add_amname="+encodeURIComponent($( "#AMName" ).val().trim());
        param += "&add_amnumber="+encodeURIComponent($( "#AMNumber" ).val().trim());
        param += "&add_techname="+encodeURIComponent($( "#TechName" ).val().trim());
        param += "&add_technumber="+encodeURIComponent($( "#TechNumber" ).val().trim());
        param += "&add_comments="+encodeURIComponent($( "#Comments" ).val().trim());
        param += "&add_dateofinstallation="+encodeURIComponent($( "#DateofInstallation" ).val().trim());
        param += "&add_dgmake="+encodeURIComponent($( "#DGMake" ).val().trim());
        param += "&add_ebcapacity="+encodeURIComponent($( "#EBCapacity" ).val().trim());
        param += "&add_ebphase="+encodeURIComponent($( "#EBPhase" ).val().trim());
        param += "&add_batteryah="+encodeURIComponent($( "#BatteryAH" ).val().trim());
        param += "&add_batteryqty="+encodeURIComponent($( "#BatteryQTY" ).val().trim());
        param += "&add_batterymake="+encodeURIComponent($( "#BatteryMake" ).val().trim());
        param += "&add_dg2kva="+encodeURIComponent($( "#DG2KVA" ).val().trim());
        param += "&add_dg2make="+encodeURIComponent($( "#DG2Make" ).val().trim());
        param += "&add_sitetype="+encodeURIComponent($( "#SiteType" ).val().trim());
        param += "&add_samemail="+encodeURIComponent($( "#SAMEmail" ).val().trim());
        param += "&add_amemail="+encodeURIComponent($( "#AMEmail" ).val().trim());
        param += "&add_techemail="+encodeURIComponent($( "#TechEmail" ).val().trim());

        var site,mode,alarm;

        if(document.getElementById("ReportFlagsSite").checked)
            site = "1";
        else
            site = "0";
        if(document.getElementById("ReportFlagsMode").checked)
             mode = "1";
        else
            mode = "0";
        if(document.getElementById("ReportFlagsAlarm").checked)
             alarm = "1";
        else
            alarm = "0";

        var reportFlags = site+mode+alarm;


           // +$("#ReportFlagsMode").checked.val().trim()+$("#ReportFlagsAlarm").checked.val().trim();

        param += "&add_reportflags="+encodeURIComponent(reportFlags.trim());
        param += "&add_siteenabled="+encodeURIComponent($( "#SiteEnabled" ).val().trim());


        //document.getElementById("ReportFlagsSite")

        if(document.getElementById("SiteId").value=="")
        {alert("Site ID is Mandatory!");}
            else if(document.getElementById("SiteName").value=="")
        {alert("Site Name is Mandatory!");}
            else
        {
            if (window.XMLHttpRequest) {
                // code for IE7+, Firefox, Chrome, Opera, Safari
                xmlhttp = new XMLHttpRequest();
            } else {
                // code for IE6, IE5
                xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
            }
            xmlhttp.onreadystatechange = function () {
                if (this.readyState == 4 && this.status == 200) {

                }
            };
            xmlhttp.open("GET", "maint-sites-addedit.php?"+param, true);
            xmlhttp.send();
            RrefreshTab(tab);


            //window.open("maint-sites-addedit.php?"+param,"_top");
            /*var response = $.ajax({url: "maint-sites-addedit.php?"+param});
            response.done(function (result) {
                RrefreshTab(tab);
				alert(result);*/
				
            //});
            //response.fail(function () {
              //  alert("There is a problem!");
           // });
        }



        //window.open("maint-sites-addedit.php?"+param,"_top");
       // url = "maint-sites-addedit.php";
        //ShowModalUrl("Add New Customer", url);
    }

    function EditSite(id,tab)
    {
        url = "maint-sites-addedit.php?id="+id+"&tab="+tab;
        ShowModalUrl("Edit Site "+id, url);
    }
</script>


<table class='table table-striped table-condensed table-responsive' onscroll="true">
    <tr><thead>
        <th>#</th>
        <th>Site Id<b>*</b></th>
        <th>Customer Id</th>
        <th>Cluster Id</th>
        <th>Device Name</th>
        <th>Device Make</th>
        <th>Circle</th>
        <th>Site Name</th>
        <th>District</th>
        <th>Mobile</th>
        <th>Longitude</th>
        <th>Latitude</th>
        <th>Site Indoor Type</th>
        <th>DG KVA</th>
        <th>SAM Name<b>*</b></th>
        <th>SAM Number</th>
        <th>AM Name</th>
        <th>AM Number</th>
        <th>Tech Name</th>
        <th>Tech Number</th>
        <th>Comments</th>
        <th>Date of Installation</th>
        <th>DG Make</th>
        <th>EB Capacity</th>
        <th>EB Phase</th>
        <th>Battery AH</th>
        <th>Battery QTY</th>
        <th>Battery Make</th>
        <th>DG2 KVA</th>
        <th>DG2 Make</th>
        <th>Site Type</th>
        <th>SAM Email</th>
        <th>AM Email</th>
        <th>Tech Email</th>
        <th>Report_Flags<br>
        <b>S-M-A</b></th>
        <th>Site Enabled</th>


        </thead></tr>
    <tbody>

    <?php
    foreach ($sites as $site)
    {
        echo "<tr>";
        echo "<td><a class='btn btn-default' href='#' onclick='event.preventDefault(); EditSite(".$site[0].','.$tab.")'>".$site[0]."</a></td>";
        echo "<td>".$site[1]."</td>";
        $customer_name = GetCustomerNameWithId($site[2]);
        echo "<td>".$site[2].'-'.$customer_name[0]."</td>";
        $cluster_name = GetClustersWithId($site[3]);
        echo "<td>".$site[3].'-'.$cluster_name[2]."</td>";
        for($i = 4; $i<34; $i++)
        echo "<td>".$site[$i]."</td>";



     //   echo $site[34]."<br>";
		if (GetReportFlag($site[34], $REPORT_FLAG_1_SITE_REPORT_EMAIL))
			$f1 = "1";
		else
			$f1 = "0";
		if (GetReportFlag($site[34], $REPORT_FLAG_2_MODE_CHANGE_EMAIL))
			$f2 = "1";
		else
			$f2 = "0";		
		if (GetReportFlag($site[34], $REPORT_FLAG_3_ALARM_SMS))
			$f3 = "1";
		else
			$f3 = "0";		

            //$hex_data = bin2hex($site[34]);
            //$bin_data = hex2bin($hex_data);
       // $flags = unpack ( "C*" , $site[34]);
        //($flags[1]& 0x80).($flags[1]& 0x40).($flags[1]& 0x20)

        echo "<td>".$f1.'-'.$f2.'-'.$f3."</td>";
        echo "<td>".$site[35]."</td>";

        //echo "<td><a class='btn btn-default' href='#' onclick='event.preventDefault(); SelectSite(".$site[0].",\"".$site[1]."\")'>Select</a></td>";
        echo "</tr>";


    }

    ?>

    <tr>
        <td>+</td>
        <td><input type="text" id="SiteId" name="SiteId" placeholder="Site ID" required></td>
        <td><input type="text" id="sCustomerId" name="sCustomerId"  placeholder="Customer ID" readonly>
        <input type="hidden" id="hdnCustomerId"></td>

    <?php   echo "<td><select id='ClusterId' name='ClusterId' onchange='set_clusters_cust(this.value)'>";
            echo "<option value=''>--Select Cluster--</option>";
                foreach ($clusters as $cluster) {
                echo "<option  value = $cluster[0]>$cluster[0] - $cluster[2]</option>";
                }
?>

        </select></td>
        <td><input type="text" id="DeviceName" name="DeviceName" placeholder="Device Name"></td>
        <td><input type="text" id="DeviceMake" name="DeviceMake" value="SYSTEM INFRA" placeholder="Device Make"></td>
        <td><input type="text" id="Circle" name="Circle" placeholder="Circle"></td>
        <td><input type="text" id="SiteName" name="SiteName" placeholder="Site Name"></td>
        <td><input type="text" id="District" name="District" placeholder="District"></td>
        <td><input type="text" id="Mobile" name="Mobile" placeholder="Mobile"></td>
        <td><input type="text" id="Longitude" name="Longitude" placeholder="Longitude"></td>
        <td><input type="text" id="Latitude" name="Latitude" placeholder="Latitude"></td>
        <td><select id="SiteIndoorType" name="SiteIndoorType">

                <option value="0">0-Indoor</option>
                <option value="1">1-Outdoor</option></select>
        </td>
        <td><input type="number" id="DGKVA" name="DGKVA" placeholder="DG KVA" min="1"></td>
        <td><input type="text" id="SAMName" name="SAMName" placeholder="SAM Name"></td>
        <td><input type="text" id="SAMNumber" name="SAMNumber" placeholder="SAM Number"></td>
        <td><input type="text" id="AMName" name="AMName" placeholder="AM Name"></td>
        <td><input type="text" id="AMNumber" name="AMNumber" placeholder="AM Number"></td>
        <td><input type="text" id="TechName" name="TechName" placeholder="Tech Name"></td>
        <td><input type="text" id="TechNumber" name="TechNumber" placeholder="Tech Number"></td>
        <td><input type="text" id="Comments" name="Comments" placeholder="Comments"></td>
        <td><input type="date" id="DateofInstallation" name="DateofInstallation" placeholder="Select Date"></td>
        <td><input type="text" id="DGMake" name="DGMake" placeholder="DG Make"></td>
        <td><input type="number" id="EBCapacity" name="EBCapacity" placeholder="EB Capacity" ></td>
        <td><input type="number" id="EBPhase" name="EBPhase" placeholder="EB Phase"></td>
        <td><input type="number" id="BatteryAH" name="BatteryAH" placeholder="Batter AH"></td>
        <td><input type="number" id="BatteryQTY" name="BatteryQTY" placeholder="Battery QTY"></td>
        <td><input type="text" id="BatteryMake" name="BatteryMake" placeholder="Battery Make"></td>
        <td><input type="number" id="DG2KVA" name="DG2KVA" placeholder="DG2 KVA"></td>
        <td><input type="text" id="DG2Make" name="DG2Make" placeholder="DG2 Make"></td>
        <td><input type="number" id="SiteType" name="SiteType" placeholder="Site Type"></td>
        <td><input type="text" id="SAMEmail" name="SAMEmail" placeholder="SAM Email"></td>
        <td><input type="text" id="AMEmail" name="AMEmail" placeholder="AM Email"></td>
        <td><input type="text" id="TechEmail" name="TechEmail" placeholder="Tech Email"></td>
        <td>

            <b>S</b><input type="checkbox" id="ReportFlagsSite">
            <b>M</b><input type="checkbox" id="ReportFlagsMode">
            <b>A</b><input type="checkbox" id="ReportFlagsAlarm">

        </td>
        <td><select id="SiteEnabled" name="SiteEnabled">

                <option value="1">1-Enabled</option>
                <option value="0">0-Disabled</option></select></td>
        </tr><tr>
        <?php
         $i = 0;
        while ($i<35){
        echo "<td></td>";
        $i++;
        }
        ?>
        <?php echo "<td><a class='btn btn-default' href='#' onclick='event.preventDefault(); AddSite($tab)'>Add Site</a></td>";



?>
    </tr>

    </tbody>

</table>

<br>

