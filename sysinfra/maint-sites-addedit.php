<?php
session_start();
if (!isset($_SESSION['sysinfra_user']))
{
    header("Location: loginsis.php");
}

include 'common.php';
//include "maintenance.php";
$tab = $_GET['tab'];
$clusters = GetAllClusters();


if (isset( $_GET['add_sitename']))
{
    $doi = $_GET['add_dateofinstallation'];     $doi = !empty($doi) ? "'$doi'" : "NULL";
    $add_dgkva = $_GET['add_dgkva'];            $add_dgkva = !empty($add_dgkva) ? "'$add_dgkva'" : "0";
    $add_ebcapacity = $_GET['add_ebcapacity'];  $add_ebcapacity = !empty($add_ebcapacity) ? "'$add_ebcapacity'" : "0";
    $add_ebphase = $_GET['add_ebphase'];        $add_ebphase = !empty($add_ebphase) ? "'$add_ebphase'" : "0";
    $add_batteryah = $_GET['add_batteryah'];    $add_batteryah = !empty($add_batteryah) ? "'$add_batteryah'" : "0";
    $add_batteryqty = $_GET['add_batteryqty'];  $add_batteryqty = !empty($add_batteryqty) ? "'$add_batteryqty'" : "0";
    $add_dg2kva = $_GET['add_dg2kva'];          $add_dg2kva = !empty($add_dg2kva) ? "'$add_dg2kva'" : "0";
    $add_sitetype = $_GET['add_sitetype'];      $add_sitetype = !empty($add_sitetype) ? "'$add_sitetype'" : "0";
    $add_customerid = $_GET['add_customerid'];  $add_customerid = !empty($add_customerid) ? "'$add_customerid'" : "0";




    //Add new record
    AddNewSiteRecord(
        $_GET['add_siteid'],
        $add_customerid,
        $_GET['add_clusterid'],
        $_GET['add_devicename'],
        $_GET['add_devicemake'],
        $_GET['add_circle'],
        $_GET['add_sitename'],
        $_GET['add_district'],
        $_GET['add_mobile'],
        $_GET['add_longitude'],
        $_GET['add_latitude'],
        $_GET['add_siteindoortype'],
        $add_dgkva,
        $_GET['add_samname'],
        $_GET['add_samnumber'],
        $_GET['add_amname'],
        $_GET['add_amnumber'],
        $_GET['add_techname'],
        $_GET['add_technumber'],
        $_GET['add_comments'],
        $doi,
        $_GET['add_dgmake'],
        $add_ebcapacity,
        $add_ebphase,
        $add_batteryah,
        $add_batteryqty,
        $_GET['add_batterymake'],
        $add_dg2kva,
        $_GET['add_dg2make'],
        $add_sitetype,
        $_GET['add_samemail'],
        $_GET['add_amemail'],
        $_GET['add_techemail'],
        $_GET['add_reportflags'],

        $_GET['add_siteenabled']

    );
    /*echo "<script>
				window.open('maintenance.php','_top');
				</script>";*/
}

if(isset($_GET['edit_sitename']))
{
    $doi = $_GET['edit_dateofinstallation'];      $doi = !empty($doi) ? "'$doi'" : "NULL";
    $edit_dgkva = $_GET['edit_dgkva'];            $edit_dgkva = !empty($edit_dgkva) ? "'$edit_dgkva'" : "0";
    $edit_ebcapacity = $_GET['edit_ebcapacity'];  $edit_ebcapacity = !empty($edit_ebcapacity) ? "'$edit_ebcapacity'" : "0";
    $edit_ebphase = $_GET['edit_ebphase'];        $edit_ebphase = !empty($edit_ebphase) ? "'$edit_ebphase'" : "0";
    $edit_batteryah = $_GET['edit_batteryah'];    $edit_batteryah = !empty($edit_batteryah) ? "'$edit_batteryah'" : "0";
    $edit_batteryqty = $_GET['edit_batteryqty'];  $edit_batteryqty = !empty($edit_batteryqty) ? "'$edit_batteryqty'" : "0";
    $edit_dg2kva = $_GET['edit_dg2kva'];          $edit_dg2kva = !empty($edit_dg2kva) ? "'$edit_dg2kva'" : "0";
    $edit_sitetype = $_GET['edit_sitetype'];      $edit_sitetype = !empty($edit_sitetype) ? "'$edit_sitetype'" : "0";
    $edit_customerid = $_GET['edit_customerid'];  $edit_customerid = !empty($edit_customerid) ? "'$edit_customerid'" : "0";




   $respn =  EditSiteWithId(
        $_GET['edit_sid'],
        $edit_customerid,
        $_GET['edit_clusterid'],
        $_GET['edit_devicename'],
        $_GET['edit_devicemake'],
        $_GET['edit_circle'],
        $_GET['edit_sitename'],
        $_GET['edit_district'],
        $_GET['edit_mobile'],
        $_GET['edit_longitude'],
        $_GET['edit_latitude'],
        $_GET['edit_siteindoortype'],
       $edit_dgkva,
        $_GET['edit_samname'],
        $_GET['edit_samnumber'],
        $_GET['edit_amname'],
        $_GET['edit_amnumber'],
        $_GET['edit_techname'],
        $_GET['edit_technumber'],
        $_GET['edit_comments'],
        $doi,
        $_GET['edit_dgmake'],
       $edit_ebcapacity,
       $edit_ebphase,
       $edit_batteryah,
       $edit_batteryqty,
        $_GET['edit_batterymake'],
       $edit_dg2kva,
        $_GET['edit_dg2make'],
       $edit_sitetype,
        $_GET['edit_samemail'],
        $_GET['edit_amemail'],
        $_GET['edit_techemail'],
        $_GET['edit_reportflags'],
        $_GET['edit_siteenabled']
    );


   /*echo "<script>
				window.open('maintenance.php','_top');
				</script>";*/
    /*echo $_GET['edit_custid']."<br>";
    echo $_GET['edit_name']."<br>";
    echo $_GET['edit_address']."<br>";*/
    echo $respn;
}

if(isset($_GET['delete_sid'])) {
//$apply_name = "Edit Cluster";
    DeleteSiteRecord($_GET['delete_sid']);
    /*echo "<script>
				window.open('maintenance.php','_top');
				</script>";*/
}

$site_id = "";
$customer_id = "";
$cluster_id = "";
$device_name = "";
$device_make = "";
$circle = "";
$site_name = "";
$district = "";
$mobile = "";
$longitude = "";
$latitude = "";
$site_indoor_type = "";
$dg_kva = "";
$sam_name = "";
$sam_number = "";
$am_name = "";
$am_number = "";
$tech_name = "";
$tech_number = "";
$comments = "";
$date_of_installation = "";
$dg_make = "";
$eb_capacity = "";
$eb_phase = "";
$battery_ah = "";
$battery_qty = "";
$battery_make = "";
$dg2_kva = "";
$dg2_make = "";
$site_type = "";
$sam_email = "";
$am_email = "";
$tech_email = "";
$report_flags = "";
$site_enabled = "";




if (isset( $_GET['id']))
{
    //Edit record
    $data = GetSiteWithId($_GET['id']);

    if ($data == null)
    {
        echo "Error: Invalid Customer Id!";
        die;
    }
    $sid = $_GET['id'];
    $site_id = $data[0];
    $customer_id = $data[1];
    $cluster_id = $data[2];
    $device_name = $data[3];
    $device_make = $data[4];
    $circle = $data[5];
    $site_name = $data[6];
    $district = $data[7];
    $mobile = $data[8];
    $longitude = $data[9];
    $latitude = $data[10];
    $site_indoor_type = $data[11];
    $dg_kva = $data[12];
    $sam_name = $data[13];
    $sam_number = $data[14];
    $am_name = $data[15];
    $am_number = $data[16];
    $tech_name = $data[17];
    $tech_number = $data[18];
    $comments = $data[19];
    $date_of_installation = $data[20];
    $dg_make = $data[21];
    $eb_capacity = $data[22];
    $eb_phase = $data[23];
    $battery_ah = $data[24];
    $battery_qty = $data[25];
    $battery_make = $data[26];
    $dg2_kva = $data[27];
    $dg2_make = $data[28];
    $site_type = $data[29];
    $sam_email = $data[30];
    $am_email = $data[31];
    $tech_email = $data[32];

    //$hex_data = bin2hex($data[33]);
    //$report_flags = hex2bin($hex_data);

    //if($report_flags[0]==true)
	if (GetReportFlag($data[33], $REPORT_FLAG_1_SITE_REPORT_EMAIL))
       $checked0 = "checked";
    //if($report_flags[1]==true)
	if (GetReportFlag($data[33], $REPORT_FLAG_2_MODE_CHANGE_EMAIL))
        $checked1 = "checked";
    //if($report_flags[2]==true)
	if (GetReportFlag($data[33], $REPORT_FLAG_3_ALARM_SMS))
        $checked2 = "checked";
    //$single_bit = unpack("C*", $report_flags);
    //echo $single_bit[0];

    $site_enabled = $data[34];

    $one_customer_name = GetCustomerNameWithId($customer_id);
    //echo $name;
}



//var_dump( $_GET);


?>

<script>
    function set_clusters_cust(str) {
        if (str == '') {
            document.getElementById("seCustomerId").value = "";
        } else {
            if (window.XMLHttpRequest) {
                // code for IE7+, Firefox, Chrome, Opera, Safari
                xmlhttp = new XMLHttpRequest();
            } else {
                // code for IE6, IE5
                xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
            }
            xmlhttp.onreadystatechange = function () {
                if (this.readyState == 4 && this.status == 200) {
                    document.getElementById("seCustomerId").value = this.responseText;
                }
            };
            xmlhttp.open("GET", "getclusters.php?q=" + str, true);
            xmlhttp.send();

            if (window.XMLHttpRequest) {
                // code for IE7+, Firefox, Chrome, Opera, Safari
                xmlhttp1 = new XMLHttpRequest();
            } else {
                // code for IE6, IE5
                xmlhttp1 = new ActiveXObject("Microsoft.XMLHTTP");
            }
            xmlhttp1.onreadystatechange = function () {
                if (this.readyState == 4 && this.status == 200) {
                    document.getElementById("ehdnCustomerId").value = this.responseText;
                }
            };
            xmlhttp1.open("GET", "getcustomer.php?q=" + str, true);
            xmlhttp1.send();
        }
    }

    var tab = <?php echo $tab;?>

    function EditSites()
    {
        var param = "edit_sid="+encodeURIComponent($( "#seSiteid" ).val().trim());
        param += "&edit_customerid="+encodeURIComponent($( "#ehdnCustomerId" ).val().trim());
        param += "&edit_clusterid="+encodeURIComponent($( "#seClusterId" ).val().trim());
        param += "&edit_devicename="+encodeURIComponent($( "#seDeviceName" ).val().trim());
        param += "&edit_devicemake="+encodeURIComponent($( "#seDeviceMake" ).val().trim());
        param += "&edit_circle="+encodeURIComponent($( "#seCircle" ).val().trim());
        param += "&edit_sitename="+encodeURIComponent($( "#seSiteName" ).val().trim());
        param += "&edit_district="+encodeURIComponent($( "#seDistrict" ).val().trim());
        param += "&edit_mobile="+encodeURIComponent($( "#seMobile" ).val().trim());
        param += "&edit_longitude="+encodeURIComponent($( "#seLongitude" ).val().trim());
        param += "&edit_latitude="+encodeURIComponent($( "#seLatitude" ).val().trim());
        param += "&edit_siteindoortype="+encodeURIComponent($( "#seSiteIndoorType" ).val().trim());
        param += "&edit_dgkva="+encodeURIComponent($( "#seDGKVA" ).val().trim());
        param += "&edit_samname="+encodeURIComponent($( "#seSAMName" ).val().trim());
        param += "&edit_samnumber="+encodeURIComponent($( "#seSAMNumber" ).val().trim());
        param += "&edit_amname="+encodeURIComponent($( "#seAMName" ).val().trim());
        param += "&edit_amnumber="+encodeURIComponent($( "#seAMNumber" ).val().trim());
        param += "&edit_techname="+encodeURIComponent($( "#seTechName" ).val().trim());
        param += "&edit_technumber="+encodeURIComponent($( "#seTechNumber" ).val().trim());
        param += "&edit_comments="+encodeURIComponent($( "#seComments" ).val().trim());
        param += "&edit_dateofinstallation="+encodeURIComponent($( "#seDateofInstallation" ).val().trim());
        param += "&edit_dgmake="+encodeURIComponent($( "#seDGMake" ).val().trim());
        param += "&edit_ebcapacity="+encodeURIComponent($( "#seEBCapacity" ).val().trim());
        param += "&edit_ebphase="+encodeURIComponent($( "#seEBPhase" ).val().trim());
        param += "&edit_batteryah="+encodeURIComponent($( "#seBatteryAH" ).val().trim());
        param += "&edit_batteryqty="+encodeURIComponent($( "#seBatteryQTY" ).val().trim());
        param += "&edit_batterymake="+encodeURIComponent($( "#seBatteryMake" ).val().trim());
        param += "&edit_dg2kva="+encodeURIComponent($( "#seDG2KVA" ).val().trim());
        param += "&edit_dg2make="+encodeURIComponent($( "#seDG2Make" ).val().trim());
        param += "&edit_sitetype="+encodeURIComponent($( "#seSiteType" ).val().trim());
        param += "&edit_samemail="+encodeURIComponent($( "#seSAMEmail" ).val().trim());
        param += "&edit_amemail="+encodeURIComponent($( "#seAMEmail" ).val().trim());
        param += "&edit_techemail="+encodeURIComponent($( "#seTechEmail" ).val().trim());

        var site,mode,alarm;

        if(document.getElementById("seReportFlagsSite").checked)
            site = "1";
        else
            site = "0";
        if(document.getElementById("seReportFlagsMode").checked)
            mode = "1";
        else
            mode = "0";
        if(document.getElementById("seReportFlagsAlarm").checked)
            alarm = "1";
        else
            alarm = "0";

        var reportFlags = site+mode+alarm;




        param += "&edit_reportflags="+encodeURIComponent(reportFlags.trim());
        param += "&edit_siteenabled="+encodeURIComponent($( "#seSiteEnabled" ).val().trim());

        if (window.XMLHttpRequest) {
            // code for IE7+, Firefox, Chrome, Opera, Safari
            xmlhttp = new XMLHttpRequest();
        } else {
            // code for IE6, IE5
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {

            }
        };
        xmlhttp.open("GET", "maint-sites-addedit.php?"+param, true);
        xmlhttp.send();
        RrefreshTab(tab);


        //window.open("maint-sites-addedit.php?"+param,'_top');
       /* var request = $.ajax({url: "maint-sites-addedit.php?"+param});

        //request.done(function () {
            RrefreshTab(tab);*/
        //});
        //url = "maint-cust-addedit.php?"+param;
        //ShowModalUrl("Add New Customer", url);
    }

    function DeleteSites() {

        var param = "delete_sid="+encodeURIComponent($( "#seSiteid" ).val().trim());
        if(confirm("Are you sure"))
        {

            if (window.XMLHttpRequest) {
                // code for IE7+, Firefox, Chrome, Opera, Safari
                xmlhttp = new XMLHttpRequest();
            } else {
                // code for IE6, IE5
                xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
            }
            xmlhttp.onreadystatechange = function () {
                if (this.readyState == 4 && this.status == 200) {

                }
            };
            xmlhttp.open("GET", "maint-sites-addedit.php?"+param, true);
            xmlhttp.send();
            RrefreshTab(tab);

            // window.open("maint-sites-addedit.php?"+param,"_top");
           /* var request = $.ajax({url: "maint-sites-addedit.php?"+param});

            request.done(function () {
                RrefreshTab(tab);
            });*/
        }

       else
            HideModal();
            //LoadCurrentContentState(3);
           //url = "maint-sites.php";

    }
    $('#seClusterId').val(<?php echo $cluster_id?>);
    $('#seSiteIndoorType').val(<?php echo $site_indoor_type?>);
    $('#seSiteEnabled').val(<?php echo $site_enabled?>);

</script>
<input type="hidden" id=seSiteid name="seSiteid" value="<?php echo $sid ?>"<br>
<table>
    <tr><td>Site Id:</td><td><input type="text" id="SiteId" name="SiteId" value="<?php echo $site_id ?>" readonly></td></tr>
    <tr><td>Customer Id:</td><td><input type="text" id="seCustomerId" name="seCustomerId" value="<?php echo $customer_id.'-'.$one_customer_name[0] ?>" readonly>
        <input type="hidden" id="ehdnCustomerId" value="<?php echo $customer_id ?>"></td></tr>

        <?php
        echo "<tr><td>Cluster Id:</td><td><select id='seClusterId' name='seClusterId' onchange='set_clusters_cust(this.value)'>";

        foreach ($clusters as $cluster) {
            echo "<option  value = $cluster[0]>$cluster[0] - $cluster[2]</option>";
        }
        ?>
    </select></td></tr>
    <tr><td>Device Name:</td><td><input type="text" id="seDeviceName" name="DeviceName" value="<?php echo $device_name ?>"></td></tr>
    <tr><td>Device Make:</td><td><input type="text" id="seDeviceMake" name="DeviceMake" value="<?php echo $device_make ?>"></td></tr>
    <tr><td>Circle:</td><td><input type="text" id="seCircle" name="Circle" value="<?php echo $circle ?>"></td></tr>
    <tr><td>Site Name:</td><td><input type="text" id="seSiteName" name="SiteName" value="<?php echo $site_name ?>" readonly></td></tr>
    <tr><td>District:</td><td><input type="text" id="seDistrict" name="District" value="<?php echo $district ?>"></td></tr>
    <tr><td>Mobile:</td><td><input type="text" id="seMobile" name="Mobile" value="<?php echo $mobile ?>"></td></tr>
    <tr><td>Longitude:</td><td><input type="text" id="seLongitude" name="Longitude" value="<?php echo $longitude ?>"></td></tr>
    <tr><td>Latitude:</td><td><input type="text" id="seLatitude" name="Latitude" value="<?php echo $latitude ?>"></td></tr>
    <tr><td>Site Indoor Type:</td><td><select id="seSiteIndoorType" name="SiteIndoorType">
                <option value="0">0-Indoor</option>
                <option value="1">1-Outdoor</option></select>
        </td></tr>
    <tr><td>DG KVA:</td><td><input type="number" id="seDGKVA" name="DGKVA" value="<?php echo $dg_kva ?>"></td></tr>
    <tr><td>SAM Name:</td><td><input type="text" id="seSAMName" name="SAMName" value="<?php echo $sam_name ?>"></td></tr>
    <tr><td>SAM Number:</td><td><input type="text" id="seSAMNumber" name="SAMNumber" value="<?php echo $sam_number ?>"></td></tr>
    <tr><td>AM Name:</td><td><input type="text" id="seAMName" name="AMName" value="<?php echo $am_name ?>"></td></tr>
    <tr><td>AM Number:</td><td><input type="text" id="seAMNumber" name="AMNumber" value="<?php echo $am_number ?>"></td></tr>
    <tr><td>Tech Name:</td><td><input type="text" id="seTechName" name="TechName" value="<?php echo $tech_name ?>"></td></tr>
    <tr><td>Tech Number:</td><td><input type="text" id="seTechNumber" name="TechNumber" value="<?php echo $tech_number ?>"></td></tr>
    <tr><td>Comments:</td><td><input type="text" id="seComments" name="Comments" value="<?php echo $comments ?>"></td></tr>
    <tr><td>Date of Installation:</td><td><input type="date" id="seDateofInstallation" name="DateofInstallation" value="<?php echo $date_of_installation ?>"></td></tr>
    <tr><td>DG Make:</td><td><input type="text" id="seDGMake" name="DGMake" value="<?php echo $dg_make ?>"></td></tr>
    <tr><td>EB Capacity:</td><td><input type="number" id="seEBCapacity" name="EBCapacity" value="<?php echo $eb_capacity ?>"></td></tr>
    <tr><td>EB Phase:</td><td><input type="number" id="seEBPhase" name="EBPhase" value="<?php echo $eb_phase ?>"></td></tr>
    <tr><td>Battery AH:</td><td><input type="number" id="seBatteryAH" name="BatteryAH" value="<?php echo $battery_ah ?>"></td></tr>
    <tr><td>Battery QTY:</td><td><input type="number" id="seBatteryQTY" name="BatteryQTY" value="<?php echo $battery_qty ?>"></td></tr>
    <tr><td>Battery Make:</td><td><input type="text" id="seBatteryMake" name="BatteryMake" value="<?php echo $battery_make ?>"></td></tr>
    <tr><td>DG2 KVA:</td><td><input type="number" id="seDG2KVA" name="DG2KVA" value="<?php echo $dg2_kva ?>"></td></tr>
    <tr><td>DG2 Make:</td><td><input type="text" id="seDG2Make" name="DG2Make" value="<?php echo $dg2_make ?>"></td></tr>
    <tr><td>Site Type:</td><td><input type="number" id="seSiteType" name="SiteType" value="<?php echo $site_type ?>"></td></tr>
    <tr><td>SAM Email:</td><td><input type="text" id="seSAMEmail" name="SAMEmail" value="<?php echo $sam_email ?>"></td></tr>
    <tr><td>AM Email:</td><td><input type="text" id="seAMEmail" name="AMEmail" value="<?php echo $am_email ?>"></td></tr>
    <tr><td>Tech Email:</td><td><input type="text" id="seTechEmail" name="TechEmail" value="<?php echo $tech_email ?>"></td></tr>
    <tr><td>Report_Flags:</td><td>
            <b>Site:</b><input type="checkbox" id="seReportFlagsSite" <?php echo  $checked0 ;?>>
            <b>Mode:</b><input type="checkbox" id="seReportFlagsMode"<?php echo  $checked1 ;?>>
            <b>Alarm:</b><input type="checkbox" id="seReportFlagsAlarm"<?php echo  $checked2 ;?>>
        </td></tr>
    <tr><td>Site Enabled:</td><td><select id="seSiteEnabled" name="SiteEnabled">
                <option value="1">1-Enabled</option>
                <option value="0">0-Disabled</option></select></td></tr>
</table>



<a class='btn btn-default' href='#' onclick='event.preventDefault(); EditSites()'>Save Changes</a><br>
<a class='btn btn-default' href='#' onclick='event.preventDefault(); DeleteSites()'>Delete Site</a>