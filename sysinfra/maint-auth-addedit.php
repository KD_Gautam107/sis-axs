<?php
session_start();
if (!isset($_SESSION['sysinfra_user']))
{
    header("Location: loginsis.php");
}

include 'common.php';
$customers = GetAllCustomers();
$tab = $_GET['tab'];
if (isset( $_GET['add_username']))
{
    //Add new record
    AddNewAuthRecord(

        $_GET['add_username'],
        $_GET['add_passcode'],
        $_GET['add_type'],
        $_GET['add_customer'],
        $_GET['add_typeref']
    );
    /*echo "<script>
            window.open('maintenance.php','_top');
            </script>";*/

}

if(isset($_GET['edit_aid']))
{//$apply_name = "Edit Cluster";
    EditAuthWithId(
        $_GET['edit_aid'],
        $_GET['edit_username'],
        $_GET['edit_passcode'],
        $_GET['edit_type'],
        $_GET['edit_customer'],
        $_GET['edit_typeref']
    );

    /*echo "<script>
                window.open('maintenance.php','_top');
                </script>";*/
    /*echo $_GET['edit_custid']."<br>";
    echo $_GET['edit_name']."<br>";
    echo $_GET['edit_address']."<br>";*/

}



if(isset($_GET['delete_aid'])) {
//$apply_name = "Edit Cluster";
    DeleteAuthRecord($_GET['delete_aid']);
    //echo "<script>RrefreshTab($ctab);</script>";
    /*echo "<script>
                window.open('maintenance.php?tab='.$ctab,'_top');
                </script>";*/
}

$username = "";
$passcode= "";
$type = "";
$customer = "";
$typeref = "";


if (isset( $_GET['id']))
{
    //Edit record
    $data = GetAuthWithId($_GET['id']);

    if ($data == null)
    {
        echo "Error: Invalid Authentication Id!";
        die;
    }
    $auth_id    = $_GET['id'];
    $username   = $data[0];
    $passcode   = $data[1];
    $type       = $data[2];
    $customer   = $data[3];
    $typeref    = $data[4];

    $typeref = trim($typeref);
    $typearr = explode(",", $typeref);
    $typearr = array_map('trim', $typearr);
    //echo $typearr[0].$typearr[1];
    //var_dump($typearr);


    for ($i=0; $i<count($typearr); $i++) {
        if (is_numeric($typearr[$i])) {
            //echo "'{$element}' is numeric", PHP_EOL;
            $typearr[$i] = intval($typearr[$i]);
        } else {
            //echo "'{$element}' is NOT numeric", PHP_EOL;
            $typearr[$i] = 0;
        }
    }

   // var_dump($typearr);
$json_typeref = json_encode($typearr);
    //var_dump($typearr);
    var_dump($json_typeref);
    //echo $name;
}



//var_dump( $_GET);


?>


<script>
    var tab = <?php echo $tab ?>;
    var acheckbox_array_length = 0;
    var atyperef = "";
    var typeref_obj = <?php echo $json_typeref ?>;
    function sendRcmd(tab) {
        //RrefreshTab(tab);
        //$.ajax({url: "maint-cust.php?done=1"});
//HideModal();
        //switchtab(tab);
        //HideModal();
    }

    function EditAuths()
    {   var param = "edit_aid="+encodeURIComponent($( "#aAuthid" ).val().trim());
        param += "&edit_username="+encodeURIComponent($( "#aUsername" ).val().trim());
        param += "&edit_passcode="+encodeURIComponent($( "#aPasscode" ).val().trim());
        param += "&edit_type="+encodeURIComponent($( "#aType" ).val().trim());
        param += "&edit_customer="+encodeURIComponent($( "#aCustomer" ).val().trim());


        if(document.getElementById("aType").value == 4)
        {
            for(i = 0; i < acheckbox_array_length; i++)
            {
                if(document.getElementById(i+"acluster").checked)
                    atyperef += document.getElementById(i+"acluster").value+",";
            }
            //alert(typeref.substring(0,typeref.length-1));
            //
            param += "&edit_typeref="+encodeURIComponent(atyperef.substring(0,atyperef.length-1).trim());
            console.log(acheckbox_array_length);
        }
        else
        {
            param += "&edit_typeref="+encodeURIComponent("");

        }





     // param += "&edit_typeref="+encodeURIComponent($( "#aTyperef" ).val().trim());

        if (window.XMLHttpRequest) {
            // code for IE7+, Firefox, Chrome, Opera, Safari
            xmlhttp = new XMLHttpRequest();
        } else {
            // code for IE6, IE5
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {

            }
        };
        xmlhttp.open("GET", "maint-auth-addedit.php?"+param, true);
        xmlhttp.send();
        RrefreshTab(tab);

        //window.open("maint-auth-addedit.php?"+param,"_top");
        //ShowModalUrl("Add New Customer", url);
        /*var request = $.ajax({url: "maint-auth-addedit.php?"+param});
        request.done(function () {
            RrefreshTab(tab);
        });*/
       /* request.fail(function () {
            alert("There is a problem!");
        });*/
    }

    function DeleteAuths() {
        var param = "delete_aid="+encodeURIComponent($( "#aAuthid" ).val().trim());
        if(confirm("Are you sure"))
        {

            if (window.XMLHttpRequest) {
                // code for IE7+, Firefox, Chrome, Opera, Safari
                xmlhttp = new XMLHttpRequest();
            } else {
                // code for IE6, IE5
                xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
            }
            xmlhttp.onreadystatechange = function () {
                if (this.readyState == 4 && this.status == 200) {

                }
            };
            xmlhttp.open("GET", "maint-auth-addedit.php?"+param, true);
            xmlhttp.send();
            RrefreshTab(tab);
            //window.open("maint-cust-addedit.php?"+param,"_top");
           /* var request = $.ajax({url: "maint-auth-addedit.php?"+param});

            request.done(function () {
                RrefreshTab(tab);
            });*/
        }
        else
            HideModal();

        //window.location.reload();
    }
    $("#aCustomer").val(<?php echo $customer?>);
    $("#aType").val(<?php echo $type?>);



    if(document.getElementById("aType").value == 4)
    {
        aClustersAjax(document.getElementById("aCustomer").value);
    }

//    function aCustomer_change()
//    {
//        if(document.getElementById("aType").value != 4)
//          document.getElementById("aTyperef").innerHTML = "";
////        else
////        {
////            document.getElementById("aTyperef").innerHTML = "";
////            aClustersAjax(document.getElementById("aCustomer").value);
////            //console.log(document.getElementById("aCustomer").value);
////        }
//
//
//    }

    function aType_change() {

        if(document.getElementById("aType").value == 4)
        {
            aClustersAjax(document.getElementById("aCustomer").value);
        }
        else if(document.getElementById("aType").value != 4)
        {
            document.getElementById("aTyperef").innerHTML = "";
        }
    }

        function aClustersAjax(acust_id) {

            jQuery.ajax({
                type: "POST",
                url: 'getclusters.php',
                dataType: 'json',
                data: {functionname: 'GetClustersByCustomerId', arguments: [acust_id]},

                success: function (obj, textstatus) {
                    if( !('error' in obj) ) {
                        document.getElementById("Typeref").innerHTML = "";document.getElementById("Typeref").innerHTML = "";

//                    for(var i=0;i<obj.length;i++)
//                    {
                        //var clustid[i] = obj[i].id;
                        //var clustname[i] = obj[i].name;
                        if(document.getElementById("aCustomer").value != "")
                        {
                            var j = 0,k = 0;

                            loopj: while(j < typeref_obj.length)
                            {

                                loopk: while(k < obj.length)
                                {
                                    if(  typeref_obj[j] == obj[k].id)
                                    {
                                        document.getElementById("aTyperef").innerHTML +=
                                            "<td><input type='checkbox' id='"+k+"acluster' value='"+obj[k].id+"' checked></td>"+obj[k].id+" - "+obj[k].name+"<br>";
                                        k++;
                                        break loopk;
                                    }
                                    else
                                    {
                                        document.getElementById("aTyperef").innerHTML +=
                                            "<td><input type='checkbox' id='"+k+"acluster' value='"+obj[k].id+"'></td>"+obj[k].id+" - "+obj[k].name+"<br>";
                                        k++;
                                    }

                                }
                                j++;
                            }

                            while (k < obj.length)
                            {
                                document.getElementById("aTyperef").innerHTML +=
                                    "<td><input type='checkbox' id='"+k+"acluster' value='"+obj[k].id+"'></td>"+obj[k].id+" - "+obj[k].name+"<br>";
                                k++;
                            }
                        }
                        // }
                        acheckbox_array_length = obj.length;
                    }
                    else {
                        console.log(obj.error);
                    }
        }

                //console.log(obj.result);
            });

        }



</script>


<input type="hidden" id="aAuthid" value="<?php echo $auth_id;?>">


<table>


    <tr><td>Username:</td><td><input type="text" id="aUsername" name="Username" value="<?php echo $username;?>"></td></tr>
    <tr><td>Passcode:</td><td><input type="text" id="aPasscode" name="Passcode" value="<?php echo $passcode;?>"></td></tr>


    <tr><td>Customer:</td><td><select id="aCustomer" name="aCustomer" disabled>
                <?php
                foreach ($customers as $customer)
                {
                    echo "<option value='$customer[0]'>$customer[0] -   $customer[1]</option>";
                }
                ?></select></td></tr>

    <tr><td>Type:</td><td>
            <select id="aType" name="aType" onchange="aType_change()">
                <option value="0">0-Admin</option>
                <option value="1">1-Head</option>
                <option value="2">2-Cluster Head</option>
                <option value="3">3-Tech</option>
                <option value="4">4-Zone Head</option>
            </select></td></tr>

<!--    <tr><td>Typeref:</td><td><input type="text" id="aTyperef" name="Typeref" value="--><?php //echo $typeref;?><!--"></td></tr>-->
    <tr><td>Typeref:</td><td><p id="aTyperef"></p></td></tr>


</table>

<a class='btn btn-default' href='#' onclick='event.preventDefault(); EditAuths()'>Save Changes</a><br>
<a class='btn btn-default' href='#' onclick='event.preventDefault(); DeleteAuths()'>Delete User</a>