<?php

	//32-bit binary bits from text 10101010 flag
	function PackBinaryFlag($report_flags)
	{
		var_dump($report_flags);
		$report_flags_binary = "0000";
		
		$stats = unpack ( "C*" , $report_flags_binary );
		$stats[1] = 0;
		$stats[2] = 0;
		$stats[3] = 0;
		$stats[4] = 0;
		
		//Byte 1
		if (substr($report_flags, 0, 1) == "1")
			$stats[1] = $stats[1] | 0x80;
			
		if (substr($report_flags, 1, 1) == "1")
			$stats[1] = $stats[1] | 0x40;
		
		if (substr($report_flags, 2, 1) == "1")
			$stats[1] = $stats[1] | 0x20;

		if (substr($report_flags, 3, 1) == "1")
			$stats[1] = $stats[1] | 0x10;			
			
		if (substr($report_flags, 4, 1) == "1")
			$stats[1] = $stats[1] | 0x08;
			
		if (substr($report_flags, 5, 1) == "1")
			$stats[1] = $stats[1] | 0x04;
		
		if (substr($report_flags, 6, 1) == "1")
			$stats[1] = $stats[1] | 0x02;

		if (substr($report_flags, 7, 1) == "1")
			$stats[1] = $stats[1] | 0x01;
			
		//Byte 2
		if (substr($report_flags, 8, 1) == "1")
			$stats[2] = $stats[2] | 0x80;
			
		if (substr($report_flags, 9, 1) == "1")
			$stats[2] = $stats[2] | 0x40;
		
		if (substr($report_flags, 10, 1) == "1")
			$stats[2] = $stats[2] | 0x20;

		if (substr($report_flags, 11, 1) == "1")
			$stats[2] = $stats[2] | 0x10;			
			
		if (substr($report_flags, 12, 1) == "1")
			$stats[2] = $stats[2] | 0x08;
			
		if (substr($report_flags, 13, 1) == "1")
			$stats[2] = $stats[2] | 0x04;
		
		if (substr($report_flags, 14, 1) == "1")
			$stats[2] = $stats[2] | 0x02;

		if (substr($report_flags, 15, 1) == "1")
			$stats[2] = $stats[2] | 0x01;
			
		//Byte 3
		if (substr($report_flags, 16, 1) == "1")
			$stats[3] = $stats[3] | 0x80;
			
		if (substr($report_flags, 17, 1) == "1")
			$stats[3] = $stats[3] | 0x40;
		
		if (substr($report_flags, 18, 1) == "1")
			$stats[3] = $stats[3] | 0x20;

		if (substr($report_flags, 19, 1) == "1")
			$stats[3] = $stats[3] | 0x10;			
			
		if (substr($report_flags, 20, 1) == "1")
			$stats[3] = $stats[3] | 0x08;
			
		if (substr($report_flags, 21, 1) == "1")
			$stats[3] = $stats[3] | 0x04;
		
		if (substr($report_flags, 22, 1) == "1")
			$stats[3] = $stats[3] | 0x02;

		if (substr($report_flags, 23, 1) == "1")
			$stats[3] = $stats[3] | 0x01;
			
		//Byte 4
		if (substr($report_flags, 24, 1) == "1")
			$stats[4] = $stats[4] | 0x80;
			
		if (substr($report_flags, 25, 1) == "1")
			$stats[4] = $stats[4] | 0x40;
		
		if (substr($report_flags, 26, 1) == "1")
			$stats[4] = $stats[4] | 0x20;

		if (substr($report_flags, 27, 1) == "1")
			$stats[4] = $stats[4] | 0x10;			
			
		if (substr($report_flags, 28, 1) == "1")
			$stats[4] = $stats[4] | 0x08;
			
		if (substr($report_flags, 29, 1) == "1")
			$stats[4] = $stats[4] | 0x04;
		
		if (substr($report_flags, 30, 1) == "1")
			$stats[4] = $stats[4] | 0x02;

		if (substr($report_flags, 31, 1) == "1")
			$stats[4] = $stats[4] | 0x01;
			
		$output = pack("CCCC", $stats[1], $stats[2],$stats[3],$stats[4]);
		
		return $output;
		
	}
	
	function GetReportFlag($report_flags, $flag_constant)
	{
		if (is_string($report_flags))
		{
			$tmp = unpack("N", $report_flags);
			$report_flags = $tmp[1];
			//echo(dechex($report_flags));
		}
		//var_dump($report_flags);
		$tmp = $report_flags & $flag_constant;
		return ($tmp != 0);
	}
	
	$REPORT_FLAG_1_SITE_REPORT_EMAIL 	= 0x80000000;
	$REPORT_FLAG_2_MODE_CHANGE_EMAIL 	= 0x40000000;
	$REPORT_FLAG_3_ALARM_SMS 			= 0x20000000;
	
	//$report_flags = ($REPORT_FLAG_1_SITE_REPORT_EMAIL | $REPORT_FLAG_2_MODE_CHANGE_EMAIL | $REPORT_FLAG_3_ALARM_SMS);
	$report_flags = ($REPORT_FLAG_1_SITE_REPORT_EMAIL | $REPORT_FLAG_3_ALARM_SMS);
	echo(dechex($report_flags));
	echo "<br><br>";
	
	if (GetReportFlag($report_flags, $REPORT_FLAG_2_MODE_CHANGE_EMAIL))
		echo "<br>2 TRUE<br>";
	else
		echo "<br>2 FALSE<br>";

	if (GetReportFlag($report_flags, $REPORT_FLAG_1_SITE_REPORT_EMAIL))
		echo "<br>1 TRUE<br>";
	else
		echo "<br>1 FALSE<br>";
		
	echo "<br><br>";
	
	echo(bin2hex(PackBinaryFlag("000")));
	echo(bin2hex(PackBinaryFlag("001")));
	echo(bin2hex(PackBinaryFlag("010")));
	echo(bin2hex(PackBinaryFlag("011")));
	echo(bin2hex(PackBinaryFlag("100")));
	echo(bin2hex(PackBinaryFlag("101")));
	echo(bin2hex(PackBinaryFlag("11100010010101010101010101010101")));
	
	$bf = PackBinaryFlag("11100010010101010101010101010101");
	if (GetReportFlag($bf, $REPORT_FLAG_2_MODE_CHANGE_EMAIL))
		echo "<br>2 TRUE<br>";
	else
		echo "<br>2 FALSE<br>";

?>
