<?php
session_start();
if (!isset($_SESSION['sysinfra_user']))
{
	header("Location: loginsis.php");
}
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Maintenance - SIS-AXS Administration Console</title>

    <!-- Bootstrap -->
    <!-- Latest compiled and minified CSS -->
	  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">

	  <!-- Optional theme -->
	  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css">

	  <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>

	  <!-- Latest compiled and minified JavaScript -->
	  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>


	
	
	<script>



		var gRefreshPage = false;
		var gTabState = 0;
		//CUSTOMERS 		0
		//AUTHENTICATIONS	1
		//CLUSTERS			2
        //SITES				3
		//REPORTS			4
		
		function SetRefresh()
		{
			gRefreshPage = true;
		}
		
		function SetTabState(state)
		{
			gTabState = state;
		}


		function LoadContentState()
		{
			url = "maint-cust.php?tab=0";
			
			switch (gTabState)
			{
				case 0:
					//REAL TIME DATA
					url = "maint-cust.php?tab=0";
					break;
					
				case 1:
					url = "maint-auth.php?tab=1";
					break;
					
				case 2:
					url = "maint-clusters.php?tab=2";
					break;	
				
				case 3:
					url = "maint-sites.php?tab=3";
					break;
				case 4:
					url = "reports.php?tab=4";
					break;
			}

			HideModal();
			AsyncLoad(url, "#context");
		}

		
		function AsyncLoad(url2load, outputLocation)
		{

			var request = $.ajax({url: url2load});


			$( outputLocation ).html( "<img src='/libs/jstree/themes/default/throbber.gif'>");
			 
			request.done(function( msg ) {
				if (gRefreshPage)
				{
					gRefreshPage = false;
					window.location.reload();
				}
				else
				{
					$(outputLocation ).html( msg );
				}
			});

			request.fail(function( jqXHR, textStatus ) {
				$( outputLocation ).html( "Request failed: " + textStatus );
			});

		}
		
		function switchtab(tabnumber)
		{
			if (tabnumber != gTabState)
			{
				$('#tab0').removeClass('active');
				$('#tab1').removeClass('active');
				$('#tab2').removeClass('active');
				$('#tab3').removeClass('active');
				$('#tab4').removeClass('active');

				var activetab = "#tab"+tabnumber;
				
				$(activetab).addClass('active');
				
				SetTabState(tabnumber);
				LoadContentState();
			}
		}
		//Modal Specific Methods
		function ShowModal(title, body)
		{
			$("#myModalLabel").html(title);
			$("#id_modal_detail").html(body);
			$('#myModal').modal('show');
		}
		function ShowModalUrl(title, url)
		{
			$("#myModalLabel").html(title);
			AsyncLoad(url, "#id_modal_detail");
			$('#myModal').modal('show');
		}
		
		function HideModal()
		{
			//$('#myModal').modal('hide');
			$("#myModal").modal('hide');
			/*$('#myModal').on('hidden.bs.modal', function (e) {
				//location.reload();
				$("#id_modal_detail").html('');
			});*/
		}
		
		function RrefreshTab(tabnumber) {

			SetTabState(tabnumber);
			LoadContentState();
		}

		$(function()
		{
			//Load Initial Tab
			LoadContentState();
		});
	</script>

	
</head>
<body>

<!--<script> var tab = <?php /*echo $_GET['tab']; */?>;
	RrefreshTab(tab);
</script>-->

	<div class="container-fluid">
		<?php include 'header.php'; ?>
	</div>
	
	<div class="container-fluid">
		<div id="context">
	</div>
		
	<div class="container-fluid">
		<?php include 'footersis.php'; ?>  
	</div>
	
	<!-- Modal -->
	<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	  <div class="modal-dialog">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel"></h4>
		  </div>
		  <div class="modal-body">
			<div id="id_modal_detail">
				Please wait...
			</div>
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		  </div>
		</div>
	  </div>
	</div>
</body>
</html>