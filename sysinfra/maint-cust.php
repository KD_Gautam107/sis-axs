<?php
session_start();
if (!isset($_SESSION['sysinfra_user']))
{
	header("Location: loginsis.php");
}

include 'common.php';

if (isset( $_GET['custid']))
{
	$custid = $_GET['custid'];
	$custname = $_GET['custname'];

	$_SESSION['current_customer_id']=$custid;
	$_SESSION['current_customer_name']=$custname;
}

if (isset( $_GET['add_name']))
{
	/*var_dump($_GET);
	//Add new record
	AddNewCustomerRecord($_GET['add_name'], $_GET['add_address'], "");
	$maint = file_get_contents('maintenance.php');
	echo $maint;
	return;*/
}

$tab = $_GET['tab'];
$customers = GetAllCustomers();

?>

<script>
	/*function SelectCustomer(custid, custname)
	{
		SetRefresh();
		url = "maint-cust.php?custid="+custid+"&custname="+custname;
		AsyncLoad(url, "#context");
	}*/
	function CustomerAjax(param) {
		if (window.XMLHttpRequest) {
			// code for IE7+, Firefox, Chrome, Opera, Safari
			xmlhttp = new XMLHttpRequest();
		} else {
			// code for IE6, IE5
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		xmlhttp.onreadystatechange = function () {
			if (this.readyState == 4 && this.status == 200) {

			}
		};
		xmlhttp.open("GET", "maint-cust-addedit.php?"+param, true);
		xmlhttp.send();
	}

	function AddCustomer(tab)
	{
		var param = "add_name="+encodeURIComponent($( "#Name" ).val().trim());
		param += "&add_address="+encodeURIComponent($( "#Address" ).val().trim());

		if($( "#Logo" ).val().split('/').pop().split('\\').pop().trim() == "")
		{
			if(document.getElementById("Name").value=="")
			{alert("Customer Name is Mandatory!");}
			else
			{
				param += "&add_logo=" + encodeURIComponent("company_logo.png");
				CustomerAjax(param);
				RrefreshTab(tab);
			}
		}
		else
		{
			if (document.getElementById("Name").value == "") {
				alert("Customer Name is Mandatory!");
			}
			else {
				param += "&add_logo=" + encodeURIComponent($("#Logo").val().split('/').pop().split('\\').pop().trim());
				var input = document.getElementById("Logo");
				file = input.files[0];
				//if(file != undefined){
				formData = new FormData();
				if (!!file.type.match(/image.*/)) {
					if (file.size < 1024 * 10) {
						formData.append("image", file);
						$.ajax({
							url: "upload.php",
							type: "POST",
							data: formData,
							processData: false,
							contentType: false,
							success: function (data) {
								//alert('success');
							}
						});
						CustomerAjax(param);
						RrefreshTab(tab);
					}
					else {
						alert("File size should be less then 10 KB!");
					}

				} else {
					alert('Not a valid image!');
				}
			}
		}
	}


	function EditCustomer(id,tab)
	{	 //url = "modaltable.php";
		url = "maint-cust-addedit.php?id="+id+"&tab="+tab;
		ShowModalUrl("Edit Customer "+id, url);
		//$.ajax({url: })
	}
</script>


<table class='table table-striped table-condensed table-responsive'>
	<tr><thead>
		<th>#</th>
		<th>Name</th>

		<th>Address</th>
		<th>Logo</th>


		</thead></tr>
	<tbody>

	<?php
	foreach ($customers as $customer)
	{
		echo "<tr>";
		echo "<td><a class='btn btn-default' href='#' onclick='event.preventDefault(); EditCustomer(".$customer[0].','.$tab.")'>".$customer[0]."</a></td>";
		echo "<td>".$customer[1]."</td>";
		echo "<td>".nl2br($customer[2])."</td>";
		echo "<td><img src='/media/tree/".$customer[4]."'></td>";
		//echo "<td><a class='btn btn-default' href='#' onclick='event.preventDefault(); SelectCustomer(".$customer[0].",\"".$customer[1]."\")'>Select</a></td>";
		echo "</tr>";
	}

	?>

	<tr>
		<td>+</td>
		<td><input type="text" class="form-control" id="Name" name="Name" placeholder="Enter Name"></td>

		<td><input type="text" class="form-control" id="Address" name="Address" placeholder="Enter Address"></td>
		<td><input type="file" class="form-control" id="Logo" name="Logo" /></td>
		</tr><tr>
		<td></td><td></td><td></td>

	<?php 	echo "<td><a class='btn btn-default' href='#' onclick='event.preventDefault(); AddCustomer($tab)'>Add Customer</a></td>";
?>

	</tr>

	</tbody>
</table>

<br>
	
