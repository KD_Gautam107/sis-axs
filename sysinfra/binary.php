<?php
	$id = $_GET['id'];
?>

<h5>Binary Data for Record #<?php echo $id;?></h5>

<?php

	include 'dbinc.php';
	include 'common.php';

	$mysqli = new mysqli("$mysql_hostname", "$mysql_user", "$mysql_password", "$mysql_database");
	if (mysqli_connect_errno())
	{
		printf("Connect failed: %s\n", mysqli_connect_error());
		return null;
	}
					
	$order = "SELECT raw_data FROM quanta where id=".$id;
	$result = $mysqli->query($order);
	$data = $result->fetch_row();
	
	$data_raw		= $data[0];		//Raw Data, for device specific information
	
	echo "<h5>Size: ".strlen($data_raw)."</h5>";
	
	hexdump($data_raw);
	
	
	$result->close();
	mysqli_close($mysqli);

?>
