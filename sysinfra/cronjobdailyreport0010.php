 <?php 
 
	// Cron Job to be Run every 10 Min
	//	40 18 * * * /usr/bin/php-cgi -f /var/www/html/sysinfra/cronjobdailyreport0010.php >/dev/null 2>&1
	
	include '../v1/common-alarms.php';
	
	function ReportAlarmSiteHistory()
	{
		include 'dbinc.php';
		
		$mysqli = new mysqli("$mysql_hostname", "$mysql_user", "$mysql_password", "$mysql_database");
		if (mysqli_connect_errno()) {
			printf("Connect failed: %s\n", mysqli_connect_error());
			exit();
		}

		$sql = "SELECT `id`, `site`, `quanta_id`, `alarm_flag`, `alarm_flag_version`, `start_time`, `last_message_level`, `last_message_time` 
					FROM `sitealarmhistory` WHERE 1";

		//echo "SQL: $sql<br>";	
		$result = $mysqli->query($sql);
		//var_dump($result);
		
		$site_in_alarm = 0;
		$site_alarms_text = "";
		
		//printf("\nSQL ERROR: %s\n", $mysqli->error);
		
		if ($result !== false)
		{
			$row_count = $result->num_rows;
			
			if ($row_count == 0)
			{
				
			}
			else
			{
				while($data = $result->fetch_row())
				{
					$site_in_alarm++;
					$alarms = GetAlarmBits($data[3]);
					
					//($alarms, $site_id, $site_name, $alarm_time)
					$text = GetAlarmText($alarms, $data[1], "", $data[5]);
					
					$site_alarms_text .= $text."\n";
					
					
					//delete alarm with id $data[0]
					
					$del_sql = "DELETE FROM `sitealarmhistory` WHERE id=".$data[0];
					$mysqli->query($del_sql);
				}
			}
		}

				
		$result->close();	
		$mysqli->close();
		
		$retval = "";
		
		if ($site_in_alarm == 0)
		{
			$retval = "No Alarm History\n\n";
		}
		else
		{
			$retval = $site_in_alarm." Alarm History:\n\n".$site_alarms_text;
		}
	
		return $retval;
	}
	
	//Duplicate Method
	function GetAllSiteList()
	{
		include 'dbinc.php'; 
		
		
		$array = array();
		
		

		$mysqli = new mysqli("$mysql_hostname", "$mysql_user", "$mysql_password", "$mysql_database");
		if (mysqli_connect_errno())
		{
			printf("Connect failed: %s\n", mysqli_connect_error());
			return null;
		}
		
		$sql = "SELECT Id, site_id, SiteName 
					FROM siteinfo ";

		//echo "$sql <br>";				
		$result = $mysqli->query($sql);

		if ($result !== false)
		{
			while($data = $result->fetch_row())
			{
				
				$arr2 = array();
				$arr2[] = $data[0]; 
				$arr2[] = $data[1]; 
				
				$array[] = $arr2;
			}
			
		}
		
		//var_dump($array);
		
		$result->close();
		
		mysqli_close($mysqli);
		//echo "NULL";
		
		return $array;
	
	}
	
	function ReportPowerSupply()
	{
		$hours_interval = 24;
		$retval = "";
		
		$site_list = GetAllSiteList();
		for ($i=0; $i<count($site_list); $i++)
		{
			echo "$i <br>";
			$site_id = $site_list[$i][0];
			$site_name = $site_list[$i][1];
			
			$sum = null;
			
			echo "X1";
			$array = GetSiteStatsFromIdStartInterval($site_id, null, $hours_interval);
			echo "X2";
			$sum = AddUp($array, $sum);
			echo "X3";
			
			$retval .= $site_name.': Mains Running: '.$sum[2].'  DG Running: '.$sum[1].'  Battery Running: '.$sum[3]."\n";
			
		}
		
		return $retval;
	}
	

	
	
	
	//Get Alarm Site History, and delete it
	$alarm_site_txt = ReportAlarmSiteHistory();
	$email_message .= "\n\n\nSite Report for sites in Alarm right now\n\n".$alarm_site_txt;
	
	$power_report = ReportPowerSupply();
	//$email_message .= "\n\n\nSite Power Report\n\n".$power_report;
	
	//($email_to, $email_from, $email_reply_to, $email_subject, $email_message)
	//$result = CommSendMailEx("toakhilesh@gmail.com,Ram.sharma@sysinfra.in,dushyant@sysinfra.in", "admin@sysinfra.in", "support@sysinfra.in", "SIS-AXS Daily Report", $email_message);
	$result = CommSendMailEx("root", "admin@sysinfra.in", "support@sysinfra.in", "SIS-AXS Daily Report", $email_message);
	
	echo "Mail Send Result: $result";

 
 ?>