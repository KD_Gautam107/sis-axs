<?php
session_start();
if (!isset($_SESSION['login_user']))
{
	header("Location: login.php");
}

include 'common.php';

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>RTD</title>

	<link rel="stylesheet" href="/jstree/themes/default/style.min.css">
    <!-- Bootstrap Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
	<!-- Bootstrap Optional theme -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css">

	<!-- jQuery  -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
	
	<!-- Latest compiled and minified JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
	
	<script src="libs/jstree/jstree.min.js"></script>
	
	<script src="libs/store.js/store.min.js"></script>
	<script src="libs/jquery-resizable-columns/jquery.resizableColumns.min.js"></script>
	
	
		<link class="include" rel="stylesheet" type="text/css" href="charts/jquery.jqplot.min.css" />
	<!--[if lt IE 9]><script language="javascript" type="text/javascript" src="charts/excanvas.js"></script><![endif]-->
	
	<script class="include" type="text/javascript" src="charts/jquery.jqplot.min.js"></script>
	<script class="include" type="text/javascript" src="charts/plugins/jqplot.pieRenderer.min.js"></script>
	<script type="text/javascript" src="charts/plugins/jqplot.highlighter.js"></script>
	
	<style>
		.jqplot-data-label{
			color: #ffffff !important ;
		 }
		 
		.jqplot-highlighter-tooltip{
			background: #ffffff;
		}

		.tree { overflow:auto; border:1px solid silver; width: 200px; min-height: 600px;}
	</style>

	

  </head>
  <body>
    
	
<div class="container-fluid">
  <?php include 'header1.php'; ?>
</div>
<div class="container-fluid">
  <div class="row">
	<div class="col-sm-2">
		<div style="width: 200px; border:1px solid silver; padding-left: 20px;">
			<img src="/media/icons/ic_refresh_black_24dp.png" id="refresh_button" title="Refresh Sites" style="cursor: pointer; margin-right:10px;">
			<img src="/media/icons/ic_unfold_more_black_24dp.png" id="expand_button" title="Expand All Sites" style="cursor: pointer; margin-right:10px;">
			<img src="/media/icons/ic_unfold_less_black_24dp.png" id="collapse_button" title="Collapse All Sites" style="cursor: pointer;">
		</div>
		<div id="tree" class="tree"></div>
	
	</div>
	<div class="col-sm-10">
		<div id="context">
		</div>
  </div>
</div>

<?php include 'footer.php'; ?>  
 
  

  
<script>

var gMenuState = 0;
	//REAL TIME DATA 	0
	//REPORTS 			1
	//ALARMS VIEW 		2
	//MAINTENANCE		3
	
var gTreeId = 0;

function SetMenuState(state)
{
	gMenuState = state;
}

function SetTreeId(id)
{
	gTreeId = id;;
}


function LoadContentState()
{
	var url = "default.php";
	
	var subUrl = "";
	
	if (gTreeId[0] == 'r')
	{
		subUrl = "-root";
	}
	else if (gTreeId[0] == 'c')
	{
		subUrl = "-cluster";
	}
	else if (gTreeId[0] == 'n')
	{
		subUrl = "-node";
	}

	
	
	switch (gMenuState)
	{
		case 0:
			url = "rtd"+subUrl+".php?id="+gTreeId;
			break;
			
		case 1:
			url = "reports.php";
			break;
			
		case 2:
			url = "alarms.php";
			break;	
		
		case 3:
			url = "maintenance.php";
			break;
			
				
	}

	AsyncLoad(url, "#context");
}

function AsyncLoad(url2load, outputLocation)
{

	var request = $.ajax({url: url2load});
	
	$( outputLocation ).html( "<img src='./jstree/themes/default/throbber.gif'>");
	 
	request.done(function( msg ) {
		$( outputLocation ).html( msg );
	});
	 
	request.fail(function( jqXHR, textStatus ) {
		$( outputLocation ).html( "Request failed: " + textStatus );
	});

}


	$('#tree')
		.on("changed.jstree", function (e, data) {
			if(data.selected.length) {
				var id = data.instance.get_node(data.selected[0]).id;
				SetTreeId(id);
				LoadContentState();
				
			}
		})
		.jstree({
		'core' : {
			'data' : {
				"url" : "./gettree.php",
				//"url" : "./tmp.txt",
				"dataType" : "json" // needed only if you do not supply JSON headers
			}
		}
	});


	// interaction and events
	$('#evts_button').on("click", function () {
		var instance = $('#tree').jstree(true);
		instance.deselect_all();
		instance.select_node('2');
	});
	
	$('#refresh_button').on("click", function () {
		var instance = $('#tree').jstree(true);
		instance.refresh();
		
	});
	
	$('#expand_button').on("click", function () {
		$('#tree').jstree('open_all');
	});
	
	$('#collapse_button').on("click", function () {
		$('#tree').jstree('close_all');
	});
	
	
	
	

	</script>   
  
</body>
</html>
