
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Frameset//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-frameset.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"
      xml:lang="en"
      lang="en"
      dir="ltr">
<head>
    <link rel="icon" href="./favicon.ico" type="image/x-icon" />
    <link rel="shortcut icon" href="./favicon.ico" type="image/x-icon" />
    <title>phpMyAdmin 3.4.11.1deb2+deb7u2 -
        192.168.56.101:12322</title>
    <meta http-equiv="Content-Type"
          content="text/html; charset=utf-8" />
    <meta name="robots" content="noindex,nofollow" />
    <script type="text/javascript">
        // <![CDATA[
        // definitions used in common.js
        var common_query = 'token=db7fee289a6741de0780b7b83aee922a';
        var opendb_url = 'db_structure.php';
        var safari_browser = true;
        var querywindow_height = 400;
        var querywindow_width = 600;
        var collation_connection = 'utf8_general_ci';
        var lang = 'en';
        var server = '1';
        var table = 'authentication';
        var db    = 'devices';
        var token = 'db7fee289a6741de0780b7b83aee922a';
        var text_dir = 'ltr';
        var pma_absolute_uri = 'https://192.168.56.101:12322/';
        var pma_text_default_tab = 'Browse';
        var pma_text_left_default_tab = 'Structure';

        // for content and navigation frames

        var frame_content = 0;
        var frame_navigation = 0;
        function getFrames() {
            frame_content = window.frames[1];
            frame_navigation = window.frames[0];
        }
        var onloadCnt = 0;
        var onLoadHandler = window.onload;
        window.onload = function() {
            if (onloadCnt == 0) {
                if (typeof(onLoadHandler) == "function") {
                    onLoadHandler();
                }
                if (typeof(getFrames) != 'undefined' && typeof(getFrames) == 'function') {
                    getFrames();
                }
                onloadCnt++;
            }
        };
        // ]]>
    </script>
    <script src="./js/jquery/jquery-1.4.4.js?ts=1344778698" type="text/javascript"></script>
    <script src="./js/update-location.js?ts=1344778698" type="text/javascript"></script>
    <script src="./js/common.js?ts=1344778698" type="text/javascript"></script>
</head>
<frameset cols="200,*" rows="*" id="mainFrameset">
    <frame frameborder="0" id="frame_navigation"
           src="navigation.php?token=db7fee289a6741de0780b7b83aee922a&amp;db=devices&amp;table=authentication"
           name="frame_navigation" />
    <frame frameborder="0" id="frame_content"
           src="sql.php?token=db7fee289a6741de0780b7b83aee922a&amp;db=devices&amp;table=authentication"
           name="frame_content" />
    <noframes>
        <body>
        <p>phpMyAdmin is more friendly with a <b>frames-capable</b> browser.</p>
        </body>
    </noframes>
</frameset>
</html>
